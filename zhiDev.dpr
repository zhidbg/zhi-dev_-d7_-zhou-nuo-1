program zhiDev;//calline;

uses
  Forms,
  windows,
  Unit5eCar in 'Unit5eCar.pas',
  About in 'dbgCore\About.pas' {FormAbout},
  Ascii in 'dbgCore\Ascii.pas' {FormAscii},
  barcodeMasks in 'dbgCore\barcodeMasks.pas',
  CreateDiode in 'dbgCore\CreateDiode.pas' {FormCreateClampDiode},
  DeleteNstep in 'dbgCore\DeleteNstep.pas' {FormDeleteN},
  FileCompare in 'dbgCore\FileCompare.pas' {FormCompare},
  FindPoint in 'dbgCore\FindPoint.pas' {FormFind},
  InsertCopyNstep in 'dbgCore\InsertCopyNstep.pas' {FormInsertCopyN},
  InsertNstep in 'dbgCore\InsertNstep.pas' {FormInsertN},
  Keyboard in 'dbgCore\Keyboard.pas' {Formkeyboard},
  LearnClampDiode in 'dbgCore\LearnClampDiode.pas' {FormClampDiode},
  LoadFile in 'dbgCore\LoadFile.pas' {FormLoad},
  LogHeadTips in 'dbgCore\LogHeadTips.pas' {FormLogHead},
  main in 'dbgCore\main.pas' {Form_main},
  ModfyN in 'dbgCore\ModfyN.pas' {FormModfyNstep},
  MoveNstep in 'dbgCore\MoveNstep.pas' {FormMoveN},
  OSstudy in 'dbgCore\OSstudy.pas' {FormOS},
  PNenter in 'dbgCore\PNenter.pas' {FormPN},
  Search in 'dbgCore\Search.pas',
  SPComm in 'dbgCore\SPComm.pas',
  StepCopy in 'dbgCore\StepCopy.pas' {FormStepCopy},
  SWave in 'dbgCore\SWave.pas' {FormWave},
  SWCheck in 'dbgCore\SWCheck.pas' {FormSWcheck},
  Unit0_globalVariant in 'dbgCore\Unit0_globalVariant.pas',
  Unit1_MainCreateShowlCode in 'dbgCore\Unit1_MainCreateShowlCode.pas',
  Unit2_Communication in 'dbgCore\Unit2_Communication.pas',
  Unit3appFSM in 'dbgCore\Unit3appFSM.pas',
  Unit4_logFileCode in 'dbgCore\Unit4_logFileCode.pas',
  ULJKTcp in 'Socket\ULJKTcp.pas' {DmLJKTcp: TDataModule},
  superobject in 'Socket\SuperObject.pas';

{$R *.RES}
var Mutex:THandle;
begin
 Mutex := CreateMutex(nil,true,'one');  {第3个参数任意设置}
 if (GetLastError <> ERROR_ALREADY_EXISTS)
    or ( (Readchis('Comm Para','LANen')<>'0') and (Readchis('Comm Para','LANen')<>'') )     //有网口通信，可多个启动
    or (Readchis('Comm Para','FormLeft')<>'')                                  //双软件系统启动
 then begin
    Application.Initialize;
    Application.Title := 'zhiDev230812';
  Application.CreateForm(TDmLJKTcp, DmLJKTcp);
    Application.CreateForm(TForm_main, Form_main);
    Application.CreateForm(TFormAbout, FormAbout);
    Application.CreateForm(TFormAscii, FormAscii);
    Application.CreateForm(TFormCreateClampDiode, FormCreateClampDiode);
    Application.CreateForm(TFormDeleteN, FormDeleteN);
    Application.CreateForm(TFormCompare, FormCompare);
    Application.CreateForm(TFormFind, FormFind);
    Application.CreateForm(TFormInsertCopyN, FormInsertCopyN);
    Application.CreateForm(TFormInsertN, FormInsertN);
    Application.CreateForm(TFormkeyboard, Formkeyboard);
    Application.CreateForm(TFormClampDiode, FormClampDiode);
    Application.CreateForm(TFormLoad, FormLoad);
    Application.CreateForm(TFormLogHead, FormLogHead);
    Application.CreateForm(TFormModfyNstep, FormModfyNstep);
    Application.CreateForm(TFormMoveN, FormMoveN);
    Application.CreateForm(TFormOS, FormOS);
    Application.CreateForm(TFormPN, FormPN);
    Application.CreateForm(TFormStepCopy, FormStepCopy);
    Application.CreateForm(TFormWave, FormWave);
    Application.CreateForm(TFormSWcheck, FormSWcheck);

    Application.Run;
 end else
    Application.MessageBox('The Program is Runing...该程序正在运行！','Tips提示',MB_OK);

  ReleaseMutex(Mutex);   {释放资源}
end.
