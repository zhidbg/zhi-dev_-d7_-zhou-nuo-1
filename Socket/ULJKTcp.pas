unit ULJKTcp;

interface

uses
  SysUtils, Classes, Forms,IdAntiFreezeBase, IdAntiFreeze, IdBaseComponent,Dialogs,
  IdComponent, IdTCPConnection, IdTCPClient,IniFiles,IdGlobal,SuperObject,SyncObjs;

type
  PBarcodeItem=^BarcodeItem; //请求主数据 对应文档1.2节，如果纳税人识别号在应用中不规定，可以动态赋值。
  BarcodeItem= record
    id:Integer;
    barcode:string;
    status:string;
    note:string;
  end;

type
  TDmLJKTcp = class(TDataModule)
    IdTCPClient1: TIdTCPClient;
    IdAntiFreeze1: TIdAntiFreeze;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    sendboole:Boolean; //已经发送了消息
    sResultBuZhou:string;//json里面的步骤
    IResultMessageClass:Integer;//json里面的messageClass
    FCsWriteLogFile:TCriticalSection;
    procedure MySplit(src,dec:string;var ts:TStringList);
    function  MyReadini(zx,mx:string):string;
    function  MySendmessage(s:string):Boolean;
    function  MySendCode(sCode,sMessageClass,sbuzhou:string):string;
    function  MyUnpack(sResult:string):Boolean;
    procedure Mysleep(MSecs: Longint);
    procedure MyAddlog(s:string);
    { Public declarations }
  end;

  TClientThread = class(TThread)
     private
       mystr:string;
       protected procedure Execute; override;
       procedure RevData;
  end;

var
  DmLJKTcp: TDmLJKTcp;
  ClientThread: TClientThread;
  exepath:string;
implementation

uses
  Unit0_globalVariant;

{$R *.dfm}

procedure TDmLJKTcp.MyAddlog(s:string);
var
  tFile: TextFile;
  filename: string;
  b:Boolean;
begin
  filename:=exepath+'log';
  b:=False;
  if not(DirectoryExists(filename)) then
  begin
    try
       ForceDirectories(filename);
       b:=True;
    except
    end;
  end
  else
  begin
       b:=True;
  end;
  if b=False then exit;
  filename:=filename+'\'+formatdatetime('yyyy-MM-dd',Now)+'.txt';
    FCsWriteLogFile.Enter();
    AssignFile(tFile,filename);
      try
       if FileExists(filename) then
         Append(tFile)
       else
         Rewrite(tFile);
         Writeln(tFile,'<'+FormatDateTime('hh:nn:ss', Now)+'>'+s);
      except
      end;
    CloseFile(tFile);
    FCsWriteLogFile.Leave;
end;

procedure TClientThread.RevData;
var sResult:string;
begin
  sResult:=mystr;
  DmLJKTcp.MyUnpack(sResult);
  //DmLJKTcp.sendboole:=False;
  DmLJKTcp.MyAddlog('收到：'+sResult);
end;

procedure TClientThread.Execute;
var buffer:array of Char;
    ilength:Integer;
    str:string;
    SResult:string;
begin
  while not Terminated do
  begin
    try
     Sleep(1000);
     if DmLJKTcp.sendboole=True then
     if DmLJKTcp.IdTCPClient1.Connected then
     begin
      if DmLJKTcp.IdTCPClient1.InputBuffer.Size = 0 then
      DmLJKTcp.IdTCPClient1.ReadFromStack(False,-1,False);
      if  DmLJKTcp.IdTCPClient1.InputBuffer.Size>0 then
      begin
         ilength:=DmLJKTcp.IdTCPClient1.InputBuffer.Size;
         SetLength(buffer,ilength);
         DmLJKTcp.IdTCPClient1.ReadBuffer(buffer[0],ilength);
         SetLength(str,ilength);
         Move(buffer[0],str[1],ilength);
         SResult:=str;
         if SResult<>'' then
         begin
            mystr:=SResult;
            synchronize(RevData);
           //DmLJKTcp.MyUnpack(sResult);
           //DmLJKTcp.sendboole:=False;
           //DmLJKTcp.MyAddlog('收到：'+str);
         end;
      end;
     end;
    except
    end;
  end;
end;

procedure TDmLJKTcp.Mysleep(MSecs: Longint);
var
  TickCount: Longint;
begin
 TickCount := GetTickCount();
 repeat
  Application.ProcessMessages;
 until (GetTickCount() - TickCount >= MSecs) or (GetTickCount()<TickCount);
end;

function TDmLJKTcp.MyUnpack(sResult:string):Boolean;
var  myjo,myjo1,myjo2:ISuperObject;
     ja,jb:TSuperArray;
     i,k:Integer;
     pcode:BarcodeItem;

     Sflag:string;
begin
   Result:=False;
   Sflag:='ok';
   myjo:=SO(SResult);
   if myjo<>nil then
   begin
      case IResultMessageClass of
       100:
        begin
             if myjo['message']<>nil then
             myjo1:=myjo.O['message'];
             if myjo1<>nil then
             if myjo1['messageItem']<>nil then
             begin
               ja:=myjo1.A['messageItem'];
               if ja.Length>0 then
               begin
                  for k:=0 to ja.Length-1 do
                  begin
                     myjo2:= ja.O[k];
                     if myjo2<>nil then
                     begin
                        if myjo2['messageBarcodeItem']<>nil then
                        begin
                           jb:=myjo2.A['messageBarcodeItem'];
                           for i:=0 to jb.Length-1 do
                           begin
                             pcode.id:=myjo2.I['id'];
                             pcode.barcode:=jb.O[i].s['barcode'];
                             pcode.status:=jb.O[i].s['status'];
                             pcode.note:=jb.O[i].s['note'];
                             if pcode.status='F' then
                             begin
                              Sflag:='ng';
                              Break;
                             end;
                           end;
                           Result:=True;
                        end;
                     end;
                  end;
               end;
             end;
        end;
       110:
        begin
             if myjo['message']<>nil then
             myjo1:=myjo.O['message'];
             if myjo1<>nil then
             if myjo1['messageItem']<>nil then
             begin
               ja:=myjo1.A['messageItem'];
               if ja.Length>0 then
               begin
                  for k:=0 to ja.Length-1 do
                  begin
                     myjo2:= ja.O[k];
                     if myjo2<>nil then
                     begin
                        if myjo2['messageBarcodeItem']<>nil then
                        begin
                           jb:=myjo2.A['messageBarcodeItem'];
                           for i:=0 to jb.Length-1 do
                           begin
                             pcode.id:=myjo2.I['id'];
                             pcode.barcode:=jb.O[i].s['barcode'];
                             pcode.status:=jb.O[i].s['status'];
                             pcode.note:=jb.O[i].s['note'];
                             if pcode.status='F' then
                             begin
                              Sflag:='ng';
                              Break;
                             end;
                           end;
                           Result:=True;
                        end;
                     end;
                  end;
               end;
             end;
        end;
       200:
        begin
             if myjo['message']<>nil then
             myjo1:=myjo.O['message'];
             if myjo1<>nil then
             if myjo1['messageItem']<>nil then
             begin
               ja:=myjo1.A['messageItem'];
               if ja.Length>0 then
               begin
                 myjo2:= myjo1.A['messageItem'].O[0];
                 if myjo2<>nil then
                 begin
                    if myjo2['messageBarcodeItem']<>nil then
                    begin
                       ja:=myjo2.A['messageBarcodeItem'];
                       for i:=0 to ja.Length-1 do
                       begin
                         pcode.id:=ja.O[i].I['id'];
                         pcode.barcode:=ja.O[i].s['barcode'];
                         pcode.status:=ja.O[i].s['status'];
                         pcode.note:=ja.O[i].s['note'];
                         if pcode.status='F' then
                         begin
                          Sflag:='ng';
                          Break;
                         end;
                       end;
                       Result:=True;
                    end;
                 end;
               end;
             end;
        end;
       300:
        begin
             if myjo['message']<>nil then
             myjo1:=myjo.O['message'];
             if myjo1<>nil then
             if myjo1['messageItem']<>nil then
             begin
               ja:=myjo1.A['messageItem'];
               if ja.Length>0 then
               begin
                 myjo2:= myjo1.A['messageItem'].O[0];
                 if myjo2<>nil then
                 begin
                    if myjo2['messageBarcodeItem']<>nil then
                    begin
                       ja:=myjo2.A['messageBarcodeItem'];
                       for i:=0 to ja.Length-1 do
                       begin
                         pcode.id:=ja.O[i].I['id'];
                         pcode.barcode:=ja.O[i].s['barcode'];
                         pcode.status:=ja.O[i].s['status'];
                         pcode.note:=ja.O[i].s['note'];
                         if pcode.status='F' then
                         begin
                          Sflag:='ng';
                          Break;
                         end;
                       end;
                       Result:=True;
                    end;
                 end;
               end;
             end;
        end;
      end;
   end;
   if Result=True then
   begin
     if Sflag='ok' then
     sbuf[5] := $FF
     else
     sbuf[5] := $00;
     sbuf[1] := 1;
     sbuf[2] := 5;
     sbuf[3] := $50;
     sbuf[4] := $00;
     sbuf[6] := $00;
     send_len := 6;
     try
      send_crcdata(send_len, 2);
     except
      MyAddlog('send_crcdata 失败');
     end;
   end;
end;

procedure TDmLJKTcp.MySplit(src,dec:string;var ts:TStringList);
var
  i : integer;
  str : string;
begin
  if src='' then
  begin
    exit;
  end;
  repeat
    i := pos(dec,src);
    str := copy(src,1,i-1);
    if (str='') and (i>0) then
    begin
      delete(src,1,length(dec));
      continue;
    end;
    if i>0 then
    begin
      if Trim(str)<>'' then
      ts.Add(str);
      delete(src,1,i+length(dec)-1);
    end;
  until i<=0;
  if Trim(src)<>'' then
  ts.Add(src);
end;

function TDmLJKTcp.MySendCode(sCode,sMessageClass,sbuzhou:string):string;
var sjson:string;
    slTempCode:TStringList;
    myjo,myjoA:ISuperObject;
    i:Integer;
begin
   Result:='';
   IResultMessageClass:=StrToInt(sMessageClass);
   sResultBuZhou:=sbuzhou;
   sendboole:=False;
   try
     if IdTCPClient1.Connected then
     IdTCPClient1.Disconnect;
   except
   end;
   try
    if not IdTCPClient1.Connected then
    begin
      IdTCPClient1.Connect;
      Mysleep(500);
    end;
   except on e:Exception do
       MyAddlog('MySendCode连接失败：'+e.Message);
   end;
   if not IdTCPClient1.Connected then
   begin
     MyAddlog('MySendCode连接失败信息未发送。');
     Exit;
   end;
   slTempCode:=TStringList.Create;
   MySplit(sCode,',',slTempCode);
   myjo:=SO();
   myjoA:=SO();
   myjo.O['message']:=SO();
   with myjo.O['message'] do
   begin
     S['messageClass']:=sMessageClass;//'300';
     I['messageCount']:=slTempCode.Count;
     S['messageStep']:=sbuzhou;
   end;
   myjo.O['message']['messageItem']:=SA([]);
   for i:=0 to slTempCode.count-1 do
   begin
    myjoA:=SO();
    myjoA.I['id']:=i+1;
    myjoA.S['barcode']:=slTempCode[I];
    myjo.O['message'].A['messageItem'].Add(myjoA);
   end;
   sjson := myjo.AsJSon(true, False);
   slTempCode.Free;

   sendboole:=True;
   if MySendmessage(sjson) then
   begin
      Result:='完成'
   end
   else
   begin
      sendboole:=False;
      Result:='失败';
   end;

end;

function TDmLJKTcp.MySendmessage(s:string):Boolean;
var buffer:array of Char;
begin
 try
   Result:=false;
  if self.IdTCPClient1.Connected=False then
  begin
   self.IdTCPClient1.Connect();
   Mysleep(500);
  end;
  if self.IdTCPClient1.Connected then
  begin
   SetLength(buffer,Length(s));
   self.IdTCPClient1.WriteBuffer(s[1],Length(s),True);
   MyAddlog('发送完成):'+s);
   Result:=true;
  end
  else
  begin
    MyAddlog('发送失败(未连接)');
  end;
 except on e:Exception do
    MyAddlog('发送失败:'+e.Message);
 end;
end;


function TDmLJKTcp.MyReadini(zx,mx:string):string;
var myfilename:Tinifile;
begin
    myfilename:=Tinifile.Create(exepath+'config.ini');
    Result:=myfilename.ReadString(zx,mx,'');
    myfilename.Free;
end;

procedure TDmLJKTcp.DataModuleCreate(Sender: TObject);
var s:string;
begin
  sendboole:=False;
  FCsWriteLogFile:=TCriticalSection.Create;
  exepath:=ExtractFilePath(ParamStr(0));
  if IdTCPClient1.Connected then
  IdTCPClient1.Disconnect;
  IdTCPClient1.Host := MyReadini('Socket','ip');
  s:=MyReadini('Socket','port');
  if Trim(s)='' then
  s:='60000';
  IdTCPClient1.Port := StrToInt(s);

  ClientThread := TClientThread.Create(True);
  ClientThread.FreeOnTerminate:=True;
  ClientThread.Resume;
end;

procedure TDmLJKTcp.DataModuleDestroy(Sender: TObject);
var i:Integer;
    pcode:PBarcodeItem;
begin
  try
    ClientThread.Suspend;
    ClientThread.Terminate;
    FCsWriteLogFile.Free;
  except
  end;
end;

end.
