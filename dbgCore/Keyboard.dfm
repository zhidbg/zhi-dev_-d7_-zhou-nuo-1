object Formkeyboard: TFormkeyboard
  Left = 189
  Top = 230
  Width = 776
  Height = 279
  Caption = 'Keyboard'#38190#30424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ikybrd1: TiKeyBoard
    Left = 8
    Top = 8
    Width = 750
    Height = 225
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    OuterMargin = 5
    SecondRowMargin = 10
    Style = ikbs104
    FocusHandle = 0
    Buttons = <
      item
        Style = ikbbsStandard
        CaptionStandard = 'Esc'
        AcceptLock = False
        VirtualKeyCode = 27
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F1'
        AcceptLock = False
        VirtualKeyCode = 112
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F2'
        AcceptLock = False
        VirtualKeyCode = 113
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F3'
        AcceptLock = False
        VirtualKeyCode = 114
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F4'
        AcceptLock = False
        VirtualKeyCode = 115
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 0
        Size = 1
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F5'
        AcceptLock = False
        VirtualKeyCode = 116
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F6'
        AcceptLock = False
        VirtualKeyCode = 117
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F7'
        AcceptLock = False
        VirtualKeyCode = 118
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F8'
        AcceptLock = False
        VirtualKeyCode = 119
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 0
        Size = 1
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F9'
        AcceptLock = False
        VirtualKeyCode = 120
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F10'
        AcceptLock = False
        VirtualKeyCode = 121
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F11'
        AcceptLock = False
        VirtualKeyCode = 122
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'F12'
        AcceptLock = False
        VirtualKeyCode = 123
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 0
        Size = 1
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'psc'
        AcceptLock = False
        VirtualKeyCode = 44
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsScroll
        CaptionStandard = 'slk'
        AcceptLock = False
        VirtualKeyCode = 145
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'brk'
        AcceptLock = False
        VirtualKeyCode = 19
        Row = 0
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '`'
        CaptionShift = '~'
        AcceptLock = False
        VirtualKeyCode = 192
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '1'
        CaptionShift = '!'
        AcceptLock = False
        VirtualKeyCode = 49
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '2'
        CaptionShift = '@'
        AcceptLock = False
        VirtualKeyCode = 50
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '3'
        CaptionShift = '#'
        AcceptLock = False
        VirtualKeyCode = 51
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '4'
        CaptionShift = '$'
        AcceptLock = False
        VirtualKeyCode = 52
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '5'
        CaptionShift = '%'
        AcceptLock = False
        VirtualKeyCode = 53
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '6'
        CaptionShift = '^'
        AcceptLock = False
        VirtualKeyCode = 54
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '7'
        CaptionShift = '&'
        AcceptLock = False
        VirtualKeyCode = 55
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '8'
        CaptionShift = '*'
        AcceptLock = False
        VirtualKeyCode = 56
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '9'
        CaptionShift = '('
        AcceptLock = False
        VirtualKeyCode = 57
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '0'
        CaptionShift = ')'
        AcceptLock = False
        VirtualKeyCode = 48
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '-'
        CaptionShift = '_'
        AcceptLock = False
        VirtualKeyCode = 189
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '='
        CaptionShift = '+'
        AcceptLock = False
        VirtualKeyCode = 187
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'bksp'
        AcceptLock = False
        VirtualKeyCode = 8
        Row = 1
        Size = 4
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 1
        Size = 1
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'ins'
        AcceptLock = False
        VirtualKeyCode = 45
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'hm'
        AcceptLock = False
        VirtualKeyCode = 36
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'pup'
        AcceptLock = False
        VirtualKeyCode = 33
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 1
        Size = 1
        DoubleHeight = False
      end
      item
        Style = ikbbsNumLock
        CaptionStandard = 'nlk'
        AcceptLock = False
        VirtualKeyCode = 144
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '/'
        AcceptLock = False
        VirtualKeyCode = 111
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '*'
        AcceptLock = False
        VirtualKeyCode = 106
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '-'
        AcceptLock = False
        VirtualKeyCode = 109
        Row = 1
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'Tab'
        AcceptLock = False
        VirtualKeyCode = 9
        Row = 2
        Size = 3
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'q'
        CaptionShift = 'Q'
        AcceptLock = True
        VirtualKeyCode = 81
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'w'
        CaptionShift = 'W'
        AcceptLock = True
        VirtualKeyCode = 87
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'e'
        CaptionShift = 'E'
        AcceptLock = True
        VirtualKeyCode = 69
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'r'
        CaptionShift = 'R'
        AcceptLock = True
        VirtualKeyCode = 82
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 't'
        CaptionShift = 'T'
        AcceptLock = True
        VirtualKeyCode = 84
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'y'
        CaptionShift = 'Y'
        AcceptLock = True
        VirtualKeyCode = 89
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'u'
        CaptionShift = 'U'
        AcceptLock = True
        VirtualKeyCode = 85
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'i'
        CaptionShift = 'I'
        AcceptLock = True
        VirtualKeyCode = 73
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'o'
        CaptionShift = 'O'
        AcceptLock = True
        VirtualKeyCode = 79
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'p'
        CaptionShift = 'P'
        AcceptLock = True
        VirtualKeyCode = 80
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '['
        CaptionShift = '{'
        AcceptLock = False
        VirtualKeyCode = 219
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = ']'
        CaptionShift = '}'
        AcceptLock = False
        VirtualKeyCode = 221
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '\'
        CaptionShift = '|'
        AcceptLock = False
        VirtualKeyCode = 220
        Row = 2
        Size = 3
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 2
        Size = 1
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'del'
        AcceptLock = False
        VirtualKeyCode = 46
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'end'
        AcceptLock = False
        VirtualKeyCode = 35
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'pdn'
        AcceptLock = False
        VirtualKeyCode = 34
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 2
        Size = 1
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '7'
        AcceptLock = False
        VirtualKeyCode = 103
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '8'
        AcceptLock = False
        VirtualKeyCode = 104
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '9'
        AcceptLock = False
        VirtualKeyCode = 105
        Row = 2
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '+'
        AcceptLock = False
        VirtualKeyCode = 107
        Row = 2
        Size = 2
        DoubleHeight = True
      end
      item
        Style = ikbbsLock
        CaptionStandard = 'Lock'
        AcceptLock = False
        VirtualKeyCode = 20
        Row = 3
        Size = 4
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'a'
        CaptionShift = 'A'
        AcceptLock = True
        VirtualKeyCode = 65
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 's'
        CaptionShift = 'S'
        AcceptLock = True
        VirtualKeyCode = 83
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'd'
        CaptionShift = 'D'
        AcceptLock = True
        VirtualKeyCode = 68
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'f'
        CaptionShift = 'F'
        AcceptLock = True
        VirtualKeyCode = 70
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'g'
        CaptionShift = 'G'
        AcceptLock = True
        VirtualKeyCode = 71
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'h'
        CaptionShift = 'H'
        AcceptLock = True
        VirtualKeyCode = 72
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'j'
        CaptionShift = 'J'
        AcceptLock = True
        VirtualKeyCode = 74
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'k'
        CaptionShift = 'K'
        AcceptLock = True
        VirtualKeyCode = 75
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'l'
        CaptionShift = 'L'
        AcceptLock = True
        VirtualKeyCode = 76
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = ';'
        CaptionShift = ':'
        AcceptLock = False
        VirtualKeyCode = 186
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = #39
        CaptionShift = '"'
        AcceptLock = False
        VirtualKeyCode = 222
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'Enter'
        AcceptLock = False
        VirtualKeyCode = 13
        Row = 3
        Size = 4
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 3
        Size = 8
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '4'
        AcceptLock = False
        VirtualKeyCode = 100
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '5'
        AcceptLock = False
        VirtualKeyCode = 101
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '6'
        AcceptLock = False
        VirtualKeyCode = 102
        Row = 3
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsShift
        CaptionStandard = 'Shift'
        AcceptLock = False
        VirtualKeyCode = 16
        Row = 4
        Size = 5
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'z'
        CaptionShift = 'Z'
        AcceptLock = True
        VirtualKeyCode = 90
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'x'
        CaptionShift = 'X'
        AcceptLock = True
        VirtualKeyCode = 88
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'c'
        CaptionShift = 'C'
        AcceptLock = True
        VirtualKeyCode = 67
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'v'
        CaptionShift = 'V'
        AcceptLock = True
        VirtualKeyCode = 86
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'b'
        CaptionShift = 'B'
        AcceptLock = True
        VirtualKeyCode = 66
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'n'
        CaptionShift = 'N'
        AcceptLock = True
        VirtualKeyCode = 78
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'm'
        CaptionShift = 'M'
        AcceptLock = True
        VirtualKeyCode = 77
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = ','
        CaptionShift = '<'
        AcceptLock = False
        VirtualKeyCode = 188
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '.'
        CaptionShift = '>'
        AcceptLock = False
        VirtualKeyCode = 190
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '/'
        CaptionShift = '?'
        AcceptLock = False
        VirtualKeyCode = 191
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsShift
        CaptionStandard = 'Shift'
        AcceptLock = False
        VirtualKeyCode = 16
        Row = 4
        Size = 5
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 4
        Size = 3
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'U'
        AcceptLock = False
        VirtualKeyCode = 38
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 4
        Size = 3
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '1'
        AcceptLock = False
        VirtualKeyCode = 97
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '2'
        AcceptLock = False
        VirtualKeyCode = 98
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '3'
        AcceptLock = False
        VirtualKeyCode = 99
        Row = 4
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'Enter'
        AcceptLock = False
        VirtualKeyCode = 13
        Row = 4
        Size = 2
        DoubleHeight = True
      end
      item
        Style = ikbbsCtrl
        CaptionStandard = 'Ctrl'
        AcceptLock = False
        VirtualKeyCode = 17
        Row = 5
        Size = 3
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'W'
        AcceptLock = False
        VirtualKeyCode = 91
        Row = 5
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsAlt
        CaptionStandard = 'Alt'
        AcceptLock = False
        VirtualKeyCode = 18
        Row = 5
        Size = 3
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        AcceptLock = False
        VirtualKeyCode = 32
        Row = 5
        Size = 12
        DoubleHeight = False
      end
      item
        Style = ikbbsAlt
        CaptionStandard = 'Alt'
        AcceptLock = False
        VirtualKeyCode = 18
        Row = 5
        Size = 3
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'W'
        AcceptLock = False
        VirtualKeyCode = 92
        Row = 5
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'L'
        AcceptLock = False
        VirtualKeyCode = 93
        Row = 5
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsCtrl
        CaptionStandard = 'Ctrl'
        AcceptLock = False
        VirtualKeyCode = 17
        Row = 5
        Size = 3
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 5
        Size = 1
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'L'
        AcceptLock = False
        VirtualKeyCode = 37
        Row = 5
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'D'
        AcceptLock = False
        VirtualKeyCode = 40
        Row = 5
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = 'R'
        AcceptLock = False
        VirtualKeyCode = 39
        Row = 5
        Size = 2
        DoubleHeight = False
      end
      item
        Style = ikbbsSpacer
        AcceptLock = False
        VirtualKeyCode = 0
        Row = 5
        Size = 1
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '0'
        AcceptLock = False
        VirtualKeyCode = 96
        Row = 5
        Size = 4
        DoubleHeight = False
      end
      item
        Style = ikbbsStandard
        CaptionStandard = '.'
        AcceptLock = False
        VirtualKeyCode = 110
        Row = 5
        Size = 2
        DoubleHeight = False
      end>
  end
end
