//////////////////////////////////////////////////////////////////////////////////////////
//Search单元 SearchMemo
///////////////////////////////////////////////////////////////////////////////////////////
  
unit Search;
  
interface

uses
  SysUtils, StdCtrls, Dialogs, StrUtils
  ,Windows, Messages, Variants, Classes, Graphics, Controls, Forms,
  Menus, ExtCtrls;

  function SearchMemo(Memo: TCustomEdit; const SearchString: string; Options: TFindOptions): Boolean;
  function mmo3WndProc(h,m,w,l: Integer): Integer stdcall;
  function mmo9WndProc(h,m,w,l: Integer): Integer stdcall;
  function mmo13WndProc(h,m,w,l: Integer): Integer stdcall;
  function mmo14WndProc(h,m,w,l: Integer): Integer stdcall;
  function panel1WndProc(h,m,w,l: Integer): Integer stdcall;
var
  hk1: HHOOK;
  hm1: HMENU;
  r1: TRect;
  bSelected1: Boolean;
  hk3: HHOOK;
  hm3: HMENU;
  r3: TRect;
  bSelected3: Boolean;
  hk9: HHOOK;
  hm9: HMENU;
  r9: TRect;
  bSelected9: Boolean;

  hk13: HHOOK;
  hm13: HMENU;
  r13: TRect;
  bSelected13: Boolean;
  hk14: HHOOK;
  hm14: HMENU;
  r14: TRect;
  bSelected14: Boolean;

implementation

uses main;

function SearchMemo(Memo: TCustomEdit; const SearchString: string; Options: TFindOptions): Boolean; 
var
  Buffer, P: PChar;
  Size: Word;
begin
  Result := False;
  if Length(SearchString) = 0 then
    Exit;
  
  Size := Memo.GetTextLen;
  if (Size = 0) then
    Exit;
  
  Buffer := SysUtils.StrAlloc(Size + 1);
  try
    Memo.GetTextBuf(Buffer, Size + 1);
  
    if frDown in Options then
      P := SearchBuf(Buffer, Size, Memo.SelStart, Memo.SelLength,SearchString, [soDown])
        
    else
      P := SearchBuf(Buffer, Size, Memo.SelStart, Memo.SelLength,SearchString, []);
         
    if (frMatchCase in Options) then
      P := SearchBuf(Buffer, Size, Memo.SelStart, Memo.SelLength, SearchString,[soMatchCase]);
        
    if (frWholeWord in Options) then
      P := SearchBuf(Buffer, Size, Memo.SelStart, Memo.SelLength, SearchString,[soWholeWord]);
  
    if P <> nil then
    begin
      Memo.SelStart := P - Buffer;
      Memo.SelLength := Length(SearchString);
      Result := True;
    end;
  
  finally
    SysUtils.StrDispose(Buffer);
  end;
end;

function mmo3WndProc(h,m,w,l: Integer): Integer stdcall;
const
  MN_GETHMENU=$1E1;
  
  function GetMsgProc(c, w: Integer; l: PMSG): Integer stdcall;
  begin
    if c=HC_ACTION then
      if l^.message=WM_LBUTTONUP then
      begin
        if l^.hwnd = form_main.mmo3.Handle then
          if PtInRect(r3, Point(LOWORD(l^.lParam), HIWORD(l^.lParam))) then
          begin
            OutputDebugString(pchar(format('click: %d,%d,%d,%d------%d,%d',[r3.Left,r3.Top,r3.Right,r3.Bottom,LOWORD(l^.lParam), HIWORD(l^.lParam)])));
            bSelected3 := True;
          end
      end else if l^.message = WM_KEYDOWN then
        if l^.hwnd = form_main.mmo3.Handle then
          if l^.wParam = VK_RETURN then
          begin
            OutputDebugString('enter');
            bSelected3 := True;
          end;

    Result := CallNextHookEx(0, c, w, Integer(l));
  end;

begin
  Result := CallWindowProc(oldEditWndProc3, h,m,w,l);
 
  if m=WM_ENTERIDLE then
  begin
    if w=MSGF_MENU then
      if hm3=0 then
      begin
        outputdebugstring('menu pop up');
        hm3 := SendMessage(l, MN_GETHMENU, 0, 0);
        AppendMenu(hm3, MF_SEPARATOR, 0, nil);
        AppendMenu(hm3, MF_STRING, 1017, '导入并保存测控程序？');
        GetMenuItemRect(0, hm3, GetMenuItemCount(hm3)-1, r3);
        hk3:=SetWindowsHookEx(WH_GETMESSAGE, @GetMsgProc, hInstance, MainThreadId);
      end
  end else if m=WM_CONTEXTMENU then
    if hk3<>0 then
    begin
      outputdebugstring('menu closed');
      UnHookWindowsHookEx(hk3);
      hk3:=0;
      hm3 := 0;
      if bSelected3 then
      begin
        bSelected3:=False;
        form_main.mmo3DblClick(form_main);//Application.MessageBox('你好，世界！', '提示', MB_OK);
      end
    end;
end;

function mmo9WndProc(h,m,w,l: Integer): Integer stdcall;
const
  MN_GETHMENU=$1E1;

  function GetMsgProc(c, w: Integer; l: PMSG): Integer stdcall;
  begin
    if c=HC_ACTION then
      if l^.message=WM_LBUTTONUP then
      begin
        if l^.hwnd = form_main.mmo9.Handle then
          if PtInRect(r9, Point(LOWORD(l^.lParam), HIWORD(l^.lParam))) then
          begin
            OutputDebugString(pchar(format('click: %d,%d,%d,%d------%d,%d',[r9.Left,r9.Top,r9.Right,r9.Bottom,LOWORD(l^.lParam), HIWORD(l^.lParam)])));
            bSelected9 := True;
          end
      end else if l^.message = WM_KEYDOWN then
        if l^.hwnd = form_main.mmo9.Handle then
          if l^.wParam = VK_RETURN then
          begin
            OutputDebugString('enter');
            bSelected9 := True;
          end;

    Result := CallNextHookEx(0, c, w, Integer(l));
  end;

begin
  Result := CallWindowProc(oldEditWndProc9, h,m,w,l);
 
  if m=WM_ENTERIDLE then
  begin
    if w=MSGF_MENU then
      if hm9=0 then
      begin
        outputdebugstring('menu pop up');
        hm9 := SendMessage(l, MN_GETHMENU, 0, 0);
        AppendMenu(hm9, MF_SEPARATOR, 0, nil);
        AppendMenu(hm9, MF_STRING, 1017, '保存python示例程序');
        GetMenuItemRect(0, hm9, GetMenuItemCount(hm9)-1, r9);
        hk9:=SetWindowsHookEx(WH_GETMESSAGE, @GetMsgProc, hInstance, MainThreadId);
      end
  end else if m=WM_CONTEXTMENU then begin
    if hk9<>0 then
    begin
      outputdebugstring('menu closed');
      UnHookWindowsHookEx(hk9);
      hk9:=0;
      hm9 := 0;
      if bSelected9 then
      begin
        bSelected9:=False;
        form_main.mmo9DblClick(form_main);//Application.MessageBox('你好，世界！', '提示', MB_OK);
      end
    end;
  end;
end;
function panel1WndProc(h,m,w,l: Integer): Integer stdcall;
const
  MN_GETHMENU=$1E1;
  
  function GetMsgProc(c, w: Integer; l: PMSG): Integer stdcall;
  begin
    if c=HC_ACTION then
      if l^.message=WM_LBUTTONUP then
      begin
        if l^.hwnd = form_main.panel1.Handle then
          if PtInRect(r1, Point(LOWORD(l^.lParam), HIWORD(l^.lParam))) then
          begin
            OutputDebugString(pchar(format('click: %d,%d,%d,%d------%d,%d',[r1.Left,r1.Top,r1.Right,r1.Bottom,LOWORD(l^.lParam), HIWORD(l^.lParam)])));
            bSelected1 := True;
          end
      end else if l^.message = WM_KEYDOWN then
        if l^.hwnd = form_main.panel1.Handle then
          if l^.wParam = VK_RETURN then
          begin
            OutputDebugString('enter');
            bSelected1 := True;
          end;

    Result := CallNextHookEx(0, c, w, Integer(l));
  end;

begin
  Result := CallWindowProc(oldEditWndProc1, h,m,w,l);

  if m=WM_ENTERIDLE then
  begin
    if w=MSGF_MENU then
      if hm1=0 then
      begin
        outputdebugstring('menu pop up');
        hm1 := SendMessage(l, MN_GETHMENU, 0, 0);
        AppendMenu(hm1, MF_SEPARATOR, 0, nil);
        AppendMenu(hm1, MF_STRING, 1017, '导出并编辑测控程序？');
        GetMenuItemRect(0, hm1, GetMenuItemCount(hm1)-1, r1);
        hk1:=SetWindowsHookEx(WH_GETMESSAGE, @GetMsgProc, hInstance, MainThreadId);
      end
  end else if m=WM_CONTEXTMENU then
    if hk1<>0 then
    begin
      outputdebugstring('menu closed');
      UnHookWindowsHookEx(hk1);
      hk1:=0;
      hm1 := 0;
      if bSelected1 then
      begin
        bSelected1:=False;
        form_main.panel1DblClick(form_main);//Application.MessageBox('你好，世界！', '提示', MB_OK);
      end
    end;
end;


function mmo13WndProc(h,m,w,l: Integer): Integer stdcall;
var
  ii:SmallInt;
const
  MN_GETHMENU=$1E1;
  
  function GetMsgProc(c, w: Integer; l: PMSG): Integer stdcall;
  begin
    if c=HC_ACTION then
      if l^.message=WM_LBUTTONUP then
      begin
        if l^.hwnd = form_main.mmo13.Handle then
          if PtInRect(r13, Point(LOWORD(l^.lParam), HIWORD(l^.lParam))) then
          begin
            OutputDebugString(pchar(format('click: %d,%d,%d,%d------%d,%d',[r13.Left,r13.Top,r13.Right,r13.Bottom,LOWORD(l^.lParam), HIWORD(l^.lParam)])));
            bSelected13 := True;
          end
      end else if l^.message = WM_KEYDOWN then
        if l^.hwnd = form_main.mmo13.Handle then
          if l^.wParam = VK_RETURN then
          begin
            OutputDebugString('enter');
            bSelected13 := True;
          end;

    Result := CallNextHookEx(0, c, w, Integer(l));
  end;

begin
  Result := CallWindowProc(oldEditWndProc13, h,m,w,l);
 
  if m=WM_ENTERIDLE then
  begin
    if w=MSGF_MENU then
      if hm13=0 then
      begin
        outputdebugstring('menu pop up');
        hm13 := SendMessage(l, MN_GETHMENU, 0, 0);
        AppendMenu(hm13, MF_SEPARATOR, 0, nil);
        AppendMenu(hm13, MF_STRING, 1017, '添加测量NG点电阻步骤+');
        GetMenuItemRect(0, hm13, GetMenuItemCount(hm13)-1, r13);
        hk13:=SetWindowsHookEx(WH_GETMESSAGE, @GetMsgProc, hInstance, MainThreadId);
      end
  end else if m=WM_CONTEXTMENU then
    if hk13<>0 then
    begin
      outputdebugstring('menu closed');
      UnHookWindowsHookEx(hk13);
      hk13:=0;
      hm13 := 0;
      if bSelected13 then
      begin
        bSelected13:=False;
        with form_main do begin
          if form_main.btn14.Enabled then begin
            edt49.tag:=Pos('NG:',mmo13.Text)+3;
            edt50.tag:=Pos('NG:',mmo14.Text)+3;
          end;

          mmo13.Text:=StringReplace(mmo13.Text, '[', '(', [rfReplaceAll]);
          mmo13.Text:=StringReplace(mmo13.Text, ']', ')', [rfReplaceAll]);
          if (Pos('(',mmo13.Text)>0)and(Pos(',',mmo13.Text)>Pos('(',mmo13.Text)) then begin     //有NG点
            for ii:=0 to 20 do begin
              if PosEx(')',mmo13.Text,edt49.tag)<=PosEx('(',mmo13.Text,edt49.tag) then begin
                edt49.tag:=Pos('NG:',mmo13.Text)+3;
                Continue;
                ShowMessage('重新开始NG点了！');
              end;

              edt49.hint:=Copy(mmo13.Text,PosEX('(',mmo13.Text,edt49.tag),PosEx(')',mmo13.Text,edt49.tag)-PosEx('(',mmo13.Text,edt49.tag));
              if Pos(',',edt49.hint)<1 then  begin
                edt49.tag:=PosEx(')',mmo13.Text,edt49.tag)+1;
                Continue;
              end;
              edt49.hint:=edt49.hint+',';                 //以,结束的字符串；

              if (Pos('(',edt49.hint)>0)and(Pos(',',edt49.hint)>Pos('(',edt49.hint)) then begin    //有一个数
                if form_main.btn14.Enabled then begin
                  form_main.edt143.Text:='0';
                end;
                form_main.edt143.Text:=IntToStr(StrToInt(form_main.edt143.Text)+1);
                form_main.edt144.Text:='0R01';


                form_main.edt49.Text:=copy(edt49.Hint,2,pos(',',edt49.Hint)-2);
                edt144.Tag:=0;
                edt144.Tag:=posEx(',',edt49.Hint,0);
                edt144.Tag:=posEx(',',edt49.Hint,edt144.Tag+1)-edt144.Tag;   // 两个,号之间字符个数
                if edt144.Tag>1 then
                  form_main.edt50.Text:=copy(edt49.Hint,pos(',',edt49.Hint)+1,edt144.Tag-1);

                form_main.btn44Click(form_main);
                mmo13.SelStart:=edt49.tag;
                mmo13.SelLength:=Length(edt49.Hint)-2;
                mmo13.SetFocus;
                edt49.tag:=PosEx(')',mmo13.Text,edt49.tag)+1;

                form_main.btn14.Enabled:=False;     //查看NG电阻，不能保存步骤
                form_main.btn16.Enabled:=False;  lbl170.Visible:=true; lbl170.Caption:='NG点观察中...';
                Break;
              end;
            end;
          end else begin
            edt49.tag:=Pos('NG:',mmo13.Text)+3;
            ShowMessage('没有NG点！添加不了NG点电阻步骤');
          end;
        end;
      end
    end;
end;

function mmo14WndProc(h,m,w,l: Integer): Integer stdcall;
var
  ii:SmallInt;
const
  MN_GETHMENU=$1E1;

  function GetMsgProc(c, w: Integer; l: PMSG): Integer stdcall;
  begin
    if c=HC_ACTION then
      if l^.message=WM_LBUTTONUP then
      begin
        if l^.hwnd = form_main.mmo14.Handle then
          if PtInRect(r14, Point(LOWORD(l^.lParam), HIWORD(l^.lParam))) then
          begin
            OutputDebugString(pchar(format('click: %d,%d,%d,%d------%d,%d',[r14.Left,r14.Top,r14.Right,r14.Bottom,LOWORD(l^.lParam), HIWORD(l^.lParam)])));
            bSelected14 := True;
          end
      end else if l^.message = WM_KEYDOWN then
        if l^.hwnd = form_main.mmo14.Handle then
          if l^.wParam = VK_RETURN then
          begin
            OutputDebugString('enter');
            bSelected14 := True;
          end;

    Result := CallNextHookEx(0, c, w, Integer(l));
  end;

begin
  Result := CallWindowProc(oldEditWndProc14, h,m,w,l);
 
  if m=WM_ENTERIDLE then
  begin
    if w=MSGF_MENU then
      if hm14=0 then
      begin
        outputdebugstring('menu pop up');
        hm14 := SendMessage(l, MN_GETHMENU, 0, 0);
        AppendMenu(hm14, MF_SEPARATOR, 0, nil);
        AppendMenu(hm14, MF_STRING, 1017, '添加测量NG点电阻步骤+');
        GetMenuItemRect(0, hm14, GetMenuItemCount(hm14)-1, r14);
        hk14:=SetWindowsHookEx(WH_GETMESSAGE, @GetMsgProc, hInstance, MainThreadId);
      end
  end else if m=WM_CONTEXTMENU then begin
    if hk14<>0 then
    begin
      outputdebugstring('menu closed');
      UnHookWindowsHookEx(hk14);
      hk14:=0;
      hm14 := 0;
      if bSelected14 then
      begin
        bSelected14:=False;

        with form_main do begin
          if form_main.btn14.Enabled then begin
            edt49.tag:=Pos('NG:',mmo13.Text)+3;
            edt50.tag:=Pos('NG:',mmo14.Text)+3;
          end;
          mmo14.Text:=StringReplace(mmo14.Text, '[', '(', [rfReplaceAll]);
          mmo14.Text:=StringReplace(mmo14.Text, ']', ')', [rfReplaceAll]);
          if (Pos('(',mmo14.Text)>0)and(Pos(',',mmo14.Text)>Pos('(',mmo14.Text)) then begin     //有NG点
            for ii:=0 to 20 do begin
              if PosEx(')',mmo14.Text,edt50.tag)<=PosEx('(',mmo14.Text,edt50.tag) then begin
                edt50.tag:=Pos('NG:',mmo14.Text)+3;
                Continue;
                ShowMessage('重新开始NG点了！');
              end;

              edt50.hint:=Copy(mmo14.Text,PosEX('(',mmo14.Text,edt50.tag),PosEx(')',mmo14.Text,edt50.tag)-PosEx('(',mmo14.Text,edt50.tag));
              if Pos(',',edt50.hint)<1 then begin
                edt50.tag:=PosEx(')',mmo14.Text,edt50.tag)+1;
                Continue;
              end;
              edt50.hint:=edt50.hint+',';                 //以,结束的字符串；

              if (Pos('(',edt50.hint)>0)and(Pos(',',edt50.hint)>Pos('(',edt50.hint)) then begin    //有一个数
                if form_main.btn14.Enabled then begin
                  form_main.edt143.Text:='0';
                end;
                form_main.edt143.Text:=IntToStr(StrToInt(form_main.edt143.Text)+1);
                form_main.edt144.Text:='0R01';


                form_main.edt49.Text:=copy(edt50.Hint,2,pos(',',edt50.Hint)-2);
                edt144.Tag:=0;
                edt144.Tag:=posEx(',',edt50.Hint,0);
                edt144.Tag:=posEx(',',edt50.Hint,edt144.Tag+1)-edt144.Tag;   // 两个,号之间字符个数
                if edt144.Tag>1 then
                  form_main.edt50.Text:=copy(edt50.Hint,pos(',',edt50.Hint)+1,edt144.Tag-1);

                form_main.btn44Click(form_main);
                mmo14.SelStart:=edt50.tag;
                mmo14.SelLength:=Length(edt50.Hint)-2;
                mmo14.SetFocus;
                edt50.tag:=PosEx(')',mmo14.Text,edt50.tag)+1;

                form_main.btn14.Enabled:=False;     //查看NG电阻，不能保存步骤
                form_main.btn16.Enabled:=False;  lbl170.Visible:=true; lbl170.Caption:='NG点观察中...';
                Break;
              end;
            end;
          end else begin
            edt50.tag:=Pos('NG:',mmo14.Text)+3;
            ShowMessage('没有NG点！添加不了NG点电阻步骤');
          end;
        end;
      end
    end;
  end;
end;
end.
