unit LearnClampDiode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,Unit0_globalVariant, StdCtrls, sButton, ExtCtrls, sPanel,
  sComboBox, sCheckBox, sGroupBox, sEdit, sSpinEdit, sLabel;

type
  TFormClampDiode = class(TForm)
    grp1: TGroupBox;
    grp2: TsGroupBox;
    chk1: TsCheckBox;
    chk2: TsCheckBox;
    chk3: TsCheckBox;
    chk4: TsCheckBox;
    chk5: TsCheckBox;
    chk6: TsCheckBox;
    chk7: TsCheckBox;
    chk8: TsCheckBox;
    chk10: TsCheckBox;
    chk11: TsCheckBox;
    chk12: TsCheckBox;
    chk13: TsCheckBox;
    chk14: TsCheckBox;
    chk15: TsCheckBox;
    edt3: TsDecimalSpinEdit;
    edt4: TsDecimalSpinEdit;
    edt5: TsDecimalSpinEdit;
    edt6: TsDecimalSpinEdit;
    edt7: TsDecimalSpinEdit;
    edt8: TsDecimalSpinEdit;
    edt9: TsDecimalSpinEdit;
    edt10: TsDecimalSpinEdit;
    edt11: TsDecimalSpinEdit;
    edt12: TsDecimalSpinEdit;
    edt13: TsDecimalSpinEdit;
    edt14: TsDecimalSpinEdit;
    edt15: TsEdit;
    grp3: TsGroupBox;
    edt16: TsEdit;
    chk9: TsCheckBox;
    cbb1: TsComboBox;
    chk16: TsCheckBox;
    chk17: TsCheckBox;
    chk18: TsCheckBox;
    chk19: TsCheckBox;
    cbb2: TsComboBox;
    cbb3: TsComboBox;
    spnl1: TsPanel;
    btn1: TsButton;
    btn2: TsButton;
    edt1: TEdit;
    edt2: TEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure chk19Click(Sender: TObject);
    procedure chk18Click(Sender: TObject);
    procedure chk17Click(Sender: TObject);
    procedure cbb3Change(Sender: TObject);
    procedure cbb2Change(Sender: TObject);
    procedure cbb1Change(Sender: TObject);
    procedure edt6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt16KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt12KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt14KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormClampDiode: TFormClampDiode;

implementation

{$R *.dfm}

procedure TFormClampDiode.btn2Click(Sender: TObject);
begin
  close;
end;

procedure TFormClampDiode.btn1Click(Sender: TObject);
var tt,i:SmallInt;
  temp8 :byte;
  str:string;
begin
    modbusfun16int:=$2F26;
    modbusfun16len:=$12;      
    sbuf[2]:=$10;

    sbuf[8]:=StrToInt('0'+edt1.Text)div 256;      //edt1.Text序号 开始步骤
    sbuf[9]:=StrToInt('0'+edt1.Text)mod 256;

    sbuf[10]:=byte('?');sbuf[11]:=byte(' ');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');
    str:= AnsiUpperCase(edt15.Text);
    Move(str[1],sbuf[10],Length(str));
    if(VIRLCROWS<60)then begin
      temp8:=sbuf[10]; sbuf[10]:=sbuf[11];sbuf[11]:=temp8;
      temp8:=sbuf[12]; sbuf[12]:=sbuf[13];sbuf[13]:=temp8;
    end;
    sbuf[14]:=StrToInt('0'+edt3.Text)shr 8;//div 256; //标准值
    sbuf[15]:=StrToInt('0'+edt3.Text)mod 256;
    sbuf[16]:=StrToInt('0'+edt2.Text)div 256; //运算值（测量值）--- 转为步骤总数
    sbuf[17]:=StrToInt('0'+edt2.Text)mod 256;
    
    sbuf[18]:=StrToInt('0'+edt4.Text)shr 8;//div 256; //上限
    sbuf[19]:=StrToInt('0'+edt4.Text)mod 256;
    sbuf[20]:=StrToInt('0'+edt5.Text)shr 8;//div 256; //下限
    sbuf[21]:=StrToInt('0'+edt5.Text)mod 256;

    sbuf[22]:=0;
    sbuf[23]:=0;
    if chk7.Checked then
    begin
      sbuf[22]:=StrToInt('0'+edt6.Text) div 256;  //高点
      sbuf[23]:=StrToInt('0'+edt6.Text);
    end;
    sbuf[24]:=0;
    sbuf[25]:=0;
    if chk6.Checked then
    begin
      sbuf[24]:=StrToInt('0'+edt7.Text) div 256;  //低点
      sbuf[25]:=StrToInt('0'+edt7.Text);
    end;

    sbuf[26]:=StrToInt('0'+edt8.Text) div 256;  //延时
    sbuf[27]:=StrToInt('0'+edt8.Text);

    sbuf[28]:=byte('0');sbuf[29]:=byte('0');sbuf[30]:=byte('0');sbuf[31]:=byte('0');    //无效
    str:= AnsiUpperCase(edt16.Text);
    Move(str[1],sbuf[28],Length(str));
    if(VIRLCROWS<60)then begin
      temp8:=sbuf[28]; sbuf[28]:=sbuf[29];sbuf[29]:=temp8;
      temp8:=sbuf[30]; sbuf[30]:=sbuf[31];sbuf[31]:=temp8;
    end;
    sbuf[32]:=StrToInt('0'+edt9.Text)div 256; //k值
    sbuf[33]:=StrToInt('0'+edt9.Text)mod 256;
    sbuf[34]:=StrToInt(edt10.Text)shr 8; //b值
    sbuf[35]:=StrToInt(edt10.Text)mod 256;
    sbuf[36]:=StrToInt('0'+edt11.Text)div 256; //上限 2
    sbuf[37]:=StrToInt('0'+edt11.Text)mod 256;
    sbuf[38]:=StrToInt('0'+edt12.Text)div 256; //下限2
    sbuf[39]:=StrToInt('0'+edt12.Text)mod 256;
    sbuf[40]:=StrToInt('0'+edt13.Text) div 256;  //高点2
    sbuf[41]:=StrToInt('0'+edt13.Text);
    sbuf[42]:=StrToInt('0'+edt14.Text) div 256;  //低点2
    sbuf[43]:=StrToInt('0'+edt14.Text);

    addToLog('芯片保护二级体自学习( GND针点号：'+edt6.Text+',VCC针点号：'+edt7.Text+')' );
    close;
end;

procedure TFormClampDiode.chk19Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormClampDiode.chk18Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormClampDiode.chk17Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormClampDiode.cbb3Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormClampDiode.cbb2Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormClampDiode.cbb1Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormClampDiode.edt6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt16KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt12KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormClampDiode.edt14KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) then
    btn1Click(self);
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

end.
