{
//=======数据处理 重点===================================================================
V时间点 \ 要素   文件夹  文件名  表头项目名  测量数据  显示在界面  MES网盘及备份
V开启软件show     √      √      √                    √
V加载完成                         √                    √
V第一次上传数据   √      √

1.数据预处理                                  √        √
2.数据显示                                              √
3.数据终处理并保存        √       √         √                    √

V关闭软件                                                           √

  依child搜edt62，所有类型数据的全过程；
//1.SaveFlagProcess数据保存入口函数
    1.1依据配置和收到的机种名生成文件夹，及设置新文件标志；
    1.2调用保存数据 函数

//2.CreatFIle生成文件（有3个时间点：开启软件时、第一次测试完成、每一次测试完成），依客户不同
    2.1主路径文件为必须的，也是多样的，都是先生成文件后保存数据
        2.1.1 处理文件名的定义；
        2.1.2 处理文件表头数据的定义；
    2.2 分只生产一个文件和一个条码一个文件；
        2.2.1txt格式基本只有一个文件
        2.2.2 专用格式一个条码一个文件；
    2.3 第二备份路径文件名生成，针对CHILDFOLDER为两位数，如=12或13
    2.4.文件表头几行 获取方法：
     2.4.0 A,早期8700数据有PC解析，直接从config指定表头，禁止加载；
     2.4.1 B,txt格式和通用CSV格式在切换机种时要刷新（从主板获取信息，先生成文件，等加载成功后再刷新）
     2.4.2 C,一个条码一个文件的专用文件禁止刷新；

//3.SaveFile保存文件 ，依客户不同
    3.1主路径文件为必须的，也是多样的 ，都是先生成文件后保存数据
        3.1.2 每天一个文件或者 MFLEX条码文件则处理是否生成文件；
        3.1.3  先保存log文件表头部分
        3.1.4   要保存的数据先在mmo2显示；
        3.1.5  区分单PCS和多PCS数据文件的保存；
     3.2 生成judge文件并保存数据
     3.3 第二备份路径文件打开或生成并保存数据 针对CHILDFOLDER=12或13，
     3.4 第三备份路径文件生成并保存数据
//4.备份数据到服务器，函数存放在main文件中；
}
unit Unit4_logFileCode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ActnList, NB30, registry, shellapi, dbclient, inifiles, db,
  adodb, FileCtrl, StdCtrls, StrUtils;

type
  Tnormal = class
    class procedure SaveFlagProcess(devnum: integer; SaveLen: SmallInt);
    class procedure FreshDatalogFromMemo(len_ss: integer);

  end;

procedure saveCsvReport(len_ss: integer);

procedure CreatAnewCsv(tt: integer; barcode: SmallInt);

procedure saveCsvdata(len_ss: integer; NewLogFileFlag: byte);

function OfflineReply(): SmallInt;

implementation

uses
  StepCopy, main, OSstudy, FindPoint, PNenter, Unit0_globalVariant, LogHeadTips,
  Unit2_Communication, Unit3appFSM, LoadFile;

function OfflineReply(): SmallInt;
var
  crc_temp, i: SmallInt;
  Ansi_s, AnsiSLINE_head: string;
  strs: TStringList;
  temp8: Byte;
begin
  usb_busyT := 2;
       //-------默认参数--------------
  rbuf[1] := sbuf[1];
  rbuf[2] := sbuf[2];
  rbuf[3] := 2 * sbuf[6];
  rbuf[4] := 0;
  rbuf[5] := 0;
  rbuf[6] := byte('0');
  rbuf[7] := byte('0');
  rbuf[8] := byte('0');
  rbuf[9] := byte('0');
  rbuf[10] := 0;
  rbuf[11] := 200;
  rbuf[12] := 0;
  rbuf[13] := 0;
  rbuf[14] := 0;
  rbuf[15] := 30;
  rbuf[16] := 0;
  rbuf[17] := 30;
  rbuf[18] := 0;
  rbuf[19] := 1;
  rbuf[20] := 0;
  rbuf[21] := 2;
  rbuf[22] := 0;
  rbuf[23] := 5;
  rbuf[24] := byte('0');
  rbuf[25] := byte('0');
  rbuf[26] := byte('0');
  rbuf[27] := byte('0');
  rbuf[28] := 0;
  rbuf[29] := 100;
  rbuf[30] := 0;
  rbuf[31] := 0;
  rbuf[32] := 0;
  rbuf[33] := 0;
  rbuf[34] := 0;
  rbuf[35] := 0;
  rbuf[36] := 0;
  rbuf[37] := 0;
  rbuf[38] := 0;
  rbuf[39] := 0;
  rbuf[40] := byte('0');
  rbuf[41] := 0;
  rbuf[42] := 0;
  rbuf[43] := 0;
  rbuf[44] := 0;
  rbuf[45] := 0;
  rbuf[46] := 0;
  rbuf[47] := 0;
  rbuf[48] := 0;
  rbuf[49] := 0;
  rbuf[50] := 0;
  rbuf[51] := 0;
  rbuf[52] := 0;
  rbuf[53] := 0;
  rbuf[54] := 0;
  rbuf[55] := 0;

  case sbuf[2] mod $20 of
    $01:
      begin
        viewstring := viewstring + '[读取：测试板读写信息]';
      end;
    $02:
      begin
        viewstring := viewstring + '[读取：测试板只读状态信息]';
      end;
    $03:
      begin
        case sbuf[3] of
          $10:
            begin
              viewstring := viewstring + '[读取：系统参数]';
            end;
          $18..$28:
            begin
              viewstring := viewstring + '[读取：短路群]';
            end;
          $30..$4F:
            begin
              viewstring := viewstring + '[读取：测试步骤]';
            end;
        else
          viewstring := viewstring + '[读取]';
        end;
      end;
    $05:
      begin
        case sbuf[3] of
          $00:
            begin
              case sbuf[4] of
                $00:
                  begin
                    viewstring := viewstring + '[导入、导出 复位命令]';
                  end;
                $01:
                  begin
                    viewstring := viewstring + '[找点命令]';
                  end;
                $05:
                  begin
                    viewstring := viewstring + '[一次短路群学习命令]';
                  end;
                $06:
                  begin
                    viewstring := viewstring + '[启二次短路群学习命令]';
                  end;
                $08:
                  begin
                    viewstring := viewstring + '[保存系统参数命令]';
                  end;
                $09:
                  begin
                    viewstring := viewstring + '[保存短路群命令]';
                  end;
                $0B:
                  begin
                    viewstring := viewstring + '[保存步骤命令]';
                  end;
                $0C:
                  begin
                    viewstring := viewstring + '[单步命令]';
                    OfflinePtr := OfflinePtr + 1;
                  end;
                $0D:
                  begin
                    viewstring := viewstring + '[上翻页命令]';
                    if OfflinePtr >= VIRLCROWS then
                      OfflinePtr := OfflinePtr - VIRLCROWS
                    else
                      OfflinePtr := 0;
                  end;
                $0E:
                  begin
                    viewstring := viewstring + '[下翻页命令]';
                    OfflinePtr := OfflinePtr + VIRLCROWS;
                  end;
                $10:
                  begin
                    viewstring := viewstring + '[启动命令]';
                  end;
                $11:
                  begin
                    viewstring := viewstring + '[复位命令]';
                  end;
              else
                viewstring := viewstring + '[无说明]';
              end;
            end;
          $50:
            begin
              case sbuf[4] of
                $00:
                  begin
                    viewstring := viewstring + '[上传PC数据已保存]';
                  end;
                $01:
                  begin
                    viewstring := viewstring + '[USB通讯正常]';
                  end;
                $02:
                  begin
                    viewstring := viewstring + '[已收到上传状态]';
                  end;
              else
                viewstring := viewstring + '[无说明]';
              end;
            end;
        else
          viewstring := viewstring + '[无说明]';
        end;
      end;
    $06:
      begin
        case sbuf[3] of
          $00:
            begin
              case sbuf[4] of
                $41:
                  begin
                    viewstring := viewstring + '[测试程序选择]';
                  end;
              else
                viewstring := viewstring + '[无说明]';
              end;
            end;
          $10:
            begin
              case sbuf[4] of
                $B0:
                  begin
                    viewstring := viewstring + '[测试步骤选择]';
                    if (sbuf[5] * 256 + sbuf[6]) > 0 then
                      OfflinePtr := sbuf[5] * 256 + sbuf[6] - 1;
                  end;
              else
                viewstring := viewstring + '[无说明]';
              end;
            end;
        else
          viewstring := viewstring + '[无说明]';
        end;
      end;
  else
    viewstring := viewstring + '[无说明]';
  end;
  with form_main do begin
    if edt45.Text <> IntToStr(OfflinePtr + 1) then begin
      edt45.Text := IntToStr(OfflinePtr + 1);     //当前步骤
      if OfflinePtr = 0 then
        Panel1.VertScrollBar.Position := 0
      else if ((Panel1.VertScrollBar.Position + 15 * strtoint(form_main.edt121.Text)) < ((OfflinePtr mod VIRLCROWS) * strtoint(form_main.edt121.Text))) //大于15行
        or (Panel1.VertScrollBar.Position > (OfflinePtr mod VIRLCROWS) * strtoint(form_main.edt121.Text)) then     //起始位置大
      begin
        Panel1.VertScrollBar.Position := (OfflinePtr mod VIRLCROWS) * strtoint(form_main.edt121.Text);  //20;
      end;
    end;
    for i := 1 to VIRLCROWS do
      zu2edit[i].color := clwindow;           //颜色归位

    zu2edit[1 + OfflinePtr mod VIRLCROWS].Color := clskyblue;      //当前步骤 名称选中
      {  if scrlbx4.Visible or scrlbx5.Visible then begin
          ildrw6.Top:=zu19edit[1+ OfflinePtr mod VIRLCROWS ].Top;
          mmo27.Top:=ildrw6.Top+ildrw6.Height;
          StepNotes(OfflinePtr);
        end;  }
  end;

  strs := TStringList.Create;
  strs.Clear;
      //接收row_num变量，从1开始
  if (row_num < 1) then
    exit;                         //空，==0跳过 ,下传时的一个0状态,真正监控会变为1
  if (row_num > 0) and (row_num <= (VIRLCROWS)) then begin //有效步骤内

    if ((OfflinePtr div VIRLCROWS) * VIRLCROWS + row_num) <= OfflineMax then begin

      Ansi_s := Char(#9);
      AnsiSLINE_head := OfflineFileList[(OfflinePtr div VIRLCROWS) * VIRLCROWS + row_num];
      while Pos(Ansi_s, AnsiSLINE_head) > 0 do begin
        i := Pos(Ansi_s, AnsiSLINE_head);
        strs.add(Copy(AnsiSLINE_head, 1, i - 1));
        AnsiSLINE_head := Copy(AnsiSLINE_head, i + length(Ansi_s), length(AnsiSLINE_head) - i);
      end;
      strs.Add(AnsiSLINE_head);

      if length(strs[0]) > 0 then begin
        if IsNumberic(strs[0]) then begin
          rbuf[4] := StrToInt(strs[0]) div 256;
          rbuf[5] := StrToInt(strs[0]) mod 256;
        end
        else begin
          Application.MessageBox('File format error! 程序文件步骤格式不对，请用记事本打开检查格式或者是否被加密；', '错误', MB_OK + MB_ICONSTOP);
          form_main.lbl28.Caption := 'Import Error,File format error! ';
          exit;
        end;
      end;

      if strs.Count > 1 then begin     //名称
        if length(strs[1]) > 0 then begin
          Move(strs[1][1], rbuf[6], Length(strs[1]));
          temp8 := rbuf[6];
          rbuf[6] := rbuf[7];
          rbuf[7] := temp8;
          temp8 := rbuf[8];
          rbuf[8] := rbuf[9];
          rbuf[9] := temp8;
          for i := 0 to 3 do begin
            if (rbuf[6 + i] >= $7F) then
              rbuf[6 + i] := byte('*');       //无效
          end;
        end;
      end;
      for i := 2 to 8 do begin
        if strs.Count > 9 then begin
          rbuf[6 + 2 * i] := 0;
          rbuf[7 + 2 * i] := 0;
          if length(strs[i]) > 0 then begin
            rbuf[6 + 2 * i] := StrToInt(strs[i]) shr 8;
            rbuf[7 + 2 * i] := StrToInt(strs[i]) mod 256;
          end;
        end;
      end;

      if strs.Count > 9 then begin    //模式
        if (length(strs[9]) > 0) then begin
          Move(strs[9][1], rbuf[24], Length(strs[9]));
          temp8 := rbuf[24];
          rbuf[24] := rbuf[25];
          rbuf[25] := temp8;
          temp8 := rbuf[26];
          rbuf[26] := rbuf[27];
          rbuf[27] := temp8;
          for i := 0 to 3 do begin
            if (rbuf[24 + i] >= $80) then
              rbuf[24 + i] := byte('*');    //2018-10-27不支持中文和空格
          end;
        end;
      end;

      for i := 10 to 15 do begin
        if strs.Count > 15 then          //有16项参数
        begin
          if length(strs[i]) > 0 then begin
            rbuf[8 + 2 * i] := StrToInt(strs[i]) shr 8; // div 256;
            rbuf[9 + 2 * i] := StrToInt(strs[i]) mod 256;
          end;
        end;
      end;
      if strs.Count > 16 then begin         //有备注参数
        Move(strs[16][1], rbuf[40], Length(strs[16]));
      end;

      crc_temp := rxd_crc(1, 55);
      rbuf[55 + 1] := crc_temp mod 256;
      rbuf[55 + 2] := crc_temp div 256;
      result := 57;
    end;
  end
  else if row_num <= (VIRLCROWS + 1) then begin   //调试系统参数 。而导出时为翻页命令05,所以不会进来！
    AnsiSLINE_head := OfflineFileList[OfflineMax + 2];
    strs.Delimiter := #9;
    strs.DelimitedText := AnsiSLINE_head;

    if not IsNumberic(strs[0]) then begin
      Application.MessageBox('File format error! 程序文件之系统参数格式不对，请用记事本打开检查格式或者是否被加密；', '错误', MB_OK + MB_ICONSTOP);
      form_main.lbl28.Caption := 'Import Error,File format error! ';
      exit;
    end;
    for i := 0 to 12 do begin
      if length(strs[i]) > 0 then begin
        rbuf[4 + 2 * i] := StrToInt(strs[i]) div 256;
        rbuf[5 + 2 * i] := StrToInt(strs[i]) mod 256;
      end;
    end;
    if IsNumberic(strs[3]) then
      TxtOSPins := StrToInt(strs[3]);     //提取OS点数

    if length(strs[13]) > 0 then begin
      Move(strs[13][1], rbuf[30], Length(strs[13]));
      temp8 := rbuf[30];
      rbuf[30] := rbuf[31];
      rbuf[31] := temp8;
      temp8 := rbuf[32];
      rbuf[32] := rbuf[33];
      rbuf[33] := temp8;
      temp8 := rbuf[34];
      rbuf[34] := rbuf[35];
      rbuf[35] := temp8;
    end;
    if (strs.Count > 14) and (length(strs[14]) > 0) then     //OKenable
    begin
      rbuf[4] := StrToInt(strs[14]);
    end;
    rbuf[36] := (OfflinePtr + 1) div 256;
    rbuf[37] := (OfflinePtr + 1) mod 256;
    if strs.Count > 18 then
      for i := 15 to 18 do begin
        if IsNumberic(strs[i]) and (length(strs[i]) > 0) then begin
          rbuf[8 + 2 * i] := StrToInt(strs[i]) div 256;
          rbuf[9 + 2 * i] := StrToInt(strs[i]) mod 256;
        end;
      end;
    crc_temp := rxd_crc(1, 45);
    rbuf[45 + 1] := crc_temp mod 256;
    rbuf[45 + 2] := crc_temp div 256;
    result := 47;
  end
  else if (row_num <= (3 + VIRLCOFFSET)) then begin     //系统状态和系统勾选变量；

  end
  else if (row_num <= (19 + VIRLCOFFSET)) then begin     //空闲

  end
  else if (row_num <= (21 + VIRLCOFFSET)) then begin     //19,20导出系统参数【rxdDebugStepPara设置row为 19+VIRLCOFFSET，而后进入这里】

  end
  else if (row_num <= (SHORTPINS + 20 + VIRLCOFFSET)) then begin        //短路群导出，以大于保存的OS点数 结束

  end
  else if (row_num <= (SHORTPINS + 20 + VIRLCOFFSET)) then begin        //短路群导出，以大于保存的OS点数 结束

  end;

  strs.Destroy;    //strs.Free;

end;
//二次刷新文件表头参数，
//标准文件格式，在加载完MCU测试程序参数后刷新，由rxdmonitorName-unit-upp函数决定文件表头
//非标准文件格式（yijia或者机箱专用、加贺格式、早期jaguar）由creatFile函数决定文件表头

class procedure Tnormal.FreshDatalogFromMemo(len_ss: integer);       //Log文件更新前面几行
var
//    f: TextFile;
  save_file_name: string;
  ResultList: TStringList;
begin
  with form_main do begin      //不同文件格式，处理方式不同
    if (Label10.caption <> 'Label10') and (StrToInt(edt62.Text) < 10) then begin   //有文件且通用；//mFlex文件只有一行数据，不能更新
      save_file_name := Label10.caption;
      ResultList := TStringList.Create;

      try
        ResultList.LoadFromFile(edt2.Text + save_file_name);
        ResultList[0] := mmo2String[0];
        if (LogType > 10) then begin   //csv文件格式 or(edt62.Text='10')一个条码一个文件 ，行数不够！

          ResultList[1] := mmo2String[2];
          ResultList[2] := mmo2String[4];
          ResultList[3] := mmo2String[5];
          if (LogType = 12) then begin

          end
          else if (Readchis('Result', 'DisShortgroups') = '') then               //没有禁止短路群
            ResultList[4] := mmo2String[9];
        end;
        ResultList.SaveToFile(edt2.Text + save_file_name); //保存到原txt文件中
        ResultList.Free;
      except
        ShowMessage('Save Err! Pls Check the File is Close 数据Log文件被其它软件占用无法保存！');
      end;

    end;
  end;
end;

procedure saveCsvReport(len_ss: integer);       //CSV文件保存统计报告
var
  f: TextFile;
  save_file_name: string;
  s, SLINE_head, SLINE_head1: string;
begin
  with form_main do begin
    if (Label10.caption <> 'Label10') and (Label10.caption <> 'OneBarcode-OneFile') then begin
      save_file_name := Label10.caption;
      if not isfileinuse(edt2.Text + save_file_name) then begin   //没有占用
        try
          assignfile(f, edt2.Text + save_file_name);      //ExtractFilePath(application.ExeName)
          Append(f); //rewrite(f);                         //application.ProcessMessages;
          SLINE_head := lbl79.caption + edt4.Text + ',' + lbl80.caption + edt63.Text + '(' + edt65.text + '%)' + ',' + edt64.Text + '(' + edt66.text + '%)';
          writeln(f, SLINE_head);
        finally
          Closefile(f);                  // Application.Terminate;
        end;
      end
      else begin
        Application.MessageBox('LogFile in use！Log文件被占用，不能添加统计信息', '警告', MB_OK + MB_ICONWARNING);
      end;
    end;
  end;
end;

class procedure Tnormal.SaveFlagProcess(devnum: integer; SaveLen: SmallInt);         //数据保存函数，唯一入口
var
//  f: TextFile;
  NewLogFileFlag: Byte;                          //应该生成新文件标志
  devi, i, j, k, crc_temp: SmallInt;    //,tt
  save_file_name, str, ss, mss, sst: string;
  UsbBuf: array[0..64] of Byte;
  Written: Cardinal;
  ToWrite: Cardinal;
  Err: DWORD;

  procedure UnlockClearBarcode();
  begin
    with form_main do begin
      if (StrToInt(Label7.hint) = 1) or (StrToInt(Label7.hint) = 2) or (StrToInt(Label7.hint) > 10) then       //自动清除条码
      begin
        if not Edit1.Enabled {and not FormLoad.Showing} then begin
          if Readchis('Model Para', 'BarCodeEdit') <> '' then begin
            if StrToInt(Readchis('Model Para', 'BarCodeEdit')) < 1000 then begin
              edit1.Enabled := true;
              if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
                Edit1.SetFocus;
            end;
          end
          else begin
            Edit1.Enabled := true;
            if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
              Edit1.SetFocus;
          end;
        end;       //开启条码框输入功能
        if not mEdit1.Enabled then
          medit1.Enabled := true;

        Edit1.Text := '';
        medit1.Text := '';
      end;
    end;
  end;

begin
  with form_main do begin

    //-------------------------------------单机数据更新 ------------------------------
    if (SaveLen > 0) then begin
      if grp45.Visible and (Readchis('Result', 'StandardSave') = '')    //通用：标准板数据不记录；只有配置了保存标准板才执行
        and ((edt171.Color <> clLime) or (edt173.Color <> clLime) or (edt175.Color <> clLime)) then begin    //有一个标准板没有验证
        Label11.Font.Color := clWindowText;
        Label11.Caption := '标准板验证...';
        if (rbuf[4] = 0) then
          lbl191.Caption := '当前结果：PASS'
        else
          lbl191.Caption := '当前结果：FAIL';
        lbl193.Caption := '当前条码：';
        edt176.Text := Trim(edit1.Text);
        if Trim(edt171.Text) = Trim(edit1.Text) then begin
          if (rbuf[4] = 0) and ((edt170.Text = 'PASS') or (edt170.Text = 'pass') or (edt170.Text = 'OK') or (edt170.Text = 'ok')) then begin
            edt171.Color := clLime;
            edt170.Color := clLime;
          end
          else if (rbuf[4] > 0) and ((edt170.Text = 'FAIL') or (edt170.Text = 'fail') or (edt170.Text = 'NG') or (edt170.Text = 'ng')) then begin
            edt171.Color := clLime;
            edt170.Color := clLime;
          end
          else
            edt171.Color := clred;
        end;
        if Trim(edt173.Text) = Trim(edit1.Text) then begin
          if (rbuf[4] = 0) and ((edt172.Text = 'PASS') or (edt172.Text = 'pass') or (edt172.Text = 'OK') or (edt172.Text = 'ok')) then begin
            edt173.Color := clLime;
            edt172.Color := clLime;
          end
          else if (rbuf[4] > 0) and ((edt172.Text = 'FAIL') or (edt172.Text = 'fail') or (edt172.Text = 'NG') or (edt172.Text = 'ng')) then begin
            edt173.Color := clLime;
            edt172.Color := clLime;
          end
          else
            edt173.Color := clred;

        end;
        if Trim(edt175.Text) = Trim(edit1.Text) then begin
          if (rbuf[4] = 0) and ((edt174.Text = 'PASS') or (edt174.Text = 'pass') or (edt174.Text = 'OK') or (edt174.Text = 'ok')) then begin
            edt175.Color := clLime;
            edt174.Color := clLime;
          end
          else if (rbuf[4] > 0) and ((edt174.Text = 'FAIL') or (edt174.Text = 'fail') or (edt174.Text = 'NG') or (edt174.Text = 'ng')) then begin
            edt175.Color := clLime;
            edt174.Color := clLime;
          end
          else
            edt175.Color := clred;

        end;
        if (Trim(edt171.Text) <> Trim(edit1.Text)) and (Trim(edt173.Text) <> Trim(edit1.Text)) and (Trim(edt175.Text) <> Trim(edit1.Text)) then begin
          lbl193.Caption := '非标准板条码：';
          lbl193.Color := clred;
          edt176.Text := Trim(edit1.Text);
        end;
        //------------数据上传成功，给MCU确认信息//////////--------------------------------------------------------------

        Sleep(40);             //延时，等待通信链接命令确认完成！
        if (Comm1.BaudRate = 9600) and (not SpeedButton12.Enabled) then        //FCT5增加100mS延时；
          Sleep(100);
        begin

          sbuf[2] := 5;
          sbuf[3] := $50;
          sbuf[4] := $00;
          sbuf[5] := $FF;
          sbuf[6] := $00;
          send_len := 6;
          try
            send_crcdata(send_len, 2);
          except
            messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);
          end;
        end;
        SaveLen := 0;    //保存数据个数清零
        UnlockClearBarcode();       //解锁条码并清空
        mmo19.Lines.Add('8.标准板测试');
        LV1MemoDisp(SaveLen);
      //  Saveflag:=0;
        exit;
      end;
                            //有收到数据，新文件导入标志清除
      NewLogFileFlag := 0;       //测量结果预处理，是否用下位机机种生产数据文件前先清除标志；

      /////////////-----------文件夹名字来源,用edt77.Text和save_file_name表示----------------------------------------
      if (edt77.Text <> '') and (edt77.Text <> '-') then begin         //不能 空或者-文件夹。
        if Length(edt77.text) < 2 then begin               //SaveLen:=Saveflag; Saveflag:=0;   //数据长度替换，避免多线程造成重复进入！
          case Application.MessageBox('请确认测试程序第一步骤是否使用AZV0正确设置P/N，继续保存log数据吗？', 'P/N warning! 料号长度异常！', MB_YESNO + MB_ICONWARNING + MB_DEFBUTTON2) of
            IDYES:
              begin
                save_file_name := edt77.Text;
              end;
            IDNO:
              begin
                edt77.Color := clRed;
                edit1.Enabled := True;
                Exit;
              end;
          end;
        end
        else
          save_file_name := edt77.Text;
      end
      else begin            //非tesla格式，文件夹名取自主板名称,MFLEX不生产文件夹，见下面代码！
        save_file_name := '';
        if (Char(rbuf[6]) >= '0') and (Char(rbuf[6]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[6]);
        if (Char(rbuf[5]) >= '0') and (Char(rbuf[5]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[5]);
        if (Char(rbuf[8]) >= '0') and (Char(rbuf[8]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[8]);
        if (Char(rbuf[7]) >= '0') and (Char(rbuf[7]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[7]);
        if (Char(rbuf[10]) >= '0') and (Char(rbuf[10]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[10]);
        if (Char(rbuf[9]) >= '0') and (Char(rbuf[9]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[9]);

        if (edt77.Text = '') or (edt77.Text = '-') then        //不能为空或者-文件夹。
          edt77.Text := save_file_name;
      end;

      //--不知何故会在此处循环警告，用edt77.visible内容控制不循环---------------------
      if edt77.Visible and ((edt77.Text = '') or (edt77.Text = '')) then begin   //PN料号不能为空或者-，否提示
        edt77.Visible := False;
        case Application.MessageBox('Invalid P/N,no logfile,continue?;料号无效，不能生成测试Log文件，继续吗？', '警告', MB_OKCANCEL + MB_ICONINFORMATION) of
          IDOK:
            begin
              edt77.Visible := true;
            end;
          IDCANCEL:
            begin
              form_main.Close;
            end;
        end;
      end;
      /////////////-----------文件夹生成，比文件生成慢？----------------------------------------
    //  if(Length(edt77.Text)>6)and(Readchis('Model Para', 'FixPNSel', '')='2')  then    //淘汰！文件名足够长前双向同步，不用前缀
    //     lbl114.Caption:='';
      if (save_file_name <> '') and (lbl90.hint <> (lbl114.Caption + save_file_name)) then    //机种名不同，产生新文件
      begin
        save_file_name := lbl114.Caption + save_file_name;    //文件名命名（加上前缀）
        lbl90.hint := save_file_name;     //机种名称，不同于label10(文件名)同时作为子文件夹名 ，可限制重复生成文件。

        if (StrToInt(edt62.Text) >= 10) then begin   //childfloder>=10: mflex数据格式，保存本地，由服务器来读取,不生成子文件夹

        end
        else if (StrToInt(edt62.Text) > 1) then begin  //childfloder>0: 生成子文件夹
        //tesla基本文件格式，其他 等.....
          if not DirectoryExists(edt2.Text + save_file_name + '\') then                            //FileExists
          try
            begin
              CreateDir(edt2.Text + save_file_name + '\');        //   创建目录
            end;
          except  //finally
            raise Exception.Create('Cannot Create ' + edt2.Text + save_file_name + '\');
          end;
          if edt157.Visible then begin
            if not DirectoryExists(edt157.Text + save_file_name + '\') then                            //FileExists
            try
              begin
                CreateDir(edt157.Text + save_file_name + '\');        //   创建目录
              end;
            except  //finally
              raise Exception.Create('Cannot Create ' + edt157.Text + save_file_name + '\');
            end;
          end;
        end;
        Sleep(10);      //2018-12-01由100改成20
      /////////////-----以上处理文件夹生成------以下处理 文件生成与写数据时的文件生成冲突？？----------------------------------------
        NewLogFileFlag := 1;
        TestNum := 0;     //标志应该新生成文件，计数清空
      end;

      //---------以上处理完文【件夹命】名问题^^^^--------------------VVVV文件命名在数据保存函数中--------------------------------------------------------

      //SaveLen:=Saveflag; Saveflag:=0;   //数据长度替换
      if SaveLen < 2 then begin           //去掉数据包头后的：字个数
        case Application.MessageBox('接收到的数据异常（少于2个字）！', '警告', MB_OKCANCEL + MB_ICONINFORMATION) of
          IDOK:
            begin
              //   form_main.Close;
            end;
          IDCANCEL:
            begin
              //   form_main.edt127.Visible:=true;
            end;
        end;
      end;
      Label7.Caption := 'BarCode';    //PC收到上传数据则更新标志，与UDP通信对应！
      /////////结果显示，记录更新，良品统计----------------------------------------------------------------------------
      TestNum := TestNum + 1;      //序号
      if TestNum > 999999 then
        TestNum := 1;

      lbl191.Caption := '-';
      lbl193.Caption := '-';
      edt176.Text := '-';
      if grp45.Visible and ((edt171.Color <> clLime) or (edt173.Color <> clLime) or (edt175.Color <> clLime)) then begin    //早期标准板：标准板数据要记录；有一个标准板没有验证
        Label11.Font.Color := clWindowText;
        Label11.Caption := '标准板验证...';
        if (rbuf[4] = 0) then
          lbl191.Caption := '当前结果：PASS'
        else
          lbl191.Caption := '当前结果：FAIL';
        lbl193.Caption := '当前条码：';
        edt176.Text := Trim(edit1.Text);
        if Trim(edt171.Text) = Trim(edit1.Text) then begin
          if (rbuf[4] = 0) and ((edt170.Text = 'PASS') or (edt170.Text = 'pass') or (edt170.Text = 'OK') or (edt170.Text = 'ok')) then begin
            edt171.Color := clLime;
          end
          else if (rbuf[4] > 0) and ((edt170.Text = 'FAIL') or (edt170.Text = 'fail') or (edt170.Text = 'NG') or (edt170.Text = 'ng')) then begin
            edt171.Color := clLime;
          end
          else
            edt171.Color := clred;
        end
        else if Trim(edt173.Text) = Trim(edit1.Text) then begin
          if (rbuf[4] = 0) and ((edt172.Text = 'PASS') or (edt172.Text = 'pass') or (edt172.Text = 'OK') or (edt172.Text = 'ok')) then begin
            edt173.Color := clLime;
          end
          else if (rbuf[4] > 0) and ((edt172.Text = 'FAIL') or (edt172.Text = 'fail') or (edt172.Text = 'NG') or (edt172.Text = 'ng')) then begin
            edt173.Color := clLime;
          end
          else
            edt173.Color := clred;

        end
        else if Trim(edt175.Text) = Trim(edit1.Text) then begin
          if (rbuf[4] = 0) and ((edt174.Text = 'PASS') or (edt174.Text = 'pass') or (edt174.Text = 'OK') or (edt174.Text = 'ok')) then begin
            edt175.Color := clLime;
          end
          else if (rbuf[4] > 0) and ((edt174.Text = 'FAIL') or (edt174.Text = 'fail') or (edt174.Text = 'NG') or (edt174.Text = 'ng')) then begin
            edt175.Color := clLime;
          end
          else
            edt175.Color := clred;

        end
        else begin
          lbl193.Caption := '非标准板条码：';
          lbl193.Color := clred;
          edt176.Text := Trim(edit1.Text);
        end;

      end
      else begin

        begin
          if (rbuf[4] = 0)              //结果OK
            then begin
            Label11.Font.Color := clLime;
            if (ts7.Tag = 0) or (StrToInt(edt59.Text) * strtoint(edt60.Text) = 1) or (edt74.Text = '10') then
              PassCount := PassCount + 1;
            Label11.Caption := 'PASS';
          end
          else begin
            Label11.Font.Color := clRed;
            if (ts7.Tag = 0) or (StrToInt(edt59.Text) * strtoint(edt60.Text) = 1) or (edt74.Text = '10') then
              NGcount := NGcount + 1;
            Label11.Caption := 'FAIL'
          end;
        end;

        PassOverT := StrToInt('0' + edt40.Text);    //超时显示待机
        NGOverT := StrToInt('0' + edt42.Text);

        if (ts7.Tag = 0){not ts7.Showing} or (StrToInt(edt59.Text) * strtoint(edt60.Text) = 1) or (edt74.Text = '10') then     //////只有单PCS测试，才此处良品与不良品统计 。否则在联板测试良品与不良品统计
        begin
          edt4.Text := inttostr(PassCount + NGcount);
          edt63.Text := inttostr(PassCount);
          edt64.Text := inttostr(NGCount);
          if (PassCount + NGcount) > 0 then begin
            edt65.Text := floattostrF((PassCount * 100) / (PassCount + NGcount), ffFixed, 5, 2); //inttostr((PassCount*100) div (PassCount+NGcount));
            edt66.Text := floattostrF((NGCount * 100) / (PassCount + NGcount), ffFixed, 5, 2); //inttostr((NGCount*100) div (PassCount+NGcount) );
          end
          else begin
            edt65.Text := '0';
            edt66.Text := '0';
          end;
        end;

      end;

      //------------------正常 数据上传及保存VVVVVVVVVVVVVVV-----------------------------------------------------------------------
      if StrToInt(edt38.text) mod 10 > 0 then begin                               //uplocal 且
        edt77.Color := clWindow;
        if Pos('?', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('*', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos(':', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('"', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('<', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('>', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('\', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('/', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('|', edt77.Text) > 0 then
          edt77.Color := clred;

        if ((StrToInt(edt38.text) div 10 = 1) and (rbuf[4] > 0))                   //仅pass保存文件
          or ((StrToInt(edt38.text) div 10 = 2) and (rbuf[4] = 0)) then begin        //仅FAIL保存文件
          form_main.label10.caption := 'test fail';
          ;                                                                //不保存
        end
        else begin
          SaveCsvData(SaveLen, NewLogFileFlag)       //保存为CSV文件
        end;
      end
      else begin
        Label11.Font.Size := 80;
        Lbl169.Caption := 'pls set Uplocal=1';
        lbl169.Font.Size := 60;
        LV1MemoDisp(SaveLen);                    //没有启用保存log文件时的显示！
      end;

      if (Readchis('Comm Para', 'DoubleBarcodeStart') = '1') and tabsheet2.TabVisible and (Label7.hint = '11') then     //双条码，显示结果指示界面
        ts7.Show;

      mmo19.Lines.Add('6.local finish');
      if StrToInt(edt75.Text) > 0 then          //打印 //结果打印
      begin
        Tmatrix.print(SaveLen);
        if (edt62.Text <> '25') then          //非 jaguar项目
          ts8.Show;
        edt75.tag := StrToInt(edt75.Text) div 1000;  //千位位选择打印
        if (edt75.tag = 2) or ((rbuf[4] = 0) and (edt75.tag = 0))       //ok打印
          or ((rbuf[4] = 1) and (edt75.tag = 1))       //ng打印
          then
          PrintClickProcess(0);
      end;

      datetimetostring(sst, 'yyyymmddhhnnss', Now);    //生成数据的时间，上传服务器用。

      /////////数据上传成功，给MCU确认信息//////////--------------------------------------------------------------
      begin
        Sleep(40);             //延时，等待通信链接命令确认完成！
        if (Comm1.BaudRate = 9600) and (not SpeedButton12.Enabled) then        //FCT5增加100mS延时；
          Sleep(100);

        begin

          sbuf[2] := 5;
          sbuf[3] := $50;
          sbuf[4] := $00;
          sbuf[5] := $FF;
          sbuf[6] := $00;
          send_len := 6;
          try
            send_crcdata(send_len, 2);
          except
            messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);
          end;
        end;
      end;

      SaveLen := 0;    //保存数据个数清零

      UnlockClearBarcode();       //解锁条码并清空
      mmo19.Lines.Add('8.barcode enable');

      if form_main.groupbox1.hint <> '5' then begin         //FCT5不支持加载和读取长文件名

        if (inportNewFileflag mod 1000) > 0 then begin     //收到一条数据后:提示重新导入文件表头!（提取更新表头会造成前一个数据文件头异常！！）；安捷利防止刷新原来的文件头格式
          FreshDatalogFromMemo(5);                     //立即更新文件头，减少重复loading..时间！；
          inportNewFileflag := 1000;    //加载中标志... ：允许重新更新文件头
        end;
      end
      else begin
        inportNewFileflag := 0;    //其它：允许重新更新文件头
      end;
    end;

  end;
end;

//////////////////////////////启动，生成一个新的带日期.CSV文件
//改成文件不存在就生成！
//<--- 输入 tt=0：开机生成文件， 参数显示在memo里；
//          tt=1:非开机生成文件  ,没有处理
//          tt=3:  备份文件生成  ,没有启用
//          tt=9000:  生成第2 log文件，等同与 =5文件；

//输入 barcode:取第几个条码框的条码 ,=9999则不生成条码文件
procedure CreatAnewCSV(tt: integer; barcode: SmallInt);
var
  nrf_dat_config: Tinifile;
  ini_path: string;
  s, SLINE_head, SLINE_head2: string;
  i, temp: integer;
  f, file2: TextFile;
  save_file_name, sst, ssdatetime: string;

  procedure createSnFile();        //车灯机箱专用
  var
    i, j: Integer;
  begin
    with form_main do begin
      if edt62.Text = '2' then begin
        if not DirectoryExists(edt135.Text + '\' + edt77.Text + '\') then
        try
          begin
            CreateDir(edt135.Text + '\' + edt77.Text + '\');        //   创建目录
          end;
        except
          raise Exception.Create('Cannot Create ' + edt135.Text + '\' + edt77.Text + '\');
        end;
      end;
//      if not fileexists(edt2.Text + save_file_name) and (barcode<9999) then    //始终为true?------barcode允许新生成文件
//      begin
//        try
//            if(tt=9000)  then
//              assignfile(f, edt157.Text + save_file_name)
//            else
//              assignfile(f, edt2.Text + save_file_name);
//            rewrite(f);Sleep(20);// else  Append(f);             //application.ProcessMessages;
      if (readchis('result', 'PrintRow') <> '') or (readchis('result', 'PrintMaxRows') <> '') then begin     //安捷利定义
        if rbuf[4] = 0 then
          SLINE_head := 'PASS'
        else
          SLINE_head := 'FAIL';
        if (readchis('Model Para', 'MachineNum') <> '') then
          SLINE_head := SLINE_head + ',' + readchis('Model Para', 'MachineNum')
        else
          SLINE_head := SLINE_head + ',ICT0' + inttostr(rbuf[11]);   //机台
        SLINE_head := SLINE_head + ',' + inttostr(TestNum);         //序号
        SLINE_head := SLINE_head + ',' + Lbl114.Caption + edt77.Text;  //机种名->程序名称
        SLINE_head := SLINE_head + ',' + trim(edit1.Text);      //条码
        SLINE_head := SLINE_head + ',' + Copy(ssdatetime, 0, 8); //日期
        SLINE_head := SLINE_head + ',' + Copy(ssdatetime, 9, 6); //时间
        if rbuf[4] = 0 then
          SLINE_head := SLINE_head + ',' + 'P'
        else
          SLINE_head := SLINE_head + ',' + 'F';       //结果
        SLINE_head := SLINE_head + ',' + '0';         //是否重测
        SLINE_head := SLINE_head + ',' + '1';         //工作表格
        SLINE_head := SLINE_head + ',' + edt79.Text;  //<11>人员编号

        SLINE_head := SLINE_head + ',' + 'PPPPPNPNNNNN';    //详细结果
        SLINE_head := SLINE_head + ',' + '0';         //多联板

        if rbuf[4] = 0 then
          SLINE_head := SLINE_head + ',' + 'P'        //多联板结果
        else
          SLINE_head := SLINE_head + ',' + 'F';       //结果
        SLINE_head := SLINE_head + ',' + '0';         //多板重测

              //  writeln(f, SLINE_head);             //原始csv文件只写一行头数据
              //  writeln(f, '');
        edt135.hint := SLINE_head;
        edt135.tag := 2;
      end
      else begin
        if rbuf[4] = 0 then
          SLINE_head := trim(edit1.Text) + ',PASS,FCT'
        else
          SLINE_head := trim(edit1.Text) + ',FAIL,FCT';

        if (readchis('Model Para', 'MachineNum') <> '') then
          SLINE_head := SLINE_head + ',' + readchis('Model Para', 'MachineNum')
        else
          SLINE_head := SLINE_head + ',CT-' + inttostr(rbuf[11]);   //机台
        SLINE_head := SLINE_head + ',' + edt77.Text;
        SLINE_head := SLINE_head + ',' + edt78.Text;   //线别          //SLINE_head :=SLINE_head+',NO.' +inttostr(rbuf[11]);    //机台
        SLINE_head := SLINE_head + ',' + edt79.Text;
        SLINE_head := SLINE_head + ',' + edt80.Text;   //线别
        SLINE_head := SLINE_head + ',' + ssdatetime;
              //  writeln(f, SLINE_head);             //原始csv文件只写一行头数据
        edt135.hint := SLINE_head;
        edt135.tag := 1;
      end;
//        finally
//          Closefile(f);
//        end;
//      end;
    end;
  end;

  procedure createNormalFile();
  var
    i, j: Integer;
  begin
    with form_main do begin
      try
        assignfile(f, {ExtractFilePath(application.ExeName)}edt2.Text + save_file_name);
        rewrite(f);
        Sleep(5);              //application.ProcessMessages;
        begin        //通用 字符串生成，名称一行，单位一行，上限下限各一行
          SLINE_head := '';
          SLINE_head2 := '';
          s := 'SerialNumber';
          s := s + ',';
          SLINE_head := SLINE_head + s;
          SLINE_head2 := SLINE_head2 + ',';
          if Readchis('result', 'BarcodeInfo') <> '' then begin    //连接日置仪器，增加条码信息
            SLINE_head := '二维码信息,二维码位数,';
            SLINE_head := SLINE_head + '二维码等级,';    //等级

            SLINE_head2 := SLINE_head2 + ',';
            SLINE_head2 := SLINE_head2 + ',';
          end;

          s := 'TestTime';
          s := s + ',';
          SLINE_head := SLINE_head + s;
          SLINE_head2 := SLINE_head2 + ',';
          s := 'TestResult';       //3
          s := s + ',';
          SLINE_head := SLINE_head + s;
          SLINE_head2 := SLINE_head2 + ',';

          if LogType <= 10 then begin
            s := 'Order';      //4 <--TestType
            s := s + ',';
            SLINE_head := SLINE_head + s;
            SLINE_head2 := SLINE_head2 + ',';

            s := 'PartNumber';     //5
            s := s + ',';
            SLINE_head := SLINE_head + s;
            SLINE_head2 := SLINE_head2 + ',';

            s := 'Line'; //6 datetostr(Date);
            s := s + ',';
            SLINE_head := SLINE_head + s;

            s := 'IP_Address'; //7 TimeToStr(Time);     //字符重重新
            s := s + ',';
            SLINE_head := SLINE_head + s;
          end;
          s := 'Machine'; //8  form_main.edt35.Text;    //EMP
          s := s + ',';
          SLINE_head := SLINE_head + s;

          if LogType = 12 then begin
            SLINE_head := SLINE_head + lbl90.Caption + ',';
            SLINE_head := SLINE_head + lbl91.Caption + ',';
            SLINE_head := SLINE_head + lbl92.Caption + ',';
            SLINE_head := SLINE_head + lbl93.Caption + ',';
            SLINE_head := SLINE_head + 'IP_Address' + ',';
          end
          else begin
            s := 'Operator'; //9 form_main.edt36.Text;    //LOT
            s := s + ',';
            SLINE_head := SLINE_head + s;
          end;
          //-------------------可选参数---------------------------------------------

          for i := 1 to UpStepMax do begin
            if NameStrZu[i] <> '' then
              SLINE_head := SLINE_head + NameStrZu[i] + ',';     //不为空
            if UnitStrZu[i] <> '' then
              SLINE_head2 := SLINE_head2 + UnitStrZu[i] + ',';    //不为空
          end;
          
          {由接收中断刷新！<-开机生成淘汰！  if tt=0 then        //显示，第一次显示或者一个条码一个文件 才会更新 ,如果数据上传产生文件则不更新名称等...
            begin               //收到数据生成文件时（可能换了机种，可以更新，也会重新读取主板信息） 不更新
              mmo2.Lines[0]:=SLINE_head;//Copy(SLINE_head,0,1024);//SLINE_head;//名称 .Add(SLINE_head) ;
              mmo2.Lines[2]:=Copy(SLINE_head2,0,1024);//SLINE_head2;//单位 .Add(SLINE_head2) ;
              mmo2String[0]:=SLINE_head;        //名称
              mmo2String[2]:=SLINE_head2;       //单位
            end;  }

          if (StrToInt(edt62.Text) < 11) then begin                            //条码文件都大于10；
            writeln(f, SLINE_head);             //原始csv文件只写一行头数据  ，余下4行不写

            if LogType >= 11 then    //通用CSV=文件，再写4行
            begin
              writeln(f, SLINE_head2);
              writeln(f, ' ');      //上限
              writeln(f, ' ');      //下限
              writeln(f, ' ');      //短路群
            end;
          end
          else begin                         //mflex专用 (因一个条码一个文件)，更新名称
            for i := 1 to StrToInt(edt81.Text) do begin
              SLINE_head := SLINE_head + Condutor[i] + '12-44,';
            end;
            mmo2String[0] := SLINE_head;        //名称
            mmo2String[2] := SLINE_head2;       //单位
          end;
        end;
      finally
        Closefile(f);
      end;
    end;
  end;

begin             //-----------------生成CSV总的入口----------------------------

  datetimetostring(ssdatetime, 'yyyymmddhhnnss', Now);
  with form_main do begin
    if (label10.tag < 24) then
      sst := Copy(ssdatetime, 0, 8)    //一天一个文件
    else
      sst := ssdatetime;
  //----------------集总主文件名 生成--------(只有MFLEX条码作为主文件)-----------------------------
    if (StrToInt(edt62.Text) >= 1) then begin    //通用：文件夹使能 ,文件名 不能含有以下9种字符：?*:"<>\/|

      if (tt = 9000) then begin        //车灯机箱专用 文件
        if edt77.Hint = 'Z10' then begin     //淘汰！第一个步骤为Z10
          edt77.tag := edt77.tag + 1;
          if edt77.tag <= 1 then
            save_file_name := edt77.Text + '-' + DelSpecialChar(trim(edit1.Text)) + '-'
          else
            Exit;
        end
        else begin
          if edt62.Text = '2' then begin
            save_file_name := edt77.Text + '\' + DelSpecialChar(trim(edit1.Text)) + '-';
          end
          else
            save_file_name := DelSpecialChar(trim(edit1.Text)) + '-';
        end;
        if (rbuf[4] = 0) then
          save_file_name := save_file_name + 'PASS' + '-' + sst   //+'.txt'
        else
          save_file_name := save_file_name + 'FAIL' + '-' + sst;  //+'.txt';
        if (lbl177.Caption = '工位1 ：') then begin
          save_file_name := save_file_name + '-1';
        end;
        if (lbl177.Caption = '工位2 ：') then begin
          save_file_name := save_file_name + '-2';
        end;
        if (lbl177.Caption = '工位3 ：') then begin
          save_file_name := save_file_name + '-3';
        end;
        if (lbl177.Caption = '工位4 ：') then begin
          save_file_name := save_file_name + '-4';
        end;
        if readchis('Comm Para', 'FileExtName') <> '' then
          save_file_name := save_file_name + '.' + readchis('Comm Para', 'FileExtName')
        else
          save_file_name := save_file_name + '.txt';
      end
      else begin                 //childfolder<10
        save_file_name := lbl90.hint + '\' + lbl90.hint + '-' + sst + '.csv';  //料号来自主板，并用于文件夹命名
      end;
    end
    else                                     //仅生产单一文件
      save_file_name := lbl90.hint + '-' + sst + '.csv';

  //-----------------------创建文件  处理-------------------------------

    if (tt = 9000) then begin   //车灯机箱专用
      lbl161.caption := save_file_name;
    //  Lbl180.Caption:=save_file_name           //获取文件名，每次生成都会更新
      createSnFile();
    end
    else begin        //通用：可生成备份文件，小于9999才可生成条码文件
      Label10.Caption := save_file_name;           //获取文件名，每次生成都会更新
      if not fileexists(edt2.Text + save_file_name) and (barcode < 9999) then begin      //通用生成文件=1 进入不执行
        begin
          createNormalFile();                //仅=11文件用到吗？有bug!
          Tnormal.FreshDatalogFromMemo(5);           //用memo内容更新文件，----行数不够
        end;
      end
      else begin
        //Append(f);
      end;

    end;
  end;
end;

{   函数：saveCsvdata(len_ss:integer)
说明:接收到的数据保存为.txt文件，可支持
1.连片输出数据的保存，仅保存到同一个文件里
2.根据条码规则自动生成所有同一规律的条码
3.区分指示灯显示
}
procedure saveCsvdata(len_ss: integer; NewLogFileFlag: byte);
var
  f: TextFile;
  save_file_name: string;
  strtemp, stime, SLINE_head, SLINE_head1, SLINE_head2, sline_sn, sntime: string;
  i, j, bb: integer;
  zuptr, dd, cboflag, zzoflag: SmallInt;
  ff: Real;

  procedure OtherDataPre();                      //数据头几个项目预处理，显示在memo框
  var
    i, j: Integer;
  begin
    with form_main do begin
      if StrToInt('0' + Label7.hint) = 2 then begin             //clearbarcode=2，条码用编号代替
        if Trim(edit1.Text) = '' then
          SLINE_head := IntToStr(TestNum) + ','
        else
          SLINE_head := Trim(edit1.Text) + ',';
      end
      else if (StrToInt(edt62.Text) >= 10) then
        SLINE_head := Trim(edit1.Text) + ','            //条形码 二维码
      else
        SLINE_head := Trim(edit1.Text) + ',';      //2017-12-20加trim

      datetimetostring(stime, 'yyyy-mm-dd HH:nn:ss', Now);
      SLINE_head := SLINE_head + stime + ',';    //时间
      if (rbuf[4] = 0)              //结果OK
        then begin                  //  sjudge:='PASS'+','+ stime+',' +Trim(edit1.Text)+',' + edt56.Text;      //实际没用吗? 条码+结果+时间+FCT类型
        SLINE_head := SLINE_head + 'PASS' + ',';
      end
      else begin              //  sjudge:='FAIL'+','+ stime+',' +Trim(edit1.Text)+',' + edt56.Text;      //实际没用吗? 条码+结果+时间+FCT类型
        SLINE_head := SLINE_head + 'FAIL' + ',';
      end;

      SLINE_head := SLINE_head + IntToStr(rbuf[11]) + ',';    // 8 机台
      if LogType = 12 then begin
        SLINE_head := SLINE_head + edt77.Text + ',';
        SLINE_head := SLINE_head + edt78.Text + ',';
        SLINE_head := SLINE_head + edt79.Text + ',';
        SLINE_head := SLINE_head + edt80.Text + ',';
        SLINE_head := SLINE_head + IdIPWatch1.LocalIP + ',';
      end
      else
        SLINE_head := SLINE_head + 'emp,';            //9 作业员

    end;
  end;

  procedure DataInMemoDisp();
  var
    i, j: Integer;
  begin
    with form_main do begin
      bb := 1;
      if rbuf[3] = $EE then     //CSV格式联板则先处理MEMO显示数据，仅显示用。
      begin
        zuptr := 1;             //表头指针，当前显示指针
        for i := bb to (len_ss div 2) do begin        //遍历所有数据,用i指示，实际显示指针为zuptr

          dd := (rbuf[10 + 4 * i] * 256 + rbuf[11 + 4 * i]);
          if ((Copy(ModeZu[i], 2, 1) = 'S') or (Copy(ModeZu[i], 2, 1) = 'O') or (Copy(ModeZu[i], 2, 1) = 'K')) and ((copy(NameStrZu[i], 0, 4) = 'OPEN') or (copy(NameStrZu[i], 0, 4) = 'SHOR') or (copy(NameStrZu[i], 0, 5) = 'SHORT')) then begin
            if dd < 1 then begin
              SLINE_head1 := SLINE_head1 + 'right' + #9;
            end
            else begin
              SLINE_head1 := SLINE_head1 + 'error' + #9;
            end;
          end
          else if (Copy(ModeZu[i], 2, 2) = 'XH') or (Copy(ModeZu[i], 2, 2) = 'YH') or (Copy(ModeZu[i], 2, 2) = 'IZ') or (Copy(ModeZu[i], 2, 2) = 'JZ') then begin      //16进制显示
            SLINE_head1 := SLINE_head1 + '0x' + inttohex(word(dd), 2) + #9;
          end
          else begin
            ff := dd / strtofloat(ScaleZu[zuptr]);
            SLINE_head1 := SLINE_head1 + copy(floattostr(ff), 0, 7) + #9;
          end;
          if (zuptr < UpStepMax) then
            zuptr := zuptr + 1;

        end;

        DispErrlist(len_ss);
      end
      else begin        //【0B00]模式，单PCS数据结果处理并显示；SLINE_head1仅用于显示和保存
        for i := bb to len_ss do begin
          dd := (rbuf[12 + 2 * i] * 256 + rbuf[13 + 2 * i]);
          if (edt48.Text = '1') and (i <= 2) then begin //barcode check=1: 核对使能
            if i = 1 then begin                   //SLINE_head:= SLINE_head+edit1.Text+',' ;
              SLINE_head1 := SLINE_head1 + edit1.Text + #9;
            end;
            if i = 2 then begin                  //SLINE_head:= SLINE_head+medit1.Text+',' ;
              SLINE_head1 := SLINE_head1 + medit1.Text + #9;
            end;
          end
          else if (copy(NameStrZu[i], 0, 4) = 'OPEN') or (copy(NameStrZu[i], 0, 4) = 'SHOR') then begin
            if dd = 0 then begin

              SLINE_head1 := SLINE_head1 + 'right' + #9;
            end
            else begin

              SLINE_head1 := SLINE_head1 + 'error' + #9;
            end;
          end
          else begin
            ff := dd / strtofloat(ScaleZu[i]);  //'100');//
            SLINE_head1 := SLINE_head1 + copy(floattostr(ff), 0, 7) + #9;
          end;
        end;
      end;
    end;
  end;

  procedure saveOnePCSresult();          //逐步转移到多PCB保存功能里！；<--yijia,早期tesla,早期weixin,早期通用
  var
    i, j: Integer;
    Teslastr: string;
  begin
    with form_main do begin
      try
        assignfile(f, edt2.Text + save_file_name);      //ExtractFilePath(application.ExeName)
        Append(f);                 //application.ProcessMessages;
        begin                        //=2(tesla),=11(通用）.....

          for i := bb to len_ss do begin
            dd := (rbuf[12 + 2 * i] * 256 + rbuf[13 + 2 * i]);
            if (edt48.Text = '1') and (i <= 2) then begin //barcode check=1: jiahe核对使能
              if i = 1 then begin
                SLINE_head := SLINE_head + edit1.Text + ',';

              end;
              if i = 2 then begin
                SLINE_head := SLINE_head + medit1.Text + ',';

              end;
            end
            else if (copy(NameStrZu[i], 0, 4) = 'OPEN') or (copy(NameStrZu[i], 0, 4) = 'SHOR') then begin
              if dd = 0 then begin
                SLINE_head := SLINE_head + 'right' + ',';
              end
              else begin
                SLINE_head := SLINE_head + 'error' + ',';
              end;
            end
            else begin
              ff := dd / strtofloat(ScaleZu[i]);
              SLINE_head := SLINE_head + copy(floattostr(ff), 0, 7) + ',';
            end;

          end;
          begin
            if ts13.TabVisible then
              SLINE_head := SLINE_head + #9 + form_main.mmo16.lines[0];
            writeln(f, SLINE_head);     //测量值+SN序列号
          end;
        end;
        if StrToInt(edt38.text) mod 10 >= 2 then       //uplocal =4： 写入多行结果  分别为NGLIST,OPEN NG,SHORT NG
        begin
          writeln(f, ',' + ',' + mmo2String[6]);        //NGLIST
          if StrToInt(edt38.text) mod 10 >= 3 then
            writeln(f, ',' + ',' + mmo2String[7]);  //OPEN NG
          if StrToInt(edt38.text) mod 10 >= 4 then
            writeln(f, ',' + ',' + mmo2String[8]);  //SHORT ng
        end;
      finally
        Closefile(f);
      end;

    end;
  end;

  procedure saveNPCSresult();
  var
    i, j: Integer;
    str1: string;
    ResultList: TStringList;

    procedure saveNormallog(j: SmallInt);
    var
      i: Integer;
    begin  //1.------先处理编号或者条码--------------------------
      with form_main do begin
        if StrToInt('0' + Label7.hint) = 2 then begin     //barcodeconfig=2:条码用编号代替
          if Trim(edit1.Text) = '' then
            SLINE_head := IntToStr(TestNum) + ','
          else
            SLINE_head := trim(edit1.Text) + ',';
        end
        else if (Label7.hint = '11') then begin      //双条码，第二个条码选择
          SLINE_head := 'nc,';
          if (j = 1) then begin                      //选第二个条码
            if (medit1.Color <> clred) and (Trim(medit1.Text) <> '') then begin
              CreatAnewCsv(1, 2);                 //save_file_name :=Label10.caption;
              SLINE_head := trim(medit1.Text) + ',';
            end;
          end
          else begin
            if (edit1.Color <> clred) and (Trim(edit1.Text) <> '') then begin
              CreatAnewCsv(1, 0);
              SLINE_head := trim(edit1.Text) + ',';
            end;
          end;
        end
        else if (StrToInt(edt59.Text) * strtoint(edt60.Text) = 1) or (edt74.Text = '10') then begin     //强制生成一条数据
          SLINE_head := trim(edit1.Text) + ',';
        end
        else begin
          if trim(edit1.Text) = 'nc' then begin            //不用区分条码
            SLINE_head := 'nc' + ',';
          end
          else begin                                 //按固定规则区分条码
            SLINE_head := ',';            //二维码 ，最后N为替换为规则字符
          end;
        end;

        if Readchis('result', 'BarcodeInfo') <> '' then begin    //连接日置仪器，增加条码信息
          SLINE_head := SLINE_head + IntToStr(Length(Trim(edit1.Text))) + ',';
          if Lbl177.Tag <= StrToInt(Readchis('result', 'BarcodeInfo')) then
            SLINE_head := SLINE_head + 'A,'     //等级
          else if Lbl177.Tag <= 2 * StrToInt(Readchis('result', 'BarcodeInfo')) then
            SLINE_head := SLINE_head + 'B,'     //等级
          else
            SLINE_head := SLINE_head + 'C,';    //等级
          stat1.Panels[2].Text := IntToStr(Lbl177.Tag) + 'mS';
        end;
        //2.------时间和结果--------------------------
        datetimetostring(stime, 'yyyy-mm-dd hh:nn:ss', Now);
        SLINE_head := SLINE_head + stime + ',';    //时间
        if (StrToInt(edt59.Text) * strtoint(edt60.Text) = 1) or (edt74.Text = '10') then begin     //强制生成一条数据
          if (rbuf[4] = 0)             //结果OK
            then begin
            SLINE_head := SLINE_head + 'PASS' + ',';
          end
          else begin
            if (btn30.hint <> '') and (readchis('Comm Para', 'OSpointPos') <> 'OFF') then
              SLINE_head := SLINE_head + 'FAIL(' + btn30.hint + '),'
            else
              SLINE_head := SLINE_head + 'FAIL' + ',';
          end;
        end
        else begin
          if Rleds[j].ActiveColor = clgreen then begin
            SLINE_head := SLINE_head + 'PASS' + ',';
          end
          else begin
            if (btn30.hint <> '') and (readchis('Comm Para', 'OSpointPos') <> 'OFF') then
              SLINE_head := SLINE_head + 'FAIL(' + btn30.hint + '),'
            else
              SLINE_head := SLINE_head + 'FAIL' + ',';
          end;
        end;
        //3.------其他项目--------------------------
        begin

          SLINE_head := SLINE_head + IntToStr(rbuf[11]) + ',';    // 8 机台
          if LogType = 12 then begin
            SLINE_head := SLINE_head + edt77.Text + ',';
            SLINE_head := SLINE_head + edt78.Text + ',';
            SLINE_head := SLINE_head + edt79.Text + ',';
            SLINE_head := SLINE_head + edt80.Text + ',';
            SLINE_head := SLINE_head + IdIPWatch1.LocalIP + ',';

          end
          else
            SLINE_head := SLINE_head + 'emp,';    //9 作业员
        end;
        //4.------数据--------------------------
        zuptr := 1;
        if (StrToInt(edt59.Text) * strtoint(edt60.Text) = 1) or (edt74.Text = '10') or (edt74.Text <> '1') then begin     //强制生成一条数据
          for i := 1 to (len_ss div 2) do      //遍历所有数据，放在一行存放
          begin

            dd := (rbuf[10 + 4 * i] * 256 + rbuf[11 + 4 * i]);   //rbuf[14]为第一个
            if ((Copy(ModeZu[i], 2, 1) = 'S') or (Copy(ModeZu[i], 2, 1) = 'O') or (Copy(ModeZu[i], 2, 1) = 'K')) and ((copy(NameStrZu[i], 0, 4) = 'OPEN') or (copy(NameStrZu[i], 0, 4) = 'SHOR') or (copy(NameStrZu[i], 0, 5) = 'SHORT')) then begin
              if dd < 1 then begin
                SLINE_head := SLINE_head + 'right' + ',';
              end
              else begin
                if (readchis('Comm Para', 'OSpointPos') <> 'OFF') then begin
                  if (Copy(ModeZu[i], 2, 1) = 'S') and (btn34.Hint <> '') then
                    SLINE_head := SLINE_head + btn34.Hint + ','
                  else if (Copy(ModeZu[i], 2, 1) = 'O') and (btn33.Hint <> '') then
                    SLINE_head := SLINE_head + btn33.Hint + ','
                  else if (Copy(ModeZu[i], 2, 1) = 'K') and (btn33.Hint <> '') then
                    SLINE_head := SLINE_head + btn33.Hint + ','
                  else
                    SLINE_head := SLINE_head + 'error' + ',';
                end
                else
                  SLINE_head := SLINE_head + 'error' + ',';
              end;
            end
            else if (Copy(ModeZu[i], 2, 2) = 'XH') or (Copy(ModeZu[i], 2, 2) = 'YH') or (Copy(ModeZu[i], 2, 2) = 'IZ') or (Copy(ModeZu[i], 2, 2) = 'JZ') then begin      //16进制显示
              SLINE_head := SLINE_head + '0x' + inttohex(word(dd), 2) + ',';
            end
            else begin
              ff := dd / strtofloat(ScaleZu[zuptr]);
              SLINE_head := SLINE_head + copy(floattostr(ff), 0, 7) + ',';
            end;
            if (zuptr < UpStepMax) then
              zuptr := zuptr + 1;
          end;
        end
        else begin                       //edt74.Text='1':分PCS保存数据
          for i := 1 to (len_ss div 2) do begin     //遍历所有数据，
          
          //  zuptr:=i;
            if (rbuf[12 + 4 * i] = j + 1) or ((j = 0) and (rbuf[12 + 4 * i] = 0)) then        //当前连片数据 指示
            begin
              dd := (rbuf[10 + 4 * i] * 256 + rbuf[11 + 4 * i]);   //rbuf[14]为第一个
              if ((Copy(ModeZu[i], 2, 1) = 'S') or (Copy(ModeZu[i], 2, 1) = 'O') or (Copy(ModeZu[i], 2, 1) = 'K')) and ((copy(NameStrZu[i], 0, 4) = 'OPEN') or (copy(NameStrZu[i], 0, 4) = 'SHOR') or (copy(NameStrZu[i], 0, 5) = 'SHORT')) then begin
                if dd < 1 then begin
                  SLINE_head := SLINE_head + 'right' + ',';
                end
                else begin
                  if (j = 0) and (readchis('Comm Para', 'OSpointPos') <> 'OFF') then begin
                    if (Copy(ModeZu[i], 2, 1) = 'S') and (btn34.Hint <> '') then
                      SLINE_head := SLINE_head + btn34.Hint + ','
                    else if (Copy(ModeZu[i], 2, 1) = 'O') and (btn33.Hint <> '') then
                      SLINE_head := SLINE_head + btn33.Hint + ','
                    else if (Copy(ModeZu[i], 2, 1) = 'K') and (btn33.Hint <> '') then
                      SLINE_head := SLINE_head + btn33.Hint + ','
                    else
                      SLINE_head := SLINE_head + 'error' + ',';
                  end
                  else
                    SLINE_head := SLINE_head + 'error' + ',';
                end;
              end
              else if (Copy(ModeZu[i], 2, 2) = 'XH') or (Copy(ModeZu[i], 2, 2) = 'YH') or (Copy(ModeZu[i], 2, 2) = 'IZ') or (Copy(ModeZu[i], 2, 2) = 'JZ') then begin      //16进制显示
                SLINE_head := SLINE_head + '0x' + inttohex(word(dd), 2) + ',';
              end
              else begin
                ff := dd / strtofloat(ScaleZu[i]); //OnePCSNamePos[zuptr]]);
                SLINE_head := SLINE_head + copy(floattostr(ff), 0, 7) + ',';
              end;
              if (zuptr < UpStepMax) then
                zuptr := zuptr + 1;        //项目连续++
            end;
          end;
        end;
        //5.------保存--------------------------
        if (Label7.hint = '11') then begin      //双条码，第二个条码选择
          if ((j = 1) and (medit1.Color <> clred) and (Trim(medit1.Text) <> '')) or ((j <> 1) and (edit1.Color <> clred) and (Trim(edit1.Text) <> '')) then begin   //条码有效才保存

            try
              assignfile(f, edt2.Text + Label10.caption);
              Append(f); //rewrite(f);                              //application.ProcessMessages;
              if ts13.TabVisible then
                SLINE_head := SLINE_head + #9 + form_main.mmo16.lines[j + 1];
              writeln(f, SLINE_head);
            finally
              Closefile(f);
            end;

          end;
        end
        else begin
          if Label10.caption <> 'OneBarcode-OneFile' then begin
            try
              assignfile(f, edt2.Text + Label10.caption);
              Append(f); //
              application.ProcessMessages;
              if ts13.TabVisible then                    //有SIM条码
                SLINE_head := SLINE_head + #9 + form_main.mmo16.lines[j + 1];
              writeln(f, SLINE_head);
            finally
              Closefile(f);
            end;
          end;
        end;
      end;
    end;

    procedure saveCarLedlog(tt: SmallInt);
    var
      i, j: Integer;
    begin
      with form_main do begin
        try
        {  if(tt=9000)  then begin
            if not FileExists(edt157.Text + lbl180.Caption) then begin
              edt157.Text:=ExtractFilePath(application.ExeName) ;
              lbl180.Caption:='temp.txt';
              assignfile(f, edt157.Text + lbl180.Caption);
              rewrite(f);
            end else
              assignfile(f, edt157.Text + lbl180.Caption)
          end else
            assignfile(f, edt2.Text + save_file_name);
          Append(f);  }

          assignfile(f, edt135.Text + lbl161.caption);
          rewrite(f);
          Sleep(5);
          writeln(f, edt135.hint);
          if (edt135.tag > 1) then
            writeln(f, '');

          if (Readchis('Server', 'MESprogram') <> '') then begin     //MES校验MCU信息
            writeln(f, btn119.hint);
            writeln(f, btn142.hint);
          end
          else if readchis('Comm Para', 'FileExtName') = '' then begin   //非安捷利定义文件格式
          //  writeln(f,'脱机测试');
          end;
          if (btn30.hint <> '') and (readchis('Comm Para', 'OSpointPos') <> 'OFF') then
            writeln(f, btn30.Hint);
        //  if (StrToInt(edt59.Text)*strtoint(edt60.Text)=1)or(edt74.Text<>'1') then begin
          for i := 1 to (len_ss div 2) do begin    //遍历所有数据， 放在多行存放

            dd := (rbuf[10 + 4 * i] * 256 + rbuf[11 + 4 * i]);   //rbuf[14]为第一个
            ff := dd / strtofloat(ScaleZu[zuptr]);
            if (Copy(ModeZu[i], 2, 2) = 'XH') or (Copy(ModeZu[i], 2, 2) = 'YH') or (Copy(ModeZu[i], 2, 2) = 'IZ') or (Copy(ModeZu[i], 2, 2) = 'JZ') then begin                    //专用文件格式
              strtemp := inttostr(zuptr) + ',' + NameStrZu[zuptr] + ',-'//+inttohex(StrToInt(StdValZu[i]),2)+ UnitStrZu[zuptr]     //序号+名称+规格值带单位
                + ',0x' + inttohex(StrToInt(UpperZu[zuptr]), 2) + UnitStrZu[zuptr] + ',0x' + inttohex(StrToInt(LowerZu[zuptr]), 2) + UnitStrZu[zuptr]   //上限带单位+下限带单位
                + ',0x' + inttohex(dd, 2) + UnitStrZu[zuptr];
            end
            else begin
              strtemp := inttostr(zuptr) + ',' + NameStrZu[zuptr] + ',-'//+StdValZu[i]+ UnitStrZu[zuptr]  //序号+名称+规格值带单位
                + ',' + UpperZu[zuptr] + UnitStrZu[zuptr] + ',' + LowerZu[zuptr] + UnitStrZu[zuptr]  //上限带单位+下限带单位
;
              if ((Copy(ModeZu[i], 2, 1) = 'S') or (Copy(ModeZu[i], 2, 1) = 'O') or (Copy(ModeZu[i], 2, 1) = 'K')) then begin
                if ((copy(NameStrZu[i], 0, 4) = 'OPEN') or (copy(NameStrZu[i], 0, 4) = 'SHOR') or (copy(NameStrZu[i], 0, 5) = 'SHORT')) then begin
                  if dd < 1 then begin
                    strtemp := strtemp + ',ok';
                  end
                  else begin
                    if (j = 0) and (readchis('Comm Para', 'OSpointPos') <> 'OFF') then begin
                      if (Copy(ModeZu[i], 2, 1) = 'S') and (btn34.Hint <> '') then
                        strtemp := strtemp + ',ng' + btn34.Hint
                      else if (Copy(ModeZu[i], 2, 1) = 'O') and (btn33.Hint <> '') then
                        strtemp := strtemp + ',ng' + btn33.Hint
                      else if (Copy(ModeZu[i], 2, 1) = 'K') and (btn33.Hint <> '') then
                        strtemp := strtemp + ',ng' + btn33.Hint
                      else
                        strtemp := strtemp + ',ng';
                    end
                    else
                      strtemp := strtemp + ',ng';
                  end;
                end
                else begin
                  strtemp := strtemp + ',' + copy(floattostr(ff), 0, 7);                //不带单位
                end;
              end
              else begin
                strtemp := strtemp + ',' + copy(floattostr(ff), 0, 7) + UnitStrZu[zuptr];                //测量值带单位
              end;
            end;

            if (Stat1.tag = 9000) and (readchis('Comm Para', 'CSV5raw') = '') then begin        //使用新的数据格式（仅ASCII字符）！
              if (rbuf[13 + 4 * i] and $01) = $01 then begin         //NG
                if ((copy(NameStrZu[zuptr], 0, 4)) = 'OPEN') then begin           //UpperCase
                  strtemp := strtemp + ',ng,--';
                end
                else if ((copy(NameStrZu[zuptr], 0, 4)) = 'SHOR') or ((copy(NameStrZu[zuptr], 0, 5)) = 'SHORT') then begin    //UpperCase
                  strtemp := strtemp + ',ng,--';
                end
                else if (UpperZu[zuptr] <> '') and (LowerZu[zuptr] <> '') and (UpperZu[zuptr] <> '30009') and (LowerZu[zuptr] <> '30009') then begin
                  if dd >= StrToint(UpperZu[zuptr]) then
                    strtemp := strtemp + ',ng,--'
                  else if dd <= StrToint(LowerZu[zuptr]) then
                    strtemp := strtemp + ',ng,--'
                  else
                    strtemp := strtemp + ',ng,--';
                end
                else
                  strtemp := strtemp + ',ng,--';
              end
              else
                strtemp := strtemp + ',ok,--';
            end
            else begin
              if (rbuf[13 + 4 * i] and $01) = $01 then begin         //NG
                if (copy(NameStrZu[zuptr], 0, 4) = 'OPEN') or (copy(NameStrZu[zuptr], 0, 4) = 'SHOR') or (copy(NameStrZu[zuptr], 0, 5) = 'SHORT') then begin
                  strtemp := strtemp + ',NG,--';
                end
                else if (UpperZu[zuptr] <> '') and (LowerZu[zuptr] <> '') and (UpperZu[zuptr] <> '30009') and (LowerZu[zuptr] <> '30009') then begin
                  if dd >= StrToint(UpperZu[zuptr]) then
                    strtemp := strtemp + '↑,NG,--'
                  else if dd <= StrToint(LowerZu[zuptr]) then
                    strtemp := strtemp + '↓,NG,--'
                  else
                    strtemp := strtemp + ',NG,--';
                end
                else
                  strtemp := strtemp + ',NG,--';
              end
              else
                strtemp := strtemp + ',PASS,--';
            end;
            if (zuptr < UpStepMax) then
              zuptr := zuptr + 1;
            writeln(f, strtemp);
          end;

          writeln(f, '--');              //标志下面数据是扩展原始数据

          if (mmo2String[10] <> '') and (Readchis('Result', 'saveRawData') = '1') then begin
            writeln(f, mmo2String[10]);
            mmo2String[10] := '';
          end;
        finally
          Closefile(f);
        end;

        if (edt53.hint = '9000') and (Stat1.tag = 9000) then begin    // 2BU0上传数据,用UDP接口转发

          idpclnt1.Active := true;
          idpclnt1.tag := 0;             //  idpsrvr1.Active:=True;
          ResultList := TStringList.Create;
          try
          //  if(tt=9000)  then
          //    ResultList.LoadFromFile(edt157.Text + lbl180.Caption)
          //  else
          //    ResultList.LoadFromFile(edt2.Text + save_file_name);
            ResultList.LoadFromFile(edt135.Text + lbl161.caption);
          finally
          end;
          if edt53.tag = 9002 then begin             //可靠发送：等待返回信号
            idpclnt1.Send(Edt100.Text, 9002, ResultList.Text);
            for i := 0 to idpsrvr1.Tag do begin
              Sleep(100);
              application.ProcessMessages;
              if idpclnt1.tag > 0 then
                Break;
            end;
            ShowMessage('secondary development error!二次开发软件没有响应！');
          end
          else
            idpclnt1.Send(Edt100.Text, 9001, ResultList.Text);    //默认：不可靠发送：udp发送
        end;

        if (StrToInt(edt6.Text) > 0) then begin         //启用映射盘，必须放在  UDP之后
          lbl181.Caption := '5';
          btn2Click(Form_main);
        end;
      end;
    end;

  begin
    with form_main do begin
      for j := 0 to StrToInt(edt59.Text) * strtoint(edt60.Text) - 1 do      //搜索有输出结果的连片序号；如果只有1连片则单独处理；
      begin
        if (Readchis('Comm Para', 'DoubleBarcodeStart') = '1') and (Label7.hint = '11') then begin       // 双条码有任一个无效，则不保存并显示红灯
          if (medit1.Color = clred) or (medit1.Color = clred) then begin  //有一个条码不符合规则
            Rleds[0].ActiveColor := clRed;
            Rleds[1].ActiveColor := clRed;
              // iLedRound1.Active:=True;
            iLedRound1.ActiveColor := clRed;
              // iLedRound2.Active:=True;
            iLedRound2.ActiveColor := clRed;
            iLabel1.Font.Color := clRed;
            iLabel1.Caption := 'FAIL';
            iLabel2.Font.Color := clRed;
            iLabel2.Caption := 'FAIL';
            break;
          end;
        end;
        if j > 0 then
          TestNum := TestNum + 1;        //序号依次加一
        if Rleds[j].Active = TRUE then begin
          zuptr := 1;
          begin       //-------------多连片 通用格式!保存 ，条码规律生成----------------------
            saveNormallog(j);

            if (edt53.hint = '9000') and (Stat1.tag = 9000) then begin
              CreatAnewCsv(Stat1.tag, 0);              //每次都生成条码命名文件
              if Readchis('Comm Para', 'ExeCommand') <> '' then begin    //python频繁开启：必须等待开启完成
                idpclnt1.Tag := 0;
                btn5Click(form_main);
                for i := 0 to idpsrvr1.Tag do begin      //默认50次，共计5秒
                  Sleep(100);
                  application.ProcessMessages;
                  if idpclnt1.tag > 0 then begin        //开启之后再发送，保证同步
                    zuptr := 1;
                    saveCarLedlog(Stat1.tag);
                    Break;
                  end;
                end;
              end
              else begin                       //其它开发语言一直待机；
                zuptr := 1;
                saveCarLedlog(Stat1.tag);
              end;
            end
            else if Readchis('Comm Para', 'BarcodeFile') <> '' then begin
              CreatAnewCsv(9000, 0);              //每次都生成条码命名文件
              zuptr := 1;
              saveCarLedlog(9000);
            end;
          end;

          if (edt74.Text <> '1') or (edt74.Text = '10') then begin     //退出连片循环，强制生成一条数据
            break;
          end;
        end;

      end;
    end;
  end;

begin                  //-------------保存CSV文件入口----------------
  with form_main do begin
  //-----------1.------预处理，软件开启后第一次收到log，可生成集总文件！--------------------------------------------------------
    //------------生成文件必须放在一起处理，否则会执行多次------------------------------S
    if (label10.Tag < 24)      //onedayonefile<24:一天一个文件(用日期命名的文件名不存在才会重新生成),
      then begin
      CreatAnewCsv(1, 0);              //每次都生成条码命名文件，调用文件生成函数      xx mflex专用，
    end
    else if NewLogFileFlag = 1 then begin                                        //应该生成新Log文件（不管文件名是否存在）//文件夹生成或者软件开启标志
      if (StrToInt(edt62.Text) < 10) and (label10.Tag >= 24) then        //集总文件：childfolde<10:不是Mflex程序,则生成log文件,onedayonefile>=24非一天一个文件
      begin
        if StrToInt(edt38.text) mod 10 > 0 then begin    //uplocal
          CreatAnewCsv(1, 0)
        end;
      end
      else begin
        Label10.Caption := save_file_name;               //预留，不可能执行；
      end;
    end;

    //-----如果不存在文件，第2次可生成文件！----------------------------------------------
    if (Label10.caption <> 'Label10') then     //有文件名存在：从FCT主板获取到机种名
    begin
      save_file_name := Label10.caption;     //默认文件名处理完成

      OtherDataPre();                     //其他 ，通用数据的前几项处理

      mmo2.Lines[1] := '测试结果：' + Copy(SLINE_head, 0, 1024);
//-^^^^^^^^^^^^^^^^^^^----以上pre预处理客户定义 开头几项参数，---------在界面显示一行-----------------------------------------

//--------2.---------------以下处理结果数据部分,仅显示用---------------------------------------------------------------
      SLINE_head1 := 'result-' + #9;                  //用于在MEMO1里显示

      DataInMemoDisp();                           //要保存的数据，SLINE_head1仅先显示一下
      mmo2.Lines[3] := Copy(SLINE_head1, 0, 1024);   //结果 显示，TAB分隔
      mmo2.Lines[1] := mmo2.Lines[1];               //使能 显示区域在开始位置
  //------------------------以上处理memo显示----------------------------------------------

  //----------3.--------------以下开始处理连片数据，以及数据保存---及单PCS保存用的数据（后面可以直接保存不用二次处理）其中多连片的开头几项重新开始------------------------------------------
        //mmo2.Lines[3]:= '-';      //空的话，不执行。必须有个空格
      if (Label10.caption <> 'Label10') and isfileinuse(edt2.Text + Label10.caption) then begin   //存在文件且被占用
        Application.MessageBox('LogFile in use！Log文件被占用，不能写入数据！', '警告', MB_OK + MB_ICONWARNING);
      end
      else begin
        if rbuf[3] <> $EE then      //非 联板保存，
        begin            //单PCS测试数据，前面已经处理完成！ 注：连片测试单步会导致接收数据错误，从而进到这里！
          saveOnePCSresult();
        end
        else begin             //rbuf[3]=$EE 连片测试，每PCS都要保存
          saveNPCSresult();
        end;
      end;
        ////--------------------------第二路径，第3路径文件保存-----------------------
    end;
  end;
end;

end.

