object FormCreateClampDiode: TFormCreateClampDiode
  Left = 251
  Top = 210
  Width = 1059
  Height = 476
  Caption = 'CreateClampDiodestep '#33258#21160#29983#25104#20108#32423#20307#27493#39588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grp1: TGroupBox
    Left = 24
    Top = 24
    Width = 545
    Height = 65
    Caption = 'Insert End '#23398#20064#21040#30340#27493#39588#33258#21160#25554#20837#21040#26368#21518
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object chk6: TsCheckBox
      Left = 297
      Top = 25
      Width = 77
      Height = 24
      Caption = 'VCC'#28857'  '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk7: TsCheckBox
      Left = 12
      Top = 25
      Width = 84
      Height = 24
      Caption = 'GND'#28857'   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt7: TsDecimalSpinEdit
      Left = 408
      Top = 21
      Width = 89
      Height = 28
      TabOrder = 2
      Text = '0'
      OnKeyDown = edt7KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt6: TsDecimalSpinEdit
      Left = 130
      Top = 21
      Width = 89
      Height = 28
      TabOrder = 3
      Text = '1'
      OnKeyDown = edt6KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
  end
  object grp2: TsGroupBox
    Left = 20
    Top = 96
    Width = 993
    Height = 305
    Caption = 'Modfy'#20462#25913#35774#23450
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'GROUPBOX'
    object lbl1: TLabel
      Left = 304
      Top = 57
      Width = 108
      Height = 20
      Caption = #26368#22823#20540#65306'mV     '
    end
    object lbl2: TLabel
      Left = 304
      Top = 88
      Width = 108
      Height = 20
      Caption = #26368#23567#20540#65306'mV     '
    end
    object chk1: TsCheckBox
      Left = 24
      Top = 24
      Width = 246
      Height = 24
      Caption = 'Num '#24207#21495#29983#25104'('#36873#20013#19981#21024#30053#65289'      '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk2: TsCheckBox
      Left = 24
      Top = 56
      Width = 154
      Height = 24
      Caption = 'Name '#27493#39588#21517#31216'      '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk3: TsCheckBox
      Left = 24
      Top = 88
      Width = 105
      Height = 24
      Caption = 'Std '#26631#20934#20540'  '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 2
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk4: TsCheckBox
      Left = 24
      Top = 120
      Width = 104
      Height = 24
      Caption = 'Upp'#19978#38480'%  '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 3
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk5: TsCheckBox
      Left = 304
      Top = 26
      Width = 103
      Height = 24
      Caption = 'Low'#19979#38480'%  '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 4
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk8: TsCheckBox
      Left = 304
      Top = 120
      Width = 104
      Height = 24
      Caption = 'Delay'#24310#26102'   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 5
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk10: TsCheckBox
      Left = 536
      Top = 24
      Width = 78
      Height = 24
      Caption = 'K'#27604#20363'    '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 6
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk11: TsCheckBox
      Left = 536
      Top = 56
      Width = 75
      Height = 24
      Caption = 'B'#20559#31227'   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 7
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk12: TsCheckBox
      Left = 535
      Top = 88
      Width = 74
      Height = 24
      Caption = 'P '#24179#22343'  '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 8
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk13: TsCheckBox
      Left = 535
      Top = 120
      Width = 100
      Height = 24
      Caption = #32852#26495#24207#21495'    '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 9
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk14: TsCheckBox
      Left = 760
      Top = 24
      Width = 94
      Height = 24
      Caption = 'H2'#39640#28857'2   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 10
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk15: TsCheckBox
      Left = 760
      Top = 56
      Width = 91
      Height = 24
      Caption = 'L2'#20302#28857'2   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 11
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt3: TsDecimalSpinEdit
      Left = 144
      Top = 80
      Width = 97
      Height = 28
      TabOrder = 12
      Text = '0'
      OnKeyDown = edt3KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      DecimalPlaces = 0
    end
    object edt4: TsDecimalSpinEdit
      Left = 144
      Top = 112
      Width = 97
      Height = 28
      TabOrder = 13
      Text = '1200'
      OnKeyDown = edt4KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      Value = 1200.000000000000000000
      DecimalPlaces = 0
    end
    object edt5: TsDecimalSpinEdit
      Left = 416
      Top = 24
      Width = 89
      Height = 28
      TabOrder = 14
      Text = '200'
      OnKeyDown = edt5KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      Value = 200.000000000000000000
      DecimalPlaces = 0
    end
    object edt8: TsDecimalSpinEdit
      Left = 416
      Top = 120
      Width = 89
      Height = 28
      TabOrder = 15
      Text = '1'
      OnKeyDown = edt8KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt9: TsDecimalSpinEdit
      Left = 640
      Top = 24
      Width = 73
      Height = 28
      TabOrder = 16
      Text = '100'
      OnKeyDown = edt9KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 100.000000000000000000
      DecimalPlaces = 0
    end
    object edt10: TsDecimalSpinEdit
      Left = 640
      Top = 56
      Width = 73
      Height = 28
      TabOrder = 17
      Text = '0'
      OnKeyDown = edt10KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt11: TsDecimalSpinEdit
      Left = 640
      Top = 88
      Width = 73
      Height = 28
      TabOrder = 18
      Text = '5'
      OnKeyDown = edt11KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 5.000000000000000000
      DecimalPlaces = 0
    end
    object edt12: TsDecimalSpinEdit
      Left = 640
      Top = 120
      Width = 73
      Height = 28
      TabOrder = 19
      Text = '0'
      OnKeyDown = edt12KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt13: TsDecimalSpinEdit
      Left = 856
      Top = 24
      Width = 81
      Height = 28
      TabOrder = 20
      Text = '0'
      OnKeyDown = edt13KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt14: TsDecimalSpinEdit
      Left = 856
      Top = 56
      Width = 81
      Height = 28
      TabOrder = 21
      Text = '0'
      OnKeyDown = edt14KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt15: TsEdit
      Left = 176
      Top = 48
      Width = 97
      Height = 28
      TabOrder = 22
      Text = 'V^??'
      OnKeyDown = edt15KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object grp3: TsGroupBox
      Left = 16
      Top = 144
      Width = 969
      Height = 153
      Caption = 'Mode'
      TabOrder = 23
      SkinData.SkinSection = 'GROUPBOX'
      object edt16: TsEdit
        Left = 216
        Top = 13
        Width = 89
        Height = 28
        TabOrder = 0
        Text = '0D01'
        OnKeyDown = edt16KeyDown
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object chk9: TsCheckBox
        Left = 68
        Top = 17
        Width = 108
        Height = 24
        Caption = 'Mode'#27169#24335'    '
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 1
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object cbb1: TsComboBox
        Left = 24
        Top = 73
        Width = 201
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        ItemHeight = 22
        ItemIndex = 0
        TabOrder = 2
        Text = '0'#65306#21333#20301#20026'10'#30340'0'#27425#26041
        Visible = False
        OnChange = cbb1Change
        Items.Strings = (
          '0'#65306#21333#20301#20026'10'#30340'0'#27425#26041
          '1'#65306#21333#20301#20026'10'#30340'1'#27425#26041
          '2'#65306#21333#20301#20026'10'#30340'2'#27425#26041
          '3'#65306#21333#20301#20026'10'#30340'3'#27425#26041
          '4'#65306#21333#20301#20026'10'#30340'4'#27425#26041
          '5'#65306#21333#20301#20026'10'#30340'5'#27425#26041
          '6'#65306#21333#20301#20026'10'#30340'6'#27425#26041
          'F'#65306#22235#32447#30005#38459#21333#20301#20026'mohm'
          'L'#65306' LED'#27979#37327'0'
          'M'#65306'LED'#27979#37327'1'
          'X'#65306#32487#30005#22120#20999#25442'+'
          'Y'#65306#32487#30005#22120#20999#25442
          'Z'#65306#32487#30005#22120#20851#38381)
      end
      object chk16: TsCheckBox
        Left = 808
        Top = 24
        Width = 37
        Height = 24
        Caption = 'nc'
        Enabled = False
        TabOrder = 3
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk17: TsCheckBox
        Left = 808
        Top = 53
        Width = 144
        Height = 24
        Caption = #32467#26524#26174#31034#22312'LCD   '
        TabOrder = 4
        OnClick = chk17Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk18: TsCheckBox
        Left = 808
        Top = 82
        Width = 100
        Height = 24
        Caption = #27493#39588#21069#26174#31034
        TabOrder = 5
        OnClick = chk18Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk19: TsCheckBox
        Left = 808
        Top = 112
        Width = 116
        Height = 24
        Caption = #26597#30475#27979#37327#32467#26524
        Checked = True
        State = cbChecked
        TabOrder = 6
        OnClick = chk19Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object cbb2: TsComboBox
        Left = 240
        Top = 73
        Width = 233
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        ItemHeight = 22
        ItemIndex = 4
        TabOrder = 7
        Text = 'D'#65306#20108#26497#31649#27979#35797
        OnChange = cbb2Change
        Items.Strings = (
          'S'#65306#30701#36335#27979#35797
          'O'#65306#24320#36335#27979#35797
          'R'#65306#30005#38459#27979#35797
          'C'#65306#30005#23481#27979#35797
          'D'#65306#20108#26497#31649#27979#35797
          'Q'#65306#36755#20986#25511#21046
          'G'#65306'Agilent'#19975#29992#34920
          'H'#65306'minghe'
          'A'#65306'485'#36890#20449
          'B'#65306#19978#20256'PC'
          'I'#65306'I2C'#36890#20449
          'P'#65306#36895#24230#27979#37327
          'F'#65306#39057#29575#27979#37327
          'T'#65306#20132#27969#20449#21495
          'Z'#65306#31995#32479#21442#25968#35774#23450
          '')
      end
      object cbb3: TsComboBox
        Left = 496
        Top = 73
        Width = 273
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        ItemHeight = 22
        ItemIndex = 0
        TabOrder = 8
        Text = '0'#65306#38454#27573'1'
        Visible = False
        OnChange = cbb3Change
        Items.Strings = (
          '0'#65306#38454#27573'1'
          '1'#65306#38454#27573'2'
          '2'#65306#38454#27573'3'
          '3'#65306#38454#27573'4'
          '4'#65306#38454#27573'5'
          'R:'#20132#27969#27979#30005#38459
          'C:'#20132#27969#27979#30005#23481
          'L:'#20132#27969#27979#30005#24863)
      end
    end
    object edt1: TEdit
      Left = 416
      Top = 56
      Width = 89
      Height = 28
      Enabled = False
      TabOrder = 24
      Text = '1000'
      OnKeyDown = edt1KeyDown
    end
    object edt2: TEdit
      Left = 416
      Top = 88
      Width = 89
      Height = 28
      Enabled = False
      TabOrder = 25
      Text = '300'
      OnKeyDown = edt2KeyDown
    end
  end
  object spnl1: TsPanel
    Left = 668
    Top = 24
    Width = 345
    Height = 49
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object btn1: TsButton
      Left = 16
      Top = 8
      Width = 113
      Height = 33
      Caption = 'OK '#30830#23450
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btn1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object btn2: TsButton
      Left = 176
      Top = 8
      Width = 129
      Height = 33
      Caption = 'Cancle '#21462#28040
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btn2Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
end
