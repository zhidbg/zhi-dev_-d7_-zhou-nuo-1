object FormDeleteN: TFormDeleteN
  Left = 241
  Top = 279
  Width = 598
  Height = 142
  Caption = 'DeleteNstep '#21024#38500#22810#20010#27493#39588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 40
    Top = 24
    Width = 160
    Height = 16
    Caption = 'StepStart '#24320#22987#27493#39588#65306
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
  end
  object lbl2: TLabel
    Left = 48
    Top = 72
    Width = 144
    Height = 16
    Caption = 'StepEnd '#32467#26463#27493#39588' :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
  end
  object btn1: TButton
    Left = 344
    Top = 31
    Width = 177
    Height = 41
    Caption = 'Delete '#21024#38500
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = btn1Click
    OnKeyUp = btn1KeyUp
  end
  object edt1: TsDecimalSpinEdit
    Left = 208
    Top = 16
    Width = 81
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = '1'
    OnChange = edt1Change
    OnKeyDown = edt1KeyDown
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    Increment = -1.000000000000000000
    MinValue = 1.000000000000000000
    Value = 1.000000000000000000
    DecimalPlaces = 0
  end
  object edt2: TsDecimalSpinEdit
    Left = 208
    Top = 64
    Width = 81
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = '1'
    OnChange = edt1Change
    OnKeyDown = edt2KeyDown
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    Increment = -1.000000000000000000
    MinValue = 1.000000000000000000
    Value = 1.000000000000000000
    DecimalPlaces = 0
  end
end
