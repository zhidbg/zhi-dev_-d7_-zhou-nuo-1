object FormWave: TFormWave
  Left = 248
  Top = 235
  Width = 1319
  Height = 715
  Caption = 'LookWaveform '#26597#30475#27979#37327#27874#24418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = mm1
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object iplt1: TiPlot
    Left = 0
    Top = 41
    Width = 1303
    Height = 615
    AutoFrameRate = False
    DataViewZHorz = 1
    DataViewZVert = 1
    UserCanEditObjects = False
    PrintMarginLeft = 1.000000000000000000
    PrintMarginTop = 1.000000000000000000
    PrintMarginRight = 1.000000000000000000
    PrintMarginBottom = 1.000000000000000000
    PrintDocumentName = 'Untitled'
    HintsFont.Charset = DEFAULT_CHARSET
    HintsFont.Color = clWindowText
    HintsFont.Height = -11
    HintsFont.Name = 'MS Sans Serif'
    HintsFont.Style = []
    EditorFormStyle = ipfsModal
    CopyToClipBoardFormat = ipefMetaFile
    TitleVisible = False
    TitleText = 'Waveform Display'
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -19
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    OnClickChannel = iplt1ClickChannel
    OnDblClickLegend = iplt1DblClickLegend
    AnnotationDefaultFont.Charset = DEFAULT_CHARSET
    AnnotationDefaultFont.Color = clWhite
    AnnotationDefaultFont.Height = -11
    AnnotationDefaultFont.Name = 'MS Sans Serif'
    AnnotationDefaultFont.Style = []
    AnnotationDefaultBrushStlye = bsSolid
    AnnotationDefaultBrushColor = clWhite
    AnnotationDefaultPenStlye = psSolid
    AnnotationDefaultPenColor = clWhite
    AnnotationDefaultPenWidth = 1
    ClipAnnotationsToAxes = True
    BackGroundGradientEnabled = False
    BackGroundGradientDirection = ifdTopBottom
    BackGroundGradientStartColor = clBlue
    BackGroundGradientStopColor = clBlack
    DataFileColumnSeparator = ipdfcsTab
    DataFileFormat = ipdffText
    BorderStyle = ibsNone
    Align = alClient
    TabOrder = 0
    DataViewZHorz = 1
    DataViewZVert = 1
    ToolBarManager = <
      item
        Name = 'Toolbar 1'
        Horizontal = True
        ZOrder = 3
        StopPercent = 100.000000000000000000
        ShowSelectButton = True
        ShowPreviewButton = True
        ZoomInOutFactor = 2.000000000000000000
      end>
    LegendManager = <
      item
        Name = 'Legend 1'
        Horizontal = False
        ZOrder = 2
        StopPercent = 100.000000000000000000
        MarginLeft = 1.000000000000000000
        MarginTop = 1.000000000000000000
        MarginRight = 1.000000000000000000
        MarginBottom = 1.000000000000000000
        SelectedItemFont.Charset = DEFAULT_CHARSET
        SelectedItemFont.Color = clBlack
        SelectedItemFont.Height = -11
        SelectedItemFont.Name = 'MS Sans Serif'
        SelectedItemFont.Style = []
        CaptionColumnTitle = 'Title'
        CaptionColumnXAxisTitle = 'X-Axis'
        CaptionColumnYAxisTitle = 'Y-Axis'
        CaptionColumnXValue = 'X'
        CaptionColumnYValue = 'Y'
        CaptionColumnYMax = 'Y-Max'
        CaptionColumnYMin = 'Y-Min'
        CaptionColumnYMean = 'Y-Mean'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ColumnSpacing = 0.500000000000000000
        RowSpacing = 0.250000000000000000
        WrapColDesiredCount = 1
        WrapColAutoCountEnabled = False
        WrapColAutoCountMax = 100
        WrapColSpacingMin = 2.000000000000000000
        WrapColSpacingAuto = True
        WrapRowDesiredCount = 5
        WrapRowAutoCountEnabled = True
        WrapRowAutoCountMax = 100
        WrapRowSpacingMin = 0.250000000000000000
        WrapRowSpacingAuto = False
        ColumnTitlesFont.Charset = DEFAULT_CHARSET
        ColumnTitlesFont.Color = clAqua
        ColumnTitlesFont.Height = -11
        ColumnTitlesFont.Name = 'MS Sans Serif'
        ColumnTitlesFont.Style = [fsBold]
      end>
    XAxisManager = <
      item
        Name = 'X-Axis 1'
        Horizontal = True
        ZOrder = 0
        StopPercent = 100.000000000000000000
        Span = 5000.000000000000000000
        Title = #37319#26679#26102#38388#28857
        TitleMargin = 0.250000000000000000
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        TitleShow = True
        LabelsMargin = 0.250000000000000000
        LabelsFont.Charset = DEFAULT_CHARSET
        LabelsFont.Color = clWhite
        LabelsFont.Height = -11
        LabelsFont.Name = 'MS Sans Serif'
        LabelsFont.Style = []
        LabelSeparation = 2.000000000000000000
        LabelsRotation = ira000
        DateTimeFormat = 'hh:nn:ss'
        LabelsMinLength = 5.000000000000000000
        ScaleLineShow = True
        StackingEndsMargin = 0.500000000000000000
        TrackingStyle = iptsScrollSmooth
        TrackingAlignFirstStyle = ipafsAuto
        CursorDateTimeFormat = 'hh:nn:ss'
        CursorPrecision = 3
        CursorMinLength = 5.000000000000000000
        LegendDateTimeFormat = 'hh:nn:ss'
        LegendPrecision = 3
        LegendMinLength = 5.000000000000000000
        CursorScaler = 1.000000000000000000
        ScrollMinMaxEnabled = False
        ScrollMax = 100.000000000000000000
        RestoreValuesOnResume = True
        MasterUIInput = False
        CartesianStyle = ipcsNone
        CartesianChildRefAxisName = '<None>'
        AlignRefAxisName = '<None>'
        GridLinesVisible = True
        ForceStacking = False
      end>
    YAxisManager = <
      item
        Name = 'Y-Axis 1'
        Horizontal = False
        ZOrder = 0
        StopPercent = 100.000000000000000000
        Span = 5000.000000000000000000
        Title = 'mV/'#37319#26679#24133#20540
        TitleMargin = 0.250000000000000000
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        TitleShow = True
        LabelsMargin = 0.250000000000000000
        LabelsFont.Charset = DEFAULT_CHARSET
        LabelsFont.Color = clWhite
        LabelsFont.Height = -11
        LabelsFont.Name = 'MS Sans Serif'
        LabelsFont.Style = []
        LabelSeparation = 2.000000000000000000
        LabelsRotation = ira000
        DateTimeFormat = 'hh:nn:ss'
        LabelsMinLength = 5.000000000000000000
        ScaleLineShow = True
        StackingEndsMargin = 0.500000000000000000
        TrackingStyle = iptsScaleMinMax
        TrackingAlignFirstStyle = ipafsNone
        CursorDateTimeFormat = 'hh:nn:ss'
        CursorPrecision = 3
        CursorMinLength = 5.000000000000000000
        LegendDateTimeFormat = 'hh:nn:ss'
        LegendPrecision = 3
        LegendMinLength = 5.000000000000000000
        CursorScaler = 1.000000000000000000
        ScrollMinMaxEnabled = False
        ScrollMax = 100.000000000000000000
        RestoreValuesOnResume = True
        MasterUIInput = False
        CartesianStyle = ipcsNone
        CartesianChildRefAxisName = '<None>'
        AlignRefAxisName = '<None>'
        GridLinesVisible = True
        ForceStacking = False
      end>
    ChannelManager = <
      item
        Name = 'Channel 1'
        TitleText = 'CH01'
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 2'
        TitleText = 'CH02'
        Color = clYellow
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 3'
        TitleText = 'CH03'
        Color = clBlue
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 4'
        TitleText = 'CH04'
        Color = clFuchsia
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 5'
        TitleText = 'CH05'
        Color = clAqua
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 6'
        TitleText = 'CH06'
        Color = clLime
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 7'
        TitleText = 'Ch07'
        Color = clWhite
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 8'
        TitleText = 'Ch08'
        Color = clMaroon
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 9'
        TitleText = 'Ch09'
        Color = clGreen
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 10'
        TitleText = 'Ch10'
        Color = clOlive
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 11'
        TitleText = 'Ch11'
        Color = clNavy
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 12'
        TitleText = 'Ch12'
        Color = clPurple
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 13'
        TitleText = 'Ch13'
        Color = clTeal
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 14'
        TitleText = 'Ch 14'
        Color = clGray
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 15'
        TitleText = 'Ch15'
        Color = 192
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end
      item
        Name = 'Channel 16'
        TitleText = 'Ch16'
        Color = 49152
        MarkersTurnOffLimit = 0
        MarkersFont.Charset = DEFAULT_CHARSET
        MarkersFont.Color = clWhite
        MarkersFont.Height = -11
        MarkersFont.Name = 'MS Sans Serif'
        MarkersFont.Style = [fsBold]
        XAxisName = 'X-Axis 1'
        YAxisName = 'Y-Axis 1'
        DataStyle = ipdsStandard
        OPCComputerName = 'Local'
        DigitalReferenceLow = 10.000000000000000000
        DigitalReferenceHigh = 90.000000000000000000
        HighLowBarColor = clAqua
        HighLowBarWidth = 0.500000000000000000
        HighLowOpenColor = clLime
        HighLowOpenWidth = 1.000000000000000000
        HighLowOpenHeight = 1.000000000000000000
        HighLowCloseWidth = 1.000000000000000000
        HighLowCloseHeight = 1.000000000000000000
        BarWidth = 5.000000000000000000
      end>
    DataViewManager = <
      item
        Name = 'Data View 1'
        Horizontal = False
        ZOrder = 0
        StopPercent = 100.000000000000000000
        GridXAxisName = 'X-Axis 1'
        GridYAxisName = 'Y-Axis 1'
        GridLineXMajorWidth = 0
        GridLineXMinorWidth = 0
        GridLineYMajorWidth = 0
        GridLineYMinorWidth = 0
        AxesControlEnabled = False
        AxesControlMouseStyle = ipacsBoth
        AxesControlWheelStyle = ipacsXAxis
      end>
    DataCursorManager = <
      item
        Name = 'Cursor 1'
        Visible = False
        ChannelName = 'Channel 1'
        ChannelAllowAll = True
        ChannelShowAllInLegend = True
        Style = ipcsValueXY
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Color = clYellow
        UseChannelColor = True
        HintShow = True
        HintHideOnRelease = False
        HintOrientationSide = iosBottomRight
        HintPosition = 50.000000000000000000
        Pointer1Position = 50.000000000000000000
        Pointer2Position = 60.000000000000000000
        PointerPenWidth = 1
        MenuItemVisibleValueXY = True
        MenuItemVisibleValueX = True
        MenuItemVisibleValueY = True
        MenuItemVisibleDeltaX = True
        MenuItemVisibleDeltaY = True
        MenuItemVisibleInverseDeltaX = True
        MenuItemCaptionValueXY = 'Value X-Y'
        MenuItemCaptionValueX = 'Value X'
        MenuItemCaptionValueY = 'Value Y'
        MenuItemCaptionDeltaX = 'Period'
        MenuItemCaptionDeltaY = 'Peak-Peak'
        MenuItemCaptionInverseDeltaX = 'Frequency'
      end
      item
        Name = 'Cursor 2'
        Visible = False
        ChannelName = 'Channel 1'
        ChannelAllowAll = True
        ChannelShowAllInLegend = True
        Style = ipcsValueXY
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Color = clYellow
        UseChannelColor = True
        HintShow = True
        HintHideOnRelease = False
        HintOrientationSide = iosBottomRight
        HintPosition = 50.000000000000000000
        Pointer1Position = 50.000000000000000000
        Pointer2Position = 60.000000000000000000
        PointerPenWidth = 1
        MenuItemVisibleValueXY = True
        MenuItemVisibleValueX = True
        MenuItemVisibleValueY = True
        MenuItemVisibleDeltaX = True
        MenuItemVisibleDeltaY = True
        MenuItemVisibleInverseDeltaX = True
        MenuItemCaptionValueXY = 'Value X-Y'
        MenuItemCaptionValueX = 'Value X'
        MenuItemCaptionValueY = 'Value Y'
        MenuItemCaptionDeltaX = 'Period'
        MenuItemCaptionDeltaY = 'Peak-Peak'
        MenuItemCaptionInverseDeltaX = 'Frequency'
      end>
    LabelManager = <
      item
        Name = 'Title'
        Visible = False
        Horizontal = True
        ZOrder = 2
        StopPercent = 100.000000000000000000
        Caption = 'Waveform Display'
        Alignment = iahCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end>
  end
  object grp1: TGroupBox
    Left = 0
    Top = 0
    Width = 1303
    Height = 41
    Align = alTop
    Caption = #37319#38598#21442#25968#35774#32622
    TabOrder = 1
    object lbl1: TLabel
      Left = 10
      Top = 18
      Width = 39
      Height = 13
      Caption = 'HS'#36890#36947
      Enabled = False
      Visible = False
    end
    object lbl2: TLabel
      Left = 126
      Top = 16
      Width = 37
      Height = 13
      Caption = 'LS'#36890#36947
      Enabled = False
      Visible = False
    end
    object lbl3: TLabel
      Left = 244
      Top = 16
      Width = 89
      Height = 13
      Caption = #26102#38388#38388#38548'x0.01mS'
      Enabled = False
      Visible = False
    end
    object lbl4: TLabel
      Left = 896
      Top = 16
      Width = 22
      Height = 13
      Caption = 'Y'#36724':'
    end
    object lbl5: TLabel
      Left = 752
      Top = 16
      Width = 22
      Height = 13
      Caption = 'X'#36724':'
    end
    object shp4: TShape
      Left = 432
      Top = 8
      Width = 26
      Height = 25
      Brush.Color = clSilver
      Shape = stCircle
    end
    object btn2: TButton
      Left = 488
      Top = 8
      Width = 97
      Height = 25
      Caption = 'Start-'#24320#22987#37319#38598
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btn2Click
    end
    object edt2: TEdit
      Left = 169
      Top = 12
      Width = 65
      Height = 21
      Enabled = False
      TabOrder = 1
      Text = '2'
      Visible = False
    end
    object edt3: TEdit
      Left = 336
      Top = 11
      Width = 65
      Height = 21
      Enabled = False
      TabOrder = 2
      Text = '1'
      Visible = False
    end
    object edt1: TEdit
      Left = 61
      Top = 13
      Width = 57
      Height = 21
      Enabled = False
      TabOrder = 3
      Text = '1'
      Visible = False
    end
    object btn3: TButton
      Left = 592
      Top = 8
      Width = 89
      Height = 25
      Caption = 'Stop'#20572#27490#37319#38598
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btn3Click
    end
    object edt4: TEdit
      Left = 928
      Top = 9
      Width = 129
      Height = 28
      Hint = 'Yaxis Unit'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Text = 'mV'
      OnChange = edt4Change
    end
    object edt5: TEdit
      Left = 784
      Top = 9
      Width = 97
      Height = 28
      Hint = 'Yaxis Unit'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      Text = 'mS'
      OnChange = edt5Change
    end
    object btn4: TBitBtn
      Left = 1208
      Top = 8
      Width = 89
      Height = 25
      Caption = 'SaveUnit'#20445#23384#21333#20301
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = btn4Click
    end
    object edt6: TEdit
      Left = 1064
      Top = 8
      Width = 121
      Height = 28
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      Text = #36890#36947'2'
    end
  end
  object btn1: TButton
    Left = 472
    Top = 48
    Width = 65
    Height = 25
    Caption = #28165#31354#27874#24418
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = C1Click
  end
  object tmr1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = tmr1Timer
    Left = 1264
    Top = 8
  end
  object mm1: TMainMenu
    Left = 560
    Top = 8
    object F1: TMenuItem
      Caption = 'File'#25991#20214
      object E1: TMenuItem
        Caption = 'Exit '#36864#20986
        ShortCut = 115
        OnClick = E1Click
      end
    end
    object N1: TMenuItem
      Caption = 'Collect'#37319#38598
      object s1: TMenuItem
        Caption = 'Start '#24320#22987#37319#38598
        ShortCut = 120
        OnClick = btn2Click
      end
      object S2: TMenuItem
        Caption = 'Stop '#20572#27490#37319#38598
        ShortCut = 121
        OnClick = btn3Click
      end
      object R1: TMenuItem
        Caption = 'Read(10mS)'#25968#25454
        ShortCut = 122
        OnClick = R1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object C1: TMenuItem
        Caption = 'Clear '#28165#31354
        ShortCut = 123
        OnClick = C1Click
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object N6: TMenuItem
        Caption = #26080#31526#21495#25968#25454#26684#24335
        OnClick = N6Click
      end
      object trigSyn: TMenuItem
        Caption = 'TrigSynch'#21452#36890#36947#21516#27493'--'
        Checked = True
        OnClick = trigSynClick
      end
    end
    object N3: TMenuItem
      Caption = 'Channel '#24320#20851#36890#36947
      object A1: TMenuItem
        Caption = 'OpenAll '#26174#31034#25152#26377#36890#36947
        OnClick = A1Click
      end
      object A2: TMenuItem
        Caption = 'CloseAll '#20851#38381#25152#26377#36890#36947
        OnClick = A2Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object c2: TMenuItem
        Caption = 'CH1 '#24320#20851#20999#25442
        OnClick = c2Click
      end
      object C3: TMenuItem
        Caption = 'CH2 '#24320#20851#20999#25442
        OnClick = C3Click
      end
      object C4: TMenuItem
        Caption = 'CH3 '
        OnClick = C4Click
      end
      object C5: TMenuItem
        Caption = 'CH4'
        OnClick = C5Click
      end
      object C6: TMenuItem
        Caption = 'CH5'
        OnClick = C6Click
      end
      object C7: TMenuItem
        Caption = 'CH6'
        OnClick = C7Click
      end
      object C8: TMenuItem
        Caption = 'CH7'
        OnClick = C8Click
      end
      object C9: TMenuItem
        Caption = 'CH8'
        OnClick = C9Click
      end
      object C10: TMenuItem
        Caption = 'CH9'
        OnClick = C10Click
      end
      object C11: TMenuItem
        Caption = 'CH10'
        OnClick = C11Click
      end
      object C12: TMenuItem
        Caption = 'CH11'
        OnClick = C12Click
      end
      object C13: TMenuItem
        Caption = 'CH12'
        OnClick = C13Click
      end
      object C14: TMenuItem
        Caption = 'CH13'
        OnClick = C14Click
      end
      object C15: TMenuItem
        Caption = 'CH14'
        OnClick = C15Click
      end
      object C16: TMenuItem
        Caption = 'CH15'
        OnClick = C16Click
      end
      object C17: TMenuItem
        Caption = 'CH16'
        OnClick = C17Click
      end
    end
    object S3: TMenuItem
      Caption = 'SelfCheck'#33258#26816
      object S4: TMenuItem
        Caption = 'SelfCheck'#33258#26816'('#20808' '#20572#27490#37319#38598')'
        OnClick = S4Click
      end
    end
    object T1: TMenuItem
      Caption = 'Test'#27979#35797
      object S5: TMenuItem
        Caption = 'Start'#21551#21160
        ShortCut = 116
        OnClick = S5Click
      end
      object R2: TMenuItem
        Caption = 'Reset'#22797#20301
        ShortCut = 117
        OnClick = R2Click
      end
    end
  end
end
