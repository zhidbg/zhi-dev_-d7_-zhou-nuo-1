unit About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Unit0_globalVariant,Unit2_Communication, jpeg, ExtCtrls;

type
  TFormAbout = class(TForm)
    edt54: TEdit;
    edt1: TEdit;
    edt55: TEdit;
    edt56: TEdit;
    lbl2: TLabel;
    edt2: TEdit;
    lbl72: TLabel;
    lbl1: TLabel;
    lbl73: TLabel;
    lbl74: TLabel;
    mmo1: TMemo;
    img1: TImage;
    lbl75: TLabel;
    procedure edt54Click(Sender: TObject);
    procedure edt1Click(Sender: TObject);
    procedure edt2Click(Sender: TObject);
    procedure edt55Click(Sender: TObject);
    procedure edt56Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormAbout: TFormAbout;

implementation

{$R *.dfm}

procedure TFormAbout.edt54Click(Sender: TObject);
var str:string;
begin
  if InputQuery('Pls Enter 16 char!', '0-9-A-F', str) then
    begin
    
      modbusfun16int:=$F308;
      modbusfun16len:=$04;
      
      sbuf[2]:=$10;
      sbuf[3]:=$F3;
      sbuf[4]:=$08;
      sbuf[5]:=$00;
      sbuf[6]:=$04;
      sbuf[7]:=2*sbuf[6];
      hextobin(pchar(str),pchar(@sbuf)+7, 8);     // edt54.text
    end;
end;

procedure TFormAbout.edt1Click(Sender: TObject);
var str:string;
begin
  if InputQuery('Pls Enter 4 char!', '0-9', str) then
    begin
      modbusfun16int:=$F330;
      modbusfun16len:=$01;
      
      sbuf[2]:=$10;
      sbuf[3]:=$F3;
      sbuf[4]:=$30;
      sbuf[5]:=$00;
      sbuf[6]:=$01;
      sbuf[7]:=2*sbuf[6];
      sbuf[8]:=StrToInt(str) div 256;
      sbuf[9]:=StrToInt(str) mod 256;

      edt1.Text:=str ;

      //hextobin(pchar(str),pchar(@sbuf)+7, 8);     // edt54.text
    end;
end;

procedure TFormAbout.edt2Click(Sender: TObject);
var str:string;
begin
  if InputQuery('Pls Enter 4 char!', '0-9', str) then
    begin
      modbusfun16int:=$F331;
      modbusfun16len:=$01;
      
      sbuf[2]:=$10;
      sbuf[3]:=$F3;
      sbuf[4]:=$31;
      sbuf[5]:=$00;
      sbuf[6]:=$01;
      sbuf[7]:=2*sbuf[6];
      sbuf[8]:=StrToInt(str) div 256;
      sbuf[9]:=StrToInt(str) mod 256;

      edt2.Text:=str ;
      //hextobin(pchar(str),pchar(@sbuf)+7, 8);     // edt54.text
    end;
end;

procedure TFormAbout.edt55Click(Sender: TObject);
var str:string;
begin
  if InputQuery('Pls Enter 16 char!', '0-9-A-F', str) then
    begin

      modbusfun16int:=$F314;
      modbusfun16len:=$04;
      
      sbuf[2]:=$10;
      sbuf[3]:=$F3;
      sbuf[4]:=$08;
      sbuf[5]:=$00;
      sbuf[6]:=$04;
      sbuf[7]:=2*sbuf[6];
      hextobin(pchar(str),pchar(@sbuf)+7, 8);     // edt54.text
    end;
end;

procedure TFormAbout.edt56Click(Sender: TObject);
var str:string;
begin
  if InputQuery('Pls Enter 16 char!', '0-9-A-F', str) then
    begin

      modbusfun16int:=$F320;
      modbusfun16len:=$04;

      sbuf[2]:=$10;
      sbuf[3]:=$F3;
      sbuf[4]:=$08;
      sbuf[5]:=$00;
      sbuf[6]:=$04;
      sbuf[7]:=2*sbuf[6];
      hextobin(pchar(str),pchar(@sbuf)+7, 8);     // edt54.text
    end;
end;

end.
