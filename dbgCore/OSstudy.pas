unit OSstudy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Unit0_globalVariant, Menus, Buttons, ExtCtrls, ToolWin,
  ComCtrls;

type
  TFormOS = class(TForm)
    mmo1: TMemo;
    btn2: TButton;
    btn3: TButton;
    btn4: TButton;
    btn6: TButton;
    mm1: TMainMenu;
    N11: TMenuItem;
    N12: TMenuItem;
    N21: TMenuItem;
    N13: TMenuItem;
    N22: TMenuItem;
    btn5: TBitBtn;
    grp13: TGroupBox;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    edt3: TEdit;
    edt12: TEdit;
    edt13: TEdit;
    tmr1: TTimer;
    mmo2: TMemo;
    mmo3: TMemo;
    tlb1: TToolBar;
    N1: TMenuItem;
    N2: TMenuItem;
    btn1: TButton;
    edt1: TEdit;
    lbl1: TLabel;
    lbl5: TLabel;
    edt2: TEdit;
    btn7: TButton;
    grp1: TGroupBox;
    edt4: TEdit;
    edt5: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);

//    procedure btn7Click(Sender: TObject);
//    procedure btn8Click(Sender: TObject);
//    procedure btn9Click(Sender: TObject);
//    procedure btn10Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure edt3Click(Sender: TObject);
    procedure edt12Click(Sender: TObject);
    procedure edt13Click(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure mmo2DblClick(Sender: TObject);
    procedure mmo3DblClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure btn7Click(Sender: TObject);
    procedure edt1Click(Sender: TObject);
    procedure edt2Click(Sender: TObject);
    procedure edt4DblClick(Sender: TObject);
    procedure edt5DblClick(Sender: TObject);
//    procedure btn7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormOS: TFormOS;

implementation

uses main;

{$R *.dfm}

procedure TFormOS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 modbusfun05:=3; modbusfun05dat:=$FF;
 syslabel[3].Color:=clBtnFace;          //系统参数值学习参数颜色回归；
 syslabel[4].Color:=clBtnFace;
 sysedit[3].Color:=clWindow;
 sysedit[4].Color:=clWindow;
 tmr1.Enabled:=false;
end;

procedure TFormOS.btn1Click(Sender: TObject);
var
    i:SmallInt;
    temp8:Byte;
    tempstr:string;
begin
    modbusfun16int:=$2F00;
    modbusfun16len:=$12;     
    sbuf[2]:=$10;

    sbuf[8]:=50 div 256;sbuf[9]:=50 mod 256; //序号
    sbuf[10]:=byte('P');sbuf[11]:=byte('O');sbuf[12]:=byte('E');sbuf[13]:=byte('N');
    sbuf[14]:=StrToInt(edt1.Text)div 256;sbuf[15]:=StrToInt(edt1.Text)mod 256; //标准值
    for i:=3 to 8 do
    begin
     sbuf[10+2*i]:=0;sbuf[11+2*i]:=0;
    end;


    tempstr:=Copy(edt4.text,0,4);
    Move(tempstr[1],sbuf[28],4);
    if tempstr='' then begin
      sbuf[28]:=byte('0');
      sbuf[29]:=byte('0');
      sbuf[30]:=byte('0');
      sbuf[31]:=byte('0');
    end;
    if(VIRLCROWS<60)then begin
      temp8:=sbuf[28]; sbuf[28]:=sbuf[29];sbuf[29]:=temp8;
      temp8:=sbuf[30]; sbuf[30]:=sbuf[31];sbuf[31]:=temp8;
    end;
    for i:=10 to 15 do begin
       sbuf[12+2*i]:=0;sbuf[13+2*i]:=0;
    end;
    sbuf[33]:=100;     //比例

    formos.tmr1.Tag:=3;
    formos.tmr1.Enabled:=true;
end;

procedure TFormOS.btn2Click(Sender: TObject);
begin
  modbusfun06dat:=$00E0;
  modbusfun06:=$0001;
  btn5.Font.Color:=clRed;
  mmo1.Lines.Clear;
  mmo1.Lines.Add('First O/S learning......');
  mmo1.Lines.Add('一次短路群学习中......');
//  modbusfun05:=5; modbusfun05dat:=$FF;
  addToLog('学习一次短路群组' );
  tmr1.Tag:=11;
  tmr1.Enabled:=true; grp1.Visible:=False;
end;

procedure TFormOS.btn3Click(Sender: TObject);
begin
  if StrToInt(sysedit[3].Text)>=4095 then begin
      ShowMessage('点数(>=4095)太多!，不支持二次学习！');
  end else  begin
      modbusfun06dat:=$00E1;
      modbusfun06:=$0001;
      btn5.Font.Color:=clRed;      //  modbusfun05:=6; modbusfun05dat:=$FF;
      mmo1.Lines.Clear;
      mmo1.Lines.Add('Next O/S learning......');
      mmo1.Lines.Add('二次短路群学习中......');
      addToLog('学习二次短路群组' );
      tmr1.Tag:=12;
      tmr1.Enabled:=true; grp1.Visible:=False;
  end;
end;

procedure TFormOS.btn4Click(Sender: TObject);
begin
    modbusfun06dat:=$00F5;
    modbusfun06:=$0001;
    formos.tmr1.Enabled:=True;
    formos.tmr1.Tag:=1;
end;

procedure TFormOS.btn5Click(Sender: TObject);
begin
 modbusfun05:=9; modbusfun05dat:=$FF;
 btn5.Font.Color:=clDefault;
 addToLog('SaveOS,保存短路群' );
 grp1.Visible:=true;
end;

procedure TFormOS.btn6Click(Sender: TObject);
begin
   modbusfun05:=14; modbusfun05dat:=$FF;

end;

procedure TFormOS.FormShow(Sender: TObject);
begin
    mmo1.Clear; mmo1.Hint:='';
 // modbusfun06dat:=$00F5;
 // modbusfun06:=$0001;
   syslabel[3].Color:=clYellow;          //系统参数值学习参数颜色提醒；
   syslabel[4].Color:=clYellow;
   sysedit[3].Color:=clYellow;
   sysedit[4].Color:=clYellow;
   tmr1.Enabled:=True;
   btn5.Font.Color:=clDefault;
   grp1.Visible:=true;

   // if(strtoint('0'+sysedit[5].text)<950) then begin
       if edt4.Text='' then edt4.Text:='0K11';
       if edt5.Text='' then edt5.Text:='0S11';
   // end else begin
   //    if edt4.Text='' then edt4.Text:='0KMA';
   //    if edt5.Text='' then edt5.Text:='0SMA';
   // end;
end;
 {
procedure TFormOS.btn7Click(Sender: TObject);
begin
      modbusfun06dat:=$00E2;
      modbusfun06:=$0001;

end;

procedure TFormOS.btn8Click(Sender: TObject);
begin
      modbusfun06dat:=$00E3;
      modbusfun06:=$0001;

end;

procedure TFormOS.btn9Click(Sender: TObject);
begin
      modbusfun06dat:=$00E4;
      modbusfun06:=$0001;

end;

procedure TFormOS.btn10Click(Sender: TObject);
begin
      modbusfun06dat:=$00E5;
      modbusfun06:=$0001;

end;     }

procedure TFormOS.N12Click(Sender: TObject);
begin
      modbusfun06dat:=$00E2;
      modbusfun06:=$0001;

end;

procedure TFormOS.N21Click(Sender: TObject);
begin
      modbusfun06dat:=$00E3;
      modbusfun06:=$0001;

end;

procedure TFormOS.N13Click(Sender: TObject);
begin
      modbusfun06dat:=$00E4;
      modbusfun06:=$0001;

end;

procedure TFormOS.N22Click(Sender: TObject);
begin
      modbusfun06dat:=$00E5;
      modbusfun06:=$0001;

end;


procedure TFormOS.edt3Click(Sender: TObject);
begin
  sysedit[3].Tag:=3;
  sysedit[3].OnClick(form_main);
  formos.tmr1.Tag:=2;
  formos.tmr1.Enabled:=true;
end;

procedure TFormOS.edt12Click(Sender: TObject);
begin
  sysedit[3].Tag:=4;
  sysedit[4].OnClick(form_main);
  formos.tmr1.Tag:=2;
  formos.tmr1.Enabled:=true;

end;

procedure TFormOS.edt13Click(Sender: TObject);
begin
  sysedit[3].Tag:=10;
  sysedit[10].OnClick(form_main);
  formos.tmr1.Tag:=2;
  formos.tmr1.Enabled:=true;

end;

procedure TFormOS.tmr1Timer(Sender: TObject);
begin
  case tmr1.Tag of
    1:begin
       btn4Click(self);
      end;
    2:begin
       modbusfun03:=$10A0;
       modbusfun03dat:=$001A;
      end;
    3:begin
       if grp1.Visible then
          mmo2DblClick(self);
      end;
    4:begin
       if grp1.Visible then
          mmo3DblClick(self);
      end;
    11:
      begin
        modbusfun06dat:=$00E0;         //冗余读取（从while循环进入学习界面可能pccomand标志已经清除）
        modbusfun06:=$0001;
        tmr1.Enabled:=false;
      end;
    12: begin
        modbusfun06dat:=$00E1;
        modbusfun06:=$0001;
        tmr1.Enabled:=false;
      end;
  else
  end;
  if formos.tmr1.Tag>4 then formos.tmr1.Tag:=2;

end;

procedure TFormOS.mmo2DblClick(Sender: TObject);
begin
      modbusfun06dat:=$00F3;
      modbusfun06:=$0001;
end;

procedure TFormOS.mmo3DblClick(Sender: TObject);
begin
      modbusfun06dat:=$00F4;
      modbusfun06:=$0001;
end;

procedure TFormOS.N2Click(Sender: TObject);
begin
   ShowMessage('提示：FCT5测控板请看LCD显示屏，此处无显示');
end;

procedure TFormOS.btn7Click(Sender: TObject);
var
    i:SmallInt;
    temp8:Byte;
    tempstr:string;
begin
    modbusfun16int:=$2F00;
    modbusfun16len:=$12;     
    sbuf[2]:=$10;

    sbuf[8]:=50 div 256;sbuf[9]:=50 mod 256; //序号
    sbuf[10]:=byte('P');sbuf[11]:=byte('O');sbuf[12]:=byte('E');sbuf[13]:=byte('N');
    sbuf[14]:=StrToInt(edt2.Text)div 256;sbuf[15]:=StrToInt(edt2.Text)mod 256; //标准值
    for i:=3 to 8 do
    begin
     sbuf[10+2*i]:=0;sbuf[11+2*i]:=0;
    end;


    tempstr:=Copy(edt5.text,0,4);
    Move(tempstr[1],sbuf[28],4);
    if tempstr='' then begin
      sbuf[28]:=byte('0');
      sbuf[29]:=byte('0');
      sbuf[30]:=byte('0');
      sbuf[31]:=byte('0');
    end;
    if(VIRLCROWS<60)then begin
      temp8:=sbuf[28]; sbuf[28]:=sbuf[29];sbuf[29]:=temp8;
      temp8:=sbuf[30]; sbuf[30]:=sbuf[31];sbuf[31]:=temp8;
    end;
    for i:=10 to 15 do begin
       sbuf[12+2*i]:=0;sbuf[13+2*i]:=0;
    end;
    sbuf[33]:=100;     //比例

    formos.tmr1.Tag:=4;
    formos.tmr1.Enabled:=true;
end;

procedure TFormOS.edt1Click(Sender: TObject);
var
  str: string;
begin
  if InputQuery('Pls Enter Integer输入整数!', '123', str) then
  begin
     if str='' then str:='0';
     if not IsFloatNum(str)  then ShowMessage('err! pls enter number,错误！请输入数字')
     else begin
        edt1.Text:=str;
       btn1Click(self);
     end;
  end;
end;

procedure TFormOS.edt2Click(Sender: TObject);
var
  str: string;
begin
  if InputQuery('Pls Enter Integer输入整数!', '123', str) then
  begin
     if str='' then str:='0';
     if not IsFloatNum(str)  then ShowMessage('err! pls enter number,错误！请输入数字')
     else begin
       edt2.Text:=str;
       btn2Click(self);
     end;
  end;
end;

procedure TFormOS.edt4DblClick(Sender: TObject);
var str: string;
  ii:SmallInt;
begin

  if InputQuery('Pls Enter 4 char!', 'AB', str) then
  begin
     for ii:=1 to length(str) do begin
       if  Byte(str[ii]) >$7F then  str[ii]:='*';
     end;
     if length(str)<4 then str :=str+'0';       //补足4个字符；
     if length(str)<4 then str :=str+'0';
     if length(str)<4 then str :=str+'0';
     if length(str)<4 then str :=str+'0';

     if(VIRLCROWS<600)then begin
        edt4.Text:=AnsiUpperCase(str);
     end else
        edt4.Text:=str;
     btn1Click(self);
  end;
end;

procedure TFormOS.edt5DblClick(Sender: TObject);
var str: string;
  ii:SmallInt;
begin

  if InputQuery('Pls Enter 4 char!', 'AB', str) then
  begin
     for ii:=1 to length(str) do begin
       if  Byte(str[ii]) >$7F then  str[ii]:='*';
     end;
     if length(str)<4 then str :=str+'0';       //补足4个字符；
     if length(str)<4 then str :=str+'0';
     if length(str)<4 then str :=str+'0';
     if length(str)<4 then str :=str+'0';

     if(VIRLCROWS<600)then begin
        edt5.Text:=AnsiUpperCase(str);
     end else
        edt5.Text:=str;
     btn2Click(self);
  end;
end;

end.
