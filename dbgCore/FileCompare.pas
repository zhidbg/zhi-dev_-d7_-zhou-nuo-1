unit FileCompare;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Buttons;

type
  TFormCompare = class(TForm)
    grp1: TGroupBox;
    grp2: TGroupBox;
    btn1: TSpeedButton;
    btn2: TSpeedButton;
    redt1: TRichEdit;
    redt2: TRichEdit;
    dlgOpen1: TOpenDialog;
    dlgOpen2: TOpenDialog;
    procedure btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormResize(Sender: TObject);

  private
    { Private declarations }
    procedure   WMSysCommand(var   Msg:TMessage);message   WM_SYSCOMMAND;
  public

    { Public declarations }
    procedure FileCompare;
  end;

var
  FormCompare: TFormCompare;

implementation

{$R *.dfm}


procedure TFormCompare.WMSysCommand(var   Msg:   TMessage);
begin 
    case   Msg.WParam   of
        //SC_MINIMIZE   :   ShowMessage( '最小化 ');
       { SC_PREVWINDOW:    begin
                          grp1.Width:=FormCompare.Width div 2;
                          grp2.Width:=FormCompare.Width div 2;
                          redt1.Height:=grp1.Height-50;
                          redt2.Height:=grp2.Height-50;
                        end; }
        SC_MAXIMIZE   :  begin
                          grp1.Width:=FormCompare.Width div 2;
                          grp2.Width:=FormCompare.Width div 2;
                          redt1.Height:=grp1.Height-50;
                          redt2.Height:=grp2.Height-50;
                        //ShowMessage( '最大化 ');
                        end;
    end; 
    inherited;
end;
procedure TFormCompare.FileCompare;
var
  CompareFileList,CompareFileList2:TStringList;
  i,pp1,pp2,selp1,selp2,lp1,lp2,tabnum,count,stependflag:SmallInt;
  save_file_name,sss1,sss2,tabs1,tabs2: string;
begin
      FormCompare.Caption:='CompareFiles On......开始比较文件...';
      redt1.Clear;
      redt2.Clear;
      try
        CompareFileList := TStringList.Create;
        CompareFileList.LoadFromFile(btn1.hint);
        CompareFileList2 := TStringList.Create;
        CompareFileList2.LoadFromFile(btn2.hint);

        if CompareFileList.Count>CompareFileList2.Count then
          count:=CompareFileList2.Count
        else
          count:=CompareFileList.Count;
        selp1:=0;selp2:=0;                        //每行的起始位置：第1行必须为0（计算会导致颜色位置不稳定，因中文支持不好），其它行由SelStart-当前行长度计算得到；
        if count>0 then begin                     //先处理最少的行数，里面有9999结束步骤
          i:=0;
          stependflag:=0;
          for i:=0 to  Count-1 do begin
            sss1:=StringReplace(CompareFileList[i], Char($FF), '', [rfReplaceAll]); //            sss:=StringReplace(sss, '@', '', [rfReplaceAll]);
            sss2:=StringReplace(CompareFileList2[i], Char($FF), '', [rfReplaceAll]);  //            sss:=StringReplace(sss, '@', '', [rfReplaceAll]);
            if i>0 then begin
              redt1.Lines.Add('');                        //直接写sss1会报错，必须分两行；
              redt2.Lines.Add('');
            end;
            redt1.SelAttributes.Color := clWindowText;
            redt2.SelAttributes.Color := clWindowText;
            redt1.lines[i]:=sss1;
            redt2.lines[i]:=sss2;
            if i>0 then begin
              selp1:=redt1.SelStart-length(redt1.lines[i]);   //必须由SelStart提取
              selp2:=redt2.SelStart-length(redt2.lines[i]);
            end;
            lp1:=0;lp2:=0;
            tabnum:=0;
            if (pos(char(#9),sss1)=0)and(pos(char(#9),sss2)=0) then begin
              if (sss1<>sss2)then begin
                redt1.SelStart:=selp1+lp1;
                redt1.SelLength:=Length(sss1);
                redt1.SelAttributes.Color := clRed;
                redt2.SelStart:=selp2+lp2;
                redt2.SelLength:=Length(sss2);
                redt2.SelAttributes.Color := clRed;
              end;
            end else while(pos(char(#9),sss1)>0)or(pos(char(#9),sss2)>0) do begin     //有TAB则分段比较
              pp1:= pos(char(#9),sss1);if (pp1=0) then pp1:=Length(sss1);
              pp2:= pos(char(#9),sss2);if (pp2=0) then pp2:=Length(sss2);
              tabs1:=Copy(sss1,0,pp1);
              tabs2:=Copy(sss2,0,pp2);
              sss1:=Copy(sss1,pp1+1,Length(sss1));
              sss2:=Copy(sss2,pp2+1,Length(sss2));
              tabnum:=tabnum+1;
              if (tabs1<>tabs2)then begin
                if (i=0)and(tabnum<>2)and(tabnum<>18)  then //Continue;       //文件头仅比较版本号
                else if (i>0)and(stependflag=0)and(tabnum=4)  then //Continue;     //步骤的测量值 不比较
                else if (i>0)and(stependflag=1)  then //Continue;        //系统参数表头，不比较
                //if (i>0)and(stependflag=2)  then Continue;
                else if (i>0)and(stependflag=3)  then //Continue;       //短路群表头，不比较
                else begin
                  redt1.SelStart:=selp1+lp1;
                  redt1.SelLength:=pp1;
                  redt1.SelAttributes.Color := clRed;
                  redt2.SelStart:=selp2+lp2;
                  redt2.SelLength:=pp2;
                  redt2.SelAttributes.Color := clRed;
                end;
              end;
              lp1:=lp1+pp1;
              lp2:=lp2+pp2;
            end;

            if(stependflag>0) then
              stependflag:=stependflag+1
            else if (Copy(CompareFileList[i],0,4)='9999')or(Copy(CompareFileList2[i],0,4)='9999') then  begin
              stependflag:=1;
            end;
          end;

        //----------多出来的行，显示蓝色----------------------------------------

          lp1:=0;lp2:=0;
          redt1.SelLength:=0;
          redt1.SelStart:=selp1+lp1;
          redt2.SelLength:=0;
          redt2.SelStart:=selp2+lp2;
          redt1.SelAttributes.Color := clBlue;
          redt2.SelAttributes.Color := clBlue;
          if CompareFileList.Count>CompareFileList2.Count then begin         //文件1有更多行数
            for i:=Count to CompareFileList.Count-1 do begin
              sss1:=StringReplace(CompareFileList[i], Char($FF), '', [rfReplaceAll]);  //              sss:=StringReplace(sss, '@', '', [rfReplaceAll]);
              redt1.SelAttributes.Color := clBlue;
              redt1.Lines.Add(sss1);
              //selp1:=redt1.SelStart-length(redt1.lines[i]);                //selp1:=selp1+length(redt1.Lines[i])+2;      //补偿0D0A换行符
            end;
            selp1:=Length(redt1.Text);
          end;
          if CompareFileList2.Count>CompareFileList.Count then begin       //文件2有更多行数
            for i:=Count to CompareFileList2.Count-1 do begin
              sss2:=StringReplace(CompareFileList2[i], Char($FF), '', [rfReplaceAll]); //              sss:=StringReplace(sss, '@', '', [rfReplaceAll]);
              redt2.SelAttributes.Color := clBlue;
              redt2.Lines.Add(sss2);
              selp2:=redt2.SelStart-length(redt2.lines[i]);                  //selp2:=selp2+length(redt2.Lines[i])+2;
            end;
            selp2:=Length(redt2.Text);
          end;

          lp1:=0;lp2:=0;
          redt1.SelStart:=selp1+lp1;
          redt1.SelLength:=pp1;
          redt2.SelStart:=selp2+lp2;
          redt2.SelLength:=pp2;
          redt1.SelAttributes.Color := clWindowText;
          redt2.SelAttributes.Color := clWindowText;
        end;
        
        CompareFileList.Free;
        CompareFileList2.Free;
        FormCompare.Caption:='CompareFiles Finish! 文件比较结束！';
      except
        ShowMessage('CompareFile Err! 比较文件错误');
      end;

end;
procedure TFormCompare.btn1Click(Sender: TObject);
begin
  if dlgOpen1.Execute then begin
    btn1.hint:= dlgOpen1.FileName;
    grp1.Caption:='File文件1：'+btn1.hint;
    if btn2.hint<>'' then begin
        FileCompare;
    end;
  end;
end;

procedure TFormCompare.FormShow(Sender: TObject);
begin
  btn1.Hint:='';
  btn2.hint:='';
  grp1.Caption:='File文件1：'+btn1.hint;
  grp2.Caption:='File文件2：'+btn2.hint;
  redt1.Clear;
  redt2.Clear;
end;

procedure TFormCompare.btn2Click(Sender: TObject);
begin
 if dlgOpen2.Execute then begin
    btn2.hint:= dlgOpen2.FileName;
    grp2.Caption:='File文件2：'+btn2.hint;
    if btn1.hint<>'' then begin
        FileCompare;
    end;
 end;

end;

procedure TFormCompare.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
 { grp1.Width:=FormCompare.Width div 2;
  grp2.Width:=FormCompare.Width div 2;
  redt1.Height:=grp1.Height-50;
  redt2.Height:=grp2.Height-50; }
end;

procedure TFormCompare.FormResize(Sender: TObject);
begin
  grp1.Width:=FormCompare.Width div 2;
  grp2.Width:=FormCompare.Width div 2;
  redt1.Height:=grp1.Height-50;
  redt2.Height:=grp2.Height-50;

end;



end.
