object FormInsertN: TFormInsertN
  Left = 213
  Top = 287
  Width = 1059
  Height = 440
  Caption = 'InsertNstep '#25554#20837#22810#20010#27493#39588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grp1: TGroupBox
    Left = 24
    Top = 16
    Width = 505
    Height = 57
    Caption = 'Insert '#25554#20837#27493#39588#20301#32622#22810#23569#20010#27493#39588
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object lbl1: TsWebLabel
      Left = 8
      Top = 24
      Width = 105
      Height = 20
      Caption = 'Pos '#25554#20837#20301#32622'--'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -17
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object lbl2: TsWebLabel
      Left = 253
      Top = 24
      Width = 100
      Height = 20
      Caption = 'Sum '#27493#39588#25968'---'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -17
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object edt1: TsDecimalSpinEdit
      Left = 114
      Top = 24
      Width = 121
      Height = 28
      MaxLength = 4
      TabOrder = 0
      Text = '1'
      OnChange = edt1Change
      OnKeyDown = edt1KeyDown
      OnKeyUp = edt1KeyUp
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = -1.000000000000000000
      MaxValue = 4999.000000000000000000
      MinValue = 1.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt2: TsDecimalSpinEdit
      Left = 352
      Top = 24
      Width = 121
      Height = 28
      MaxLength = 4
      TabOrder = 1
      Text = '1'
      OnChange = edt1Change
      OnKeyDown = edt2KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = -1.000000000000000000
      MaxValue = 4999.000000000000000000
      MinValue = 1.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
  end
  object grp2: TsGroupBox
    Left = 20
    Top = 80
    Width = 993
    Height = 313
    Caption = 'Insert '#35774#23450
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'GROUPBOX'
    object lbl4: TLabel
      Left = 40
      Top = 51
      Width = 140
      Height = 20
      Caption = 'Name '#27493#39588#21517#31216' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl5: TLabel
      Left = 40
      Top = 85
      Width = 100
      Height = 20
      Caption = 'Std '#26631#20934#20540
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl6: TLabel
      Left = 58
      Top = 117
      Width = 80
      Height = 20
      Caption = 'Upp'#19978#38480'%'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl7: TLabel
      Left = 330
      Top = 26
      Width = 80
      Height = 20
      Caption = 'Low'#19979#38480'%'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl8: TLabel
      Left = 303
      Top = 58
      Width = 110
      Height = 20
      Caption = 'Hpoint '#39640#28857
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl9: TLabel
      Left = 314
      Top = 90
      Width = 100
      Height = 20
      Caption = 'Lpoint'#20302#28857
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl10: TLabel
      Left = 322
      Top = 122
      Width = 90
      Height = 20
      Caption = 'Delay'#24310#26102
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl11: TLabel
      Left = 582
      Top = 26
      Width = 50
      Height = 20
      Caption = 'K'#27604#20363
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl12: TLabel
      Left = 583
      Top = 58
      Width = 50
      Height = 20
      Caption = 'B'#20559#31227
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl13: TLabel
      Left = 584
      Top = 90
      Width = 60
      Height = 20
      Caption = 'P'#24179#22343' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl14: TLabel
      Left = 554
      Top = 122
      Width = 80
      Height = 20
      Caption = #32852#26495#24207#21495
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl15: TLabel
      Left = 778
      Top = 26
      Width = 70
      Height = 20
      Caption = 'H2'#39640#28857'2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object lbl16: TLabel
      Left = 778
      Top = 58
      Width = 70
      Height = 20
      Caption = 'L2'#20302#28857'2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object chk1: TsCheckBox
      Left = 0
      Top = 12
      Width = 246
      Height = 24
      Caption = 'Num '#24207#21495#29983#25104'('#36873#20013#19981#21024#30053#65289'      '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk2: TsCheckBox
      Left = 0
      Top = 44
      Width = 154
      Height = 24
      Caption = 'Name '#27493#39588#21517#31216'      '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk3: TsCheckBox
      Left = 0
      Top = 76
      Width = 105
      Height = 24
      Caption = 'Std '#26631#20934#20540'  '
      Checked = True
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 2
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk4: TsCheckBox
      Left = 0
      Top = 108
      Width = 104
      Height = 24
      Caption = 'Upp'#19978#38480'%  '
      Checked = True
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 3
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk5: TsCheckBox
      Left = 277
      Top = 14
      Width = 103
      Height = 24
      Caption = 'Low'#19979#38480'%  '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 4
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk6: TsCheckBox
      Left = 276
      Top = 44
      Width = 111
      Height = 24
      Caption = 'Hpoint '#39640#28857'  '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 5
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk7: TsCheckBox
      Left = 276
      Top = 76
      Width = 108
      Height = 24
      Caption = 'Lpoint'#20302#28857'   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 6
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk8: TsCheckBox
      Left = 276
      Top = 108
      Width = 104
      Height = 24
      Caption = 'Delay'#24310#26102'   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 7
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk10: TsCheckBox
      Left = 512
      Top = 12
      Width = 78
      Height = 24
      Caption = 'K'#27604#20363'    '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 8
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk11: TsCheckBox
      Left = 512
      Top = 44
      Width = 75
      Height = 24
      Caption = 'B'#20559#31227'   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 9
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk12: TsCheckBox
      Left = 511
      Top = 76
      Width = 78
      Height = 24
      Caption = 'P'#24179#22343'    '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 10
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk13: TsCheckBox
      Left = 511
      Top = 108
      Width = 100
      Height = 24
      Caption = #32852#26495#24207#21495'    '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 11
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk14: TsCheckBox
      Left = 736
      Top = 12
      Width = 94
      Height = 24
      Caption = 'H2'#39640#28857'2   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 12
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk15: TsCheckBox
      Left = 736
      Top = 44
      Width = 91
      Height = 24
      Caption = 'L2'#20302#28857'2   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 13
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt3: TsDecimalSpinEdit
      Left = 144
      Top = 80
      Width = 97
      Height = 28
      TabOrder = 14
      Text = '0'
      OnKeyDown = edt3KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt4: TsDecimalSpinEdit
      Left = 144
      Top = 112
      Width = 97
      Height = 28
      TabOrder = 15
      Text = '0'
      OnKeyDown = edt4KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt5: TsDecimalSpinEdit
      Left = 416
      Top = 24
      Width = 89
      Height = 28
      TabOrder = 16
      Text = '0'
      OnKeyDown = edt5KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt6: TsDecimalSpinEdit
      Left = 416
      Top = 56
      Width = 89
      Height = 28
      TabOrder = 17
      Text = '0'
      OnKeyDown = edt6KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt7: TsDecimalSpinEdit
      Left = 416
      Top = 88
      Width = 89
      Height = 28
      TabOrder = 18
      Text = '0'
      OnKeyDown = edt7KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt8: TsDecimalSpinEdit
      Left = 416
      Top = 120
      Width = 89
      Height = 28
      TabOrder = 19
      Text = '1'
      OnKeyDown = edt8KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt9: TsDecimalSpinEdit
      Left = 640
      Top = 24
      Width = 73
      Height = 28
      TabOrder = 20
      Text = '100'
      OnKeyDown = edt9KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 100.000000000000000000
      DecimalPlaces = 0
    end
    object edt10: TsDecimalSpinEdit
      Left = 640
      Top = 56
      Width = 73
      Height = 28
      TabOrder = 21
      Text = '0'
      OnKeyDown = edt10KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt11: TsDecimalSpinEdit
      Left = 640
      Top = 88
      Width = 73
      Height = 28
      TabOrder = 22
      Text = '0'
      OnKeyDown = edt11KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt12: TsDecimalSpinEdit
      Left = 640
      Top = 120
      Width = 73
      Height = 28
      TabOrder = 23
      Text = '0'
      OnKeyDown = edt12KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt13: TsDecimalSpinEdit
      Left = 856
      Top = 24
      Width = 81
      Height = 28
      TabOrder = 24
      Text = '0'
      OnKeyDown = edt13KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt14: TsDecimalSpinEdit
      Left = 856
      Top = 56
      Width = 81
      Height = 28
      TabOrder = 25
      Text = '0'
      OnKeyDown = edt14KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt15: TsEdit
      Left = 176
      Top = 48
      Width = 97
      Height = 28
      TabOrder = 26
      Text = 'R???'
      OnKeyDown = edt15KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object grp3: TsGroupBox
      Left = 16
      Top = 148
      Width = 969
      Height = 153
      Caption = 'Mode'
      TabOrder = 27
      SkinData.SkinSection = 'GROUPBOX'
      object lbl3: TLabel
        Left = 208
        Top = 24
        Width = 86
        Height = 20
        Caption = 'Mode'#27169#24335
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = #26032#23435#20307
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edt16: TsEdit
        Left = 296
        Top = 21
        Width = 89
        Height = 28
        TabOrder = 0
        Text = '0R01'
        OnKeyDown = edt16KeyDown
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object chk9: TsCheckBox
        Left = 84
        Top = 17
        Width = 108
        Height = 24
        Caption = 'Mode'#27169#24335'    '
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 1
        Visible = False
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object cbb1: TsComboBox
        Left = 24
        Top = 73
        Width = 201
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Enabled = False
        ItemHeight = 22
        ItemIndex = -1
        TabOrder = 2
        Text = '0---'#37327#31243#36873#25321#25110#27425#27169#24335
        OnChange = cbb1Change
        Items.Strings = (
          '0'#65306#21333#20301#20026'10'#30340'0'#27425#26041
          '1'#65306#21333#20301#20026'10'#30340'1'#27425#26041
          '2'#65306#21333#20301#20026'10'#30340'2'#27425#26041
          '3'#65306#21333#20301#20026'10'#30340'3'#27425#26041
          '4'#65306#21333#20301#20026'10'#30340'4'#27425#26041
          '5'#65306#21333#20301#20026'10'#30340'5'#27425#26041
          '6'#65306#21333#20301#20026'10'#30340'6'#27425#26041
          'F'#65306#22235#32447#30005#38459#21333#20301#20026'mohm'
          '?:'#20854#20182#25193#23637#29992#27861)
      end
      object chk16: TsCheckBox
        Left = 808
        Top = 24
        Width = 93
        Height = 24
        Caption = #19981#26174#31034'     -'
        Enabled = False
        TabOrder = 3
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk17: TsCheckBox
        Left = 808
        Top = 53
        Width = 149
        Height = 24
        Caption = #32467#26524#26174#31034#22312'LCD   -'
        TabOrder = 4
        OnClick = chk17Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk18: TsCheckBox
        Left = 808
        Top = 82
        Width = 121
        Height = 24
        Caption = #27493#39588#21069#26174#31034'    -'
        TabOrder = 5
        OnClick = chk18Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk19: TsCheckBox
        Left = 808
        Top = 112
        Width = 141
        Height = 24
        Caption = #26597#30475#27979#37327#32467#26524'     -'
        TabOrder = 6
        OnClick = chk19Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object cbb2: TsComboBox
        Left = 240
        Top = 73
        Width = 233
        Height = 33
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 27
        ItemIndex = -1
        ParentFont = False
        TabOrder = 7
        Text = '2---'#20027#27979#37327#27169#24335
        OnChange = cbb2Change
        Items.Strings = (
          'O'#65306#24320#36335#27979#35797
          'S'#65306#30701#36335#27979#35797
          'D'#65306#20108#26497#31649#27979#35797
          'R'#65306#30005#38459#27979#35797
          'C'#65306#30005#23481#27979#35797
          'Q'#65306#36755#20986#25511#21046
          'W'#65306#36755#20837#31561#24453
          'V'#65306#39640#39057#23567#20449#21495#27979#37327
          'A'#65306#20018#21475'232/485'#36890#20449
          'B'#65306#19978#20256'PC'
          'Z'#65306#31995#32479#21442#25968#35774#23450
          '0'#65306#32467#26463#27493#39588#65292'FCT6'#20197#19978)
      end
      object cbb3: TsComboBox
        Left = 496
        Top = 73
        Width = 273
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Enabled = False
        ItemHeight = 22
        ItemIndex = 0
        TabOrder = 8
        Text = '0'#65306#22522#26412
        OnChange = cbb3Change
        Items.Strings = (
          '0'#65306#22522#26412
          '1'#65306#19968#27425#25193#23637
          '...'
          '?:'#20854#20182#25193#23637)
      end
    end
    object chk21: TsCheckBox
      Left = 760
      Top = 96
      Width = 106
      Height = 24
      Caption = 'Notes'#22791#27880'   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 28
      OnClick = chk21Click
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt151: TsEdit
      Left = 760
      Top = 120
      Width = 217
      Height = 28
      TabOrder = 29
      Text = '-'
      OnKeyDown = edt15KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object spnl1: TsPanel
    Left = 668
    Top = 24
    Width = 345
    Height = 49
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object btn1: TsButton
      Left = 16
      Top = 8
      Width = 113
      Height = 33
      Caption = 'OK '#30830#23450
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btn1Click
      OnKeyDown = btn1KeyDown
      SkinData.SkinSection = 'BUTTON'
    end
    object btn2: TsButton
      Left = 176
      Top = 8
      Width = 129
      Height = 33
      Caption = 'Cancle '#21462#28040
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btn2Click
      OnKeyDown = btn2KeyDown
      SkinData.SkinSection = 'BUTTON'
    end
  end
end
