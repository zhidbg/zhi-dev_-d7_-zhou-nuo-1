object FormOS: TFormOS
  Left = 229
  Top = 196
  Width = 1199
  Height = 617
  Caption = 'OSwindow '#24320'/'#30701#36335#23398#20064#12289#35843#35797
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Verdana'
  Font.Style = []
  Menu = mm1
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 18
  object grp13: TGroupBox
    Left = 0
    Top = 49
    Width = 529
    Height = 509
    Align = alLeft
    Caption = 'o/s*'#23398#20064#28857#25968
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -17
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object lbl2: TLabel
      Left = 8
      Top = 32
      Width = 63
      Height = 20
      Caption = 'pin'#28857#25968
    end
    object lbl3: TLabel
      Left = 168
      Top = 32
      Width = 72
      Height = 20
      Caption = #23398#20064#38408#20540
    end
    object lbl4: TLabel
      Left = 344
      Top = 32
      Width = 46
      Height = 20
      Caption = #24179#22343'P'
    end
    object edt3: TEdit
      Left = 80
      Top = 24
      Width = 73
      Height = 28
      TabOrder = 0
      OnClick = edt3Click
    end
    object edt12: TEdit
      Left = 248
      Top = 24
      Width = 73
      Height = 28
      TabOrder = 1
      OnClick = edt12Click
    end
    object edt13: TEdit
      Left = 392
      Top = 24
      Width = 73
      Height = 28
      TabOrder = 2
      OnClick = edt13Click
    end
    object mmo1: TMemo
      Left = 2
      Top = 80
      Width = 525
      Height = 427
      Align = alBottom
      Lines.Strings = (
        'shortGroups'#30701#36335#32676#65306)
      ScrollBars = ssVertical
      TabOrder = 3
      OnDblClick = btn4Click
    end
  end
  object tlb1: TToolBar
    Left = 0
    Top = 0
    Width = 1183
    Height = 49
    ButtonHeight = 39
    Caption = 'tlb1'
    TabOrder = 1
    object btn2: TButton
      Left = 0
      Top = 2
      Width = 233
      Height = 39
      Caption = '1'#39's  '#36890#29992#65306#23398#20064#19968#27425#30701#36335#32676
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btn2Click
    end
    object btn4: TButton
      Left = 233
      Top = 2
      Width = 137
      Height = 39
      Caption = 'Read '#35835#30701#36335#32676
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btn4Click
    end
    object btn5: TBitBtn
      Left = 370
      Top = 2
      Width = 137
      Height = 39
      Caption = 'Save '#20445#23384#30701#36335#32676
      TabOrder = 2
      OnClick = btn5Click
    end
    object btn3: TButton
      Left = 507
      Top = 2
      Width = 201
      Height = 39
      Caption = '2'#39's  '#19987#29992#65306#23398#20064#20108#27425#30701#36335#32676
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btn3Click
    end
    object btn6: TButton
      Left = 708
      Top = 2
      Width = 137
      Height = 39
      Caption = #32763#39029' PageDown'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = btn6Click
    end
  end
  object grp1: TGroupBox
    Left = 529
    Top = 49
    Width = 654
    Height = 509
    Align = alClient
    TabOrder = 2
    object lbl1: TLabel
      Left = 30
      Top = 20
      Width = 107
      Height = 18
      Caption = #24320#36335#38408#20540#65288'R'#65289
    end
    object lbl5: TLabel
      Left = 30
      Top = 52
      Width = 107
      Height = 18
      Caption = #30701#36335#38408#20540#65288'R'#65289
    end
    object mmo2: TMemo
      Left = 2
      Top = 88
      Width = 650
      Height = 208
      Align = alBottom
      Lines.Strings = (
        'openNG:')
      ScrollBars = ssBoth
      TabOrder = 0
      OnDblClick = mmo2DblClick
    end
    object mmo3: TMemo
      Left = 2
      Top = 296
      Width = 650
      Height = 211
      Align = alBottom
      Lines.Strings = (
        'shortNG:')
      ScrollBars = ssBoth
      TabOrder = 1
      OnDblClick = mmo3DblClick
    end
    object edt1: TEdit
      Left = 136
      Top = 16
      Width = 57
      Height = 26
      TabOrder = 2
      Text = '50'
      OnClick = edt1Click
    end
    object btn1: TButton
      Left = 200
      Top = 16
      Width = 121
      Height = 25
      Caption = #24320#36335#27979#35797
      TabOrder = 3
      OnClick = btn1Click
    end
    object btn7: TButton
      Left = 200
      Top = 48
      Width = 121
      Height = 25
      Caption = #30701#36335#27979#35797
      TabOrder = 4
      OnClick = btn7Click
    end
    object edt2: TEdit
      Left = 136
      Top = 48
      Width = 57
      Height = 26
      TabOrder = 5
      Text = '200'
      OnClick = edt2Click
    end
    object edt4: TEdit
      Left = 336
      Top = 16
      Width = 73
      Height = 26
      TabOrder = 6
      OnClick = edt4DblClick
      OnDblClick = edt4DblClick
    end
    object edt5: TEdit
      Left = 336
      Top = 48
      Width = 73
      Height = 26
      TabOrder = 7
      OnClick = edt5DblClick
      OnDblClick = edt5DblClick
    end
  end
  object mm1: TMainMenu
    Left = 760
    Top = 32
    object N11: TMenuItem
      Caption = 'ExLearn '#25193#23637#23398#20064
      Enabled = False
      object N12: TMenuItem
        Caption = '1'#39'sA'
        OnClick = N12Click
      end
      object N21: TMenuItem
        Caption = '2'#39'sA'
        OnClick = N21Click
      end
      object N13: TMenuItem
        Caption = '1'#39'sB '#19968#27425#23398#20064#65288'5'#20493#39537#21160#30005#27969#65289
        Enabled = False
        OnClick = N13Click
      end
      object N22: TMenuItem
        Caption = '2'#39'sB '#20108#27425#23398#20064#65288'5'#20493#39537#21160#30005#27969#65289
        OnClick = N22Click
      end
    end
    object N1: TMenuItem
      Caption = #35828#26126
      object N2: TMenuItem
        Caption = #25552#31034
        OnClick = N2Click
      end
    end
  end
  object tmr1: TTimer
    Enabled = False
    Interval = 300
    OnTimer = tmr1Timer
    Left = 888
    Top = 16
  end
end
