object FormInsertCopyN: TFormInsertCopyN
  Left = 201
  Top = 229
  Width = 916
  Height = 581
  Caption = 'InsertCopyNstep '#25554#20837#22797#21046#22810#20010#27493#39588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grp2: TsGroupBox
    Left = 20
    Top = 336
    Width = 933
    Height = 225
    Caption = 'Insert '#35774#23450
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    SkinData.SkinSection = 'GROUPBOX'
    object chk1: TsCheckBox
      Left = 16
      Top = 32
      Width = 246
      Height = 24
      Caption = 'Num '#24207#21495#29983#25104'('#36873#20013#19981#21024#30053#65289'      '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk2: TsCheckBox
      Left = 8
      Top = 64
      Width = 154
      Height = 24
      Caption = 'Name '#27493#39588#21517#31216'      '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk8: TsCheckBox
      Left = 236
      Top = 80
      Width = 104
      Height = 24
      Caption = 'Delay'#24310#26102'   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 2
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk10: TsCheckBox
      Left = 432
      Top = 48
      Width = 78
      Height = 24
      Caption = 'K'#27604#20363'    '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 3
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk14: TsCheckBox
      Left = 656
      Top = 48
      Width = 94
      Height = 24
      Caption = 'H2'#39640#28857'2   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 4
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk15: TsCheckBox
      Left = 656
      Top = 80
      Width = 91
      Height = 24
      Caption = 'L2'#20302#28857'2   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 5
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt8: TsDecimalSpinEdit
      Left = 344
      Top = 80
      Width = 89
      Height = 28
      TabOrder = 6
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      DecimalPlaces = 0
    end
    object edt9: TsDecimalSpinEdit
      Left = 536
      Top = 48
      Width = 73
      Height = 28
      TabOrder = 7
      Text = '100'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 100.000000000000000000
      DecimalPlaces = 0
    end
    object edt13: TsDecimalSpinEdit
      Left = 752
      Top = 48
      Width = 81
      Height = 28
      TabOrder = 8
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt14: TsDecimalSpinEdit
      Left = 752
      Top = 80
      Width = 81
      Height = 28
      TabOrder = 9
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt15: TsEdit
      Left = 168
      Top = 56
      Width = 97
      Height = 28
      TabOrder = 10
      Text = 'R???'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object grp3: TsGroupBox
      Left = 8
      Top = 112
      Width = 969
      Height = 137
      Caption = 'Mode'
      TabOrder = 11
      SkinData.SkinSection = 'GROUPBOX'
      object edt16: TsEdit
        Left = 216
        Top = 13
        Width = 89
        Height = 28
        TabOrder = 0
        Text = '0R01'
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object chk9: TsCheckBox
        Left = 68
        Top = 17
        Width = 108
        Height = 24
        Caption = 'Mode'#27169#24335'    '
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 1
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object cbb1: TsComboBox
        Left = 16
        Top = 49
        Width = 201
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Enabled = False
        ItemHeight = 22
        ItemIndex = -1
        TabOrder = 2
        Text = '1---'#37327#31243#36873#25321#25110#27425#27169#24335
        OnChange = cbb1Change
        Items.Strings = (
          '0'#65306#21333#20301#20026'10'#30340'0'#27425#26041
          '1'#65306#21333#20301#20026'10'#30340'1'#27425#26041
          '2'#65306#21333#20301#20026'10'#30340'2'#27425#26041
          '3'#65306#21333#20301#20026'10'#30340'3'#27425#26041
          '4'#65306#21333#20301#20026'10'#30340'4'#27425#26041
          '5'#65306#21333#20301#20026'10'#30340'5'#27425#26041
          '6'#65306#21333#20301#20026'10'#30340'6'#27425#26041
          'F'#65306#22235#32447#30005#38459#21333#20301#20026'mohm'
          'L'#65306' LED'#27979#37327'0'
          'M'#65306'LED'#27979#37327'1'
          'X'#65306#32487#30005#22120#20999#25442'+'
          'Y'#65306#32487#30005#22120#20999#25442
          'Z'#65306#32487#30005#22120#20851#38381)
      end
      object chk16: TsCheckBox
        Left = 808
        Top = 24
        Width = 37
        Height = 24
        Caption = 'nc'
        Enabled = False
        TabOrder = 3
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk17: TsCheckBox
        Left = 808
        Top = 53
        Width = 144
        Height = 24
        Caption = #32467#26524#26174#31034#22312'LCD   '
        TabOrder = 4
        OnClick = chk17Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk18: TsCheckBox
        Left = 808
        Top = 82
        Width = 100
        Height = 24
        Caption = #27493#39588#21069#26174#31034
        TabOrder = 5
        OnClick = chk18Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk19: TsCheckBox
        Left = 808
        Top = 112
        Width = 116
        Height = 24
        Caption = #26597#30475#27979#37327#32467#26524
        TabOrder = 6
        OnClick = chk19Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object cbb2: TsComboBox
        Left = 232
        Top = 49
        Width = 233
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        ItemHeight = 22
        ItemIndex = -1
        TabOrder = 7
        Text = '2---'#20027#27979#37327#27169#24335
        OnChange = cbb2Change
        Items.Strings = (
          'S'#65306#30701#36335#27979#35797
          'O'#65306#24320#36335#27979#35797
          'R'#65306#30005#38459#27979#35797
          'C'#65306#30005#23481#27979#35797
          'D'#65306#20108#26497#31649#27979#35797
          'Q'#65306#36755#20986#25511#21046
          'G'#65306'Agilent'#19975#29992#34920
          'H'#65306'minghe'
          'A'#65306'485'#36890#20449
          'B'#65306#19978#20256'PC'
          'I'#65306'I2C'#36890#20449
          'P'#65306#36895#24230#27979#37327
          'F'#65306#39057#29575#27979#37327
          'T'#65306#20132#27969#20449#21495
          'Z'#65306#31995#32479#21442#25968#35774#23450
          '')
      end
      object cbb3: TsComboBox
        Left = 488
        Top = 49
        Width = 273
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Enabled = False
        ItemHeight = 22
        ItemIndex = -1
        TabOrder = 8
        Text = '3----'#27425#27979#37327#27169#24335
        OnChange = cbb3Change
        Items.Strings = (
          '0'#65306#38454#27573'1'
          '1'#65306#38454#27573'2'
          '2'#65306#38454#27573'3'
          '3'#65306#38454#27573'4'
          '4'#65306#38454#27573'5'
          'R:'#20132#27969#27979#30005#38459
          'C:'#20132#27969#27979#30005#23481
          'L:'#20132#27969#27979#30005#24863)
      end
    end
    object chk11: TsCheckBox
      Left = 441
      Top = 80
      Width = 63
      Height = 22
      Caption = 'B'#20559#31227
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 12
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt10: TsDecimalSpinEdit
      Left = 520
      Top = 88
      Width = 89
      Height = 28
      TabOrder = 13
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt6: TsDecimalSpinEdit
      Left = 304
      Top = 37
      Width = 121
      Height = 28
      TabOrder = 14
      Text = '0'
      OnChange = edt6Change
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object chk4: TCheckBox
      Left = 304
      Top = 16
      Width = 97
      Height = 17
      Caption = #39640#28857
      TabOrder = 15
    end
  end
  object spnl1: TsPanel
    Left = 728
    Top = 32
    Width = 149
    Height = 145
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object btn1: TsButton
      Left = 8
      Top = 8
      Width = 129
      Height = 33
      Caption = 'OK '#30830#23450
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btn1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object btn2: TsButton
      Left = 8
      Top = 88
      Width = 129
      Height = 33
      Caption = 'Cancle '#21462#28040
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btn2Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object grp1: TGroupBox
    Left = 24
    Top = 16
    Width = 641
    Height = 321
    Caption = 'Insert '#25554#20837'('#27169#26495#27493#39588#22312#25335#36125#27493#39588#20301#32622#21069')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object lbl1: TsWebLabel
      Left = 20
      Top = 24
      Width = 157
      Height = 20
      Caption = 'Pos '#25335#36125#27493#39588#20301#32622'------'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object lbl2: TsWebLabel
      Left = 20
      Top = 73
      Width = 116
      Height = 20
      Caption = 'Sum '#25335#36125#27425#25968'---'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object spl1: TSplitter
      Left = 2
      Top = 212
      Width = 637
      Height = 3
      Align = alNone
      Beveled = True
    end
    object spl2: TSplitter
      Left = 2
      Top = 104
      Width = 637
      Height = 3
      Align = alNone
      Beveled = True
    end
    object lbl3: TsWebLabel
      Left = 372
      Top = 24
      Width = 117
      Height = 20
      Caption = 'Num '#27169#26495#27493#39588#25968
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object lbl4: TsWebLabel
      Left = 372
      Top = 72
      Width = 116
      Height = 20
      Caption = 'PCS'#32852#26495#20559#31227'----'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object lbl11: TsWebLabel
      Left = 20
      Top = 123
      Width = 174
      Height = 20
      Caption = 'PIN '#39640#28857'/'#20302#28857' '#20559#31227'--------'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object lbl21: TsWebLabel
      Left = 20
      Top = 172
      Width = 149
      Height = 20
      Caption = #39640#28857'2/'#20302#28857'2 '#20559#31227'   ---'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object lbl31: TsWebLabel
      Left = 372
      Top = 168
      Width = 105
      Height = 20
      Caption = #31532'1'#22823#20998#21306#28857#25968
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object lbl211: TsWebLabel
      Left = 20
      Top = 272
      Width = 142
      Height = 20
      Caption = #22797#21046#30701#36335#32676#27425#25968'------'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object edt1: TsDecimalSpinEdit
      Left = 200
      Top = 21
      Width = 121
      Height = 28
      Color = clSkyBlue
      MaxLength = 4
      TabOrder = 0
      Text = '2'
      OnChange = edt1Change
      OnKeyDown = edt1KeyDown
      OnKeyUp = edt1KeyUp
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = -1.000000000000000000
      MaxValue = 4999.000000000000000000
      MinValue = 1.000000000000000000
      Value = 2.000000000000000000
      DecimalPlaces = 0
    end
    object edt2: TsDecimalSpinEdit
      Left = 200
      Top = 69
      Width = 121
      Height = 28
      Color = clSkyBlue
      MaxLength = 4
      TabOrder = 1
      Text = '1'
      OnKeyDown = edt2KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 4999.000000000000000000
      MinValue = 1.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt3: TsDecimalSpinEdit
      Left = 496
      Top = 21
      Width = 97
      Height = 28
      Color = clSkyBlue
      TabOrder = 2
      Text = '1'
      OnChange = edt1Change
      OnKeyDown = edt3KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      MinValue = 1.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt4: TsDecimalSpinEdit
      Left = 200
      Top = 118
      Width = 121
      Height = 28
      Color = clMoneyGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Text = '0'
      OnKeyDown = edt4KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      MinValue = -9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt5: TsDecimalSpinEdit
      Left = 496
      Top = 70
      Width = 97
      Height = 28
      Color = clSkyBlue
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      Text = '0'
      OnKeyDown = edt5KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      MinValue = -9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt11: TsDecimalSpinEdit
      Left = 200
      Top = 167
      Width = 121
      Height = 28
      Color = clMoneyGreen
      TabOrder = 5
      Text = '0'
      OnKeyDown = edt11KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      MinValue = -9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt12: TsDecimalSpinEdit
      Left = 496
      Top = 167
      Width = 89
      Height = 28
      Color = clMoneyGreen
      TabOrder = 6
      Text = '0'
      OnKeyDown = edt12KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt7: TsDecimalSpinEdit
      Left = 200
      Top = 270
      Width = 121
      Height = 28
      TabOrder = 7
      Text = '0'
      OnKeyDown = edt7KeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object chk3: TCheckBox
      Left = 24
      Top = 232
      Width = 193
      Height = 17
      Caption = #22797#21046#30701#36335#32676#24635#28857#25968
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = chk3Click
      OnKeyDown = chk3KeyDown
    end
  end
end
