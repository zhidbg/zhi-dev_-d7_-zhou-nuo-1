object FormFind: TFormFind
  Left = 235
  Top = 241
  Width = 789
  Height = 418
  Caption = 'Search Pin --'#27979#35797#38024#26816#27979'-'#25214#28857
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  Menu = mm1
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 8
    Top = 8
    Width = 107
    Height = 13
    Caption = 'Command'#25193#23637#21629#20196
  end
  object lbl2: TLabel
    Left = 152
    Top = 8
    Width = 29
    Height = 13
    Caption = 'PIN#'
    Enabled = False
  end
  object lbl3: TLabel
    Left = 288
    Top = 8
    Width = 32
    Height = 13
    Caption = 'IniAD'
    Enabled = False
  end
  object lbl4: TLabel
    Left = 152
    Top = 59
    Width = 112
    Height = 16
    Caption = 'Probe '#21457#29616#28857#21495
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
  end
  object edt1: TEdit
    Left = 8
    Top = 24
    Width = 113
    Height = 21
    TabOrder = 0
    Text = 'edt1'
    OnClick = edt1Click
  end
  object edt2: TEdit
    Left = 152
    Top = 24
    Width = 121
    Height = 21
    Enabled = False
    TabOrder = 1
    Text = 'edt2'
  end
  object edt3: TEdit
    Left = 288
    Top = 24
    Width = 121
    Height = 21
    Enabled = False
    TabOrder = 2
    Text = 'edt3'
  end
  object mmo1: TMemo
    Left = 152
    Top = 80
    Width = 601
    Height = 257
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Lines.Strings = (
      '')
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object mmo2: TMemo
    Left = 8
    Top = 48
    Width = 113
    Height = 289
    Enabled = False
    Lines.Strings = (
      #21629#20196#21015#34920#65306
      '101'#65306#36890#29992#25214#28857
      '105'#65306#26597#30475'HD-LS'#38459#20540
      '106'#65306#26597#30475'HS-LD'#38459#20540
      '111'#65306#20986#21378#27979#35797#36755#20837#36755#20986
      '112'#65306#32487#30005#22120#20999#25442#26495#26816#27979
      '113'#65306#25351#31034#28783#26495#26816#27979
      '114'#65306#26597#30475#33258#26816#27874#24418
      ''
      '133'#65306'SIM'#21345'#01'#25214#28857
      '134'#65306'SIM'#21345'#02'#25214#28857
      '...'
      '148'#65306'SIM'#21345'#16'#25214#28857)
    ScrollBars = ssHorizontal
    TabOrder = 4
  end
  object tmr1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = tmr1Timer
    Left = 744
    Top = 56
  end
  object mm1: TMainMenu
    Left = 232
    Top = 8
    object b1: TMenuItem
      Caption = #27874#24418
      object N11: TMenuItem
        Caption = '101'#25214#28857#27874#24418
        Enabled = False
        OnClick = N11Click
      end
      object N12: TMenuItem
        Caption = '114'#26597#30475#33258#26816#27874#24418
        Enabled = False
        OnClick = N12Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object t1: TMenuItem
        Caption = #20572#27490#25214#28857
        Enabled = False
        OnClick = t1Click
      end
    end
    object N4: TMenuItem
      Caption = #21629#20196
      object D1: TMenuItem
        Caption = #20986#21378#27979#35797'DIDO'#29366#24577
        OnClick = D1Click
      end
    end
    object N2: TMenuItem
      Caption = #35828#26126
      object N3: TMenuItem
        Caption = #25552#31034
        OnClick = N3Click
      end
    end
  end
end
