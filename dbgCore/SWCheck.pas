unit SWCheck;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Unit0_globalVariant;

type
  TFormSWcheck = class(TForm)
    mmo1: TMemo;
    btn2: TButton;
    procedure btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSWcheck: TFormSWcheck;

implementation

uses main, SWave;

{$R *.dfm}

procedure TFormSWcheck.btn1Click(Sender: TObject);
begin
  CLOSE;
end;

procedure TFormSWcheck.FormShow(Sender: TObject);
begin
  //btn1.SetFocus;
end;

procedure TFormSWcheck.btn2Click(Sender: TObject);
begin
  modbusfun06dat:=$00F7;
  modbusfun06:=$0001;
  mmo1.Clear;
  mmo1.Lines.Add('Check Again...');
end;

procedure TFormSWcheck.btn3Click(Sender: TObject);
begin
  form_main.ts3.TabVisible:=True;
  form_main.PageControl1.TabIndex:=5;
  formwave.showmodal;
end;

end.
