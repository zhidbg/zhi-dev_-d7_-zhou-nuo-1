unit SWave;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, iComponent, iVCLComponent, iCustomComponent,
  iPlotComponent, iPlot,Unit0_globalVariant, sButton, ExtCtrls, Menus,
  sEdit;

type
  TFormWave = class(TForm)
    iplt1: TiPlot;
    btn2: TButton;
    btn3: TButton;
    edt1: TEdit;
    edt2: TEdit;
    edt3: TEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    tmr1: TTimer;
    mm1: TMainMenu;
    F1: TMenuItem;
    E1: TMenuItem;
    N1: TMenuItem;
    s1: TMenuItem;
    S2: TMenuItem;
    N2: TMenuItem;
    C1: TMenuItem;
    R1: TMenuItem;
    grp1: TGroupBox;
    N3: TMenuItem;
    c2: TMenuItem;
    N4: TMenuItem;
    A1: TMenuItem;
    A2: TMenuItem;
    C3: TMenuItem;
    C4: TMenuItem;
    C5: TMenuItem;
    C6: TMenuItem;
    C7: TMenuItem;
    C8: TMenuItem;
    C9: TMenuItem;
    C10: TMenuItem;
    C11: TMenuItem;
    C12: TMenuItem;
    C13: TMenuItem;
    C14: TMenuItem;
    C15: TMenuItem;
    C16: TMenuItem;
    C17: TMenuItem;
    S3: TMenuItem;
    S4: TMenuItem;
    edt4: TEdit;
    edt5: TEdit;
    lbl4: TLabel;
    lbl5: TLabel;
    btn4: TBitBtn;
    T1: TMenuItem;
    S5: TMenuItem;
    R2: TMenuItem;
    btn1: TButton;
    shp4: TShape;
    N5: TMenuItem;
    N6: TMenuItem;
    trigSyn: TMenuItem;
    edt6: TEdit;
    procedure btn45Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure C1Click(Sender: TObject);
    procedure R1Click(Sender: TObject);
    procedure E1Click(Sender: TObject);
    procedure c2Click(Sender: TObject);
    procedure C3Click(Sender: TObject);
    procedure C17Click(Sender: TObject);
    procedure C16Click(Sender: TObject);
    procedure C15Click(Sender: TObject);
    procedure C14Click(Sender: TObject);
    procedure C13Click(Sender: TObject);
    procedure C11Click(Sender: TObject);
    procedure C12Click(Sender: TObject);
    procedure C10Click(Sender: TObject);
    procedure C9Click(Sender: TObject);
    procedure C8Click(Sender: TObject);
    procedure C7Click(Sender: TObject);
    procedure C6Click(Sender: TObject);
    procedure C5Click(Sender: TObject);
    procedure C4Click(Sender: TObject);
    procedure A2Click(Sender: TObject);
    procedure A1Click(Sender: TObject);
    procedure S4Click(Sender: TObject);
    procedure edt4Change(Sender: TObject);
    procedure edt5Change(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure iplt1ClickChannel(Index: Integer);
    procedure R2Click(Sender: TObject);
    procedure S5Click(Sender: TObject);
    procedure iplt1DblClickLegend(Index: Integer);
    procedure N6Click(Sender: TObject);
    procedure trigSynClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormWave: TFormWave;

implementation

uses main;

{$R *.dfm}

procedure TFormWave.btn45Click(Sender: TObject);
begin
    iplt1.ClearAllData;
   { iplt1.Channel[0].Clear ;
    iplt1.Channel[1].Clear ;
    iplt1.Channel[2].Clear ;
    iplt1.Channel[3].Clear ;
    iplt1.Channel[4].Clear ;
    iplt1.Channel[5].Clear ;     }
    px0:=0;px1:=0;//px2:=0;px3:=0;px4:=0;px5:=0;

end;

procedure TFormWave.btn1Click(Sender: TObject);
begin
{   Writechis('result','YaxisUnit',edt4.Text);
   iplt1.YAxis[0].Title:='unit:'+ Readchis('result','YaxisUnit');

   Writechis('result','XaxisUnit',edt5.Text);
   iplt1.XAxis[0].Title:='unit:'+ Readchis('result','XaxisUnit');
  }
end;

procedure TFormWave.btn3Click(Sender: TObject);
begin
  btn2.Enabled:=True;
  btn3.Enabled:=False;
  tmr1.Enabled:=False;      //关闭波形查看
  shp4.Brush.Color:=clSilver;
end;


procedure TFormWave.btn2Click(Sender: TObject);
begin
  tmr1.Enabled:=True;     //启动波形查看
  shp4.Brush.Color:=clGreen;
  btn3.Enabled:=True;
  btn2.Enabled:=False;
end;

procedure TFormWave.tmr1Timer(Sender: TObject);
begin
    modbusfun16int:=$00D2;     //读取选定通道波形 ，定时发送读取命令！
    modbusfun16len:=$0004;
    sbuf[2]:=$10;

    sbuf[8+2*0]:=StrToInt('0'+edt1.Text) div 256;
    sbuf[9+2*0]:=StrToInt('0'+edt1.Text) mod 256;
    sbuf[8+2*1]:=StrToInt('0'+edt2.Text) div 256;
    sbuf[9+2*1]:=StrToInt('0'+edt2.Text) mod 256;
    if(edt3.Text='') or (edt3.Text='0') then edt3.Text:='1';
    sbuf[8+2*2]:=StrToInt('0'+edt3.Text) div 256;
    sbuf[9+2*2]:=StrToInt('0'+edt3.Text) mod 256;  //由固件设置  if IsNumberic(edt3.Text) then   //    px_internal:=StrToInt('0'+edt3.Text);
    sbuf[8+2*3]:=$00 div 256;
    sbuf[9+2*3]:=$00 mod 256;
end;

procedure TFormWave.FormShow(Sender: TObject);
begin
  px_internal:=1;
  px0RxdNum:=0;
  px1RxdNum:=0;
  if Readchis('result','XaxisUnit')<>'' then begin
    iplt1.XAxis[0].Title:='unit:'+ Readchis('result','XaxisUnit');
    edt5.Text:=Readchis('result','XaxisUnit');
  end;
  if Readchis('result','YaxisUnit')<>'' then begin
    iplt1.YAxis[0].Title:='unit:'+ Readchis('result','YaxisUnit');
    edt4.Text:=Readchis('result','YaxisUnit');
  end;
  btn4.Font.Color:=clWindowText;
  btn2Click(self);
end;

procedure TFormWave.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  btn2.Enabled:=True;
  btn3.Enabled:=False;
  tmr1.Enabled:=False;

  modbusfun16int:=$00;     //清空命令 ，否则debug响应延长几秒。
  modbusfun16len:=$00;
  usb_busyT:=(5 div 5)+1;
end;

procedure TFormWave.C1Click(Sender: TObject);
begin
    iplt1.ClearAllData;
   { iplt1.Channel[0].Clear ;
    iplt1.Channel[1].Clear ;
    iplt1.Channel[2].Clear ;
    iplt1.Channel[3].Clear ;
    iplt1.Channel[4].Clear ;
    iplt1.Channel[5].Clear ;   }
    px0:=0;px1:=0;//px2:=0;px3:=0;px4:=0;px5:=0;

end;

procedure TFormWave.R1Click(Sender: TObject);
begin
      modbusfun06dat:=$00D0;    //波形显示
      modbusfun06:=$0001;

end;

procedure TFormWave.E1Click(Sender: TObject);
begin
  Close;
end;


procedure TFormWave.C17Click(Sender: TObject);
begin
    if iplt1.Channel[15].Visible then  iplt1.Channel[15].Visible:=False else  iplt1.Channel[15].Visible:=True;
    if iplt1.Channel[15].Visible then
      iplt1.Channel[15].TitleText:='CH16'
    else
      iplt1.Channel[15].TitleText:='CH16关闭x';

end;

procedure TFormWave.C16Click(Sender: TObject);
begin
    if iplt1.Channel[14].Visible then  iplt1.Channel[14].Visible:=False else  iplt1.Channel[14].Visible:=True;
    if iplt1.Channel[14].Visible then
      iplt1.Channel[14].TitleText:='CH15'
    else
      iplt1.Channel[14].TitleText:='CH15关闭x';

end;

procedure TFormWave.C15Click(Sender: TObject);
begin
    if iplt1.Channel[13].Visible then  iplt1.Channel[13].Visible:=False else  iplt1.Channel[13].Visible:=True;
    if iplt1.Channel[13].Visible then
      iplt1.Channel[13].TitleText:='CH14'
    else
      iplt1.Channel[13].TitleText:='CH14关闭x';

end;

procedure TFormWave.C14Click(Sender: TObject);
begin
    if iplt1.Channel[12].Visible then  iplt1.Channel[12].Visible:=False else  iplt1.Channel[12].Visible:=True;
    if iplt1.Channel[12].Visible then
      iplt1.Channel[12].TitleText:='CH13'
    else
      iplt1.Channel[12].TitleText:='CH13关闭x';

end;

procedure TFormWave.C13Click(Sender: TObject);
begin
    if iplt1.Channel[11].Visible then  iplt1.Channel[11].Visible:=False else  iplt1.Channel[11].Visible:=True;
    if iplt1.Channel[11].Visible then
      iplt1.Channel[11].TitleText:='CH12'
    else
      iplt1.Channel[11].TitleText:='CH12关闭x';

end;


procedure TFormWave.C12Click(Sender: TObject);
begin
    if iplt1.Channel[10].Visible then  iplt1.Channel[10].Visible:=False else  iplt1.Channel[10].Visible:=True;
    if iplt1.Channel[10].Visible then
      iplt1.Channel[10].TitleText:='CH11'
    else
      iplt1.Channel[10].TitleText:='CH11关闭x';

end;

procedure TFormWave.C11Click(Sender: TObject);
begin
      if iplt1.Channel[9].Visible then  iplt1.Channel[9].Visible:=False else  iplt1.Channel[9].Visible:=True;
    if iplt1.Channel[9].Visible then
      iplt1.Channel[9].TitleText:='CH10'
    else
      iplt1.Channel[9].TitleText:='CH10关闭x';

end;

procedure TFormWave.C10Click(Sender: TObject);
begin
    if iplt1.Channel[8].Visible then  iplt1.Channel[8].Visible:=False else  iplt1.Channel[8].Visible:=True;
    if iplt1.Channel[8].Visible then
      iplt1.Channel[8].TitleText:='CH09'
    else
      iplt1.Channel[8].TitleText:='CH09关闭x';

end;

procedure TFormWave.C9Click(Sender: TObject);
begin
    if iplt1.Channel[7].Visible then  iplt1.Channel[7].Visible:=False else  iplt1.Channel[7].Visible:=True;
    if iplt1.Channel[7].Visible then
      iplt1.Channel[7].TitleText:='CH08'
    else
      iplt1.Channel[7].TitleText:='CH08关闭x';

end;

procedure TFormWave.C8Click(Sender: TObject);
begin
    if iplt1.Channel[6].Visible then  iplt1.Channel[6].Visible:=False else  iplt1.Channel[6].Visible:=True;
    if iplt1.Channel[6].Visible then
      iplt1.Channel[6].TitleText:='CH07'
    else
      iplt1.Channel[6].TitleText:='CH07关闭x';

end;

procedure TFormWave.C7Click(Sender: TObject);
begin
    if iplt1.Channel[5].Visible then  iplt1.Channel[5].Visible:=False else  iplt1.Channel[5].Visible:=True;
    if iplt1.Channel[5].Visible then
      iplt1.Channel[5].TitleText:='CH06'
    else
      iplt1.Channel[5].TitleText:='CH06关闭x';

end;

procedure TFormWave.C6Click(Sender: TObject);
begin
    if iplt1.Channel[4].Visible then  iplt1.Channel[4].Visible:=False else  iplt1.Channel[4].Visible:=True;
    if iplt1.Channel[4].Visible then
      iplt1.Channel[4].TitleText:='CH05'
    else
      iplt1.Channel[4].TitleText:='CH05关闭x';

end;

procedure TFormWave.C5Click(Sender: TObject);
begin
    if iplt1.Channel[3].Visible then  iplt1.Channel[3].Visible:=False else  iplt1.Channel[3].Visible:=True;
    if iplt1.Channel[3].Visible then
      iplt1.Channel[3].TitleText:='CH04'
    else
      iplt1.Channel[3].TitleText:='CH04关闭x';

end;

procedure TFormWave.C4Click(Sender: TObject);
begin
    if iplt1.Channel[2].Visible then  iplt1.Channel[2].Visible:=False else  iplt1.Channel[2].Visible:=True;
    if iplt1.Channel[2].Visible then
      iplt1.Channel[2].TitleText:='CH03'
    else
      iplt1.Channel[2].TitleText:='CH03关闭x';

end;

procedure TFormWave.C3Click(Sender: TObject);
begin
    if iplt1.Channel[1].Visible then  iplt1.Channel[1].Visible:=False else  iplt1.Channel[1].Visible:=True;
    if iplt1.Channel[1].Visible then
      iplt1.Channel[1].TitleText:='CH02'
    else
      iplt1.Channel[1].TitleText:='CH02关闭x';
end;
procedure TFormWave.c2Click(Sender: TObject);
begin
    if iplt1.Channel[0].Visible then  iplt1.Channel[0].Visible:=False else  iplt1.Channel[0].Visible:=True;
    if iplt1.Channel[0].Visible then
      iplt1.Channel[0].TitleText:='CH01'
    else
      iplt1.Channel[0].TitleText:='CH01关闭x';
end;

procedure TFormWave.A2Click(Sender: TObject);
begin
      iplt1.Channel[0].Visible:=False;     iplt1.Channel[0].TitleText:='CH01关闭x';
      iplt1.Channel[1].Visible:=False;     iplt1.Channel[1].TitleText:='CH02关闭x';
      iplt1.Channel[2].Visible:=False;     iplt1.Channel[2].TitleText:='CH03关闭x';
      iplt1.Channel[3].Visible:=False;     iplt1.Channel[3].TitleText:='CH04关闭x';
      iplt1.Channel[4].Visible:=False;     iplt1.Channel[4].TitleText:='CH05关闭x';
      iplt1.Channel[5].Visible:=False;     iplt1.Channel[5].TitleText:='CH06关闭x';
      iplt1.Channel[6].Visible:=False;     iplt1.Channel[6].TitleText:='CH07关闭x';
      iplt1.Channel[7].Visible:=False;     iplt1.Channel[7].TitleText:='CH08关闭x';
      iplt1.Channel[8].Visible:=False;     iplt1.Channel[8].TitleText:='CH09关闭x';
      iplt1.Channel[9].Visible:=False;     iplt1.Channel[9].TitleText:='CH10关闭x';
      iplt1.Channel[10].Visible:=False;    iplt1.Channel[10].TitleText:='CH11关闭x';
      iplt1.Channel[11].Visible:=False;    iplt1.Channel[11].TitleText:='CH12关闭x';
      iplt1.Channel[12].Visible:=False;    iplt1.Channel[12].TitleText:='CH13关闭x';
      iplt1.Channel[13].Visible:=False;    iplt1.Channel[13].TitleText:='CH14关闭x';
      iplt1.Channel[14].Visible:=False;    iplt1.Channel[14].TitleText:='CH15关闭x';
      iplt1.Channel[15].Visible:=False;    iplt1.Channel[15].TitleText:='CH16关闭x';
end;

procedure TFormWave.A1Click(Sender: TObject);
begin
      iplt1.Channel[0].Visible:=True;        iplt1.Channel[0].TitleText:='CH01';
      iplt1.Channel[1].Visible:=True;        iplt1.Channel[1].TitleText:='CH02';
      iplt1.Channel[2].Visible:=True;        iplt1.Channel[2].TitleText:='CH03';
      iplt1.Channel[3].Visible:=True;        iplt1.Channel[3].TitleText:='CH04';
      iplt1.Channel[4].Visible:=True;        iplt1.Channel[4].TitleText:='CH05';
      iplt1.Channel[5].Visible:=True;        iplt1.Channel[5].TitleText:='CH06';
      iplt1.Channel[6].Visible:=True;        iplt1.Channel[6].TitleText:='CH07';
      iplt1.Channel[7].Visible:=True;        iplt1.Channel[7].TitleText:='CH08';
      iplt1.Channel[8].Visible:=True;        iplt1.Channel[8].TitleText:='CH09';
      iplt1.Channel[9].Visible:=True;        iplt1.Channel[9].TitleText:='CH10';
      iplt1.Channel[10].Visible:=True;       iplt1.Channel[10].TitleText:='CH11';
      iplt1.Channel[11].Visible:=True;       iplt1.Channel[11].TitleText:='CH12';
      iplt1.Channel[12].Visible:=True;       iplt1.Channel[12].TitleText:='CH13';
      iplt1.Channel[13].Visible:=True;       iplt1.Channel[13].TitleText:='CH14';
      iplt1.Channel[14].Visible:=True;       iplt1.Channel[14].TitleText:='CH15';
      iplt1.Channel[15].Visible:=True;       iplt1.Channel[15].TitleText:='CH16';

end;

procedure TFormWave.S4Click(Sender: TObject);
begin
  if btn2.Enabled then begin
    if not S4.Checked then begin      //读取自检波形。
       modbusfun06dat:=114;
       modbusfun06:=$1080;
       S4.Checked:=True;
    end else begin
       modbusfun06dat:=101;
       modbusfun06:=$1080;
       S4.Checked:=False;
    end;
  end;
end;

procedure TFormWave.edt4Change(Sender: TObject);
begin
  btn4.Font.Color:=clRed;
end;

procedure TFormWave.edt5Change(Sender: TObject);
begin
  btn4.Font.Color:=clRed;
end;

procedure TFormWave.btn4Click(Sender: TObject);
begin

   Writechis('result','XaxisUnit',edt5.Text);
   iplt1.XAxis[0].Title:='unit:'+ Readchis('result','XaxisUnit');

   Writechis('result','YaxisUnit',edt4.Text);
   iplt1.YAxis[0].Title:='unit:'+ Readchis('result','YaxisUnit');

   btn4.Font.Color:=clWindowtext;
end;

procedure TFormWave.iplt1ClickChannel(Index: Integer);
begin
  if Index=0 then   c2Click(Self);
  if Index=1 then   c3Click(Self);
  if Index=2 then   c4Click(Self);
  if Index=3 then   c5Click(Self);
  if Index=4 then   c6Click(Self);
  if Index=5 then   c7Click(Self);
  if Index=6 then   c8Click(Self);
  if Index=7 then   c9Click(Self);
  if Index=8 then   c10Click(Self);
  if Index=9 then   c11Click(Self);
  if Index=10 then   c12Click(Self);
  if Index=11 then   c13Click(Self);
  if Index=12 then   c14Click(Self);
  if Index=13 then   c15Click(Self);
  if Index=14 then   c16Click(Self);
  if Index=15 then   c17Click(Self);

end;

procedure TFormWave.R2Click(Sender: TObject);
begin
  form_main.btn26Click(form_main);
end;

procedure TFormWave.S5Click(Sender: TObject);
begin
  form_main.btn25Click(form_main);
end;

procedure TFormWave.iplt1DblClickLegend(Index: Integer);
begin
    if iplt1.Channel[0].Visible=False  then
      A1Click(self)
    else
      A2Click(Self);
end;

procedure TFormWave.N6Click(Sender: TObject);
begin
  if n6.Checked then N6.Checked:=False else N6.Checked:=true;
end;

procedure TFormWave.trigSynClick(Sender: TObject);
begin
  if trigSyn.Checked then trigSyn.Checked:=False else trigSyn.Checked:=true;
end;

end.
