{  描述：     main主窗口生成及显示专用函数文件；
  文件名：	  Unit_MaincreateSHowCode...
  应用语言:   delphi
  版本：			V1.0
  zhi至研测控 http://www.zhzhi.cn
  Copyright(C) Jin 2013   All rights reserved
  作者：Jin        公众号：zhi至研测控
  建立日期：2019-05-09
//=======重点===================================================================
//1.主窗口生成cReate函数，
//2.主窗口显示函数Show函数；
}

unit Unit1_MainCreateShowlCode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ActnList, NB30, registry, shellapi, dbclient, inifiles, db,
  adodb, iComponent, iVCLComponent, iCustomComponent, iPlotComponent, iPlot,
  IdUDPBase, IdUDPServer, IdBaseComponent, IdComponent, IdIPWatch, ScktComp,
  IdAntiFreezeBase, IdAntiFreeze, IdTCPConnection, IdTCPClient, IdSocketHandle,
  IdUDPClient, Menus, ToolWin, sScrollBox, iLed, iLedRound, sMemo, iLabel,
  iEditCustom, iEdit, iLedArrow, sLabel, iLedMatrix, //RzLaunch,
  FileCtrl, StdCtrls;

 /////////////--------------------函数------------------------------------
procedure MatrixLed_createProcess(len: integer);         //矩阵LED指示灯生成

procedure ReadConfig_iniProcess(len: integer);         //读取Ini文件，并配置PC软件

procedure PNbarcode_fileCreateProcess(len: integer);

procedure sysParaKey(Sender: TObject; key: Word; hh: SmallInt);

procedure stepKeyEdit(key: Word; hh: SmallInt);

procedure bakToServerDisk();

implementation

uses
  StepCopy, main, OSstudy, FindPoint, PNenter, LoadFile, Unit0_globalVariant,
  Unit4_logFileCode, InsertNstep;

procedure ReadConfig_iniProcess(len: integer);         //读取Ini文件，并配置PC软件
var
  temp: byte;
  i: SmallInt;
  nrf_dat_config: Tinifile;
  save_file_name, ini_path, sst: string;
begin

  with form_main do begin
    /////////////读配置文件---------------------------------------------------
    ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
    try
      nrf_dat_config := Tinifile.Create(ini_path);    //关联INI文件

    //-----------------------须提前读取的[Comm Para]组机种参数---------------------------------------------------------------------
      if nrf_dat_config.Readstring('Comm Para', 'Path', '') <> '' then begin
        edt2.Text := nrf_dat_config.Readstring('Comm Para', 'Path', '');
        if not DirectoryExists(edt2.Text) then                            //FileExists
          edt2.Text := ExtractFilePath(application.ExeName);
      end
      else
        edt2.Text := ExtractFilePath(application.ExeName);

      if nrf_dat_config.Readstring('Comm Para', 'Uplocal', '') <> '' then
        edt38.Text := nrf_dat_config.Readstring('Comm Para', 'Uplocal', '')     //是否保存在本地磁盘
      else
        edt38.Text := '0';
      if nrf_dat_config.Readstring('Comm Para', 'barcodeConfig', '') <> '' then begin
        Label7.hint := nrf_dat_config.Readstring('Comm Para', 'barcodeConfig', '');     //是否清barcode
      end
      else begin
        if nrf_dat_config.Readstring('Comm Para', 'clearbarcode', '') <> '' then
          Label7.hint := nrf_dat_config.Readstring('Comm Para', 'clearbarcode', '');     //是否清barcode
      end;
      if not IsNumberic(Label7.hint) then
        Label7.hint := '2'
      else begin
        if StrToInt(Label7.hint) >= 1000 then begin
          grp36.Visible := false;
          Label7.hint := IntToStr(StrToInt(Label7.hint) mod 1000)
        end;
      end;
      if Label7.hint = '2' then begin      //不使能条码
        Label7.Color := clGray;
      end
      else begin
        Label7.Color := clGreen;
      end;
      if Label7.hint = '11' then begin     //双工位使能
        medit1.Visible := True;
        iLedRound1.Visible := true;
        iLedRound2.Visible := true;
        iLabel1.Visible := True;
        iLabel2.Visible := true;
        if (Label7.hint = '11') then begin          //位置对齐
          iledround1.Top := Rleds[0].Top;
          iledround1.Left := Rleds[0].Left;
          ilabel1.Top := iledround1.Top + 70;
          ilabel1.Left := iledround1.Left + 50;

          iledround2.Top := Rleds[1].Top;
          iledround2.Left := Rleds[1].Left;
          ilabel2.Top := iledround2.Top + 70;
          ilabel2.Left := iledround2.Left + 50;
        end;
      end
      else begin
        medit1.Visible := False;
        edit1.Width := 800;
      end;

      if nrf_dat_config.Readstring('Comm Para', 'barcodeMinLen', '') <> '' then      //barcodeMinlen 最小长度
        edt55.Text := nrf_dat_config.Readstring('Comm Para', 'barcodeMinLen', '')
      else
        edt55.Text := '0';
      if nrf_dat_config.Readstring('Comm Para', 'barcodeSelT', '') <> '' then begin     //barcode长度
        edt76.Text := nrf_dat_config.Readstring('Comm Para', 'barcodeSelT', '');
        edt76.Visible := true;
        lbl139.Visible := True;
      end;
    //------------------------------------[Result]组机种参数---------------------------------------------------------------------

      if nrf_dat_config.Readstring('result', 'PassOverT', '') <> '' then
        edt40.Text := nrf_dat_config.Readstring('result', 'PassOverT', '');
      if nrf_dat_config.Readstring('result', 'NGOverT', '') <> '' then
        edt42.Text := nrf_dat_config.Readstring('result', 'NGOverT', '');

      if nrf_dat_config.Readstring('result', 'StopMaxCount', '') <> '' then     //计数到，停止工作
      begin
        edt111.Text := nrf_dat_config.Readstring('result', 'StopMaxCount', '');
        form_main.grp46.Visible := True;
      end
      else begin
        btn75.Visible := false;
        edt111.Visible := False;
      end;
      if nrf_dat_config.Readstring('result', 'StopYield', '') <> '' then     //良率低于，停止工作
      begin
        if IsNumberic(nrf_dat_config.Readstring('result', 'StopYield', '')) then
          lbl82.Tag := StrToInt(nrf_dat_config.Readstring('result', 'StopYield', ''));
      end
      else begin
        if nrf_dat_config.Readstring('Comm Para', 'CSVen', '') = '2' then
          lbl82.Tag := 90;
      end;
      if lbl82.Tag > 0 then begin
        lbl82.Color := clGreen;
        lbl82.hint := '>=' + inttostr(lbl82.Tag) + '%';
      end;

      if nrf_dat_config.Readstring('result', 'StopYieldTotal', '') <> '' then     //启用良率的OK数量，停止工作
      begin
        if IsNumberic(nrf_dat_config.Readstring('result', 'StopYieldTotal', '')) then
          lbl80.Tag := StrToInt(nrf_dat_config.Readstring('result', 'StopYieldTotal', ''));
      end
      else begin
        if nrf_dat_config.Readstring('Comm Para', 'CSVen', '') = '2' then
          lbl80.Tag := 20;
      end;
      if lbl80.Tag > 0 then begin
        lbl80.Color := clGreen;
        lbl80.hint := '>=' + inttostr(lbl80.Tag);
      end;

      if nrf_dat_config.Readstring('result', 'CloseReport', '') = '1' then begin
        grp17.Visible := False;
        lbl169.Left := 0;
        Label11.Left := 8;
      end
      else begin
        grp17.Visible := true;
        lbl169.Left := 120;
        Label11.Left := 128;
      end;
      if (Readchis('result', 'BarcodeStart') = '1') then begin
        grp32.Caption := grp32.Caption + '---注意：启用了<接收到条码[回车符]立即 启动测试>！';
      end
      else if ((Readchis('result', 'UDPBarcodeStart') = '1') or (Readchis('result', 'UDPBarcodeStart') = 'ON')) then begin
        grp32.Caption := grp32.Caption + '---注意：启用了<接收到[UDP]条码立即 启动测试>！';
      end;
    //------------------------------------[Server]组机种参数---------------------------------------------------------------------

      if (nrf_dat_config.Readstring('server', 'TraceManEn', '') = '1') then begin  //使能了traceman软件交互功能
        grp33.Visible := True;

        if nrf_dat_config.Readstring('server', 'TracePathA', '') <> '' then begin
          edt128.Text := nrf_dat_config.Readstring('server', 'TracePathA', '');
          if not DirectoryExists(edt128.Text) then                            //FileExists
            edt128.Text := ExtractFilePath(application.ExeName);
        end
        else
          edt128.Text := ExtractFilePath(application.ExeName);

        if nrf_dat_config.Readstring('server', 'TracePathB', '') <> '' then begin
          edt129.Text := nrf_dat_config.Readstring('server', 'TracePathB', '');
          if not DirectoryExists(edt129.Text) then                            //FileExists
            edt129.Text := ExtractFilePath(application.ExeName);
        end
        else
          edt129.Text := ExtractFilePath(application.ExeName);

        if nrf_dat_config.Readstring('server', 'TracePathBname', '') <> '' then
          lbl154.Caption := nrf_dat_config.Readstring('server', 'TracePathBname', ''); //默认为.csv <--- +'.txt';
      end;

      if nrf_dat_config.Readstring('Server', 'Port', '') <> '' then
        cbb4.Text := nrf_dat_config.Readstring('Server', 'Port', '');
      if nrf_dat_config.Readstring('Server', 'Baud', '') <> '' then
        cbb5.Text := nrf_dat_config.Readstring('Server', 'Baud', '');
    //------------------------------------[Comm Para]组机种参数---------------------------------------------------------------------
      if nrf_dat_config.Readstring('Comm Para', 'UploadServerT', '') <> '' then begin     //如>0：上传映射服务器间隔分钟数
        edt6.Text := nrf_dat_config.Readstring('Comm Para', 'UploadServerT', '');
        edt6.Hint := nrf_dat_config.Readstring('Comm Para', 'UploadServerHint', '');      //早期：=UpSinglePCS 两种文件剪切到映射盘 ；否则升级1：生成临时文件夹temp\并剪切到映射盘  //jaguar连片启用 单PCS也上传
        if nrf_dat_config.Readstring('Comm Para', 'UploadServerNoBarcode', '') <> '' then
          lbl2.Hint := nrf_dat_config.Readstring('Comm Para', 'UploadServerNoBarcode', '') //可指定不上传MES的条码标签  //UploadServer(minute) 标签；
        else
          lbl2.Hint := 'ERROR';
      end
      else
        edt6.Text := '0';                         //不传映射盘    '1440';   //24小时
      if (StrToInt(edt6.Text) > 0) then begin     //显示服务器文件路径

        grp1.Visible := True;
        grp1.Caption := 'Server Path 网盘路径';
        if nrf_dat_config.Readstring('Comm Para', 'PathServer', '') <> '' then begin
          edt1.Text := nrf_dat_config.Readstring('Comm Para', 'PathServer', '');
          if not DirectoryExists(edt1.Text) and (StrToInt(edt6.Text) > 0) then begin
            edt1.Color := clRed;
            ShowMessage('Server Path not exist!网盘地址不存在！');
          end;
        end
        else
          edt1.Text := ExtractFilePath(application.ExeName);
      end
      else
        grp1.Visible := False;      //不显示服务器文件路径

      if nrf_dat_config.Readstring('Comm Para', 'HIDdelay', '') <> '' then      //允hid通信延时
        edt15.Text := nrf_dat_config.Readstring('Comm Para', 'HIDdelay', '');
      if nrf_dat_config.Readstring('Comm Para', 'COMMdelay', '') <> '' then      //允hid通信延时
        edt14.Text := nrf_dat_config.Readstring('Comm Para', 'COMMdelay', '');

            /////////////////是否生产csv记录文件
      if nrf_dat_config.Readstring('Comm Para', 'OneDayOneFile', '') <> '' then begin
        label10.Hint := nrf_dat_config.Readstring('Comm Para', 'OneDayOneFile', '');
        if IsNumberic(label10.Hint) then
          label10.Tag := StrToInt(label10.Hint);
      end;
      for i := 1 to UpStepMax do begin
        ScaleZu[i] := '1';
        if nrf_dat_config.Readstring('Model Para', 'Scale' + inttostr(i), '') <> '' then begin
          ScaleZu[i] := nrf_dat_config.Readstring('Model Para', 'Scale' + inttostr(i), '');
        end;
      end;

      if (nrf_dat_config.Readstring('Comm Para', 'CSVen', '') = '2') or (nrf_dat_config.Readstring('Comm Para', 'CSVen', '') = 'Tesla') then      //tesla格式
      begin
        LogType := 2;
        grp18.Visible := True;
        if Readchis('Model Para', 'LoginItem4') = '' then
          lbl93.Caption := '1号夹具';           //ts9.Visible:=False;
      end
      else if (nrf_dat_config.Readstring('Comm Para', 'CSVen', '') = '3') or (nrf_dat_config.Readstring('Comm Para', 'CSVen', '') = 'Jiahe') then      //jiahe格式
      begin
        LogType := 3;
        grp33.Tag := 3;
        grp18.Visible := True;
        if Readchis('Model Para', 'LoginItem4') = '' then
          lbl93.Caption := 'Fixture';
      end
      else begin      //其他CSV格式文件或者tab格式文本文件
        if nrf_dat_config.Readstring('Comm Para', 'CSVen', '') <> '' then begin
          if IsNumberic(nrf_dat_config.Readstring('Comm Para', 'CSVen', '')) then
            LogType := StrToInt(nrf_dat_config.Readstring('Comm Para', 'CSVen', ''))
          else
            LogType := 12;
        end
        else
          LogType := 12;
      end;

      /////////////////是否网口连接，-----------------------------------------------------

      if nrf_dat_config.Readstring('Comm Para', 'CommOpen', '') <> '' then begin
        groupbox1.hint := nrf_dat_config.Readstring('Comm Para', 'CommOpen', '');     //是否启动软件打开串口
        //触摸屏翻页命令 btn77.Visible:=true;
        //btn78.Visible:=true;
        if groupbox1.hint = '5' then begin       //定义FCT5主板，不不能从主板读取步骤名，则必须从config读取；
          NameCount := GetNameUnitFromConfig($33);  //PowerOnCreatNewFile:=0;         //Comm1.ReadIntervalTimeout:=400;
          edt14.Text := '600';               //因早期FCT5主板翻页有500mS的延时；
        end;
      end;
      if groupbox1.hint = '0' then begin    //非串口，不显示页内翻页
      //改成重做   btn77.Visible:=false;
      //改成撤销  btn78.Visible:=false;
      end;

      if ComboBox1.Items.IndexOf(nrf_dat_config.Readstring('Comm Para', 'Port', '')) < 0 then

      else begin
        ComboBox1.Text := nrf_dat_config.Readstring('Comm Para', 'Port', '');
        cbb3.Text := ComboBox1.Text;
      end;

      if ComboBox2.Items.IndexOf(nrf_dat_config.Readstring('Comm Para', 'Baud', '')) < 0 then

      else begin
        ComboBox2.Text := nrf_dat_config.Readstring('Comm Para', 'Baud', '');
        cbb2.Text := ComboBox2.Text;
      end;
      /////////////////是否显示调试界面,DebugMode有更高优先级

      if (not FileExists(ExtractFilePath(application.ExeName) + 'config.ini')) and (FileExists(ExtractFilePath(application.ExeName) + 'zhi.exe') or FileExists(ExtractFilePath(application.ExeName) + 'zhidbg*.exe') or FileExists(ExtractFilePath(application.ExeName) + 'zhiDE*.exe')) then begin
        Ts3.TabVisible := True;
        rb5.Checked := True;             //  允许调试+助手界面
        lbl79.Color := clGray;
      end
      else if (nrf_dat_config.Readstring('Comm Para', 'DebugMode', '') = '1') then begin     //调试 模式
        Ts3.TabVisible := True;
        rb5.Checked := True;             //  允许调试助手界面
        lbl79.Color := clGray;
      end
      else begin
        Ts3.TabVisible := False;         // 数据监控解析
        rb4.Checked := True;             //仅显示串口数据；
        lbl79.Color := clGreen;
      end;

      if nrf_dat_config.Readstring('Comm Para', 'ChildFolder', '') <> '' then begin
        edt62.Text := nrf_dat_config.Readstring('Comm Para', 'ChildFolder', '');
        if nrf_dat_config.Readstring('Comm Para', 'ReTest', '') <> '' then begin
          edt169.Text := nrf_dat_config.Readstring('Comm Para', 'ReTest', '');
          grp43.Visible := true;
        end;
        if (edt62.Text = '25') then begin

          btn101.Enabled := False;
          btn130.Visible := True;
          edt137.Text := 'FAIL';
          if nrf_dat_config.Readstring('server', 'StandardIP', '') <> '' then
            edt141.Text := nrf_dat_config.Readstring('server', 'StandardIP', '')
          else
            edt141.Text := 'mycsmtet.mflex.com.cn/';

          btn118.Caption := 'ET测试前校验[ABP2]';
          if nrf_dat_config.Readstring('server', 'BPCommand', '') <> '' then
            edt153.Text := nrf_dat_config.Readstring('server', 'BPCommand', '')
          else
            edt153.Text := '/prevalidation?';

          btn114.Caption := 'ET测试结果上传[2BU0]';
          if nrf_dat_config.Readstring('server', 'BUCommand', '') <> '' then
            edt154.Text := nrf_dat_config.Readstring('server', 'BUCommand', '')
          else
            edt154.Text := '';  //'batches';
          if nrf_dat_config.Readstring('server', 'DoneCommand', '') <> '' then
            edt155.Text := nrf_dat_config.Readstring('server', 'DoneCommand', '')
          else
            edt155.Text := '---';
          if nrf_dat_config.Readstring('server', 'DirCommand', '') <> '' then
            edt156.Text := nrf_dat_config.Readstring('server', 'DirCommand', '')
          else
            edt156.Text := 'api/ettestrecords';          //取消 /

          btn119.Enabled := false;
          if (Readchis('Server', 'MESfun') <> '') then begin
            chk25.Visible := true;
            chk26.Visible := true;
            chk27.Visible := true;
            chk28.Visible := true;

            btn130.Caption := 'MES联机';
            btn130.Font.Color := clGreen;
            btn130.Hint := Readchis('Server', 'MESfun');
            WriteChis('Server', 'MESdefault', btn130.Hint);        //把设置的MES代码填入默认值；
          end
          else begin
            btn130.Caption := 'MES脱机';
            btn130.Font.Color := clDefault;
          end;

          //Frrr:=GetDateFormat(DateDelta);                              //获取当前系统日期分隔符
          //Dat:=StrToDate(Format('2016%s4%s11',[Fr,Fr]));  //这样始终都会跟当前系统日期分隔符同步，这样不管你把当前系统日期分隔
          SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SSHORTDATE, 'yyyy-MM-dd');
          Sleep(100);

          grp41.Visible := True;
        end
        else if (Readchis('Server', 'MESprogram') <> '') or (Readchis('Server', 'MESprogramdefault') <> '') then begin    //联机启用和脱机启用
          btn56.Caption := 'up上传' + char($0A) + '文件';

          grp41.Visible := True;
          btn101.Enabled := False;
          btn130.Visible := True;
          if (Readchis('Server', 'MESprogramdefault') = 'auto') then begin   //开机进入联机状态
            WriteChis('Server', 'MESprogram', 'auto');
          end;
          if (Readchis('Server', 'MESprogram') <> '') then begin
            btn130.Caption := 'MES联机';
            btn130.Font.Color := clGreen;
            edt155.Enabled := true;
            btn130.Hint := Readchis('Server', 'MESprogram');
            WriteChis('Server', 'MESprogramdefault', btn130.Hint);        //把设置的MES代码填入默认值；
          end
          else begin
            btn130.Caption := 'MES脱机';
            btn130.Font.Color := clDefault;
            edt155.Enabled := False;
          end;

          if nrf_dat_config.Readstring('server', 'StandardIP', '') <> '' then
            edt141.Text := nrf_dat_config.Readstring('server', 'StandardIP', '')
          else
            edt141.Text := '180.166.252.70:17000/';

          btn118.Caption := '从MES加载测试程序';
          if nrf_dat_config.Readstring('server', 'BPCommand', '') <> '' then
            edt153.Text := nrf_dat_config.Readstring('server', 'BPCommand', '')
          else
            edt153.Text := '/program/dwnfilestojson';

          btn114.Caption := '测试程序上传MES';
          if nrf_dat_config.Readstring('server', 'BUCommand', '') <> '' then
            edt154.Text := nrf_dat_config.Readstring('server', 'BUCommand', '')
          else
            edt154.Text := '/program/upfilestojson';
          btn119.Caption := 'MCU校验';
          if nrf_dat_config.Readstring('server', 'DoneCommand', '') <> '' then
            edt155.Text := nrf_dat_config.Readstring('server', 'DoneCommand', '')
          else
            edt155.Text := '/fct/validfctbymcu';
          if nrf_dat_config.Readstring('server', 'UpCommand', '') <> '' then
            edt180.Text := nrf_dat_config.Readstring('server', 'UpCommand', '')
          else
            edt180.Text := '/fct/validfctbyledadc';

          edt136.Text := 'P00091729H0013S00114T1100010L030W03';
          edt73.Text := 'pn-23447700';
          edt83.Text := 'rl-AB';
          edt165.Text := 'llc-W1';
          edt177.Text := 'hv-5.0';               //上海丽清319的硬件版本
          edt178.Text := 'sv-S010';
          edt179.Text := 'ld-10102019';
          edt142.Text := 'cs'; //-0x86498332';
          edt177.Visible := true;
          edt178.Visible := true;
          edt179.Visible := true;

          if nrf_dat_config.Readstring('server', 'DirCommand', '') <> '' then
            edt156.Text := nrf_dat_config.Readstring('server', 'DirCommand', '')
          else
            edt156.Text := 'api';

        end
        else begin

        end;

      //------------------登录信息界面---------------------------------------
        if (StrToInt(edt62.Text) >= 1) then      //文件夹参数        //读取FCT主板的机种名；依机种分文件夹生成log，[高级应用]，
          grp18.Visible := true;

        if ((LogType = 5) or ((stat1.tag = 9000) or (Readchis('Comm Para', 'BarcodeFile') <> ''))   //高级定义
          ) then begin            //早期：=5文件只一份，不支持第2路径
          edt135.Visible := true;
          btn100.Visible := true;
          lbl161.Visible := True;
          grp12.Visible := True;
          if nrf_dat_config.Readstring('Comm Para', 'PathSNfile', '') <> '' then begin
            edt135.Text := nrf_dat_config.Readstring('Comm Para', 'PathSNfile', '');
            if not DirectoryExists(edt135.Text) then                            //FileExists
              edt135.Text := ExtractFilePath(application.ExeName);
          end
          else begin
            if (LogType = 5) then begin
              edt135.Text := ExtractFilePath(application.ExeName);
            end
            else
              edt135.Text := ExtractFilePath(application.ExeName) + 'temp\';
            if not DirectoryExists(edt135.Text) then                 //本地备份路径
            try
              begin
                CreateDir(edt135.Text);  //创建备份子目录
              end;
            except
              stat1.Panels[4].Text := 'Cannot Create ' + edt135.Text;
               //   Exit;
            end;
          end;
          if (LogType = 5) then begin
            GroupBox3.Visible := false;
          end;
        end;
      //------------------备份映射盘的应用场景---------------------------------------
        if (form_main.edt62.text = '25') then begin                               //jaguar
      //    or( ( (stat1.tag=9000)or(Readchis('Comm Para','BarcodeFile')<>'') )   //高级定义
      //         and(LogType<>5) ) then begin            //早期：=5文件只一份，不支持第2路径

          if nrf_dat_config.Readstring('Comm Para', 'UploadServerT', '') = '' then begin     //不启用网盘；
            edt1.Visible := False;
            btn4.Visible := False;
            lbl2.Visible := false;
            edt6.Visible := False;
            btn2.Visible := False;
            grp1.Visible := false;
          end
          else
            grp1.Visible := true;

          edt157.Visible := true;                           //二次升级
          btn125.Visible := true;
          lbl180.Visible := True;
          lbl180.Caption := 'SNfilename';
          if nrf_dat_config.Readstring('Comm Para', 'TeslaSNPath', '') <> '' then begin

            edt157.Text := nrf_dat_config.Readstring('Comm Para', 'TeslaSNPath', '');
            if not DirectoryExists(edt157.Text) then                            //FileExists
              edt157.Text := ExtractFilePath(application.ExeName);
          end
          else
            edt157.Text := ExtractFilePath(application.ExeName);

          if nrf_dat_config.Readstring('Comm Para', 'BakPathName', '') <> '' then      //备份文件夹名称，不为空
          begin
            lbl183.Caption := nrf_dat_config.Readstring('Comm Para', 'BakPathName', '');
          end;

        end;
        if (edt62.Text = '24') then begin
          nrf_dat_config.WriteString('Server', 'MESfun', '');      //强制脱机

          edt157.Visible := true;
          btn125.Visible := true;
          lbl180.Visible := True;
          lbl180.Caption := 'SNfilename';
          if nrf_dat_config.Readstring('Comm Para', 'TeslaSNPath', '') <> '' then begin

            edt157.Text := nrf_dat_config.Readstring('Comm Para', 'TeslaSNPath', '');
            if not DirectoryExists(edt157.Text) then                            //FileExists
              edt157.Text := ExtractFilePath(application.ExeName);
          end
          else
            edt157.Text := ExtractFilePath(application.ExeName);

          if nrf_dat_config.Readstring('Comm Para', 'BakPathName', '') <> '' then      //备份文件夹名称，不为空
          begin
            lbl183.Caption := nrf_dat_config.Readstring('Comm Para', 'BakPathName', '');
          end;
        end;
      end;
    //------------------------------------[Barcode Check]组机种参数---------------------------------------------------------------------
      edt48.Text := nrf_dat_config.Readstring('Barcode Check', 'BarCheckEn', '');       //专业：核对2个条码是否匹配！
      if edt48.text = '1' then begin
        if (nrf_dat_config.Readstring('Barcode Check', 'Bar1Pos', '') <> '') then
          edt123.Text := nrf_dat_config.Readstring('Barcode Check', 'Bar1Pos', '');
        if (nrf_dat_config.Readstring('Barcode Check', 'Bar1Len', '') <> '') then
          edt124.Text := nrf_dat_config.Readstring('Barcode Check', 'Bar1Len', '');
        if (nrf_dat_config.Readstring('Barcode Check', 'Bar2Pos', '') <> '') then
          edt125.Text := nrf_dat_config.Readstring('Barcode Check', 'Bar2Pos', '');
        if (nrf_dat_config.Readstring('Barcode Check', 'Bar2Len', '') <> '') then
          edt126.Text := nrf_dat_config.Readstring('Barcode Check', 'Bar2Len', '');
        if (nrf_dat_config.Readstring('Barcode Check', 'Bar1Name', '') <> '') then
          lbl155.Caption := nrf_dat_config.Readstring('Barcode Check', 'Bar1Name', '');
        if (nrf_dat_config.Readstring('Barcode Check', 'Bar2Name', '') <> '') then
          lbl156.Caption := nrf_dat_config.Readstring('Barcode Check', 'Bar2Name', '');
      end
      else begin
        edt48.Visible := false;
        lbl151.Visible := false;
        edt123.Visible := false;
        edt124.Visible := false;
        edt125.Visible := false;
        edt126.Visible := false;
        lbl155.Visible := false;
        lbl156.Visible := false;
      end;
      
    //------------------------------------[Model Para]组机种参数---------------------------------------------------------------------
      Lbl114.Caption := nrf_dat_config.Readstring('Model Para', 'prefix', '');
      if (lbl90.hint = 'model') and (StrToInt(edt62.Text) = 0) then                 //已淘汰！用config.ini配置机种名称
        lbl90.hint := nrf_dat_config.Readstring('Model Para', 'Model', '');       //测试界面里的辅助机种名

      if Readchis('Model Para', 'BarCodeEdit') <> '' then begin
        if StrToInt(Readchis('Model Para', 'BarCodeEdit')) >= 1000 then
          edit1.Enabled := false;

        if (StrToInt(Readchis('Model Para', 'BarCodeEdit')) mod 1000) > 70 then begin
          edit1.EditMask := '';
          edit1.Width := 1000;
          medit1.EditMask := edit1.EditMask;

          edit1.MaxLength := StrToInt(Readchis('Model Para', 'BarCodeEdit')) mod 1000;
        end;
      end;
      if (nrf_dat_config.Readstring('Model Para', 'enPN', '') <> '')              //使能机种选择
        then begin
        edt82.Text := nrf_dat_config.Readstring('Model Para', 'enPN', '');       //料号索引,与下面读取不能分开 ,否则会0出现-1报错
        if (edt82.Text = '-1') then
          edt82.Text := '1';                             //-1 会出错
        edt82.Tag := StrToInt(edt82.Text);

        if (nrf_dat_config.Readstring('Model Para', 'PNsize', '') <> '') then    //用于tesla对Order和Operator统一最大长度的限制，通过下面各个机种参数可取消
        begin
          FormPN.edt1.Text := nrf_dat_config.Readstring('Model Para', 'PNsize', '');

          FormPN.edt2.MaxLength := StrToInt(FormPN.edt1.Text);
          FormPN.edt3.MaxLength := StrToInt(FormPN.edt1.Text);
          FormLoad.edt5.MaxLength := StrToInt(FormPN.edt1.Text);
          FormLoad.edt6.MaxLength := StrToInt(FormPN.edt1.Text);
        end;
        FormPN.edt2.Hint := nrf_dat_config.Readstring('Model Para', 'OrderSize', '');
        if IsNumberic(FormPN.edt2.Hint) then begin
          FormPN.edt2.MaxLength := StrToInt(FormPN.edt2.Hint);
          FormLoad.edt5.MaxLength := StrToInt(FormPN.edt2.Hint);
        end;
        FormPN.edt3.Hint := nrf_dat_config.Readstring('Model Para', 'OperatorSize', '');
        if IsNumberic(FormPN.edt3.Hint) then begin
          FormPN.edt3.MaxLength := StrToInt(FormPN.edt3.Hint);
          FormLoad.edt6.MaxLength := StrToInt(FormPN.edt3.Hint);
        end;

        if (Readchis('Model Para', 'PNbitDis', '') <> '') then    //输入选项使能，=0的关闭输入
        begin
          FormPN.edt4.Text := Readchis('Model Para', 'PNbitDis', '');
        end;
        if IsNumberic(FormPN.edt4.Text) then begin
          if Readchis('Model Para', 'LoginItem1En') = '1' then
            FormPN.edt4.Text := IntToStr(StrToInt(FormPN.edt4.Text) and $0E);
          if Readchis('Model Para', 'LoginItem2En') = '1' then
            FormPN.edt4.Text := IntToStr(StrToInt(FormPN.edt4.Text) and $0D);
          if Readchis('Model Para', 'LoginItem3En') = '1' then
            FormPN.edt4.Text := IntToStr(StrToInt(FormPN.edt4.Text) and $0B);
          if Readchis('Model Para', 'LoginItem4En') = '1' then
            FormPN.edt4.Text := IntToStr(StrToInt(FormPN.edt4.Text) and $07);
          if Readchis('Model Para', 'LoginItem1Dis') = '1' then
            FormPN.edt4.Text := IntToStr(StrToInt(FormPN.edt4.Text) or $01);
          if Readchis('Model Para', 'LoginItem2Dis') = '1' then
            FormPN.edt4.Text := IntToStr(StrToInt(FormPN.edt4.Text) or $02);
          if Readchis('Model Para', 'LoginItem3Dis') = '1' then
            FormPN.edt4.Text := IntToStr(StrToInt(FormPN.edt4.Text) or $04);
          if Readchis('Model Para', 'LoginItem4Dis') = '1' then
            FormPN.edt4.Text := IntToStr(StrToInt(FormPN.edt4.Text) or $08);
        end;

        for i := 1 to 50 do begin           //遍历最大10个机种及治具
          if nrf_dat_config.Readstring('Model Para', 'PN' + inttostr(i), '') <> '' then begin
            FormPN.cbb1.Items.Strings[i] := nrf_dat_config.Readstring('Model Para', 'PN' + inttostr(i), '');
          end;
          if nrf_dat_config.Readstring('Model Para', 'Fix' + inttostr(i), '') <> '' then begin
            FormPN.cbb2.Items.Strings[i] := nrf_dat_config.Readstring('Model Para', 'Fix' + inttostr(i), '');
            FormLoad.cbb2.Items.Strings[i] := nrf_dat_config.Readstring('Model Para', 'Fix' + inttostr(i), '');
          end;
          if nrf_dat_config.Readstring('Model Para', 'ST' + inttostr(i), '') <> '' then begin
            FormPN.cbb3.Items.Strings[i] := nrf_dat_config.Readstring('Model Para', 'ST' + inttostr(i), '');
          end;
        end;
        for i := 51 to 200 do begin           //遍历最大200个机种及治具
          if nrf_dat_config.Readstring('Model Para', 'Fix' + inttostr(i), '') <> '' then begin
            FormPN.cbb2.Items.Add(nrf_dat_config.Readstring('Model Para', 'Fix' + inttostr(i), ''));
            FormLoad.cbb2.Items.Add(nrf_dat_config.Readstring('Model Para', 'Fix' + inttostr(i), ''));
          end;
        end;
        if StrToInt(edt82.Text) >= 99 then                     //PN
          FormPN.cbb1.ItemIndex := 99
        else
          FormPN.cbb1.ItemIndex := StrToInt(edt82.Text);
        if (Readchis('Server', 'MESprogram') <> '') or (Readchis('Server', 'MESprogramdefault') <> '') then begin    //liqing可选站别
          if (nrf_dat_config.Readstring('Model Para', 'enFix', '') <> '') then begin       //备用；选择治具名称
            FormPN.cbb2.ItemIndex := StrToInt(nrf_dat_config.Readstring('Model Para', 'enFix', ''));
            FormLoad.cbb2.ItemIndex := StrToInt(nrf_dat_config.Readstring('Model Para', 'enFix', ''));
          end;
          if (nrf_dat_config.Readstring('Model Para', 'enST', '') <> '') then        //备用；选择治具名称
            FormPN.cbb3.ItemIndex := StrToInt(nrf_dat_config.Readstring('Model Para', 'enST', ''));
        end
        else begin           //jaguar
          if (nrf_dat_config.Readstring('Model Para', 'enFix', '') <> '') then begin       //备用；选择治具名称
            FormPN.cbb2.ItemIndex := StrToInt(nrf_dat_config.Readstring('Model Para', 'enFix', ''));
            FormLoad.cbb2.ItemIndex := StrToInt(nrf_dat_config.Readstring('Model Para', 'enFix', ''));
          end;
        end;
      end;

    finally
      nrf_dat_config.free;
    end;
  end;
end;

procedure PNbarcode_fileCreateProcess(len: integer);         //读取Ini文件的PN配置，barcode配置，并初始化文件生成
var
  temp: byte;
  i: SmallInt;
  nrf_dat_config: Tinifile;
  save_file_name, ini_path, sst: string;
begin

  with form_main do begin
    for i := 1 to UpStepMax do begin
      OnePCSNamePos[i] := UpStepMax;          //默认用最后一个项目；
      ScaleZu[i] := '1';
    end;
    if strtoint(edt82.Text) >= 100 then begin                     //jaguar等 启用PC存放程序  and(Readchis('Model Para','PNformBarcode')='1')
      edt82.Text := '99';
      grp18.Caption := '';
      edt82.Visible := false;

      modbusFSM := $60;
      btn46Click(form_main);   //读取固件版本
      Application.ProcessMessages;       //定时发送复位命令
      Sleep(100);
      modbusFSM := $60;
      btn46Click(form_main);   //读取固件版本
      Application.ProcessMessages;       //定时发送复位命令
      Sleep(100);

      btn26Click(form_main);              //加载之前先复位？
      tmr1Timer(form_main);
      Application.ProcessMessages;       //定时发送复位命令
      Sleep(100);
      Application.ProcessMessages;       //接收复位命令回传

      formLOad.btn3.Visible := false;    //加载按钮禁止！
      formLOad.edt4.Visible := true;     //扫工单条码，加载程序；
      formLOad.grp1.Visible := True;

      formLOad.ShowModal;

      if StrToInt(edt82.Text) < 99 then begin        //jaguar            //使用基本登录窗口参数
        writechis('Model Para', 'enFix', IntToStr(FormPN.cbb2.ItemIndex))
      end
      else
        writechis('Model Para', 'enFix', IntToStr(FormLoad.cbb2.ItemIndex));              //扩展登录参数

      sbtbtn1.Enabled := False;       //loading...之后才使能
      sbtbtn1.Visible := false;
    end
    else if StrToInt('0' + trim(form_main.edt82.Text)) > 0 then begin    //enPN=1..99 ,通用！ 读取配置字段的出处不同。    // tesla，jiahe专用

      if form_main.CLoseFlag > 1 then
        form_main.CLoseFlag := 1;
      FormPN.showmodal;
      if form_main.CLoseFlag = 0 then begin
        Application.Terminate;
        Exit;
      end;

      try
        ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
        nrf_dat_config := Tinifile.Create(ini_path);

        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'conductorNum', '') <> '' then      //默认1，不为空
        begin
          edt81.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'conductorNum', '');
        end;
        if edt81.Text = '0' then
          ts9.TabVisible := False
        else
          ts9.TabVisible := True;         //导线卡关闭

        for i := 1 to UpStepMax do begin
          ScaleZu[i] := '1';
          if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'Scale' + inttostr(i), '') <> '' then begin
            ScaleZu[i] := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'Scale' + inttostr(i), '');
          end;
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarCodeLen', '') <> '' then      //设置条码长度规则
        begin
          edt7.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarCodeLen', '');
        end;

        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos', '') <> '' then      //下面3个参数组合来判断条码含有固定字符
        begin
          edt8.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos', '');
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos2', '') <> '' then      //第2位置条码规则
        begin
          edt182.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos2', '');
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos3', '') <> '' then      //第3位置条码规则
        begin
          edt183.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos3', '');
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos4', '') <> '' then      //第4位置条码规则
        begin
          edt184.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos4', '');
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'MaskBarPos', '') <> '' then   //通配符*？位置条码规则
        begin
          edt181.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'MaskBarPos', '');
        end;

        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarChars', '') <> '' then      //
        begin
          edt10.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarChars', '');
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarLen', '') <> '' then      //
        begin
          edt9.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarLen', '');
        end
        else begin
          edt9.Text := IntToStr(Length(edt10.Text));
        end;

      //////////////-------------------PN料号-----------------------------------
        if StrToInt(edt82.Text) < 99 then       //启用PN登录窗口！否则：大于99为程序存放测控板，不可变
          edt82.Text := IntToStr(FormPN.cbb1.ItemIndex);
        nrf_dat_config.WriteString('Model Para', 'enPN', edt82.Text);
        if (Readchis('Server', 'MESprogram') <> '') or (Readchis('Server', 'MESprogramdefault') <> '') then begin    //liqing可选站别
          nrf_dat_config.WriteString('Model Para', 'enFix', IntToStr(FormPN.cbb2.ItemIndex));
          nrf_dat_config.WriteString('Model Para', 'enST', IntToStr(FormPN.cbb3.ItemIndex));
        end
        else begin     //   jaguar
          if StrToInt(edt82.Text) < 99 then                     //使用基本登录窗口参数
            nrf_dat_config.WriteString('Model Para', 'enFix', IntToStr(FormPN.cbb2.ItemIndex))
          else
            nrf_dat_config.WriteString('Model Para', 'enFix', IntToStr(FormLoad.cbb2.ItemIndex));              //扩展登录参数
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'enPrint', '') <> '' then begin      //自动打印使能 ,并设置大小
          edt75.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'enPrint', '');
          if edt75.Text = '0' then begin
            btn55.Visible := false;
            btn28.Visible := false;
            btn29.Visible := false;
            btn27.Enabled := false;
          end
          else begin
            btn55.Visible := true;
            btn28.Visible := true;
             //不能用 btn29.Visible:=true;
            btn27.Enabled := true;
          end;
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintVHdir', '') <> '' then        //设置 打印机字符长度（与打印机方向设置有关）+ + 空行数+信息长短
          edt11.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintVHdir', '');
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintRow', '') <> '' then           //此两个参数影响软件启动时间
          edt159.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintRow', '');
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintMaxRows', '') <> '' then        //打印最大行数，高优先级；
          edt159.Text := nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintMaxRows', '');
        if edt159.Text <> '0' then begin
          edt159.Visible := True;
          lbl184.Visible := true;
        end;
      finally
        nrf_dat_config.free;
      end;
    end
    else begin          //读取机种下面关于条码的规则
      if Readchis('Model Para', 'Process') <> '' then begin   //淘汰！ 早期数据库专用   //初始化上传参数
        if Readchis('Model Para', 'LoginItem4') = '' then
          lbl93.Caption := 'Process';
        stat1.Font.Size := 15;

        datetimetostring(sst, 'yyyymmddhhnnss', Now);
        btn1.Font.Color := clDefault; //clWindow;

        if (Readchis('Model Para', 'PNbitDis', '') <> '') then    //输入选项使能，=0的关闭输入
        begin
          FormPN.edt4.Text := Readchis('Model Para', 'PNbitDis', '');
        end;

        edt80.Text := Readchis('Model Para', 'Process');           //制程<--治具名称
        if form_main.CLoseFlag > 1 then
          form_main.CLoseFlag := 1;
        FormPN.showmodal;
        if form_main.CLoseFlag = 0 then begin
          Application.Terminate;
          Exit;
        end;
      end;
    end;

    if (StrToInt('0' + trim(form_main.edt82.Text)) = 0) or (StrToInt('0' + trim(form_main.edt82.Text)) >= 99) then begin   //enPN>0,通用！
      try
        ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
        nrf_dat_config := Tinifile.Create(ini_path);

        if nrf_dat_config.Readstring('Model Para', 'BarCodeLen', '') <> '' then      //默认1，不为空
        begin
          edt7.Text := nrf_dat_config.Readstring('Model Para', 'BarCodeLen', '');
        end;

        if nrf_dat_config.Readstring('Model Para', 'BarPos', '') <> '' then         //下面3个参数组合来判断条码含有固定字符
        begin
          edt8.Text := nrf_dat_config.Readstring('Model Para', 'BarPos', '');
        end;
        if nrf_dat_config.Readstring('Model Para', 'BarPos2', '') <> '' then      //第2位置条码规则
        begin
          edt182.Text := nrf_dat_config.Readstring('Model Para', 'BarPos2', '');
        end;
        if nrf_dat_config.Readstring('Model Para', 'BarPos3', '') <> '' then      //第3位置条码规则
        begin
          edt183.Text := nrf_dat_config.Readstring('Model Para', 'BarPos3', '');
        end;
        if nrf_dat_config.Readstring('Model Para', 'BarPos4', '') <> '' then      //第4位置条码规则
        begin
          edt184.Text := nrf_dat_config.Readstring('Model Para', 'BarPos4', '');
        end;
        if nrf_dat_config.Readstring('Model Para', 'MaskBarPos', '') <> '' then   //通配符*？位置条码规则
        begin
          edt181.Text := nrf_dat_config.Readstring('Model Para', 'MaskBarPos', '');
        end;
        if nrf_dat_config.Readstring('Model Para', 'BarChars', '') <> '' then      //
        begin
          edt10.Text := nrf_dat_config.Readstring('Model Para', 'BarChars', '');
        end;

        if nrf_dat_config.Readstring('Model Para', 'BarLen', '') <> '' then begin
          edt9.Text := nrf_dat_config.Readstring('Model Para', 'BarLen', '');
        end
        else begin
          edt9.Text := IntToStr(Length(edt10.Text));
        end;

      finally
        nrf_dat_config.free;
      end;
    end;
    if Readchis('Model Para', 'BarCodeEdit') = '' then begin     //默认初始化上传参数
      edt134.Visible := False;
      lbl160.Visible := False;

      btn98.Visible := False;
      if Readchis('Model Para', 'MonitorResetKey') = '1' then         //可设置显示复位按键<---默认："复位"按钮不显示
        btn98.Visible := True;
    end
    else begin                    //超宽条码使能，并使能Load文件
      edt14.Visible := false;          //通讯延时参数不显示；
      edt15.Visible := false;

      L6.Enabled := true;             //菜单里 加载功能
      sbtbtn1.Enabled := False;       //loading...之后才使能
      sbtbtn1.Visible := true;

      if (StrToInt('0' + trim(form_main.edt82.Text)) > 0) and (StrToInt('0' + trim(form_main.edt82.Text)) < 99) then begin   //enPN>0,通用！
        if Readchis(FormPN.cbb1.Text, 'BarCodeLen', '') <> '' then begin    //默认1，不为空

          edt134.Text := Readchis(FormPN.cbb1.Text, 'BarCodeLen', '');
        end;
      end
      else begin
        if Readchis('Model Para', 'BarCodeLen') <> '' then begin
          edt134.Text := Readchis('Model Para', 'BarCodeLen');
        end;
      end;
      btn1.Font.Color := clDefault;
      edit1.Width := 700;             //超宽条码

      if Readchis('Model Para', 'MonitorResetKey') = '0' then         //"复位"按钮:可设置不显示复位按键，默认：显示
        btn98.Visible := false;
      if btn98.Visible then begin      //CAN机箱，不允许修改log路径
        btn3.Enabled := false;
        edt2.Enabled := false;

        btn100.Enabled := false;
        edt135.Enabled := false;

        btn4.Enabled := false;
        edt1.Enabled := false;
      end;
    end;

    for i := 1 to UpStepMax do begin
      SpecValZu[i] := '0';
      StdValZu[i] := '0';
      HpointZu[i] := '0';
      LpointZu[i] := '0';
      ModeZu[i] := '0';
    end;
  //------------ 设定程序本身所使用的日期时间格式，文件保存用-------------------

    LongDateFormat := 'yyyy-MM-dd';
    ShortDateFormat := 'yyyy-MM-dd';

    LongTimeFormat := 'HH:nn:ss';
    ShortTimeFormat := 'HH:nn:ss';
    DateSeparator := '-';
    TimeSeparator := ':';
  
    //-------------生产一个带日期的文本文件---------------------------------------
    TestNum := 0;
    if StrToInt(edt62.Text) = 0 then begin              //已淘汰！child=0:用config.ini设置的机种名生产数据文件

      if StrToInt(edt38.text) mod 10 > 0 then begin       //uplocal>0
        CreatAnewCsv(0, 0)
      end;
    end;
    mmo2String[10] := ''; //记录设备 数据上传字符串
    mmo2String[17] := '';
  end;
end;

procedure MatrixLed_createProcess(len: integer);         //矩阵LED指示灯生成
var
  temp16, i, j, k, onewide: SmallInt;
begin

  with form_main do begin
    if (strtoint(edt59.Text) > 0) and (strtoint(edt60.Text) > 0) then begin
      if (strtoint(edt145.Text) mod 1000) > 0 then begin
        onewide := (scrlbx2.Width - 2 * lbl73.Width) div (StrToInt(edt60.Text) * (StrToInt(edt145.Text) mod 1000)); //单位宽度
        for k := 0 to (StrToInt(edt145.Text) mod 1000) - 1 do begin     //遍历所有组

          for j := 0 to StrToInt(edt59.Text) - 1 do begin            //遍历所有行

            for i := 0 to StrToInt(edt60.Text) - 1 do begin         //遍历所有列

              Redits[k * StrToInt(edt59.Text) * StrToInt(edt60.Text) + j * StrToInt(edt60.Text) + i].free;
              Redits[k * StrToInt(edt59.Text) * StrToInt(edt60.Text) + j * StrToInt(edt60.Text) + i] := Tiedit.create(Application);

              Rleds[k * StrToInt(edt59.Text) * StrToInt(edt60.Text) + j * StrToInt(edt60.Text) + i].Free;
              Rleds[k * StrToInt(edt59.Text) * StrToInt(edt60.Text) + j * StrToInt(edt60.Text) + i] := Tiledround.Create(Application);

              with Rleds[k * StrToInt(edt59.Text) * StrToInt(edt60.Text) + j * StrToInt(edt60.Text) + i] do begin
                    //Visible  := False;
                if (j mod 2) = 1 then begin     //奇数行位置 像右缩进，或者向下偏移
                      
                      //if ildrw1.Style=ilasLeft then    //右到左顺序              (Rled1.Width+10)*16
                      //  Left     := Rled1.Left + (StrToInt(edt60.Text)-1-I )*StrToInt(edt120.Text) + ( (StrToInt(edt60.Text)-1-I )*(scrlbx2.Width-2*lbl73.Width) div (StrToInt(edt60.Text)+1))
                      //              +Strtoint(edt69.Text)  //整行右偏移
                      //else
                  begin
                    if (i mod 2) = 1 then       //奇数列左移动
                      Left := Rled1.Left + i * StrToInt(edt120.Text) + ((i + k * (StrToInt(edt60.Text))) * onewide) + Strtoint(edt69.Text) + strtoint(edt112.Text)   //  (Rled1.Width+10)*16
                    else
                      Left := Rled1.Left + i * StrToInt(edt120.Text) + ((i + k * (StrToInt(edt60.Text))) * onewide) + Strtoint(edt69.Text);  //(Rled1.Width+10)*16整行右偏移
                  end;
                  if (i mod 2) = 1 then              //列数为偶数，下偏位
                    Top := Rled1.Top + j * (Rled1.Height + 30) * 5 div (2 + strtoint(edt59.Text))  //由行数控制间距
                      + Strtoint(edt70.Text)  //偶数下偏移
                      + Strtoint(edt71.Text)                  //整行下偏移
                  else
                    Top := Rled1.Top + j * (Rled1.Height + 30) * 5 div (2 + strtoint(edt59.Text))  //由行数控制间距
                      + Strtoint(edt71.Text);                //整行下偏移
                end
                else begin

                  if (i mod 2) = 1 then    //奇数列左移动
                    Left := Rled1.Left + i * StrToInt(edt120.Text) + ((i + k * (StrToInt(edt60.Text))) * onewide) + strtoint(edt112.Text)   //  (Rled1.Width+10)*16
                  else
                    Left := Rled1.Left + i * StrToInt(edt120.Text) + ((i + k * (StrToInt(edt60.Text))) * onewide);   //  (Rled1.Width+10)*16

                  if (i mod 2) = 1 then              //列数为偶数，下偏位
                    Top := Rled1.Top + j * (Rled1.Height + 30) * 5 div (2 + strtoint(edt59.Text))  //由行数控制间距
                      + Strtoint(edt70.Text) //偶数下偏移
                  else
                    Top := Rled1.Top + j * (Rled1.Height + 30) * 5 div (2 + strtoint(edt59.Text))  //由行数控制间距;
                end;

                Width := Rled1.Width;
                Anchors := Rled1.Anchors;
                ActiveColor := Rled1.ActiveColor;
                InactiveColor := Rled1.InactiveColor;
                AutoInactiveColor := Rled1.AutoInactiveColor;
                    //if not Assigned(Parent) then
                Parent := Rled1.Parent;
                TabOrder := Rled1.TabOrder + k * StrToInt(edt59.Text) * StrToInt(edt60.Text) + j * StrToInt(edt60.Text) + i;
              end;

              with Redits[k * StrToInt(edt59.Text) * StrToInt(edt60.Text) + j * StrToInt(edt60.Text) + i] do begin
                Visible := true;
                Top := Rleds[k * StrToInt(edt59.Text) * StrToInt(edt60.Text) + j * StrToInt(edt60.Text) + i].Top;
                Left := Rleds[k * StrToInt(edt59.Text) * StrToInt(edt60.Text) + j * StrToInt(edt60.Text) + i].Left;
                Alignment := Redit1.Alignment;
                Width := Redit1.Width;

                Parent := Redit1.Parent;
                Value := inttostr(k * StrToInt(edt59.Text) * StrToInt(edt60.Text) + j * StrToInt(edt60.Text) + i + 1);
                Enabled := Redit1.Enabled;
              end;
               // end;
            end;
          end;
        end;
      end
      else begin
        for j := 0 to StrToInt(edt59.Text) - 1 do begin     //遍历所有行

          for i := 0 to StrToInt(edt60.Text) - 1 do begin   //遍历所有列

            Redits[j * StrToInt(edt60.Text) + i].free;
            Redits[j * StrToInt(edt60.Text) + i] := Tiedit.create(Application);

            Rleds[j * StrToInt(edt60.Text) + i].Free;
            Rleds[j * StrToInt(edt60.Text) + i] := Tiledround.Create(Application);

            with Rleds[j * StrToInt(edt60.Text) + i] do begin

              if (j mod 2) = 1 then      //奇数行位置 像右缩进，或者向下偏移
              begin
                if ildrw1.Style = ilasLeft then    //右到左顺序              (Rled1.Width+10)*16
                  Left := Rled1.Left + (StrToInt(edt60.Text) - 1 - i) * StrToInt(edt120.Text) + ((StrToInt(edt60.Text) - 1 - i) * (scrlbx2.Width - 2 * lbl73.Width) div (StrToInt(edt60.Text) + 1)) + Strtoint(edt69.Text)  //整行右偏移
                else begin
                  if (i mod 2) = 1 then       //奇数列左移动
                    Left := Rled1.Left + i * StrToInt(edt120.Text) + (i * (scrlbx2.Width - 2 * lbl73.Width) div (StrToInt(edt60.Text) + 1)) + Strtoint(edt69.Text) + strtoint(edt112.Text)   //  (Rled1.Width+10)*16
                  else
                    Left := Rled1.Left + i * StrToInt(edt120.Text) + (i * (scrlbx2.Width - 2 * lbl73.Width) div (StrToInt(edt60.Text) + 1)) + Strtoint(edt69.Text);  //(Rled1.Width+10)*16整行右偏移
                end;
                if (i mod 2) = 1 then              //列数为偶数，下偏位
                  Top := Rled1.Top + j * (Rled1.Height + 30) * 5 div (2 + strtoint(edt59.Text))  //由行数控制间距
                    + Strtoint(edt70.Text)  //偶数下偏移
                    + Strtoint(edt71.Text)                  //整行下偏移
                else
                  Top := Rled1.Top + j * (Rled1.Height + 30) * 5 div (2 + strtoint(edt59.Text))  //由行数控制间距
                    + Strtoint(edt71.Text);                //整行下偏移
              end
              else begin
                if ildrw1.Style = ilasLeft then   //右到左顺序      (Rled1.Width+10)*16
                  Left := Rled1.Left + (StrToInt(edt60.Text) - 1 - i) * StrToInt(edt120.Text) + (StrToInt(edt60.Text) - 1 - i) * (scrlbx2.Width - 2 * lbl73.Width) div (StrToInt(edt60.Text) + 1)
                else begin
                  if (i mod 2) = 1 then    //奇数列左移动
                    Left := Rled1.Left + i * StrToInt(edt120.Text) + (i * (scrlbx2.Width - 2 * lbl73.Width) div (StrToInt(edt60.Text) + 1)) + strtoint(edt112.Text)   //  (Rled1.Width+10)*16
                  else
                    Left := Rled1.Left + i * StrToInt(edt120.Text) + i * (scrlbx2.Width - 2 * lbl73.Width) div (StrToInt(edt60.Text) + 1);   //  (Rled1.Width+10)*16
                end;
                if (i mod 2) = 1 then              //列数为偶数，下偏位
                  Top := Rled1.Top + j * (Rled1.Height + 30) * 5 div (2 + strtoint(edt59.Text))  //由行数控制间距
                    + Strtoint(edt70.Text) //偶数下偏移
                else
                  Top := Rled1.Top + j * (Rled1.Height + 30) * 5 div (2 + strtoint(edt59.Text))  //由行数控制间距;
              end;

              Width := Rled1.Width;
              Anchors := Rled1.Anchors;
              ActiveColor := Rled1.ActiveColor;
              InactiveColor := Rled1.InactiveColor;
              AutoInactiveColor := Rled1.AutoInactiveColor;

              Parent := Rled1.Parent;
              TabOrder := Rled1.TabOrder + j * StrToInt(edt60.Text) + i;
              hint := 'ErrCode';
            end;

            with Redits[j * StrToInt(edt60.Text) + i] do begin
              Visible := true;
              Top := Rleds[j * StrToInt(edt60.Text) + i].Top;
              Left := Rleds[j * StrToInt(edt60.Text) + i].Left;
              Width := Redit1.Width;
              Alignment := Redit1.Alignment;
              Parent := Redit1.Parent;
              Value := inttostr(j * StrToInt(edt60.Text) + i + 1);
              Enabled := Redit1.Enabled;
            end;
             // end;
          end;
        end;
      end;
    end;
  end;
end;

procedure stepKeyEdit(key: Word; hh: SmallInt);
var
  keyArrowflag, ii, jj, nline: SmallInt;

  procedure zuSetFocus(row: SmallInt; nn: SmallInt);     //选中行、列位置的edit框，从1开始；
  begin
    case row of
      1:
        zu1edit[nn - 0].SetFocus;
      2:
        zu2edit[nn - 0].SetFocus;
      3:
        zu3edit[nn - 0].SetFocus;
      4:
        zu4edit[nn - 0].SetFocus;
      5:
        zu5edit[nn - 0].SetFocus;
      6:
        zu6edit[nn - 0].SetFocus;
      7:
        zu7edit[nn - 0].SetFocus;
      8:
        zu8edit[nn - 0].SetFocus;
      9:
        zu9edit[nn - 0].SetFocus;
      10:
        zu10edit[nn - 0].SetFocus;
      11:
        zu11edit[nn - 0].SetFocus;
      12:
        zu12edit[nn - 0].SetFocus;
      13:
        zu13edit[nn - 0].SetFocus;
      14:
        zu14edit[nn - 0].SetFocus;
      15:
        zu15edit[nn - 0].SetFocus;
      16:
        zu16edit[nn - 0].SetFocus;
      17:
        zu17edit[nn - 0].SetFocus;
    end;

  end;

begin
  with form_main do begin
    if not Panel1.Showing then
      Exit;
    keyArrowflag := 0;
    if (key = VK_HOME) then begin     //程序最小步骤
      keyArrowflag := keyArrowflag + 1;
      modbusfun06dat := 1;
      modbusfun06 := $10B0;
      edt45.Text := '1';
      StepNowFresh := 0;
      zuSetfocus((hh div 60) + 1, StepNowFresh + 1);
    end;
    if (key = VK_END) then begin      //最大
      keyArrowflag := keyArrowflag + 1;
      modbusfun06dat := 38 * VIRLCROWS; //1900;
      modbusfun06 := $10B0;
      edt45.Text := inttostr(38 * VIRLCROWS); //'900';
      StepNowFresh := VIRLCROWS - 1;
      zuSetfocus((hh div 60) + 1, StepNowFresh + 1);
    end;
    if (GetKeyState(VK_CONTROL) and $8000) = $8000 then begin
      if key = VK_SPACE then
        N60Click(form_main);
      Exit;
    end;

    if (key = VK_PRIOR) then begin     //上翻页   or(Key=Byte('R'))
      keyArrowflag := keyArrowflag + 1;
      btn12Click(form_main);
      if IsNumberic(edt45.Text) and (StrToInt(edt45.Text) > VIRLCROWS) then
        edt45.Text := IntToStr(StrToInt(edt45.Text) - VIRLCROWS);
    end;
    if (key = VK_NEXT) then begin      //下翻页 or(Key=Byte('F'))
      keyArrowflag := keyArrowflag + 1;
      btn13Click(form_main);
      if IsNumberic(edt45.Text) and (StrToInt(edt45.Text) + VIRLCROWS <= 38 * VIRLCROWS) then // 1900
        edt45.Text := IntToStr(StrToInt(edt45.Text) + VIRLCROWS);
    end;

    if (key = VK_LEFT) then begin    // or(Key=Byte('A'))
      keyArrowflag := keyArrowflag + 1;
      if hh > 60 then
        hh := hh - 60;
      zuSetfocus((hh div 60) + 1, StepNowFresh + 1);
    end;
    if (key = VK_RIGHT) then begin    // or(Key=Byte('D'))
      keyArrowflag := keyArrowflag + 1;
      hh := hh + 60;
      zuSetfocus((hh div 60) + 1, StepNowFresh + 1);
    end;
    if (key = VK_UP) then begin    //or(Key=Byte('W'))
      keyArrowflag := keyArrowflag + 1;
      if (StepNowFresh > 0) then begin
        StepNowFresh := StepNowFresh - 1;
        zuSetfocus((hh div 60) + 1, StepNowFresh + 1);
        if StrToInt(edt45.Text) > 1 then begin
          modbusfun06dat := ((StrToInt(edt45.Text) - 1) div VIRLCROWS) * VIRLCROWS + 1 + StepNowFresh;
          modbusfun06 := $10B0;   //等同          //btn18Click(Self);
          edt45.Text := IntToStr(modbusfun06dat);          //  edt45.Text:=IntToStr(StrToInt(edt45.Text)-1);
        end;
      end
      else if StrToInt(edt45.Text) > 1 then begin
        StepNowFresh := VIRLCROWS - 1;
        zuSetfocus((hh div 60) + 1, StepNowFresh + 1);    //最后一行
        if StrToInt(edt45.Text) > 1 then begin
          modbusfun06dat := ((StrToInt(edt45.Text) - VIRLCROWS) div VIRLCROWS) * VIRLCROWS + 1 + StepNowFresh; //+1+StepNowFresh-1;
          modbusfun06 := $10B0;   //等同          //btn18Click(Self);
          edt45.Text := IntToStr(modbusfun06dat);          //  edt45.Text:=IntToStr(StrToInt(edt45.Text)-1);
        end;
      end;
    end;
    if (key = VK_DOWN) then begin   //or(Key=Byte('S'))
      keyArrowflag := keyArrowflag + 1;
      if (StepNowFresh + 1 < VIRLCROWS) then begin      //同一页
        StepNowFresh := StepNowFresh + 1;
        zuSetfocus((hh div 60) + 1, (StepNowFresh + 1));    //+1
        modbusfun06dat := ((StrToInt(edt45.Text) - 1) div VIRLCROWS) * VIRLCROWS + 1 + StepNowFresh;
      end
      else begin
        StepNowFresh := 0;
        zuSetfocus((hh div 60) + 1, (StepNowFresh + 1));    //+1
        modbusfun06dat := ((StrToInt(edt45.Text) - 1) div VIRLCROWS) * VIRLCROWS + VIRLCROWS + 1 + StepNowFresh;
      end;
      modbusfun06 := $10B0;   //等同         //btn17Click(Self);
      edt45.Text := IntToStr(modbusfun06dat);         //  edt45.Text:=IntToStr(StrToInt(edt45.Text)+1);
    end;
    //   StepNowFresh:=(TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(form_main.edt121.Text);   //刷新开始行数
    //   hh:=TEdit(Sender).Left+panel1.HorzScrollBar.Position +10 ;      //+10补偿模式编辑框左移！
    if IsNumberic(edt45.Text) and (StrToInt(edt45.Text) > 0) then
      StepNowPage := (StrToInt(edt45.Text) - 1) div VIRLCROWS;
    if (key = VK_ESCAPE) then begin
      AreaDisSelect(0);
    end
    else if keyArrowflag > 0 then begin   //方向按键选择！    GetKeyState(VK_SHIFT)<0 then begin
      if (mmo22.Tag = -1) then
        mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行
      if (mmo24.Tag = -1) then
        mmo24.Tag := mmo22.Tag;            //最小行
      if StepNowFresh + StepNowPage * VIRLCROWS <= mmo24.Tag then
        mmo24.Tag := StepNowFresh + StepNowPage * VIRLCROWS
      else begin
        mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
      end;

      if (mmo23.Tag = -1) then
        mmo23.Tag := hh div 60;             //当前列：0...
      if (mmo25.Tag = -1) then
        mmo25.Tag := mmo23.Tag;           //最小列
      if (hh div 60) < mmo25.Tag then
        mmo25.Tag := hh div 60
      else begin
        mmo23.Tag := hh div 60;             //当前列：0...
      end;

      for ii := 1 to VIRLCROWS do begin        //颜色先还原
        if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
          zu1edit[ii].Color := clWhite;
        zu2edit[ii].Color := clWhite;
        zu3edit[ii].Color := clWhite;

        if (zu5edit[ii].Color <> clGreen) then
          zu5edit[ii].Color := clWhite;
        if (zu6edit[ii].Color <> clGreen) then
          zu6edit[ii].Color := clWhite;
        zu7edit[ii].Color := clWhite;
        zu8edit[ii].Color := clWhite;
        if (zu9edit[ii].Color <> clGray) then
          zu9edit[ii].Color := clWhite;
        zu10edit[ii].Color := clWhite;
        zu11edit[ii].Color := clWhite;
        zu12edit[ii].Color := clWhite;
        zu13edit[ii].Color := clWhite;
        zu14edit[ii].Color := clWhite;
        zu15edit[ii].Color := clWhite;
        zu16edit[ii].Color := clWhite;
      end;
      for ii := mmo24.Tag to mmo22.Tag do begin
        if (ii < StepNowPage * VIRLCROWS) then
          Continue;   //小于当前页最小值
        if (ii >= StepNowPage * VIRLCROWS + VIRLCROWS) then
          Break;   //大于等于后一页最小值
        for jj := mmo25.Tag to mmo23.Tag do begin          //  if(lineInsert+ii)>VIRLCROWS then Break;

          nline := (ii mod VIRLCROWS) + 1;
          if (jj = 0) and (zu1edit[nline].text <> '0') and (zu1edit[nline].Color <> clred) then
            zu1edit[nline].Color := clMoneyGreen;    //名字和数组都是从1...开始
          if jj = 1 then
            zu2edit[nline].Color := clMoneyGreen;
          if jj = 2 then
            zu3edit[nline].Color := clMoneyGreen;

          if (jj = 4) and (zu5edit[nline].Color <> clGreen) then
            zu5edit[nline].Color := clMoneyGreen;
          if (jj = 5) and (zu6edit[nline].Color <> clGreen) then
            zu6edit[nline].Color := clMoneyGreen;
          if jj = 6 then
            zu7edit[nline].Color := clMoneyGreen;
          if jj = 7 then
            zu8edit[nline].Color := clMoneyGreen;
          if (jj = 8) and (zu9edit[nline].Color <> clGray) then
            zu9edit[nline].Color := clMoneyGreen;
          if (jj = 9) and (zu10edit[nline].Color <> clYellow) then
            zu10edit[nline].Color := clSkyBlue;
          if jj = 10 then
            zu11edit[nline].Color := clMoneyGreen;
          if jj = 11 then
            zu12edit[nline].Color := clMoneyGreen;
          if jj = 12 then
            zu13edit[nline].Color := clMoneyGreen;
          if jj = 13 then
            zu14edit[nline].Color := clMoneyGreen;
          if jj = 14 then
            zu15edit[nline].Color := clMoneyGreen;
          if jj = 15 then
            zu16edit[nline].Color := clMoneyGreen;
        end;
      end;
    end;

    if (key = VK_DELETE) then begin     //删除  VK_BACK
      D5Click(form_main);
    end;
    if (key = VK_INSERT) then begin   //or(Key=Byte('T'))
      if IsNumberic(form_main.edt45.Text) then begin
        FormInsertN.edt1.Text := form_main.edt45.Text;
      end;
      I2Click(form_main);
    end;
   //   if (Key=Byte('G'))  then begin          //copy步骤
   //      I3Click(form_main);
   //   end;

  end;
end;

procedure sysParaKey(Sender: TObject; key: Word; hh: SmallInt);
var
  ii: SmallInt;
begin
  with form_main do begin
    if (hh div 60) = 17 - 1 then begin                //
      if (key = VK_ADD) or (key = 187) then begin           //微调不记录Log!
        if IsNumberic(zu17edit[StepNowFresh + 1].text[1]) then begin
          ii := StrToInt(zu17edit[StepNowFresh + 1].text[1]);
          if ii < 9 then begin
            modbusfun16 := $3012 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32;
            modbusfun16str := IntToStr(ii + 1) + Copy(zu17edit[StepNowFresh + 1].text, 2, 15);
            modbusfun16len := 8;
          end;
        end;
      end;
      if (key = VK_SUBTRACT) or (key = 189) then begin       //大键盘‘-‘
        if IsNumberic(zu17edit[StepNowFresh + 1].text[1]) then begin
          ii := StrToInt(zu17edit[StepNowFresh + 1].text[1]);
          if ii > 0 then begin
            modbusfun16 := $3012 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32;
            modbusfun16str := IntToStr(ii - 1) + Copy(zu17edit[StepNowFresh + 1].text, 2, 15);
            modbusfun16len := 8;
          end;
        end;
      end;
     {  if Key=VK_MULTIPLY then begin
          if IsNumberic(zu17edit[StepNowFresh+1].text[4]) then begin
            ii:=StrToInt(zu17edit[StepNowFresh+1].text[4]);
            if ii<9 then begin
               modbusfun16:=$3012+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10) div strtoint(form_main.edt121.Text) ) *32 ;
               modbusfun16str:=Copy(zu17edit[StepNowFresh+1].text,1,3)+IntToStr(ii+1);
                modbusfun16len:=2;
            end;
          end;
       end;
       if Key=VK_DIVIDE then begin
          if IsNumberic(zu17edit[StepNowFresh+1].text[4]) then begin
            ii:=StrToInt(zu17edit[StepNowFresh+1].text[4]);
            if ii>0 then begin
               modbusfun16:=$3012+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10) div strtoint(form_main.edt121.Text) ) *32 ;
               modbusfun16str:=Copy(zu17edit[StepNowFresh+1].text,1,3)+IntToStr(ii-1);
                modbusfun16len:=2;
            end;

          end;
       end;  }
    end
    else if (hh div 60) = 2 - 1 then begin                //
      if (key = VK_ADD) or (key = 187) then begin           //微调不记录Log!
        if IsNumberic(zu2edit[StepNowFresh + 1].text[1]) then begin
          ii := StrToInt(zu2edit[StepNowFresh + 1].text[1]);
          if ii < 9 then begin
            modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 0;
            modbusfun16str := IntToStr(ii + 1) + Copy(zu2edit[StepNowFresh + 1].text, 2, 3);
            modbusfun16len := 2;
          end;
        end;
      end;
      if (key = VK_SUBTRACT) or (key = 189) then begin       //大键盘‘-‘
        if IsNumberic(zu2edit[StepNowFresh + 1].text[1]) then begin
          ii := StrToInt(zu2edit[StepNowFresh + 1].text[1]);
          if ii > 0 then begin
            modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 0;
            modbusfun16str := IntToStr(ii - 1) + Copy(zu2edit[StepNowFresh + 1].text, 2, 3);
            modbusfun16len := 2;
          end;
        end;
      end;
      if key = VK_MULTIPLY then begin
        if IsNumberic(zu2edit[StepNowFresh + 1].text[4]) then begin
          ii := StrToInt(zu2edit[StepNowFresh + 1].text[4]);
          if ii < 9 then begin
            modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 0;
            modbusfun16str := Copy(zu2edit[StepNowFresh + 1].text, 1, 3) + IntToStr(ii + 1);
            modbusfun16len := 2;
          end;
        end;
      end;
      if key = VK_DIVIDE then begin
        if IsNumberic(zu2edit[StepNowFresh + 1].text[4]) then begin
          ii := StrToInt(zu2edit[StepNowFresh + 1].text[4]);
          if ii > 0 then begin
            modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 0;
            modbusfun16str := Copy(zu2edit[StepNowFresh + 1].text, 1, 3) + IntToStr(ii - 1);
            modbusfun16len := 2;
          end;

        end;
      end;
    end
    else if (hh div 60) = 10 - 1 then begin                //
      if (key = VK_ADD) or (key = 187) then begin           //微调不记录Log!
        if IsNumberic(zu10edit[StepNowFresh + 1].text[1]) then begin
          ii := StrToInt(zu10edit[StepNowFresh + 1].text[1]);
          if ii < 9 then begin
            modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 1;
            modbusfun16str := IntToStr(ii + 1) + Copy(zu10edit[StepNowFresh + 1].text, 2, 3);
            modbusfun16len := 2;
          end;
        end;
      end;
      if (key = VK_SUBTRACT) or (key = 189) then begin       //大键盘‘-‘
        if IsNumberic(zu10edit[StepNowFresh + 1].text[1]) then begin
          ii := StrToInt(zu10edit[StepNowFresh + 1].text[1]);
          if ii > 0 then begin
            modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 1;
            modbusfun16str := IntToStr(ii - 1) + Copy(zu10edit[StepNowFresh + 1].text, 2, 3);
            modbusfun16len := 2;
          end;
        end;
      end;
      if key = VK_MULTIPLY then begin
        if IsNumberic(zu10edit[StepNowFresh + 1].text[4]) then begin
          ii := StrToInt(zu10edit[StepNowFresh + 1].text[4]);
          if ii < 9 then begin
            modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 1;
            modbusfun16str := Copy(zu10edit[StepNowFresh + 1].text, 1, 3) + IntToStr(ii + 1);
            modbusfun16len := 2;
          end;
        end;
      end;
      if key = VK_DIVIDE then begin
        if IsNumberic(zu10edit[StepNowFresh + 1].text[4]) then begin
          ii := StrToInt(zu10edit[StepNowFresh + 1].text[4]);
          if ii > 0 then begin
            modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 1;
            modbusfun16str := Copy(zu10edit[StepNowFresh + 1].text, 1, 3) + IntToStr(ii - 1);
            modbusfun16len := 2;
          end;

        end;
      end;
    end
    else if ((hh div 60) > 1) and ((hh div 60) < 16) then begin                //
      if (key = VK_ADD) or (key = 187) then begin           //微调不记录Log!
        case (hh div 60) + 1 of
          1:
            if IsSignedNumbericHex(zu1edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + 0;
              modbusfun06dat := StrToInt(zu1edit[StepNowFresh + 1].text) + 1;
            end;
          2:
            if IsSignedNumbericHex(zu2edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 0;
              modbusfun06dat := StrToInt(zu2edit[StepNowFresh + 1].text) + 1;
            end;

          3:
            if IsSignedNumbericHex(zu3edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu3edit[StepNowFresh + 1].text) + 1;
            end;
          4:
            if IsSignedNumbericHex(zu4edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu4edit[StepNowFresh + 1].text) + 1;
            end;
          5:
            if IsSignedNumbericHex(zu5edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu5edit[StepNowFresh + 1].text) + 1;
            end;
          6:
            if IsSignedNumbericHex(zu6edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu6edit[StepNowFresh + 1].text) + 1;
            end;
          7:
            if IsSignedNumbericHex(zu7edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu7edit[StepNowFresh + 1].text) + 1;
            end;
          8:
            if IsSignedNumbericHex(zu8edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu8edit[StepNowFresh + 1].text) + 1;
            end;
          9:
            if IsSignedNumbericHex(zu9edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu9edit[StepNowFresh + 1].text) + 1;
            end;

          11:
            if IsSignedNumbericHex(zu11edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu11edit[StepNowFresh + 1].text) + 1;
            end;
          12:
            if IsSignedNumbericHex(zu12edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu12edit[StepNowFresh + 1].text) + 1;
            end;
          13:
            if IsSignedNumbericHex(zu13edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu13edit[StepNowFresh + 1].text) + 1;
            end;
          14:
            if IsSignedNumbericHex(zu14edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu14edit[StepNowFresh + 1].text) + 1;
            end;
          15:
            if IsSignedNumbericHex(zu15edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu15edit[StepNowFresh + 1].text) + 1;
            end;
          16:
            if IsSignedNumbericHex(zu16edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu16edit[StepNowFresh + 1].text) + 1;
            end;
          17:
            if IsSignedNumbericHex(zu17edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu17edit[StepNowFresh + 1].text) + 1;
            end;
        end;

      end;
      if (key = VK_SUBTRACT) or (key = 189) then begin       //大键盘‘-‘
        case (hh div 60) + 1 of
          1:
            if IsSignedNumbericHex(zu1edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + 0;
              modbusfun06dat := StrToInt(zu1edit[StepNowFresh + 1].text) - 1;
            end;
          2:
            if IsSignedNumbericHex(zu2edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 0;
              modbusfun06dat := StrToInt(zu2edit[StepNowFresh + 1].text) - 1;
            end;
          3:
            if IsSignedNumbericHex(zu3edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu3edit[StepNowFresh + 1].text) - 1;
            end;
          4:
            if IsSignedNumbericHex(zu4edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu4edit[StepNowFresh + 1].text) - 1;
            end;
          5:
            if IsSignedNumbericHex(zu5edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu5edit[StepNowFresh + 1].text) - 1;
            end;
          6:
            if IsSignedNumbericHex(zu6edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu6edit[StepNowFresh + 1].text) - 1;
            end;
          7:
            if IsSignedNumbericHex(zu7edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu7edit[StepNowFresh + 1].text) - 1;
            end;
          8:
            if IsSignedNumbericHex(zu8edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu8edit[StepNowFresh + 1].text) - 1;
            end;
          9:
            if IsSignedNumbericHex(zu9edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 1;
              modbusfun06dat := StrToInt(zu9edit[StepNowFresh + 1].text) - 1;
            end;

          11:
            if IsSignedNumbericHex(zu11edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu11edit[StepNowFresh + 1].text) - 1;
            end;
          12:
            if IsSignedNumbericHex(zu12edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu12edit[StepNowFresh + 1].text) - 1;
            end;
          13:
            if IsSignedNumbericHex(zu13edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu13edit[StepNowFresh + 1].text) - 1;
            end;
          14:
            if IsSignedNumbericHex(zu14edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu14edit[StepNowFresh + 1].text) - 1;
            end;
          15:
            if IsSignedNumbericHex(zu15edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu15edit[StepNowFresh + 1].text) - 1;
            end;
          16:
            if IsSignedNumbericHex(zu16edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu16edit[StepNowFresh + 1].text) - 1;
            end;
          17:
            if IsSignedNumbericHex(zu17edit[StepNowFresh + 1].text) then begin
              modbusfun06 := $3000 + StepNowFresh * 32 + (hh div 60) + 2;
              modbusfun06dat := StrToInt(zu17edit[StepNowFresh + 1].text) - 1;
            end;
        end;
      end;
      if key = VK_MULTIPLY then begin
        if IsNumberic(zu10edit[StepNowFresh + 1].text[4]) then begin
          ii := StrToInt(zu10edit[StepNowFresh + 1].text[4]);
          if ii < 9 then begin
            modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 1;
            modbusfun16str := Copy(zu10edit[StepNowFresh + 1].text, 1, 3) + IntToStr(ii + 1);
            modbusfun16len := 2;
          end;
        end;
      end;
      if key = VK_DIVIDE then begin
        if IsNumberic(zu10edit[StepNowFresh + 1].text[4]) then begin
          ii := StrToInt(zu10edit[StepNowFresh + 1].text[4]);
          if ii > 0 then begin
            modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 1;
            modbusfun16str := Copy(zu10edit[StepNowFresh + 1].text, 1, 3) + IntToStr(ii - 1);
            modbusfun16len := 2;
          end;

        end;
      end;
    end;
  end;
end;

procedure bakToServerDisk();
var
  f: TextFile;
  Edt2temp, save_file_name, ss, save_file_name_Sever: string;

  procedure MoveBarcodefile(barfiletype: SmallInt);        //定时：条码文件复制到网盘；
  var
    SearchRec: TSearchRec;
    found, fCopynum: integer;
    F: TextFile;
    ss, strc, PNnameTemp: string;
  begin
    with form_main do begin                 //仅找一个txt文件 Result := TStringList.Create;
      Edt2temp := ''; //temp\';
      //---------轮询 遍历其它机种文件---------------------------------------

      fCopynum := 0;                                                              //单次上传计数；

      PNnameTemp := edt77.text;
      found := FindFirst(edt2.Text + Edt2temp + PNnameTemp + '\' + '*.csv', faAnyFile, SearchRec); //第一文件
      while found = 0 do begin             //找到文件             //AssignFile(F,edt2.Text+lbl90.hint+'\' +SearchRec.Name);
        if lbl181.Caption <> '1' then         //状态改变，退出定时上传
          Exit;
        save_file_name := PNnameTemp + '\' + SearchRec.Name;
        save_file_name_Sever := SearchRec.Name;
        if (Label10.caption <> save_file_name) then             //非当前被占用文件
        try
          if not DirectoryExists(edt1.Text) then begin
            edt1.Color := clRed;
            stat1.Panels[3].Text := 'SerVer Folder not exist!';
            Exit;
          end
          else begin
            if not CopyFile(PChar(edt2.Text + Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then begin //如果文件已存在则返回错误
              stat1.Panels[4].Text := 'Copy File Failed!';
                  //Exit;
            end
            else begin
              if not DirectoryExists(edt2.Text + lbl183.Caption) then                 //备份路径
              try
                begin
                  CreateDir(edt2.Text + lbl183.Caption);  //创建目录
                end;
              except  //finally
                stat1.Panels[4].Text := 'Cannot Create ' + edt2.Text + lbl183.Caption;
                Exit;
              end;
              if not DirectoryExists(edt2.Text + lbl183.Caption + PNnameTemp + '\') then   //PN路径
              try
                begin
                  CreateDir(edt2.Text + lbl183.Caption + PNnameTemp + '\');  //创建目录
                end;
              except  //finally
                stat1.Panels[4].Text := 'Cannot Create ' + edt2.Text + lbl183.Caption + PNnameTemp + '\';
                Exit;
              end;                                                          //重复copy不会成功，防止遗漏！
              CopyFile(PChar(edt2.Text + Edt2temp + save_file_name), PChar(edt2.Text + lbl183.Caption + save_file_name), True); ////RenameFile(edt2.Text + save_file_name, edt2.Text +'bak'+ save_file_name);
              DeleteFile(edt2.Text + Edt2temp + save_file_name);
              stat1.Panels[4].Text := 'Copy File Succeed!';
            end;
            fCopynum := fCopynum + 1;
            if fCopynum > 10 * strtoint(edt6.Text) then
              Exit;
          end;
        finally
          //Closefile(f);
        end;
        found := FindNext(SearchRec);
      end;
      FindClose(SearchRec);

      if (edt157.Visible) then begin                //单PCS路径不同，则分别轮询；不用备份  and(edt2.Text<>edt157.Text)
        fCopynum := 0;                                                              //单次上传计数；

        if readchis('Comm Para', 'FileExtName') <> '' then
          found := FindFirst(edt157.Text + Edt2temp + PNnameTemp + '\' + '*.' + readchis('Comm Para', 'FileExtName'), faAnyFile, SearchRec) //第一文件
        else
          found := FindFirst(edt157.Text + Edt2temp + PNnameTemp + '\' + '*.txt', faAnyFile, SearchRec); //第一文件
        while found = 0 do begin             //找到文件             //AssignFile(F,edt2.Text+lbl90.hint+'\' +SearchRec.Name);
          if lbl181.Caption <> '1' then         //状态改变，退出定时上传
            Exit;
          save_file_name := PNnameTemp + '\' + SearchRec.Name;
          save_file_name_Sever := SearchRec.Name;               //一次保存完，不存在占用！if(Label10.caption<>save_file_name) then             //非当前被占用文件
          try
            if not DirectoryExists(edt1.Text) then begin
              edt1.Color := clRed;
              stat1.Panels[3].Text := 'SerVer Folder not exist!';
              Exit;
            end
            else begin
              if not CopyFile(PChar(edt157.Text + Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then begin //如果文件已存在则返回错误
                stat1.Panels[4].Text := 'Copy File Failed!';
                    //Exit;
              end
              else begin
                DeleteFile(edt157.Text + Edt2temp + save_file_name);
                stat1.Panels[4].Text := 'Copy File Succeed!';
              end;
              fCopynum := fCopynum + 1;
              if fCopynum > 10 * strtoint(edt6.Text) then
                Exit;
            end;
          finally             //Closefile(f);
          end;
          found := FindNext(SearchRec);
        end;
        FindClose(SearchRec);
      end;
    end;
  end;

begin
  with form_main do begin
    if ((Label10.caption <> 'Label10') and (lbl181.Caption = '10')) then    //1.关闭窗口；仅一次，对其它程序无影响；第一文件子路径+文件名 已生成！
    begin
      Edt2temp := ''; //temp\';                                              //SMT默认：集总文件复制到临时文件夹temp\并剪切到映射盘

      save_file_name := Label10.caption;
      if Pos('\', Label10.caption) > 0 then
        save_file_name_Sever := copy(Label10.caption, pos('\', Label10.caption) + 1, length(Label10.caption))
      else
        save_file_name_Sever := Label10.caption;
      try
      //--1------------------条码文件先备份起来 -----------------------------
        if not DirectoryExists(edt2.Text + lbl183.Caption) then
        try
          begin
            CreateDir(edt2.Text + lbl183.Caption);  //创建备份目录
          end;
        except  //finally
          stat1.Panels[4].Text := 'Cannot Create ' + edt2.Text + lbl183.Caption;
          Exit;
        end;
        if not DirectoryExists(edt2.Text + lbl183.Caption + lbl90.hint + '\') then                 //本地备份路径
        try
          begin
            CreateDir(edt2.Text + lbl183.Caption + lbl90.hint + '\');  //创建备份子目录
          end;
        except  //finally
          stat1.Panels[4].Text := 'Cannot Create ' + edt2.Text + lbl183.Caption + lbl90.hint + '\';
          Exit;
        end;
        CopyFile(PChar(edt2.Text + save_file_name), PChar(edt2.Text + lbl183.Caption + save_file_name), True);

        if not DirectoryExists(edt1.Text) then begin
          if CopyFile(PChar(edt2.Text + save_file_name), PChar(edt2.Text + Edt2temp + save_file_name), True) then
            DeleteFile(edt2.Text + save_file_name);
          edt1.Color := clRed;
          stat1.Panels[3].Text := 'SerVer Folder not exist!';
        end
        else begin
          if not CopyFile(PChar(edt2.Text + Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then  //如果文件已存在则返回错误
            stat1.Panels[4].Text := 'Copy File Failed!'
          else begin
            DeleteFile(edt2.Text + Edt2temp + save_file_name);
            stat1.Panels[4].Text := 'Copy File Succeed!';
          end;
        end;
      finally
      end;
    end;

    if lbl181.Caption = '1' then begin                        //2.轮询时间到，一次结束； //if stat1.Panels[4].Text='Copy File Succeed!' then
      MoveBarcodefile(0);
      lbl181.Caption := '0';
    end
    else if ((lbl181.Caption = '5')) then begin           //3.刚生成条码文件（早期jaguar剪切单PCS文件到映射盘）
      if ((lbl180.caption <> 'lbl180') and edt157.Visible) then    //生成单PCS文件时，触发进入剪切单PCS文件
      begin
        Edt2temp := ''; //temp\';
        save_file_name := lbl180.caption;
        if Pos('\', lbl180.caption) > 0 then
          save_file_name_Sever := copy(lbl180.caption, pos('\', lbl180.caption) + 1, length(lbl180.caption))
        else
          save_file_name_Sever := lbl180.caption;
        try
          if not DirectoryExists(edt1.Text) then begin
            edt1.Color := clRed;
            stat1.Panels[3].Text := 'SerVer Folder not exist!';
          end
          else begin
            if not CopyFile(PChar(edt157.Text + Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then  //如果文件已存在则返回错误
              stat1.Panels[4].Text := 'Copy File Failed!'
            else begin
              DeleteFile(edt157.Text + Edt2temp + save_file_name);
              stat1.Panels[4].Text := 'Copy File Succeed!';
            end;
          end;
        finally
        end;
      end;
    end;
  end;

end;

end.

