{  描述：    主启动问题
  文件名：	 main
  应用语言:  delphi7
  版本：		 V1.0
  Copyright(C) Jin 2013   All rights reserved
  作者：Jin    公众号：zhi至研测控
  建立日期：2019-05-09
、
v557.03
  1.修正strtofloat(遇到hex数据卡死问题；
  2.在标题栏显示公司信息、版本号等信息
  3.输出.exe文件路径选择桌面，以免不必要的文件上传gitee

v557.01
  1. 增加串口助手log

v556.02
  1.手动发送统一解析指令！ (form_main.btn91.Tag>0) )                    //手动发送
v556.01
  1. 简化控件安装
  2.Unit3matrix衔接口文件改名：Unit3appFSM

     }
unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, ValEdit, Mask, Buttons, SPComm,
  Unit0_globalVariant, ComCtrls, IniFiles, FileCtrl, strUtils, sEdit, sSpinEdit,
  sMaskEdit, sCustomComboEdit, sToolEdit, DBCtrls, Printers, ActnList, jpeg,
  iComponent, iVCLComponent, iCustomComponent, iPlotComponent, iPlot, IdUDPBase,
  IdUDPServer, IdBaseComponent, IdComponent, IdIPWatch, ScktComp,
  IdAntiFreezeBase, IdAntiFreeze, IdTCPConnection, IdTCPClient, IdSocketHandle,
  IdUDPClient, IdHTTP, HTTPApp, Menus, ToolWin, sScrollBox, iLed, iLedRound,
  sMemo, iLabel, iEditCustom, iEdit, iLedArrow, sLabel, iLedMatrix, shellapi,
  sRadioButton, sBitBtn, acProgressBar, sListView, sSplitter, TLHelp32, acPNG,
  acImage, iSevenSegmentDisplay, iSevenSegmentClock, acPathDialog, pngimage,
  IdMultipartFormData, IdSMTP, IdMessage, IdMessageClient, TeEngine, Series,
  TeeProcs, Chart, OleCtrls, VCFI, TeeFunci, JvHidControllerClass, Search,
  Unit3appFSM, Consts, Clipbrd, math;

const
  xlUnderlineStyleSingle = 2;
  xlCenter = -4108;
  xlBottom = -4107;

type
  TReport = packed record
    ReportID: Byte;
    Bytes: array[0..63] of Byte;
  end;

  TForm_main = class(TForm)
    Comm1: TComm;
    SpeedButton12: TSpeedButton;
    SpeedButton13: TSpeedButton;
    stat1: TStatusBar;
    tmr1: TTimer;
    tmr2: TTimer;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Label2: TLabel;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    edt2: TEdit;
    btn3: TButton;
    GroupBox5: TGroupBox;
    grp2: TGroupBox;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    lbl15: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    lbl17: TLabel;
    lbl18: TLabel;
    lbl19: TLabel;
    lbl20: TLabel;
    lbl21: TLabel;
    lbl22: TLabel;
    lbl23: TLabel;
    lbl24: TLabel;
    lbl25: TLabel;
    Label10: TLabel;
    lbl16: TLabel;
    Label11: TLabel;
    ts1: TTabSheet;
    grp3: TGroupBox;
    btn16: TBitBtn;
    btn14: TBitBtn;
    btn15: TBitBtn;
    chk1: TCheckBox;
    chk2: TCheckBox;
    chk3: TCheckBox;
    chk4: TCheckBox;
    BitBtn1: TBitBtn;
    TabSheet4: TTabSheet;
    btn23: TSpeedButton;
    btn24: TSpeedButton;
    lbl28: TLabel;
    dlgOpen1: TOpenDialog;
    lbl29: TLabel;
    btn2: TButton;
    grp1: TGroupBox;
    edt1: TEdit;
    btn4: TButton;
    edt6: TEdit;
    lbl2: TLabel;
    edt38: TEdit;
    lbl38: TLabel;
    btn8: TButton;
    lbl43: TLabel;
    lbl44: TLabel;
    lbl45: TLabel;
    lbl46: TLabel;
    lbl47: TLabel;
    lbl48: TLabel;
    lbl49: TLabel;
    lbl50: TLabel;
    lbl51: TLabel;
    lbl52: TLabel;
    lbl53: TLabel;
    lbl54: TLabel;
    lbl55: TLabel;
    lbl56: TLabel;
    lbl57: TLabel;
    lbl58: TLabel;
    lbl59: TLabel;
    lbl60: TLabel;
    ts3: TTabSheet;
    DevListBox: TListBox;
    HistoryListBox: TListBox;
    grp6: TGroupBox;
    grp9: TGroupBox;
    ClearBtn: TSpeedButton;
    ReadBtn: TSpeedButton;
    WriteBtn: TSpeedButton;
    SaveDialog: TSaveDialog;
    edit1: TMaskEdit;
    chk5: TCheckBox;
    dlgPnt1: TPrintDialog;
    dlgPntSet1: TPrinterSetupDialog;
    lbl63: TLabel;
    GpcResult: TPageControl;
    ts4: TTabSheet;
    mmo2: TMemo;
    tmr3: TTimer;
    edt45: TEdit;
    lbl64: TLabel;
    ts6: TTabSheet;
    tmr4: TTimer;
    btn28: TButton;
    grp10: TGroupBox;
    img3: TImage;
    tmr5: TTimer;
    IdIPWatch1: TIdIPWatch;
    shp2: TShape;
    btn27: TBitBtn;
    btn29: TButton;
    chk7: TCheckBox;
    chk8: TCheckBox;
    chk9: TCheckBox;
    mm1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    Panel1: TScrollBox;
    x1: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N18: TMenuItem;
    q1: TMenuItem;
    f1: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N12: TMenuItem;
    N22: TMenuItem;
    Panel2: TPanel;
    edt55: TEdit;              //不使用
    D1: TMenuItem;
    S2: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    H1: TMenuItem;
    S3: TMenuItem;
    B1: TMenuItem;
    btn17: TSpeedButton;
    btn18: TSpeedButton;
    N30: TMenuItem;
    O1: TMenuItem;
    P1: TMenuItem;
    U1: TMenuItem;
    N31: TMenuItem;
    P2: TMenuItem;
    S4: TMenuItem;
    O2: TMenuItem;
    N24: TMenuItem;
    P3: TMenuItem;
    P4: TMenuItem;
    U2: TMenuItem;
    D4: TMenuItem;
    I2: TMenuItem;
    D5: TMenuItem;
    N33: TMenuItem;
    C1: TMenuItem;
    ts7: TTabSheet;
    Rled1: TiLedRound;
    scrlbx2: TScrollBox;
    edt59: TsEdit;
    edt60: TsEdit;
    lbl68: TLabel;
    lbl72: TLabel;
    I3: TMenuItem;
    lbl73: TLabel;
    ildrw1: TiLedArrow;
    N36: TMenuItem;
    C2: TMenuItem;
    lbl78: TLabel;
    edt62: TEdit;
    ts8: TTabSheet;
    lbl79: TLabel;
    lbl80: TLabel;
    lbl81: TLabel;
    lbl82: TLabel;
    edt4: TEdit;
    edt63: TEdit;
    edt64: TEdit;
    edt65: TEdit;
    edt66: TEdit;
    lbl83: TLabel;
    grp17: TGroupBox;
    N37: TMenuItem;
    N38: TMenuItem;
    edt69: TEdit;
    edt70: TEdit;
    lbl75: TLabel;
    lbl76: TLabel;
    edt71: TEdit;
    lbl84: TLabel;
    edt74: TEdit;
    lbl88: TLabel;
    chk10: TCheckBox;
    mmo5: TMemo;
    edt75: TEdit;
    btn55: TButton;
    lbl89: TLabel;
    ts9: TTabSheet;
    mmo6: TMemo;
    edt76: TEdit;
    grp18: TGroupBox;
    lbl90: TLabel;
    lbl91: TLabel;
    lbl92: TLabel;
    lbl93: TLabel;
    edt77: TEdit;
    edt78: TEdit;
    edt79: TEdit;
    edt80: TEdit;
    btn56: TSpeedButton;
    edt81: TEdit;
    lbl94: TLabel;
    edt82: TEdit;
    N39: TMenuItem;
    btn1: TSpeedButton;
    edt7: TEdit;
    lbl6: TLabel;
    lbl41: TLabel;
    lbl42: TLabel;
    edt8: TEdit;
    edt9: TEdit;
    lbl98: TLabel;
    edt11: TEdit;
    edt14: TEdit;
    edt15: TEdit;
    lbl114: TLabel;
    ts12: TTabSheet;
    j1: TMenuItem;
    edt96: TEdit;
    lbl119: TLabel;
    grp23: TGroupBox;
    mmo13: TMemo;
    L2: TMenuItem;
    mmo14: TMemo;
    mmo15: TMemo;
    N42: TMenuItem;
    N43: TMenuItem;
    ts13: TTabSheet;
    mmo16: TMemo;
    lbl133: TLabel;
    edt111: TEdit;
    btn75: TSpeedButton;
    lbl134: TLabel;
    edt112: TEdit;
    lbl135: TLabel;
    medit1: TMaskEdit;
    lbl137: TLabel;
    lbl139: TLabel;
    lbl140: TLabel;
    btn77: TBitBtn;
    btn78: TBitBtn;
    pgc1: TPageControl;
    ts15: TTabSheet;
    ts16: TTabSheet;
    grp28: TGroupBox;
    pnl1: TPanel;
    iLedRound1: TiLedRound;
    iLedRound2: TiLedRound;
    iLabel1: TiLabel;
    iLabel2: TiLabel;
    edt120: TEdit;
    lbl148: TLabel;
    C4: TMenuItem;
    N34: TMenuItem;
    edt121: TEdit;
    N47: TMenuItem;
    btn92: TButton;
    tmr6: TTimer;
    grp30: TGroupBox;
    edt48: TEdit;
    lbl151: TLabel;
    edt123: TEdit;
    edt124: TEdit;
    edt125: TEdit;
    edt126: TEdit;
    grp31: TGroupBox;
    spl1: TSplitter;
    spl2: TSplitter;
    grp32: TGroupBox;
    spl3: TSplitter;
    lbl155: TLabel;
    lbl156: TLabel;
    edt130: TEdit;
    grp34: TGroupBox;
    rb3: TsRadioButton;
    rb4: TsRadioButton;
    edt134: TsDecimalSpinEdit;
    lbl160: TsLabel;
    U3: TMenuItem;
    btn98: TButton;
    edt135: TEdit;
    btn100: TButton;
    lbl161: TLabel;
    L6: TMenuItem;
    sprgrsbr1: TsProgressBar;
    lv1: TsListView;
    grp35: TGroupBox;
    grp36: TGroupBox;
    grp37: TGroupBox;
    grp39: TGroupBox;
    btn101: TBitBtn;
    sprgrsbr2: TsProgressBar;
    btn12: TBitBtn;
    btn13: TBitBtn;
    mmo3: TMemo;
    grp41: TGroupBox;
    btn114: TButton;
    mmo19: TMemo;
    edt73: TEdit;
    edt136: TEdit;
    edt137: TEdit;
    btn115: TButton;
    btn116: TButton;
    lbl164: TLabel;
    btn118: TButton;
    edt139: TEdit;
    edt140: TEdit;
    edt141: TEdit;
    btn119: TButton;
    edt83: TEdit;
    edt142: TEdit;
    edt145: TEdit;
    lbl168: TLabel;
    lbl169: TLabel;
    C5: TMenuItem;
    btn121: TButton;
    lbl177: TLabel;
    lbl178: TLabel;
    edt153: TEdit;
    edt154: TEdit;
    edt155: TEdit;
    edt156: TEdit;
    lbl179: TLabel;
    lbl180: TLabel;
    btn125: TButton;
    edt157: TEdit;
    lbl181: TLabel;                                                             //上传网盘标志符号；
    sbtbtn1: TBitBtn;
    edt158: TEdit;
    lbl182: TLabel;
    rb5: TsRadioButton;
    lbl183: TLabel;
    Redit1: TiEdit;
    edt159: TEdit;
    lbl184: TLabel;
    lbl185: TLabel;
    lbl186: TLabel;
    lbl187: TLabel;
    chk24: TCheckBox;
    edt163: TEdit;
    chk25: TCheckBox;
    chk27: TCheckBox;
    chk26: TCheckBox;
    chk28: TCheckBox;
    btn129: TButton;
    lbl188: TLabel;
    edt164: TEdit;
    edt165: TEdit;
    btn130: TBitBtn;
    chk29: TCheckBox;
    Comm2: TComm;
    A1: TMenuItem;
    N50: TMenuItem;
    N51: TMenuItem;
    spthdlg1: TsPathDialog;
    chk30: TCheckBox;
    chk31: TCheckBox;
    ts23: TTabSheet;
    chk32: TCheckBox;
    chk33: TCheckBox;
    chk34: TCheckBox;
    chk35: TCheckBox;
    chk36: TCheckBox;
    mmo22: TMemo;
    mmo23: TMemo;
    mmo24: TMemo;
    mmo25: TMemo;
    mmo26: TMemo;
    edt168: TEdit;
    edt169: TEdit;
    lbl192: TLabel;
    grp43: TGroupBox;
    grp44: TGroupBox;
    grp45: TGroupBox;
    edt170: TEdit;
    edt171: TEdit;
    edt172: TEdit;
    edt173: TEdit;
    edt174: TEdit;
    edt175: TEdit;
    lbl191: TLabel;
    lbl193: TLabel;
    edt176: TEdit;
    btn138: TButton;
    btn139: TBitBtn;
    isvnsgmntclck1: TiSevenSegmentClock;
    Btn120: TButton;
    edt177: TEdit;
    edt178: TEdit;
    edt179: TEdit;
    btn142: TButton;
    edt180: TEdit;
    M3: TMenuItem;
    N52: TMenuItem;
    N53: TMenuItem;
    N54: TMenuItem;
    P5: TMenuItem;
    btn143: TBitBtn;
    chk38: TCheckBox;
    edt181: TEdit;
    edt182: TEdit;
    edt183: TEdit;
    edt184: TEdit;
    lbl136: TLabel;
    grp46: TGroupBox;
    btn25: TsBitBtn;
    btn26: TsBitBtn;
    Chart1: TChart;
    Series1: THorizBarSeries;
    Series2: THorizBarSeries;
    Series3: THorizBarSeries;
    Series4: THorizBarSeries;
    Series5: THorizBarSeries;
    Series6: THorizBarSeries;
    Series7: THorizBarSeries;
    Series8: THorizBarSeries;
    Series9: THorizBarSeries;
    Series10: THorizBarSeries;
    btn144: TButton;
    s8: TMenuItem;
    N59: TMenuItem;
    N60: TMenuItem;
    pgc2: TPageControl;
    ts20: TTabSheet;
    grp16: TGroupBox;
    btn9: TButton;
    btn10: TButton;
    btn20: TButton;
    btn21: TButton;
    edt5: TsDecimalSpinEdit;
    grp38: TGroupBox;
    btn102: TButton;
    btn103: TButton;
    btn104: TButton;
    btn105: TButton;
    btn106: TButton;
    btn107: TButton;
    btn108: TButton;
    btn109: TButton;
    btn110: TButton;
    btn111: TButton;
    btn73: TBitBtn;
    btn74: TBitBtn;
    ts18: TTabSheet;
    grp29: TGroupBox;
    mmo17: TMemo;
    mmo18: TMemo;
    pnl2: TPanel;
    rb1: TRadioButton;
    chk18: TCheckBox;
    chk19: TCheckBox;
    rb2: TRadioButton;
    edt122: TsDecimalSpinEdit;
    btn91: TButton;
    btn90: TButton;
    grp40: TGroupBox;
    btn113: TSpeedButton;
    shp4: TShape;
    lbl87: TLabel;
    lbl162: TLabel;
    btn112: TSpeedButton;
    cbb2: TComboBox;
    cbb3: TComboBox;
    ts19: TTabSheet;
    grp11: TGroupBox;
    lbl166: TLabel;
    btn32: TBitBtn;
    btn31: TBitBtn;
    btn30: TBitBtn;
    btn33: TBitBtn;
    btn34: TBitBtn;
    btn35: TBitBtn;
    btn36: TBitBtn;
    btn37: TBitBtn;
    btn38: TBitBtn;
    btn39: TBitBtn;
    btn40: TBitBtn;
    btn41: TBitBtn;
    btn42: TBitBtn;
    btn43: TBitBtn;
    btn46: TBitBtn;
    chk6: TCheckBox;
    edt143: TEdit;
    grp47: TGroupBox;
    lbl69: TLabel;
    lbl70: TLabel;
    lbl71: TLabel;
    lbl167: TLabel;
    edt49: TEdit;
    edt51: TEdit;
    edt50: TEdit;
    edt144: TEdit;
    btn44: TBitBtn;
    btn145: TButton;
    edt99: TEdit;
    btn146: TButton;
    edt185: TEdit;
    btn147: TButton;
    btn148: TButton;
    ts22: TTabSheet;
    lbl189: TLabel;
    lbl190: TLabel;
    shp6: TShape;
    btn133: TSpeedButton;
    btn134: TSpeedButton;
    btn135: TSpeedButton;
    btn127: TButton;
    mmo21: TMemo;
    edt160: TEdit;
    btn128: TButton;
    edt161: TEdit;
    edt162: TEdit;
    cbb4: TComboBox;
    cbb5: TComboBox;
    btn132: TButton;
    edt166: TEdit;
    edt167: TEdit;
    lbl150: TLabel;
    edt40: TEdit;
    edt42: TEdit;
    HIDctl: TJvHidDeviceController;
    lbl194: TLabel;
    U4: TMenuItem;
    tsUDP: TTabSheet;
    edt53: TEdit;
    edt100: TEdit;
    idpclnt1: TIdUDPClient;
    idpsrvr1: TIdUDPServer;
    mmo9: TMemo;
    UDPip: TLabel;
    UDPport: TLabel;
    P6: TMenuItem;
    F3: TMenuItem;
    chk11: TCheckBox;
    N21: TMenuItem;
    edt10: TMemo;
    F5: TMenuItem;
    btn5: TButton;
    btn6: TButton;
    N5: TMenuItem;
    y1: TMenuItem;
    S1: TMenuItem;
    W1: TMenuItem;
    N6: TMenuItem;
    C3: TMenuItem;
    CT5: TMemo;
    ct6: TMemo;
    C6: TMenuItem;
    C7: TMenuItem;
    C8: TMenuItem;
    C9: TMenuItem;
    CT7D: TMenuItem;
    CT7: TMemo;
    CT8: TMemo;
    CT9: TMemo;
    S5: TMenuItem;
    pgc3: TPageControl;
    ts2: TTabSheet;
    chk12: TCheckBox;
    chk13: TCheckBox;
    O3: TMenuItem;
    ts5: TTabSheet;
    ts10: TTabSheet;
    ts11: TTabSheet;
    ts14: TTabSheet;
    chk20: TCheckBox;
    grp4: TGroupBox;
    chk21: TCheckBox;
    chk22: TCheckBox;
    chk23: TCheckBox;
    chk37: TCheckBox;
    chk39: TCheckBox;
    chk14: TCheckBox;
    chk40: TCheckBox;
    chk41: TCheckBox;
    chk42: TCheckBox;
    chk17: TCheckBox;
    chk43: TCheckBox;
    chk44: TCheckBox;
    chk45: TCheckBox;
    chk46: TCheckBox;
    chk47: TCheckBox;
    chk48: TCheckBox;
    chk49: TCheckBox;
    chk50: TCheckBox;
    chk51: TCheckBox;
    chk15: TCheckBox;
    chk16: TCheckBox;
    chk52: TCheckBox;
    ts17: TTabSheet;
    chk53: TCheckBox;
    N16: TMenuItem;
    Find1: TMenuItem;
    Replace1: TMenuItem;
    FindDialog1: TFindDialog;
    ReplaceDialog1: TReplaceDialog;
    FindNext1: TMenuItem;
    pgc4: TPageControl;
    ts21: TTabSheet;
    chk54: TCheckBox;
    grp5: TGroupBox;
    chk55: TCheckBox;
    chk56: TCheckBox;
    chk57: TCheckBox;
    chk58: TCheckBox;
    chk59: TCheckBox;
    chk60: TCheckBox;
    chk61: TCheckBox;
    ts24: TTabSheet;
    chk64: TCheckBox;
    chk65: TCheckBox;
    ts25: TTabSheet;
    chk66: TCheckBox;
    chk67: TCheckBox;
    chk68: TCheckBox;
    chk69: TCheckBox;
    chk70: TCheckBox;
    chk71: TCheckBox;
    chk72: TCheckBox;
    ts26: TTabSheet;
    chk73: TCheckBox;
    chk74: TCheckBox;
    chk75: TCheckBox;
    ts27: TTabSheet;
    chk76: TCheckBox;
    chk77: TCheckBox;
    chk78: TCheckBox;
    c10: TMenuItem;
    C11: TMenuItem;
    grp7: TGroupBox;
    chk79: TCheckBox;
    chk80: TCheckBox;
    grp8: TGroupBox;
    chk81: TCheckBox;
    chk82: TCheckBox;
    chk83: TCheckBox;
    chk84: TCheckBox;
    grp33: TGroupBox;
    lbl153: TLabel;
    lbl154: TLabel;
    edt128: TEdit;
    btn94: TButton;
    btn95: TButton;
    edt129: TEdit;
    btn93: TButton;
    edt56: TEdit;
    grp12: TGroupBox;
    spl4: TSplitter;
    grp13: TGroupBox;
    lbl1: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    edt3: TEdit;
    edt12: TEdit;
    edt13: TEdit;
    btn7: TButton;
    btn19: TBitBtn;
    btn22: TButton;
    spl5: TSplitter;
    spl6: TSplitter;
    spl7: TSplitter;
    spl8: TSplitter;
    chk63: TCheckBox;
    chk62: TCheckBox;
    chk85: TCheckBox;
    spl9: TSplitter;
    spl10: TSplitter;
    spl11: TSplitter;
    Savebtn: TButton;
    ts28: TTabSheet;
    ildmtrx1: TiLedMatrix;
    ildmtrx2: TiLedMatrix;
    ildmtrx3: TiLedMatrix;
    ildmtrx4: TiLedMatrix;
    ildmtrx5: TiLedMatrix;
    lbl170: TLabel;
    chk86: TCheckBox;
    N17: TMenuItem;
    A2: TMenuItem;
    H2: TMenuItem;
    Memo1: TMemo;
    mmo1: TMemo;
    A3: TMenuItem;
    N23: TMenuItem;
    A4: TMenuItem;
    A5: TMenuItem;
    X2: TMenuItem;
    N25: TMenuItem;
    N32: TMenuItem;
    L1: TMenuItem;
    shp5: TShape;
    R1: TMenuItem;
    N35: TMenuItem;
    b2: TMenuItem;
    N40: TMenuItem;
    N29: TMenuItem;
    grp14: TGroupBox;
    btn11: TButton;
    btn45: TButton;
    grp141: TGroupBox;
    btn117: TButton;
    btn451: TButton;
    btn452: TButton;
    btn1110: TButton;
    btn1171: TButton;
    btn4511: TButton;
    grp1411: TGroupBox;
    btn1172: TButton;
    btn11711: TButton;
    N41: TMenuItem;
    K1: TMenuItem;
    ts29: TTabSheet;
    mmo4: TMemo;
    N44: TMenuItem;
    grp14111: TGroupBox;
    btn1173: TButton;
    N45: TMenuItem;
    N46: TMenuItem;
    ts30: TTabSheet;
    edt16: TEdit;
    edt17: TEdit;
    btn47: TButton;
    btn48: TButton;
    edt18: TEdit;
    edt19: TEdit;
    btn49: TButton;
    btn50: TButton;
    edt20: TEdit;
    btn51: TButton;
    cbb1: TComboBox;
    pnl3: TPanel;
/////////////通用函数
    procedure FormCreate(Sender: TObject);
    procedure Comm1ReceiveData(Sender: TObject; Buffer: Pointer; BufferLength: Word);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure tmr2Timer(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn12Click(Sender: TObject);
    procedure btn13Click(Sender: TObject);
    procedure btn14Click(Sender: TObject);
    procedure btn15Click(Sender: TObject);
    procedure chk1Click(Sender: TObject);
    procedure chk2Click(Sender: TObject);
    procedure chk3Click(Sender: TObject);
    procedure chk4Click(Sender: TObject);
    procedure btn17Click(Sender: TObject);
    procedure btn18Click(Sender: TObject);
    procedure btn20Click(Sender: TObject);
    procedure btn21Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btn24Click(Sender: TObject);
    procedure btn23Click(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

    procedure FormShow(Sender: TObject);
    procedure btn25Click(Sender: TObject);
    procedure btn26Click(Sender: TObject);
    procedure chk5Click(Sender: TObject);
    procedure TabSheet2Hide(Sender: TObject);
    procedure btn27Click(Sender: TObject);
    procedure tmr3Timer(Sender: TObject);
    procedure edt45Click(Sender: TObject);
    procedure tmr4Timer(Sender: TObject);
    procedure btn28Click(Sender: TObject);
    procedure btn29Click(Sender: TObject);
    procedure tmr5Timer(Sender: TObject);
//    procedure edt48Click(Sender: TObject);
    procedure btn30Click(Sender: TObject);
    procedure btn31Click(Sender: TObject);
    procedure btn32Click(Sender: TObject);
    procedure btn33Click(Sender: TObject);
    procedure btn34Click(Sender: TObject);
    procedure btn35Click(Sender: TObject);
    procedure btn36Click(Sender: TObject);
    procedure btn37Click(Sender: TObject);
    procedure btn38Click(Sender: TObject);
    procedure btn39Click(Sender: TObject);
    procedure btn40Click(Sender: TObject);
    procedure btn41Click(Sender: TObject);
    procedure btn42Click(Sender: TObject);
    procedure btn43Click(Sender: TObject);
    procedure btn44Click(Sender: TObject);
    procedure btn46Click(Sender: TObject);
    procedure ServerSocket1ClientRead(Sender: TObject; Socket: TCustomWinSocket);
    procedure ClientSocket1Read(Sender: TObject; Socket: TCustomWinSocket);
    procedure chk7Click(Sender: TObject);
    procedure chk8Click(Sender: TObject);
    procedure chk9Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure Panel1MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure Panel1MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure N21Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure btn9Click(Sender: TObject);
    procedure btn10Click(Sender: TObject);
    procedure D1Click(Sender: TObject);
    procedure N27Click(Sender: TObject);
    procedure H1Click(Sender: TObject);
    procedure S3Click(Sender: TObject);
    procedure N31Click(Sender: TObject);
    procedure P2Click(Sender: TObject);
    procedure O1Click(Sender: TObject);
    procedure P1Click(Sender: TObject);
    procedure S4Click(Sender: TObject);
    procedure B1Click(Sender: TObject);
    procedure TabSheet4Hide(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure I2Click(Sender: TObject);
    procedure D5Click(Sender: TObject);
    procedure N33Click(Sender: TObject);
    procedure I3Click(Sender: TObject);
    procedure C2Click(Sender: TObject);
    procedure N38Click(Sender: TObject);
    procedure btn55Click(Sender: TObject);
    procedure edit1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btn56Click(Sender: TObject);
    procedure tmr6Timer(Sender: TObject);
    procedure ts1Show(Sender: TObject);
    procedure btn61Click(Sender: TObject);
    procedure btn59Click(Sender: TObject);
    procedure btn62Click(Sender: TObject);
    procedure j1Click(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure L2Click(Sender: TObject);
    procedure mmo13DblClick(Sender: TObject);
    procedure mmo14DblClick(Sender: TObject);
    procedure N42Click(Sender: TObject);
    procedure N43Click(Sender: TObject);
    procedure edt103Change(Sender: TObject);
    procedure edt106Change(Sender: TObject);
    procedure edt102Change(Sender: TObject);
    procedure edt109Change(Sender: TObject);
    procedure btn73Click(Sender: TObject);
    procedure btn74Click(Sender: TObject);
    procedure btn75Click(Sender: TObject);
    procedure medit1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btn77Click(Sender: TObject);
    procedure btn78Click(Sender: TObject);
    procedure btn80Click(Sender: TObject);
    procedure btn81Click(Sender: TObject);
    procedure btn82Click(Sender: TObject);
    procedure btn83Click(Sender: TObject);
    procedure btn84Click(Sender: TObject);
    procedure btn85Click(Sender: TObject);
    procedure btn86Click(Sender: TObject);
    procedure btn87Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure C4Click(Sender: TObject);
    procedure N34Click(Sender: TObject);
    procedure N47Click(Sender: TObject);
    procedure btn90Click(Sender: TObject);
    procedure btn91Click(Sender: TObject);
    procedure btn92Click(Sender: TObject);
    procedure chk18Click(Sender: TObject);
    procedure edt122Change(Sender: TObject);
    procedure rb2Click(Sender: TObject);
    procedure rb1Click(Sender: TObject);
    procedure edt134Change(Sender: TObject);
    procedure btn98Click(Sender: TObject);
    procedure U3Click(Sender: TObject);
    procedure btn100Click(Sender: TObject);
    procedure btn125Click(Sender: TObject);
    procedure sbtbtn1Click(Sender: TObject);
    procedure lv1AdvancedCustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState; Stage: TCustomDrawStage; var DefaultDraw: Boolean);
    procedure ts16Show(Sender: TObject);
    procedure ts15Show(Sender: TObject);
    procedure btn101Click(Sender: TObject);
    procedure btn102Click(Sender: TObject);
    procedure btn103Click(Sender: TObject);
    procedure btn104Click(Sender: TObject);
    procedure btn105Click(Sender: TObject);
    procedure btn106Click(Sender: TObject);
    procedure btn107Click(Sender: TObject);
    procedure btn108Click(Sender: TObject);
    procedure btn109Click(Sender: TObject);
    procedure btn110Click(Sender: TObject);
    procedure btn111Click(Sender: TObject);
    procedure cbb3Change(Sender: TObject);
    procedure cbb2Change(Sender: TObject);
    procedure mmo17KeyPress(Sender: TObject; var Key: Char);
    procedure mmo18KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure mmo17Change(Sender: TObject);
    procedure btn114Click(Sender: TObject);
    procedure btn115Click(Sender: TObject);
    procedure btn116Click(Sender: TObject);
    procedure btn118Click(Sender: TObject);
    procedure btn119Click(Sender: TObject);
    procedure btn120Click(Sender: TObject);
    procedure C5Click(Sender: TObject);
    procedure btn127Click(Sender: TObject);
    procedure btn128Click(Sender: TObject);
    procedure btn129Click(Sender: TObject);
    procedure btn130Click(Sender: TObject);
    procedure btn132Click(Sender: TObject);
    procedure btn133Click(Sender: TObject);
    procedure Comm2ReceiveData(Sender: TObject; Buffer: Pointer; BufferLength: Word);
    procedure btn134Click(Sender: TObject);
    procedure cbb5Change(Sender: TObject);
    procedure cbb4Change(Sender: TObject);
    procedure btn135Click(Sender: TObject);
    procedure A1Click(Sender: TObject);
    procedure Label7DblClick(Sender: TObject);
    procedure lbl79DblClick(Sender: TObject);
    procedure N51Click(Sender: TObject);
    procedure N50Click(Sender: TObject);
    procedure chk32Click(Sender: TObject);
    procedure chk33Click(Sender: TObject);
    procedure chk34Click(Sender: TObject);
    procedure chk35Click(Sender: TObject);
    procedure chk36Click(Sender: TObject);
    procedure ts23Show(Sender: TObject);
    procedure grp43DblClick(Sender: TObject);
    procedure btn138Click(Sender: TObject);
    procedure btn139Click(Sender: TObject);
    procedure btn140Click(Sender: TObject);
    procedure btn141Click(Sender: TObject);
    procedure mmo28Click(Sender: TObject);
    procedure btn112Click(Sender: TObject);
    procedure btn142Click(Sender: TObject);
    procedure M3Click(Sender: TObject);
    procedure N52Click(Sender: TObject);
    procedure N53Click(Sender: TObject);
    procedure P5Click(Sender: TObject);
//    procedure A2Click(Sender: TObject);
    procedure btn143Click(Sender: TObject);
    procedure C7Click(Sender: TObject);
    procedure ts18Show(Sender: TObject);
    procedure btn144Click(Sender: TObject);
    procedure pnl1DblClick(Sender: TObject);
    procedure grp28DblClick(Sender: TObject);
    procedure lv1DblClick(Sender: TObject);
    procedure s8Click(Sender: TObject);
    procedure btn146Click(Sender: TObject);
    procedure btn147Click(Sender: TObject);
    procedure btn148Click(Sender: TObject);
    procedure lbl140DblClick(Sender: TObject);
    procedure lbl82DblClick(Sender: TObject);
    procedure lbl80DblClick(Sender: TObject);
    procedure N59Click(Sender: TObject);
    procedure stepModePopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure sysParaPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure N60Click(Sender: TObject);
    procedure edt45ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure edt45KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure HIDctlArrival(HidDev: TJvHidDevice);
    procedure HIDctlDeviceChange(Sender: TObject);
    procedure HIDctlDeviceDataError(HidDev: TJvHidDevice; Error: Cardinal);
    function HIDctlEnumerate(HidDev: TJvHidDevice; const Idx: Integer): Boolean;
    procedure HIDctlRemoval(HidDev: TJvHidDevice);
    procedure ReadBtnClick(Sender: TObject);
    procedure WriteBtnClick(Sender: TObject);
    procedure ClearBtnClick(Sender: TObject);
    procedure DevListBoxClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure U4Click(Sender: TObject);
    procedure idpclnt1Status(ASender: TObject; const AStatus: TIdStatus; const AStatusText: string);
    procedure idpsrvr1UDPRead(Sender: TObject; AData: TStream; ABinding: TIdSocketHandle);
    procedure idpsrvr1Status(ASender: TObject; const AStatus: TIdStatus; const AStatusText: string);
    procedure F3Click(Sender: TObject);
    procedure F5Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure mmo9DblClick(Sender: TObject);
    procedure y1Click(Sender: TObject);
    procedure S1Click(Sender: TObject);
    procedure W1Click(Sender: TObject);
    procedure C3Click(Sender: TObject);
    procedure mmo3DblClick(Sender: TObject);
    procedure C6Click(Sender: TObject);
    procedure C9Click(Sender: TObject);
    procedure C8Click(Sender: TObject);
    procedure CT7DClick(Sender: TObject);
    procedure S5Click(Sender: TObject);
    procedure chk12Click(Sender: TObject);
    procedure O3Click(Sender: TObject);
    procedure mmo3Change(Sender: TObject);
    procedure Find1Click(Sender: TObject);
    procedure FindDialog1Find(Sender: TObject);
    procedure ReplaceDialog1Replace(Sender: TObject);
    procedure ReplaceDialog1Find(Sender: TObject);
    procedure Replace1Click(Sender: TObject);
    procedure FindNext1Click(Sender: TObject);
    procedure c10Click(Sender: TObject);
    procedure C11Click(Sender: TObject);
    procedure chk54Click(Sender: TObject);
    procedure btn93Click(Sender: TObject);
    procedure edt3Click(Sender: TObject);
    procedure edt12Click(Sender: TObject);
    procedure edt13Click(Sender: TObject);
    procedure btn7Click(Sender: TObject);
    procedure btn19Click(Sender: TObject);
    procedure btn22Click(Sender: TObject);
    procedure Panel1DblClick(Sender: TObject);
    procedure edt3ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure edt12ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure edt13ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure chk86Click(Sender: TObject);
    procedure A2Click(Sender: TObject);
    procedure H2Click(Sender: TObject);
    procedure mmo1Change(Sender: TObject);
    procedure mmo1DblClick(Sender: TObject);
    procedure A3Click(Sender: TObject);
    procedure A4Click(Sender: TObject);
    procedure A5Click(Sender: TObject);
    procedure X2Click(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure L1Click(Sender: TObject);
    procedure N32Click(Sender: TObject);
    procedure C1Click(Sender: TObject);
    procedure R1Click(Sender: TObject);
    procedure btn11Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure mmo1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btn45Click(Sender: TObject);
    procedure btn117Click(Sender: TObject);
    procedure btn451Click(Sender: TObject);
    procedure btn1110Click(Sender: TObject);
    procedure btn452Click(Sender: TObject);
    procedure btn1171Click(Sender: TObject);
    procedure btn4511Click(Sender: TObject);
    procedure btn1172Click(Sender: TObject);
    procedure btn11711Click(Sender: TObject);
    procedure N41Click(Sender: TObject);
    procedure mmo3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure mmo3ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure btn94Click(Sender: TObject);
    procedure btn95Click(Sender: TObject);
    procedure K1Click(Sender: TObject);
    procedure pgc1Change(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure ts28Show(Sender: TObject);
    procedure ts29Show(Sender: TObject);
    procedure N44Click(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure Panel1ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure Panel2ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure pgc1ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure stepMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure N7Click(Sender: TObject);
    procedure Panel2Click(Sender: TObject);
    procedure btn1173Click(Sender: TObject);
    procedure N45Click(Sender: TObject);
    procedure btn47Click(Sender: TObject);
    procedure btn48Click(Sender: TObject);
    procedure btn49Click(Sender: TObject);
    procedure btn50Click(Sender: TObject);
    procedure btn51Click(Sender: TObject);
    procedure cbb1Select(Sender: TObject);
    procedure cbb1CloseUp(Sender: TObject);

  private
    { Private declarations }

  public
    CLoseFlag: integer;            //登录窗口是否按了直接 CLOSE按键的标志 ；
    { Public declarations }
    procedure PrintClickProcess(len: Integer);
    function DeviceName(HidDev: TJvHidDevice): string;
    procedure ShowRead(HidDev: TJvHidDevice; ReportID: Byte; const Data: Pointer; Size: Word);
    procedure AddToHistory(Str: string);

    procedure sysIntClick(Sender: TObject);
    procedure CommandIntClick(Sender: TObject);
    procedure stepIntClick(Sender: TObject);
    procedure sysStrClick(Sender: TObject);
    procedure stepNameStrClick(Sender: TObject);
    procedure stepModeStrClick(Sender: TObject);
    procedure stepParaKeydown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sysParaKeydown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure stepNoteClick(Sender: TObject);
    procedure ExpertClick(Sender: TObject);
    function UpServerOneLog(SLINE_head: string): Boolean;      // function    UpServerOneLog(len_ss:string): Boolean;

    procedure InputQueryPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure InputQueryKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure InputQueryKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

  end;

var
  Form_main: TForm_main;
  Rleds: array[0..200] of TiLedRound;
  Redits: array[0..200] of TiEdit;                       //  xy2machine:xybutton;

  oldEditWndProc1: Pointer;
  oldEditWndProc3: Pointer;
  oldEditWndProc9: Pointer;
  oldEditWndProc13: Pointer;
  oldEditWndProc14: Pointer;

implementation

uses
  FindPoint, OSstudy, StepCopy, ModfyN, MoveNstep, InsertNstep, DeleteNstep,
  InsertCopyNstep, PNenter, Unit1_MainCreateShowlCode, Unit2_Communication,
  Unit4_logFileCode, LoadFile, LogHeadTips, SWave, FileCompare, About, Ascii,
  KeyBoard, LearnClampDiode, CreateDiode, Unit5eCar, SWCheck, ULJKTcp;

{$R *.dfm}

/////////////窗口生产初始化
procedure TForm_main.FormCreate(Sender: TObject);
var //temp:byte;
  temp16, i, j: SmallInt;
  nrf_dat_config: Tinifile;
  ini_path: string;
begin                    //变量在生成时就执行

  if not FileExists('zhiDE*.exe') then begin  //ide启动不刷新标题 not FileExists('..\zhiDE.exe') and
    form_main.Caption := 'zhi';
  end;
  CLoseFlag := 1;

  PageControl1.TabIndex := 2 - 1;    //最先进入PCset界面；
  oldEditWndProc3 := Pointer(SetWindowLong(mmo3.Handle, GWL_WNDPROC, Integer(@Mmo3WndProc)));
  oldEditWndProc9 := Pointer(SetWindowLong(mmo9.Handle, GWL_WNDPROC, Integer(@mmo9WndProc)));
//没有右键方法，启动慢？  oldEditWndProc1:=Pointer(SetWindowLong(panel1.Handle, GWL_WNDPROC, Integer(@panel1WndProc)));
  oldEditWndProc13 := Pointer(SetWindowLong(mmo13.Handle, GWL_WNDPROC, Integer(@Mmo13WndProc)));
  oldEditWndProc14 := Pointer(SetWindowLong(mmo14.Handle, GWL_WNDPROC, Integer(@mmo14WndProc)));

  NameCount := 0;
  GpcResult.TabIndex := 0;
  LogType := 0;
  modbusFSM := 0; //Saveflag:=0;
  UploadSerT := 0;
  PassOverT := 0;
  NGOverT := 0;
  PassCount := 0;
  NGcount := 0;
  TripCount := 0;
  lbl134.Caption := inttostr(tripcount);     //单次 计数  ，达到最大数后停止测试
  modbusfun05 := 0;
  modbusfun05dat := $FF;
  modbusfun06 := 0;
  modbusfun16 := 0;
  modbusfun16int := 0;
  modbusfun16len := 0;
  modbusfun02 := 0;
  modbusfun03 := 0;
  modbusfun03dat := $00;
  working := $08;   //默认：初始化中...
  inportNewFileflag := $00;

  Series1.AddX(0);
  Series2.AddX(0);
  series2.Title := '';
  Series3.AddX(0);
  Series4.AddX(0);
  series4.Title := '';
  Series5.AddX(0);
  Series6.AddX(0);
  series6.Title := '';
  Series7.AddX(0);
  Series8.AddX(0);
  series8.Title := '';
  Series9.AddX(0);
  Series10.AddX(0);
  series10.Title := '';

  /////////////读配置文件
  ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
  try
    nrf_dat_config := Tinifile.Create(ini_path);    //关联INI文件
  //-----------------[Comm Para]---------------------------------------------------

    if nrf_dat_config.Readstring('Comm Para', 'FormLeft', '') <> '' then begin    //多PC软件系统，此设定窗口位置

      if (Readchis2('Comm Para', 'UpLocal') = '') then begin              //非第1工位用软件
        if Pos('\2\', ExtractFilePath(application.ExeName)) > 0 then begin
          form_main.Left := SCREEN.WIDTH div 2;
          if nrf_dat_config.Readstring('Comm Para', 'FormLeft', '') = '4' then
            form_main.Top := 0;
          lbl177.Caption := '工位2 ：';
          StationNowNum := 2;
        end;
        if Pos('\3\', ExtractFilePath(application.ExeName)) > 0 then begin
          form_main.Left := 0;
          form_main.Top := SCREEN.Height div 2;
          lbl177.Caption := '工位3 ：';
          StationNowNum := 3;
        end;
        if Pos('\4\', ExtractFilePath(application.ExeName)) > 0 then begin
          form_main.Left := SCREEN.WIDTH div 2;
          form_main.Top := SCREEN.Height div 2;
          lbl177.Caption := '工位4 ：';
          StationNowNum := 4;
        end;
      end
      else begin                                                 //第1 工位
        //if(Readchis3('Comm Para','UpLocal')='')then begin            //超过2个工位
        if nrf_dat_config.Readstring('Comm Para', 'FormLeft', '') = '4' then
          form_main.Top := 0;
        //end;
        form_main.Left := 0;
        lbl177.Caption := '工位1 ：';

      end;
      if nrf_dat_config.Readstring('Comm Para', 'FormLeft', '') = '4' then
        form_main.Height := SCREEN.Height div 2;
      form_main.Width := SCREEN.WIDTH div 2;
      grp18.Width := 200;
    end
    else
      lbl177.Caption := '';
    if nrf_dat_config.Readstring('Comm Para', 'FormTop', '') <> '' then begin    //双PC软件系统，此设定窗口位置
      form_main.Top := strtoint(nrf_dat_config.Readstring('Comm Para', 'FormTop', ''));
    end;

    edt100.Text := IdIPWatch1.LocalIP;
    if (nrf_dat_config.Readstring('Comm Para', 'UDPport', '') <> '') then begin   //因为防火墙缘故，软件启动时必须立即配置！
      edt53.Text := nrf_dat_config.Readstring('Comm Para', 'UDPport', '');
      if (edt53.Text = '9001') or (edt53.Text = '9002') or (edt53.Text = '9003') or (edt53.Text = '9004') then begin
        edt53.tag := StrToInt(edt53.Text);
        edt53.Text := '9000';

        idpsrvr1.DefaultPort := StrToInt(edt53.Text);
        idpsrvr1.Active := true;
        if edt53.tag >= 9002 then begin
          Stat1.Panels[3].Text := '主UDP=9000,从UDP=' + IntToStr(edt53.Tag);
        end
        else
          Stat1.Panels[3].Text := '主UDP=9000,从UDP=' + IntToStr(9001);

        if IsNumberic(nrf_dat_config.Readstring('Comm Para', 'UDPoverTimes', '')) then begin
          idpsrvr1.Tag := 10 * StrToInt(nrf_dat_config.Readstring('Comm Para', 'UDPoverTimes', ''));
        end
        else
          idpsrvr1.Tag := 50;       //默认：5秒接收UDP数据超时
        Stat1.Tag := 9000;
        u4.Enabled := True;
      end;
    end
    else begin
      edt53.Text := '9000';             //监听默认9000：立即读会报错<--nrf_dat_config.Readstring('Comm Para', 'UDPport', '');
      idpsrvr1.DefaultPort := StrToInt(edt53.Text);
    end;
                                                                                //小于9000：保留！
    if (edt53.Text = '9000') then begin                                           //9000~9049：二次开发启用
      edt53.hint := edt53.Text;
    end
    else if (IsNumberic(edt53.Text) and (StrToInt(edt53.Text) >= 9050)) then begin  //大于9050：实际应用端口
      edt53.tag := StrToInt(edt53.Text);
      edt53.hint := edt53.Text;
      idpsrvr1.DefaultPort := StrToInt(edt53.Text);                               //UDP默认端口必须在此设置一次！

      idpsrvr1.Active := true;
      Stat1.Panels[3].Text := '监听UDP=' + IntToStr(edt53.Tag);
    //   Stat1.Tag:=9050;
    //  mmo10.Text:='sn1234567890abcedefg';           //模拟条形码发送
    end;

    temp16 := StrToInt('0' + nrf_dat_config.Readstring('Comm Para', 'PageRow', ''));
    if (temp16 >= 10) and (temp16 < VIRLCROWS) then begin
      VIRLCROWS := temp16;                         //10--69可指定定义
    end
    else if (temp16 < 10) then
      VIRLCROWS := 66;                            //自适应根据固件版本确定行数

    if nrf_dat_config.Readstring('Comm Para', 'TextHeight', '') <> '' then        //此设定debug编辑框高度
      edt121.Text := nrf_dat_config.Readstring('Comm Para', 'TextHeight', '');
    CreateRowEdit(VIRLCROWS);
    if (VIRLCROWS < 66) then begin                 //config设置了每页步骤数
      sysedit[5].Text := '806';                   //通讯会使用版本号，此处必须强行设置固件版本号；
      if (VIRLCROWS >= 60) then begin
        NAMEc1 := 6;
        NAMEc2 := 7;
        NAMEc3 := 8;
        NAMEc4 := 9;
        MODEc1 := 24;
        MODEc2 := 25;
        MODEc3 := 26;
        MODEc4 := 27;
      end;
    end;
  //-----------------[result]---------------------------------------------------
    if nrf_dat_config.Readstring('result', 'Row', '') <> '' then        //此两个参数影响软件启动时间
      edt59.Text := nrf_dat_config.Readstring('result', 'Row', '');
    if edt59.Text = '0' then
      edt59.Text := '1';      //不能为0
    if nrf_dat_config.Readstring('result', 'column', '') <> '' then
      edt60.Text := nrf_dat_config.Readstring('result', 'column', '');
    if edt60.Text = '0' then
      edt60.Text := '1';     //不能为0

    if nrf_dat_config.Readstring('result', 'Group', '') <> '' then
      edt145.Text := nrf_dat_config.Readstring('result', 'Group', '');

    if nrf_dat_config.Readstring('result', 'colMirror', '') = '1' then
      ildrw1.Style := ilasLeft;

    if nrf_dat_config.Readstring('result', 'UpOnePCSName', '') <> '' then        //上传联板所有测试项目名称：统一用第1PCS吗？
      edt74.Text := nrf_dat_config.Readstring('result', 'UpOnePCSName', '');    //默认：10<-- 0
    if nrf_dat_config.Readstring('result', 'OnePCSOneLine', '') <> '' then        //上传联板所有测试项目名称：统一用第1PCS吗？
      edt74.Text := nrf_dat_config.Readstring('result', 'OnePCSOneLine', '');    //默认：10<-- 0

    if nrf_dat_config.Readstring('result', 'enPrint', '') <> '' then        //自动打印使能 ,并设置大小
      edt75.Text := nrf_dat_config.Readstring('result', 'enPrint', '')
    else begin
      btn55.Visible := False;
      btn28.Visible := false;
      btn29.Visible := false;
      btn27.Enabled := false;
    end;
    if nrf_dat_config.Readstring('result', 'PrintVHdir', '') <> '' then        //设置 打印机字符长度（与打印机方向设置有关）+ + 空行数+信息长短
      edt11.Text := nrf_dat_config.Readstring('result', 'PrintVHdir', '');
    if nrf_dat_config.Readstring('result', 'PrintRow', '') <> '' then           //此两个参数影响软件启动时间
      edt159.Text := nrf_dat_config.Readstring('result', 'PrintRow', '');
    if nrf_dat_config.Readstring('result', 'PrintMaxRows', '') <> '' then        //打印最大行数，高优先级；
      edt159.Text := nrf_dat_config.Readstring('result', 'PrintMaxRows', '');
    if edt159.Text <> '0' then begin
      edt159.Visible := True;
      lbl184.Visible := true;
    end;

    if nrf_dat_config.Readstring('result', 'OddRowOffset', '') <> '' then
      edt69.Text := nrf_dat_config.Readstring('result', 'OddRowOffset', '');
    if nrf_dat_config.Readstring('result', 'OddColOffset', '') <> '' then
      edt70.Text := nrf_dat_config.Readstring('result', 'OddColOffset', '');
    if nrf_dat_config.Readstring('result', 'OddColVOffset', '') <> '' then
      edt70.Text := nrf_dat_config.Readstring('result', 'OddColVOffset', '');
    if nrf_dat_config.Readstring('result', 'OddColHOffset', '') <> '' then
      edt112.Text := nrf_dat_config.Readstring('result', 'OddColHOffset', '');
    if nrf_dat_config.Readstring('result', 'AllColHOffset', '') <> '' then
      edt120.Text := nrf_dat_config.Readstring('result', 'AllColHOffset', '');

    if nrf_dat_config.Readstring('result', 'OddRowDown', '') <> '' then
      edt71.Text := nrf_dat_config.Readstring('result', 'OddRowDown', '');

    if StrToInt(edt59.Text) * strtoint(edt60.Text) > High(Rleds) then      //必须小于200个
    begin
      edt59.Text := '10';
      edt60.Text := '20';    //列
    end;

    if nrf_dat_config.Readstring('result', 'BarcodeInfo', '') <> '' then begin    //连接日置仪器，增加条码信息
      Comm1.DtrControl := DtrDisable;
      Comm1.RtsControl := RtsDisable;
      Comm2.DtrControl := DtrDisable;
      Comm2.RtsControl := RtsDisable;
      lbl28.Caption := 'lcr';
    end;
  //-----------------[Model Para]---------------------------------------------------
    if nrf_dat_config.Readstring('Model Para', 'FromMcuPNMinSize', '') <> '' then
      edt158.Text := nrf_dat_config.Readstring('Model Para', 'FromMcuPNMinSize', '');
  //-----------------other---------------------------------------------------

    if (nrf_dat_config.Readstring('Comm Para', 'CSVen', '') = '2') then begin	//后面有同样操作；
      Rled1.Width := 100;
      Rled1.Height := 100;
      Redit1.Width := 300;
    end;
    if IsNumberic(nrf_dat_config.Readstring('result', 'RledWidth', '')) then
      Rled1.Width := StrToInt(nrf_dat_config.Readstring('result', 'RledWidth', ''));
    if IsNumberic(nrf_dat_config.Readstring('result', 'RledHeight', '')) then
      Rled1.Height := StrToInt(nrf_dat_config.Readstring('result', 'RledHeight', ''));
    MatrixLed_createProcess(0);
    if (nrf_dat_config.Readstring('Comm Para', 'CSVen', '') = '2') then begin   //不能使用LogType=2
      if StrToInt(edt59.Text) * strtoint(edt60.Text) = 1 then begin
        lbl73.Visible := True;
        lbl73.Top := Redits[0].Top + 50;
        lbl73.Left := Redits[0].Left + 100;
        chk25.Left := Redits[0].Left;
        chk25.Top := Redits[0].Top + 100;
      end
      else if StrToInt(edt59.Text) * strtoint(edt60.Text) > 3 then begin
        lbl73.Visible := True;
        lbl73.Top := Redits[0].Top + 50;
        lbl73.Left := Redits[0].Left + 100;
        lbl185.Visible := True;
        lbl185.Top := Redits[1].Top + 50;
        lbl185.Left := Redits[1].Left + 100;
        lbl186.Visible := True;
        lbl186.Top := Redits[2].Top + 50;
        lbl186.Left := Redits[2].Left + 100;
        lbl187.Visible := True;
        lbl187.Top := Redits[3].Top + 50;
        lbl187.Left := Redits[3].Left + 100;

        chk25.Left := Redits[0].Left;
        chk26.Left := Redits[1].Left;
        chk27.Left := Redits[2].Left;
        chk28.Left := Redits[3].Left;
        chk25.Top := Redits[0].Top + 100;
        chk26.Top := Redits[1].Top + 100;
        chk27.Top := Redits[2].Top + 100;
        chk28.Top := Redits[3].Top + 100;
      end;
    end;

  finally
    nrf_dat_config.free;
  end;
end;

procedure TForm_main.CommandIntClick(Sender: TObject);
var
  str: string;
begin
  if InputQuery('Pls Enter Command!', '111', str) then begin        // str := InputBox('输入整数', '123', '000000');
    if str = '' then
      str := '0';       //edt27.Text:=str;
    modbusfun06dat := StrToInt(str);
    modbusfun06 := $1080;
  end;
end;

procedure TForm_main.stepIntClick(Sender: TObject);
var
  str, tip, demostr: string;
  ii, jj, hh, stepline, nline: Integer;

  function HexChar(c: Char): Byte;
  begin
    case c of
      '0'..'9':
        Result := Byte(c) - Byte('0');
      'a'..'f':
        Result := (Byte(c) - Byte('a')) + 10;
      'A'..'F':
        Result := (Byte(c) - Byte('A')) + 10;
    else
      Result := 0;
    end;
  end;

begin
  ii := TEdit(Sender).Top + panel1.VertScrollBar.Position;
  hh := TEdit(Sender).Left + panel1.HorzScrollBar.Position + 10;      //+10补偿模式编辑框左移！
  StepNowFresh := (ii - 10) div strtoint(form_main.edt121.Text);   //刷新开始行数
  stepline := StepNowFresh + 1;
  if GetKeyState(VK_SHIFT) < 0 then begin
    if (mmo22.Tag = -1) then
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行
    if (mmo24.Tag = -1) then
      mmo24.Tag := mmo22.Tag;            //最小行
    if StepNowFresh + StepNowPage * VIRLCROWS <= mmo24.Tag then
      mmo24.Tag := StepNowFresh + StepNowPage * VIRLCROWS
    else begin
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
    end;

    if (mmo23.Tag = -1) then
      mmo23.Tag := hh div 60;             //当前列：0...
    if (mmo25.Tag = -1) then
      mmo25.Tag := mmo23.Tag;           //最小列
    if (hh div 60) < mmo25.Tag then
      mmo25.Tag := hh div 60
    else begin
      mmo23.Tag := hh div 60;             //当前列：0...
    end;

    for ii := 1 to VIRLCROWS do begin        //颜色先还原
      if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
        zu1edit[ii].Color := clWhite;
      zu2edit[ii].Color := clWhite;
      zu3edit[ii].Color := clWhite;

      zu5edit[ii].Color := clWhite;
      zu6edit[ii].Color := clWhite;
      zu7edit[ii].Color := clWhite;
      zu8edit[ii].Color := clWhite;
      zu9edit[ii].Color := clWhite;
      zu10edit[ii].Color := clWhite;
      zu11edit[ii].Color := clWhite;
      zu12edit[ii].Color := clWhite;
      zu13edit[ii].Color := clWhite;
      zu14edit[ii].Color := clWhite;
      zu15edit[ii].Color := clWhite;
      zu16edit[ii].Color := clWhite;
    end;
    for ii := mmo24.Tag to mmo22.Tag do begin
      if (ii < StepNowPage * VIRLCROWS) then
        Continue;   //小于当前页最小值
      if (ii >= StepNowPage * VIRLCROWS + VIRLCROWS) then
        Break;   //大于等于后一页最小值
      for jj := mmo25.Tag to mmo23.Tag do begin          //  if(lineInsert+ii)>VIRLCROWS then Break;

        nline := (ii mod VIRLCROWS) + 1;
        if jj = 0 then
          zu1edit[nline].Color := clMoneyGreen;    //名字和数组都是从1...开始
        if jj = 1 then
          zu2edit[nline].Color := clMoneyGreen;
        if jj = 2 then
          zu3edit[nline].Color := clMoneyGreen;

        if jj = 4 then
          zu5edit[nline].Color := clMoneyGreen;
        if jj = 5 then
          zu6edit[nline].Color := clMoneyGreen;
        if jj = 6 then
          zu7edit[nline].Color := clMoneyGreen;
        if jj = 7 then
          zu8edit[nline].Color := clMoneyGreen;
        if jj = 8 then
          zu9edit[nline].Color := clMoneyGreen;
        if jj = 9 then
          zu10edit[nline].Color := clSkyBlue;
        if jj = 10 then
          zu11edit[nline].Color := clMoneyGreen;
        if jj = 11 then
          zu12edit[nline].Color := clMoneyGreen;
        if jj = 12 then
          zu13edit[nline].Color := clMoneyGreen;
        if jj = 13 then
          zu14edit[nline].Color := clMoneyGreen;
        if jj = 14 then
          zu15edit[nline].Color := clMoneyGreen;
        if jj = 15 then
          zu16edit[nline].Color := clMoneyGreen;
      end;
    end;
    Exit;
  end
  else begin
    if mmo24.Tag >= 0 then begin
      mmo24.Tag := -1;
      mmo25.Tag := -1;
      AreaDisSelect(0);
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
      mmo23.Tag := hh div 60;             //当前列：0...
     //  exit;
    end;
  end;

  tip := 'Enter Int输入<十进制> 或 <十六进制> 整数:';
  demostr := '1234 或 xABCD   :';
  form_main.mmo4.lines.add(tip);
  if InputQuery(tip, demostr, str) then begin
    if ((hh + 10) div 60) = 10 then begin
      if str = '' then
        str := '100';                //比例K默认100% ；2021-12-20改
    end
    else begin
      if str = '' then
        str := '0';
    end;
    if tip = 'Enter Int输入十六进制数' //( ( (Copy(zu10edit[stepline].Text,2,2)='AC')or (Copy(zu10edit[stepline].Text,2,2)='NC') )and( (((hh+10) div 60)=7-1)or(((hh+10) div 60)=15-1) ) )
      //or( ( (Copy(zu10edit[stepline].Text,2,2)='YH') )and( (((hh+10) div 60)=3-1)or(((hh+10) div 60)=4-1)or(((hh+10) div 60)=15-1)or(((hh+10) div 60)=16-1) )  )
      //or( ( (Copy(zu10edit[stepline].Text,2,2)='NL') )and( (((hh+10) div 60)=15-1) )  )
      then begin
      if not IsHexNum(str) then
        ShowMessage('err! pls enter number,错误！请输入16进制数字')
      else begin

        modbusfun06dat := 0;
        for jj := 0 to Length(str) do begin
          if (str[jj] = 'x') or (str[jj] = 'X') then
            Continue;
          modbusfun06dat := modbusfun06dat * 16 + HexChar(str[jj]);
        end;
        modbusfun06 := $3000 + ((ii - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 1;   //TEdit(Sender).Top  modbus地址加一，
        if (hh div 60) > 9 then
          modbusfun06 := modbusfun06 + 1;
        addToLog('第' + inttostr((StrToInt(form_main.edt45.text) div VIRLCROWS) * VIRLCROWS + StepNowFresh + 1) + '步骤的第' + inttostr(((hh + 10) div 60) + 1) + '个【参数】修改为：' + str);
      end;
    end
    else begin
      if IsxFloatNum(str) then
        modbusfun06dat := StrToInt(StringReplace(str, '.', '', [rfReplaceAll]));
      if (TEdit(Sender).Left = 0) then begin
        if (str = ' ') or (str = '  ') then begin                       //序号填写一个空格:删略切换
          modbusfun06dat := (StrToInt(form_main.edt45.text) div VIRLCROWS) * VIRLCROWS + StepNowFresh + 1;
          modbusfun06 := $3000 + ((ii - 10) div strtoint(form_main.edt121.Text)) * 32;
          addToLog('第' + inttostr((StrToInt(form_main.edt45.text) div VIRLCROWS) * VIRLCROWS + StepNowFresh + 1) + '步骤的第' + inttostr(((hh + 10) div 60) + 1) + '个【参数】修改为：' + inttostr(modbusfun06dat));
        end
        else begin
          if not IsxFloatNum(str) then
            ShowMessage('err! pls enter number,错误！请输入数字')
          else begin
            modbusfun06 := $3000 + ((ii - 10) div strtoint(form_main.edt121.Text)) * 32;
            addToLog('第' + inttostr((StrToInt(form_main.edt45.text) div VIRLCROWS) * VIRLCROWS + StepNowFresh + 1) + '步骤的第' + inttostr(((hh + 10) div 60) + 1) + '个【参数】修改为：' + inttostr(modbusfun06dat));
          end;
        end;
      end
      else begin
        if not IsxFloatNum(str) then
          ShowMessage('err! pls enter number,错误！请输入数字')
        else begin
          modbusfun06 := $3000 + ((ii - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 1;   //TEdit(Sender).Top  modbus地址加一，
          if (hh div 60) > 9 then
            modbusfun06 := modbusfun06 + 1;                                         //模式之后的参数
          addToLog('第' + inttostr((StrToInt(form_main.edt45.text) div VIRLCROWS) * VIRLCROWS + StepNowFresh + 1) + '步骤的第' + inttostr(((hh + 10) div 60) + 1) + '个【参数】修改为：' + inttostr(modbusfun06dat));
        end;
      end;
    end;
  end;

end;

procedure TForm_main.sysIntClick(Sender: TObject);
var
  str: string;
begin
  if InputQuery('Pls Enter Integer输入整数!', '123', str) then begin
    if str = '' then
      str := '0';
    if not IsFloatNum(str) then
      ShowMessage('err! pls enter number,错误！请输入数字')
    else begin
      modbusfun06dat := StrToInt(StringReplace(str, '.', '', [rfReplaceAll]));
      case sysedit[3].Tag of
        3, 4, 10:
          begin
            modbusfun06 := $10A0 + sysedit[3].Tag;
            SysExplainNum := 9 + sysedit[3].Tag;
            addToLog('系统参数第' + inttostr(1 + sysedit[3].Tag) + '个【参数】，修改为：' + str);
          end;
      else
        modbusfun06 := $10A0 + (TEdit(Sender).Left div 60) * 1;
        SysExplainNum := 9 + (TEdit(Sender).Left div 60);
        addToLog('系统参数第' + inttostr(1 + (TEdit(Sender).Left div 60)) + '个【参数】，修改为：' + str);
      end;
      sysedit[3].Tag := 0;           //命令标志清空

      StepNowFresh := VIRLCROWS - 1;
    end;
  end;
 //ShowMessage(str); //显示输入的内容
end;

procedure TForm_main.stepNameStrClick(Sender: TObject);    //名称
var
  str: string;
  ii, jj, hh, nline: SmallInt;
begin
  StepNowFresh := (TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text);   //刷新开始行数
  hh := TEdit(Sender).Left + panel1.HorzScrollBar.Position + 10;      //+10补偿模式编辑框左移！
  if GetKeyState(VK_SHIFT) < 0 then begin
    if (mmo22.Tag = -1) then
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行
    if (mmo24.Tag = -1) then
      mmo24.Tag := mmo22.Tag;            //最小行
    if StepNowFresh + StepNowPage * VIRLCROWS <= mmo24.Tag then
      mmo24.Tag := StepNowFresh + StepNowPage * VIRLCROWS
    else begin
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
    end;

    if (mmo23.Tag = -1) then
      mmo23.Tag := hh div 60;             //当前列：0...
    if (mmo25.Tag = -1) then
      mmo25.Tag := mmo23.Tag;           //最小列
    if (hh div 60) < mmo25.Tag then
      mmo25.Tag := hh div 60
    else begin
      mmo23.Tag := hh div 60;             //当前列：0...
    end;

    for ii := 1 to VIRLCROWS do begin        //颜色先还原
      if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
        zu1edit[ii].Color := clWhite;
      zu2edit[ii].Color := clWhite;
      zu3edit[ii].Color := clWhite;

      zu5edit[ii].Color := clWhite;
      zu6edit[ii].Color := clWhite;
      zu7edit[ii].Color := clWhite;
      zu8edit[ii].Color := clWhite;
      zu9edit[ii].Color := clWhite;
      zu10edit[ii].Color := clWhite;
      zu11edit[ii].Color := clWhite;
      zu12edit[ii].Color := clWhite;
      zu13edit[ii].Color := clWhite;
      zu14edit[ii].Color := clWhite;
      zu15edit[ii].Color := clWhite;
      zu16edit[ii].Color := clWhite;
    end;
    for ii := mmo24.Tag to mmo22.Tag do begin
      if (ii < StepNowPage * VIRLCROWS) then
        Continue;   //小于当前页最小值
      if (ii >= StepNowPage * VIRLCROWS + VIRLCROWS) then
        Break;   //大于等于后一页最小值
      for jj := mmo25.Tag to mmo23.Tag do begin          //  if(lineInsert+ii)>VIRLCROWS then Break;

        nline := (ii mod VIRLCROWS) + 1;
        if jj = 0 then
          zu1edit[nline].Color := clMoneyGreen;    //名字和数组都是从1...开始
        if jj = 1 then
          zu2edit[nline].Color := clMoneyGreen;
        if jj = 2 then
          zu3edit[nline].Color := clMoneyGreen;

        if jj = 4 then
          zu5edit[nline].Color := clMoneyGreen;
        if jj = 5 then
          zu6edit[nline].Color := clMoneyGreen;
        if jj = 6 then
          zu7edit[nline].Color := clMoneyGreen;
        if jj = 7 then
          zu8edit[nline].Color := clMoneyGreen;
        if jj = 8 then
          zu9edit[nline].Color := clMoneyGreen;
        if jj = 9 then
          zu10edit[nline].Color := clSkyBlue;
        if jj = 10 then
          zu11edit[nline].Color := clMoneyGreen;
        if jj = 11 then
          zu12edit[nline].Color := clMoneyGreen;
        if jj = 12 then
          zu13edit[nline].Color := clMoneyGreen;
        if jj = 13 then
          zu14edit[nline].Color := clMoneyGreen;
        if jj = 14 then
          zu15edit[nline].Color := clMoneyGreen;
        if jj = 15 then
          zu16edit[nline].Color := clMoneyGreen;
      end;
    end;
    Exit;
  end
  else begin
    if mmo24.Tag >= 0 then begin
      mmo24.Tag := -1;
      mmo25.Tag := -1;
      AreaDisSelect(0);
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
      mmo23.Tag := hh div 60;             //当前列：0...
     //  exit;
    end;
  end;
  form_main.mmo4.lines.add('Pls Enter 4 char输入最多4个字母：');
  if InputQuery('Pls Enter 4 char输入最多4个字母：', 'abAB', str) then begin //str := InputBox('输入4位字符', 'abcd', '0000');
    for ii := 1 to length(str) do begin
      if Byte(str[ii]) > $7F then
        str[ii] := '*';
    end;
    modbusfun16str := Trim(str); //仅去除空格
    modbusfun16len := 2;

  //   hh:=TEdit(Sender).Left+panel1.HorzScrollBar.Position +10 ;      //+10补偿模式编辑框左移！
    modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60);
    
  //   StepNowFresh:=(TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(form_main.edt121.Text);   //刷新开始行数
    addToLog('第' + inttostr((StrToInt(form_main.edt45.text) div VIRLCROWS) * VIRLCROWS + StepNowFresh + 1) + '步骤的【步骤名】修改为：' + modbusfun16str);
  end;

end;

procedure TForm_main.stepModeStrClick(Sender: TObject);    //模式
var
  str: string;
  ii, jj, hh, nline: SmallInt;
begin
  StepNowFresh := (TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text);   //刷新开始行数
  hh := TEdit(Sender).Left + panel1.HorzScrollBar.Position + 10;      //+10补偿模式编辑框左移！
  if GetKeyState(VK_SHIFT) < 0 then begin
    if (mmo22.Tag = -1) then
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行
    if (mmo24.Tag = -1) then
      mmo24.Tag := mmo22.Tag;            //最小行
    if StepNowFresh + StepNowPage * VIRLCROWS <= mmo24.Tag then
      mmo24.Tag := StepNowFresh + StepNowPage * VIRLCROWS
    else begin
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
    end;

    if (mmo23.Tag = -1) then
      mmo23.Tag := hh div 60;             //当前列：0...
    if (mmo25.Tag = -1) then
      mmo25.Tag := mmo23.Tag;           //最小列
    if (hh div 60) < mmo25.Tag then
      mmo25.Tag := hh div 60
    else begin
      mmo23.Tag := hh div 60;             //当前列：0...
    end;

    for ii := 1 to VIRLCROWS do begin        //颜色先还原
      if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
        zu1edit[ii].Color := clWhite;
      zu2edit[ii].Color := clWhite;
      zu3edit[ii].Color := clWhite;

      zu5edit[ii].Color := clWhite;
      zu6edit[ii].Color := clWhite;
      zu7edit[ii].Color := clWhite;
      zu8edit[ii].Color := clWhite;
      zu9edit[ii].Color := clWhite;
      zu10edit[ii].Color := clWhite;
      zu11edit[ii].Color := clWhite;
      zu12edit[ii].Color := clWhite;
      zu13edit[ii].Color := clWhite;
      zu14edit[ii].Color := clWhite;
      zu15edit[ii].Color := clWhite;
      zu16edit[ii].Color := clWhite;
    end;
    for ii := mmo24.Tag to mmo22.Tag do begin
      if (ii < StepNowPage * VIRLCROWS) then
        Continue;   //小于当前页最小值
      if (ii >= StepNowPage * VIRLCROWS + VIRLCROWS) then
        Break;   //大于等于后一页最小值
      for jj := mmo25.Tag to mmo23.Tag do begin          //  if(lineInsert+ii)>VIRLCROWS then Break;

        nline := (ii mod VIRLCROWS) + 1;
        if jj = 0 then
          zu1edit[nline].Color := clMoneyGreen;    //名字和数组都是从1...开始
        if jj = 1 then
          zu2edit[nline].Color := clMoneyGreen;
        if jj = 2 then
          zu3edit[nline].Color := clMoneyGreen;

        if jj = 4 then
          zu5edit[nline].Color := clMoneyGreen;
        if jj = 5 then
          zu6edit[nline].Color := clMoneyGreen;
        if jj = 6 then
          zu7edit[nline].Color := clMoneyGreen;
        if jj = 7 then
          zu8edit[nline].Color := clMoneyGreen;
        if jj = 8 then
          zu9edit[nline].Color := clMoneyGreen;
        if jj = 9 then
          zu10edit[nline].Color := clSkyBlue;
        if jj = 10 then
          zu11edit[nline].Color := clMoneyGreen;
        if jj = 11 then
          zu12edit[nline].Color := clMoneyGreen;
        if jj = 12 then
          zu13edit[nline].Color := clMoneyGreen;
        if jj = 13 then
          zu14edit[nline].Color := clMoneyGreen;
        if jj = 14 then
          zu15edit[nline].Color := clMoneyGreen;
        if jj = 15 then
          zu16edit[nline].Color := clMoneyGreen;
      end;
    end;

    Exit;
  end
  else begin
    if mmo24.Tag >= 0 then begin
      mmo24.Tag := -1;
      mmo25.Tag := -1;
      AreaDisSelect(0);
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
      mmo23.Tag := hh div 60;             //当前列：0...
     //  exit;
    end;
  end;

  form_main.mmo4.lines.add('Pls Enter 4 char输入最多4个字母：');
  if InputQuery('Pls Enter 4 char输入最多4个字母：', 'AB', str) then begin //str := InputBox('输入4位字符', 'abcd', '0000');
    for ii := 1 to length(str) do begin
      if Byte(str[ii]) > $7F then
        str[ii] := '*';
    end;
    if length(str) < 4 then
      str := str + '0';       //补足4个字符；
    if length(str) < 4 then
      str := str + '0';
    if length(str) < 4 then
      str := str + '0';
    if length(str) < 4 then
      str := str + '0';

    if (VIRLCROWS < 600) then begin
      modbusfun16str := AnsiUpperCase(str);
    end
    else
      modbusfun16str := str;
    modbusfun16len := 2;
  //   hh:=TEdit(Sender).Left+panel1.HorzScrollBar.Position +10 ;      //+10补偿模式编辑框左移！
    modbusfun16 := $3000 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32 + ((hh + 10) div 60) + 1;

  //   StepNowFresh:=(TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(form_main.edt121.Text);   //刷新开始行数
    addToLog('第' + inttostr((StrToInt(form_main.edt45.text) div VIRLCROWS) * VIRLCROWS + StepNowFresh + 1) + '步骤的【模式】修改为：' + modbusfun16str);
  end;

end;

procedure TForm_main.stepNoteClick(Sender: TObject);
var
  str: string;
  ii, jj, hh, nline: SmallInt;
begin
  StepNowFresh := (TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text);   //刷新开始行数
  hh := TEdit(Sender).Left + panel1.HorzScrollBar.Position + 10;      //+10补偿模式编辑框左移！
  if GetKeyState(VK_SHIFT) < 0 then begin
    if (mmo22.Tag = -1) then
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行
    if (mmo24.Tag = -1) then
      mmo24.Tag := mmo22.Tag;            //最小行
    if StepNowFresh + StepNowPage * VIRLCROWS <= mmo24.Tag then
      mmo24.Tag := StepNowFresh + StepNowPage * VIRLCROWS
    else begin
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
    end;

    if (mmo23.Tag = -1) then
      mmo23.Tag := hh div 60;             //当前列：0...
    if (mmo25.Tag = -1) then
      mmo25.Tag := mmo23.Tag;           //最小列
    if (hh div 60) < mmo25.Tag then
      mmo25.Tag := hh div 60
    else begin
      mmo23.Tag := hh div 60;             //当前列：0...
    end;

    for ii := 1 to VIRLCROWS do begin        //颜色先还原
      if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
        zu1edit[ii].Color := clWhite;
      zu2edit[ii].Color := clWhite;
      zu3edit[ii].Color := clWhite;

      zu5edit[ii].Color := clWhite;
      zu6edit[ii].Color := clWhite;
      zu7edit[ii].Color := clWhite;
      zu8edit[ii].Color := clWhite;
      zu9edit[ii].Color := clWhite;
      zu10edit[ii].Color := clWhite;
      zu11edit[ii].Color := clWhite;
      zu12edit[ii].Color := clWhite;
      zu13edit[ii].Color := clWhite;
      zu14edit[ii].Color := clWhite;
      zu15edit[ii].Color := clWhite;
      zu16edit[ii].Color := clWhite;
    end;
    for ii := mmo24.Tag to mmo22.Tag do begin
      if (ii < StepNowPage * VIRLCROWS) then
        Continue;   //小于当前页最小值
      if (ii >= StepNowPage * VIRLCROWS + VIRLCROWS) then
        Break;   //大于等于后一页最小值
      for jj := mmo25.Tag to mmo23.Tag do begin          //  if(lineInsert+ii)>VIRLCROWS then Break;

        nline := (ii mod VIRLCROWS) + 1;
        if jj = 0 then
          zu1edit[nline].Color := clMoneyGreen;    //名字和数组都是从1...开始
        if jj = 1 then
          zu2edit[nline].Color := clMoneyGreen;
        if jj = 2 then
          zu3edit[nline].Color := clMoneyGreen;

        if jj = 4 then
          zu5edit[nline].Color := clMoneyGreen;
        if jj = 5 then
          zu6edit[nline].Color := clMoneyGreen;
        if jj = 6 then
          zu7edit[nline].Color := clMoneyGreen;
        if jj = 7 then
          zu8edit[nline].Color := clMoneyGreen;
        if jj = 8 then
          zu9edit[nline].Color := clMoneyGreen;
        if jj = 9 then
          zu10edit[nline].Color := clSkyBlue;
        if jj = 10 then
          zu11edit[nline].Color := clMoneyGreen;
        if jj = 11 then
          zu12edit[nline].Color := clMoneyGreen;
        if jj = 12 then
          zu13edit[nline].Color := clMoneyGreen;
        if jj = 13 then
          zu14edit[nline].Color := clMoneyGreen;
        if jj = 14 then
          zu15edit[nline].Color := clMoneyGreen;
        if jj = 15 then
          zu16edit[nline].Color := clMoneyGreen;
      end;
    end;
    Exit;
  end
  else begin
    if mmo24.Tag >= 0 then begin
      mmo24.Tag := -1;
      mmo25.Tag := -1;
      AreaDisSelect(0);
      mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
      mmo23.Tag := hh div 60;             //当前列：0...

    end;
  end;
  form_main.mmo4.lines.add('Pls Enter 16 char输入最多16个字母或汉字：');
  if InputQuery('Pls Enter 16 char输入最多16个字母或汉字：', 'ab中文', str) then begin
    modbusfun16str := str;      //ZU17EDIT
    modbusfun16len := $08;
    modbusfun16 := $3012 + ((TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text)) * 32;
  //   StepNowFresh:=(TEdit(Sender).Top+panel1.VertScrollBar.Position-10) div strtoint(form_main.edt121.Text);   //刷新开始行数
    addToLog('第' + inttostr((StrToInt(form_main.edt45.text) div VIRLCROWS) * VIRLCROWS + StepNowFresh + 1) + '步骤的[备注]修改为：' + modbusfun16str);
  end;
end;

procedure TForm_main.ExpertClick(Sender: TObject);
begin
  if (Readchis('Result', 'top5') <> '0') then begin
    StepNowFresh := (TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text);   //刷新开始行数
  //ExpertNotes( (StrToInt(form_main.edt45.text)div VIRLCROWS)*VIRLCROWS +StepNowFresh,0 );
  end;
end;

procedure TForm_main.sysStrClick(Sender: TObject);
var
  str: string;
begin
  if InputQuery('Pls Enter 6 char!', 'ab中文', str) then begin
    modbusfun16 := $10A0 + (TEdit(Sender).Left div 60) * 1;
    SysExplainNum := 9 + (TEdit(Sender).Left div 60);
    modbusfun16str := Trim(str); //仅去除空格，不改变小写字符
    modbusfun16len := 3;
    addToLog('系统参数[机种名]修改为：' + modbusfun16str);
    StepNowFresh := VIRLCROWS - 1;
  end;
end;

procedure TForm_main.Comm1ReceiveData(Sender: TObject; Buffer: Pointer; BufferLength: Word);
var
  temp: byte;
  i: Integer;
  lanrbuf: array of Char;

  k:Integer;
  pcode:PBarcodeItem;
  scode:string;
  Sflag:string;
begin      //串口接收到数据
  temp := 0;
  viewstring := '';
  if not rb4.Checked then       //非ASCII格式显示
    viewstring := '[Recv:][HEX]' + viewstring;
  move(Buffer^, pchar(@rbuf)^, BufferLength);
  if form_main.chk38.Checked then
    viewstring := GetTimemS + viewstring;
  for i := 1 to BufferLength do begin
    if rb4.Checked then
      viewstring := viewstring + char(rbuf[i])
    else
      viewstring := viewstring + inttohex(rbuf[i], 2) + ' ';
    temp := temp + 1;
    if temp = 100 then begin
      temp := 0;
      if not rb5.Checked then
        form_main.Memo1.Lines.add(viewstring);
      viewstring := '';
    end;
  end;
  if not rb5.Checked then
    form_main.Memo1.Lines.add(viewstring);
  if (rbuf[2] = $05) and (rbuf[3] = $00) and ((rbuf[4] = $10) or (rbuf[4] = $11))      //复位启动后面跟F9的话！
    and (rbuf[10] = $03) and (rbuf[11] = $F9) and (BufferLength > 11) then begin
    for i := 9 to BufferLength do begin                                   //扔掉前8个字节！
      rbuf[i - 8] := rbuf[i];
    end;
  end;
  rxdprocess(BufferLength, 0);
  usb_busyT := 1;           //必须为1以下，否则刷新很慢；

end;

procedure TForm_main.SpeedButton12Click(Sender: TObject);
begin
  try
    Comm1.CommName := combobox1.Text;
    Comm1.BaudRate := strtoint(combobox2.Text);
    form_main.SpeedButton12.Enabled := false;
    btn112.Enabled := False;     //runing...
    form_main.SpeedButton13.Enabled := true;
    btn113.Enabled := true;
    form_main.Comm1.StartComm;
    sleep(20);
    shp2.Brush.Color := clGreen;
    shp4.Brush.Color := clGreen;    //开启串口助手标志
      //form_main.tmr1.Enabled:=True;
    Stat1.Panels[0].Text := 'Port:' + combobox1.Text;
    Stat1.Panels[1].Text := 'Baut' + combobox2.Text + 'bps';
  except
    form_main.Comm1.StopComm;
    form_main.SpeedButton13.Enabled := false;
    btn113.Enabled := False;
    form_main.SpeedButton12.Enabled := true;
    btn112.Enabled := True;
    shp2.Brush.Color := clSilver;
    shp4.Brush.Color := clSilver;
    messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);   //     fswitch.StatusBar1.Panels[4].Text:='通讯状态：错误';
  end;
end;

procedure TForm_main.SpeedButton13Click(Sender: TObject);
begin
  form_main.chk18.Checked := False;
  form_main.SpeedButton13.Enabled := false;
  btn113.Enabled := False;
  form_main.SpeedButton12.Enabled := true;
  btn112.Enabled := true;
  form_main.Comm1.StopComm;
  shp2.Brush.Color := clSilver;
  shp4.Brush.Color := clSilver;          //form_main.tmr1.Enabled:=False;
end;
//////////////定时发送串口指令，唯一发送命令入口--------------------------------------------------------------

procedure TForm_main.tmr1Timer(Sender: TObject);
var
  temp8: byte;
  btemp: array[0..3] of byte;
  save_file_name, SLINE_head, str: string;  //,s
  Ansi_s, AnsiSLINE_head: string;         //  strs:array [0..32] of string; //
  strs: TStrings;
  i, j, temp16, line: Integer;
begin

  if ((FormLoad.Showing                                        //加载界面，可以后台执行导入步骤
    or ((TabSheet4.Showing and form_main.Active)            //debug界面
    or (modbusfun16 > 0) or (modbusfun16int > 0) or (modbusfun02 > 0) or (modbusfun03 > 0) or (modbusfun05 > 0) or (modbusfun06 > 0))  //命令可用，可同时启用，单需要借用SBUF的命令优先级高！
    ) and (((Assigned(CurrentDevice)  //使能USB调试
    or not SpeedButton12.Enabled)      //串口设备可用
    and (usb_busyT = 0)         //设备不忙 ，通信时延自适应
    ) or (modbusFSM = $44) or (modbusFSM = $54)           //通信错误，提示信息；
    or n44.Checked                     //本地离线查看
    )) then begin
    if FormLogHead.Showing and (modbusfun16int <> $0021) then
      FormLogHead.Close;
    DebugWin_txdProcess(0);

  end
  else begin
    sbuf[1] := 1;
  end;
end;

procedure TForm_main.tmr2Timer(Sender: TObject);
var
  i, j, ii, jj, nline: SmallInt;
begin
  if edt45.Focused then
    edt45.Color := clSkyBlue
  else
    edt45.Color := clWindow;
  mmo28Click(Self);
  if xyout[00].Left <> zu1edit[1].Left then begin
    xyout[00].Left := zu1edit[1].Left;
    xyout[01].Left := zu2edit[1].Left;
    xyout[02].Left := zu3edit[1].Left;
    xyout[03].Left := zu4edit[1].Left;
    xyout[04].Left := zu5edit[1].Left;
    xyout[05].Left := zu6edit[1].Left;
    xyout[06].Left := zu7edit[1].Left;
    xyout[07].Left := zu8edit[1].Left;
    xyout[08].Left := zu9edit[1].Left;
    xyout[09].Left := zu10edit[1].Left;
    xyout[10].Left := zu11edit[1].Left;
    xyout[11].Left := zu12edit[1].Left;
    xyout[12].Left := zu13edit[1].Left;
    xyout[13].Left := zu14edit[1].Left;
    xyout[14].Left := zu15edit[1].Left;
    xyout[15].Left := zu16edit[1].Left;
    xyout[16].Left := zu17edit[1].Left;
    xyout[17].Left := zu18edit[1].Left;
    xyout[18].Left := zu19edit[1].Left;
  end;
  if isvnsgmntclck1.Enabled then begin
    if isvnsgmntclck1.Tag > 0 then begin           //0.5秒一次中断；两次中断计数一次；
      if isvnsgmntclck1.Seconds >= 59 then begin
        if isvnsgmntclck1.Minutes < 59 then begin
          isvnsgmntclck1.Seconds := 0;
          isvnsgmntclck1.Minutes := isvnsgmntclck1.Minutes + 1;
        end;
      end
      else begin
        isvnsgmntclck1.Seconds := isvnsgmntclck1.Seconds + 1;
      end;
      isvnsgmntclck1.Tag := 0;
    end
    else
      isvnsgmntclck1.Tag := isvnsgmntclck1.Tag + 1;
  end;
  if form_main.TabSheet4.Showing then begin
    edt5.Tag := StrToInt('0' + edt45.Text);
    if edt5.Tag > 0 then
      edt5.Tag := edt5.Tag - 1;
    if ((working and $01) = $01) and (zu2edit[1 + edt5.Tag mod VIRLCROWS].Color = clskyblue) then      //工作中，闪烁；名称选中
      zu2edit[1 + edt5.Tag mod VIRLCROWS].Color := clWindow
    else
      zu2edit[1 + edt5.Tag mod VIRLCROWS].Color := clSkyBlue;
  end;

  if (mmo24.Tag >= 0) then begin //选择区域定时刷新！ GetKeyState(VK_SHIFT)<0 then begin

    for ii := 1 to VIRLCROWS do begin        //颜色先还原
      if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
        zu1edit[ii].Color := clWhite;
      zu2edit[ii].Color := clWhite;
      zu3edit[ii].Color := clWhite;

      if (zu5edit[ii].Color <> clGreen) then
        zu5edit[ii].Color := clWhite;
      if (zu6edit[ii].Color <> clGreen) then
        zu6edit[ii].Color := clWhite;
      zu7edit[ii].Color := clWhite;
      zu8edit[ii].Color := clWhite;
      if (zu9edit[ii].Color <> clGray) then
        zu9edit[ii].Color := clWhite;
      if (zu10edit[ii].Color <> clYellow) then
        zu10edit[ii].Color := clWhite;
      zu11edit[ii].Color := clWhite;
      zu12edit[ii].Color := clWhite;
      zu13edit[ii].Color := clWhite;
      zu14edit[ii].Color := clWhite;
      zu15edit[ii].Color := clWhite;
      zu16edit[ii].Color := clWhite;
      zu17edit[ii].Color := clWhite;
    end;
    for ii := mmo24.Tag to mmo22.Tag do begin
      if (ii < StepNowPage * VIRLCROWS) then
        Continue;   //小于当前页最小值
      if (ii >= StepNowPage * VIRLCROWS + VIRLCROWS) then
        Break;   //大于等于后一页最小值
      for jj := mmo25.Tag to mmo23.Tag do begin          //  if(lineInsert+ii)>VIRLCROWS then Break;

        nline := (ii mod VIRLCROWS) + 1;
        if (jj = 0) and (zu1edit[nline].text <> '0') and (zu1edit[nline].Color <> clred) then
          zu1edit[nline].Color := clMoneyGreen;    //名字和数组都是从1...开始
        if jj = 1 then
          zu2edit[nline].Color := clMoneyGreen;
        if jj = 2 then
          zu3edit[nline].Color := clMoneyGreen;

        if (jj = 4) and (zu5edit[nline].Color <> clGreen) then
          zu5edit[nline].Color := clMoneyGreen;
        if (jj = 5) and (zu6edit[nline].Color <> clGreen) then
          zu6edit[nline].Color := clMoneyGreen;
        if jj = 6 then
          zu7edit[nline].Color := clMoneyGreen;
        if jj = 7 then
          zu8edit[nline].Color := clMoneyGreen;
        if (jj = 8) and (zu9edit[nline].Color <> clGray) then
          zu9edit[nline].Color := clMoneyGreen;
        if (jj = 9) and (zu10edit[nline].Color <> clYellow) then
          zu10edit[nline].Color := clSkyBlue;
        if jj = 10 then
          zu11edit[nline].Color := clMoneyGreen;
        if jj = 11 then
          zu12edit[nline].Color := clMoneyGreen;
        if jj = 12 then
          zu13edit[nline].Color := clMoneyGreen;
        if jj = 13 then
          zu14edit[nline].Color := clMoneyGreen;
        if jj = 14 then
          zu15edit[nline].Color := clMoneyGreen;
        if jj = 15 then
          zu16edit[nline].Color := clMoneyGreen;
        if jj = 16 then
          zu17edit[nline].Color := clMoneyGreen;
      end;
    end;
    stat1.Panels[2].Text := '框选区：行(' + inttostr(mmo24.tag + 1) + '~' + inttostr(mmo22.tag + 1) + ')' + ',列(' + inttostr(mmo25.tag + 1) + '~' + inttostr(mmo23.tag + 1) + ')';
  end
  else if mmo22.Tag >= 0 then begin
    stat1.Panels[2].Text := '框选区取消！';
    mmo22.Tag := -1;
    mmo23.Tag := -1;
  end;
  if TabSheet4.Showing and (mmo4.Hint <> stat1.Panels[2].Text) then begin      //有变化：记录
    form_main.mmo4.lines.add(stat1.Panels[2].Text);
    form_main.ts29.Show;
  end;
  mmo4.Hint := stat1.Panels[2].Text;
  if not FormPN.Showing then begin
  //  if form_main.Caption = 'zhidbg' then begin
  //    //form_main.Caption :='zhidbg';
  //  end
  //  else begin
      form_main.Caption := Readchis('Comm Para', 'Company') + '-' + lbl90.hint;

      form_main.Caption := form_main.Caption + '-DataLogger' + Label9.Caption + '     ' + datetostr(Date) + '    ' + TimeToStr(Time) + '     -     ' + form_main.lbl29.Caption + '     -     ' + form_main.lbl28.Caption;
  //  end;

    if (barcodeSelT > 0) and TabSheet2.TabVisible then begin
      if (barcodeSelT = 1) and (TabSheet2.Showing) then    //条码输入时间到，设置全选中状态以便重新输入条码！
      begin
        if (Label7.hint = '11') {and edit1.Enabled and grp36.Visible} then
          if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
            edit1.SetFocus;   //双条码框
        edit1.SelectAll;
      end;
      if (Label7.hint <> '11') or (trim(medit1.Text) <> '')    //双条码模式，条码非空    //or ( length( trim(medit1.Text) ) >=strtoint(edt55.text) )       //满足最小长度
        then      //双产品时 且第二个条码不为空
        barcodeSelT := barcodeSelT - 1;
    end;
  end;
   ///////////////////////////PASS NG显示超时 --------------------------------------------------------
  if PassOverT = 1 then begin
    if Trim(Label11.Caption) = 'PASS' then                         //不适用双工位同步系统
    begin
      Label11.Font.Color := clWindowText;
      Label11.Caption := 'Ready';
      Label11.Font.Size := 150;
      Lbl169.Caption := '';
      lbl169.Font.Size := 10;
      if (strtoint(edt59.Text) > 0) and (strtoint(edt60.Text) > 0) then begin
        for j := 0 to StrToInt(edt59.Text) - 1 do begin
          for i := 0 to StrToInt(edt60.Text) - 1 do
            with Rleds[j * StrToInt(edt60.Text) + i] do begin
              Active := False;
              ActiveColor := clGreen;
              //AutoInactiveColor:=False;
            end;
        end;
      end;
    end;
  end;
  if PassOverT > 0 then
    PassOverT := PassOverT - 1;

  if NGOverT = 1 then begin
    if (Trim(Label11.Caption) = 'NG') or (Trim(Label11.Caption) = 'FAIL') then    //不适用双工位同步系统
    begin
      Label11.Font.Color := clWindowText;
      Label11.Caption := 'Ready';
      Label11.Font.Size := 150;
      Lbl169.Caption := '';
      lbl169.Font.Size := 10;
      if (strtoint(edt59.Text) > 0) and (strtoint(edt60.Text) > 0) then begin
        for j := 0 to StrToInt(edt59.Text) - 1 do begin
          for i := 0 to StrToInt(edt60.Text) - 1 do
            with Rleds[j * StrToInt(edt60.Text) + i] do begin
              Active := False;
              ActiveColor := clGreen;                     //AutoInactiveColor:=False;
            end;
        end;
      end;
    end;
  end;
  if NGOverT > 0 then
    NGOverT := NGOverT - 1;
///////////----不常用-----------------////////////////数据文件备份服务器映射盘，如果时间（分钟）到则自动备份上传！
  UploadSerT := UploadSerT + 1;
  if (UploadSerT >= 60 * StrToInt(edt6.Text)) and (StrToInt(edt6.Text) > 0) then begin
    UploadSerT := 0;
    if LogType = 5 then begin                          //仅仅条码log文件
      lbl181.Caption := '1';
      Tcar.bakToServerDisk()
    end
    else if (Label10.caption <> 'Label10') then begin   //集总log文件
      lbl181.Caption := '1';
      btn2Click(self);
    end;
  end;
end;

//FCT5不能上传测试状态，不能上传Loading数据 ，必须在PC软件设置如下区别
//设置CommOpen=5，从而禁止由主板数据更新Log文件头几行，禁止Loading数据，禁止读取长机种名，
//设置PassOverT=5,从而实现PC定时复位结果标志；
procedure TForm_main.btn1Click(Sender: TObject);         //保存参数
var
  nrf_dat_config: Tinifile;
  ini_path: string;
begin
  try
    ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
    nrf_dat_config := Tinifile.Create(ini_path);
  //------------------------------------[Result]组机种参数---------------------------------------------------------------------
    if nrf_dat_config.Readstring('result', 'PassOverT', '') = '' then begin    //默认
      edt40.Text := '0';
      nrf_dat_config.WriteString('result', 'PassOverT', edt40.Text);     //FCT5主板必须由上位机通过超时来关闭显示结果；
    end;
    if nrf_dat_config.Readstring('result', 'NGOverT', '') = '' then begin    //默认
      edt42.Text := '0';
      nrf_dat_config.WriteString('result', 'NGOverT', edt42.Text);
    end;

  //------------------------------------[Server]组机种参数---------------------------------------------------------------------
    nrf_dat_config.WriteString('Server', '*', '*');
    if grp33.Visible then begin
      nrf_dat_config.WriteString('Server', 'TracePathA', edt128.Text);
      nrf_dat_config.WriteString('Server', 'TracePathB', edt129.Text);
    end;

  //------------------------------------[Comm Para]组机种参数---------------------------------------------------------------------
    //Stat1.Panels[0].Text := 'Comm'+combobox1.Text;   //Stat1.Panels[1].Text := 'Baut'+combobox2.Text + 'bps';
    //add by sunny  写Ini档通讯参数选择&误差设定
    Comm1.CommName := combobox1.Text;
    Comm1.BaudRate := strtoint(combobox2.Text);
  //  nrf_dat_config.WriteString('单位信息', '单位', Label5.Caption);
    nrf_dat_config.WriteString('Comm Para', 'Path', edt2.Text);
    nrf_dat_config.WriteString('Comm Para', 'CommOpen', groupbox1.hint);          //保存串口使能位；

    if GroupBox1.Visible then begin
      nrf_dat_config.WriteString('Comm Para', 'Port', ComboBox1.Text);
      nrf_dat_config.WriteString('Comm Para', 'Baud', ComboBox2.Text);
    end;
    if grp33.Tag = 3 then begin
      nrf_dat_config.WriteString('Server', 'Port', cbb4.Text);
      nrf_dat_config.WriteString('Server', 'Baud', cbb5.Text);
    end;

    if nrf_dat_config.Readstring('Comm Para', 'Uplocal', '') = '' then begin    //默认
      edt38.Text := '1';
      nrf_dat_config.WriteString('Comm Para', 'Uplocal', edt38.Text);
    end;
    if Readchis('Comm Para', 'ChildFolder') = '' then begin
      edt62.Text := '2';
      nrf_dat_config.WriteString('Comm Para', 'ChildFolder', '2');    //没有设置过则默认：2
    end;
    if nrf_dat_config.Readstring('Comm Para', 'DebugMode', '') = '' then begin    //默认
      lbl79.Color := clGray; //edt61.Text:='1';
      nrf_dat_config.WriteString('Comm Para', 'DebugMode', '1');
    end;

    if edt1.Text <> '' then begin
      nrf_dat_config.WriteString('Comm Para', 'PathServer', edt1.Text);
      nrf_dat_config.WriteString('Comm Para', 'UploadServerT', edt6.Text);
    end;
    if edt135.Visible then
      nrf_dat_config.WriteString('Comm Para', 'PathSNfile', edt135.Text);
    if edt157.Visible then
      nrf_dat_config.WriteString('Comm Para', 'TeslaSNPath', edt157.Text);
  //------------------------------------[Name->Redef]组机种参数---------------------------------------------------------------------
    nrf_dat_config.WriteString('Name->Redef', '*', '*');
  //------------------------------------[Model Para]组机种参数---------------------------------------------------------------------
    nrf_dat_config.WriteString('Model Para', '*', '*');
    if Readchis('Model Para', 'BarCodeEdit') <> '' then begin           //启用条码自锁功能
      if (StrToInt('0' + trim(form_main.edt82.Text)) > 0) and (StrToInt('0' + trim(form_main.edt82.Text)) < 99) then begin     //enPN>0,通用！
        nrf_dat_config.WriteString(FormPN.cbb1.Text, 'BarCodeLen', edt134.Text);
      end
      else begin
        nrf_dat_config.WriteString('Model Para', 'BarCodeLen', edt134.Text);
      end;
      edt7.Text := edt134.Text;            //不需锁死 edt134.Enabled:=False;           //修改完条码后锁死编辑框；
    end;
  except
  end;

  nrf_dat_config.free;
  btn1.Font.Color := clDefault;
end;

const
  SELDIRHELP = 1000;

procedure TForm_main.btn3Click(Sender: TObject);         //选择文件夹
var
  Dir: string;
begin
  Dir := '';
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    edt2.Text := Dir + '\';
    btn1.Font.Color := clRed;
    Label10.Caption := 'Label10';
    lbl90.hint := 'modal';
  end;
end;

procedure TForm_main.btn12Click(Sender: TObject);
var
  ii: SmallInt;
begin

  if (VIRLCROWS >= 50) then begin
    for ii := 1 to VIRLCROWS do begin        //颜色还原
      if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
        zu1edit[ii].Color := clWhite;
      zu2edit[ii].Color := clWhite;
      zu3edit[ii].Color := clWhite;

      zu5edit[ii].Color := clWhite;
      zu6edit[ii].Color := clWhite;
      zu7edit[ii].Color := clWhite;
      zu8edit[ii].Color := clWhite;
      zu9edit[ii].Color := clWhite;
      zu10edit[ii].Color := clWhite;
      zu11edit[ii].Color := clWhite;
      zu12edit[ii].Color := clWhite;
      zu13edit[ii].Color := clWhite;
      zu14edit[ii].Color := clWhite;
      zu15edit[ii].Color := clWhite;
      zu16edit[ii].Color := clWhite;

    end;
  end;
  modbusfun05 := 13;
  modbusfun05dat := $FF;

  form_main.btn14.Enabled := true;     //查看NG电阻，不能保存步骤
  form_main.btn16.Enabled := true;
  lbl170.Visible := False;

  form_main.mmo4.lines.add('发送上翻页命令');

end;

procedure TForm_main.btn13Click(Sender: TObject);
var
  ii: SmallInt;
begin
//  ts15.Show;
//  ts16.Tag:=0;


  if (VIRLCROWS >= 50) then begin
    for ii := 1 to VIRLCROWS do begin        //颜色还原
      if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
        zu1edit[ii].Color := clWhite;
      zu2edit[ii].Color := clWhite;
      zu3edit[ii].Color := clWhite;

      zu5edit[ii].Color := clWhite;
      zu6edit[ii].Color := clWhite;
      zu7edit[ii].Color := clWhite;
      zu8edit[ii].Color := clWhite;
      zu9edit[ii].Color := clWhite;
      zu10edit[ii].Color := clWhite;
      zu11edit[ii].Color := clWhite;
      zu12edit[ii].Color := clWhite;
      zu13edit[ii].Color := clWhite;
      zu14edit[ii].Color := clWhite;
      zu15edit[ii].Color := clWhite;
      zu16edit[ii].Color := clWhite;

    end;
  end;
  modbusfun05 := 14;
  modbusfun05dat := $FF;

  form_main.btn14.Enabled := true;     //查看NG电阻，不能保存步骤
  form_main.btn16.Enabled := true;
  lbl170.Visible := False;

  form_main.mmo4.lines.add('发送下翻页命令');

end;

procedure TForm_main.btn14Click(Sender: TObject);
begin
  AreaDisSelect(0);
  btn16.Tag := 5;
  if btn14.Font.Color = clRed then begin
    modbusfun05 := 8;
    modbusfun05dat := $FF;
    addToLog('SaveSys,保存系统参数');
  end
  else if btn16.Font.Color = clRed then begin
    modbusfun05 := 11;
    modbusfun05dat := $FF;
    addToLog('SaveStep,保存当前页步骤');
  end
  else begin
    modbusfun05 := 8;
    modbusfun05dat := $FF;
    addToLog('SaveSys,保存系统参数');
  end;
end;

procedure TForm_main.btn15Click(Sender: TObject);
begin
  AreaDisSelect(0);
  btn15.Tag := 2;       //单步按下，用于working...是单步还是自动
  modbusfun05 := 12;
  modbusfun05dat := $FF;
  working := 1;
  form_main.mmo4.lines.add('发送单步调试命令');
end;

procedure TForm_main.chk1Click(Sender: TObject);
begin
  if not chk1.Focused then
    Exit;      //只有鼠标点击触发
  SysExplainNum := 1; //-1; btn141Click(Self);
  //if Panel2.Showing then
  if btn14.Showing and btn14.Enabled then
    btn14.SetFocus;
  modbusfun05 := 32;
  if chk1.Checked then begin
    modbusfun05dat := $FF;
    form_main.mmo4.lines.add('总结果输出:勾选');
  end
  else begin
    modbusfun05dat := 0;
    form_main.mmo4.lines.add('总结果输出:取消勾选');
  end;
end;

procedure TForm_main.chk2Click(Sender: TObject);
begin
  if not chk2.Focused then
    Exit;
  SysExplainNum := 2; //-1; btn141Click(Self);       //if Panel2.Showing then
  if btn14.Showing and btn14.Enabled then
    btn14.SetFocus;
  modbusfun05 := 33;                               //if chk2.Checked then modbusfun05dat:=$FF else modbusfun05dat:=0;
  if chk2.Checked then begin
    modbusfun05dat := $FF;
    form_main.mmo4.lines.add('辅助串转并驱动板输出:勾选');
  end
  else begin
    modbusfun05dat := 0;
    form_main.mmo4.lines.add('辅助串转并驱动板输出:取消勾选，默认！');
  end;

end;

procedure TForm_main.chk3Click(Sender: TObject);
begin
  SysExplainNum := 3; //-1; btn141Click(Self);
  modbusfun05 := 34;
  if chk3.Checked then
    modbusfun05dat := $FF
  else
    modbusfun05dat := 0;
end;

procedure TForm_main.chk4Click(Sender: TObject);
begin
  SysExplainNum := 4; //-1; btn141Click(Self);
  modbusfun05 := 35;
  if chk4.Checked then
    modbusfun05dat := $FF
  else
    modbusfun05dat := 0;
end;

procedure TForm_main.btn17Click(Sender: TObject);
begin
  modbusfun06dat := StrToInt('0' + edt45.Text) + 1;
  modbusfun06 := $10B0;
  form_main.mmo4.lines.add('选中步骤+1命令');
end;

procedure TForm_main.btn18Click(Sender: TObject);
begin
  modbusfun06dat := StrToInt('0' + edt45.Text);
  if modbusfun06dat > 0 then
    modbusfun06dat := modbusfun06dat - 1;
  modbusfun06 := $10B0;
  form_main.mmo4.lines.add('选中步骤-1命令');
end;

procedure TForm_main.btn20Click(Sender: TObject);
begin
  modbusfun16int := $5000;
  modbusfun16len := StrToInt(edt5.Text);

  sbuf[2] := $03;
  sbuf[3] := $50;
  sbuf[4] := $00;
  sbuf[5] := $00;
  sbuf[6] := StrToInt(edt5.Text);

end;

procedure TForm_main.btn21Click(Sender: TObject);
begin
  modbusfun02 := $5000;

  sbuf[2] := $02;
  sbuf[3] := $50;
  sbuf[4] := $00;
  sbuf[5] := $00;
  sbuf[6] := $20;
end;

procedure TForm_main.InputQueryPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
var
  str: string;
  hh: SmallInt;
begin
  Handled := True;
end;

procedure TForm_main.InputQueryKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (btn98.Tag > 0) and ((Key = ord('C'))) then begin
    if Clipboard.HasFormat(CF_TEXT) then
      ClipBoard.Clear;   //  Edit1.SelText := '"Paste" DISABLED!';
    Key := 0;
    btn98.Tag := 0;
  end;
end;

procedure TForm_main.InputQueryKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (ssCtrl in Shift) and ((Key = ord('C'))) then    //按下CTRL+C
  begin
    btn98.Tag := 1;
  end;
  if (ssCtrl in Shift) and ((Key = ord('V'))) then begin
    if Clipboard.HasFormat(CF_TEXT) then
      ClipBoard.Clear;       //  Edit1.SelText := '"Paste" DISABLED!';
    Key := 0;
  end;
end;

function ScanCodeInputQuery(const ACaption, APrompt: string; var Value: string): Boolean;
var
  Form: TForm;
  Prompt: TLabel;
  Edit: TEdit;
  DialogUnits: TPoint;
  ButtonTop, ButtonWidth, ButtonHeight: Integer;

  function GetAveCharSize(Canvas: TCanvas): TPoint;
  var
    I: Integer;
    Buffer: array[0..51] of Char;
  begin
    for I := 0 to 25 do
      Buffer[I] := Chr(I + Ord('A'));
    for I := 0 to 25 do
      Buffer[I + 26] := Chr(I + Ord('a'));
    GetTextExtentPoint(Canvas.Handle, Buffer, 52, TSize(Result));
    Result.X := Result.X div 52;
  end;

begin
  Result := False;
  Form := TForm.Create(Application);
  with Form do
  try
    Canvas.Font := Font;
    DialogUnits := GetAveCharSize(Canvas);
    BorderStyle := bsDialog;
    Caption := ACaption;
    ClientWidth := MulDiv(180, DialogUnits.X, 4);
    Position := poScreenCenter;
    Prompt := TLabel.Create(Form);
    with Prompt do begin
      Parent := Form;
      Caption := APrompt;
      Left := MulDiv(8, DialogUnits.X, 4);
      Top := MulDiv(8, DialogUnits.Y, 8);
      Constraints.MaxWidth := MulDiv(164, DialogUnits.X, 4);
      WordWrap := True;
    end;
    Edit := TEdit.Create(Form);
    with Edit do begin
      Parent := Form;
      Left := Prompt.Left;
      Top := Prompt.Top + Prompt.Height + 5;
      Width := MulDiv(164, DialogUnits.X, 4);
      MaxLength := 255;
      Text := Value;
      OnContextPopup := form_main.InputQueryPopup;
      OnKeyDown := form_main.InputQueryKeyDown;
      OnKeyUp := form_main.InputQueryKeyUp;
      SelectAll;
    end;
    ButtonTop := Edit.Top + Edit.Height + 15;
    ButtonWidth := MulDiv(50, DialogUnits.X, 4);
    ButtonHeight := MulDiv(14, DialogUnits.Y, 8);
    with TButton.Create(Form) do begin
      Parent := Form;
      Caption := SMsgDlgOK;
      ModalResult := mrOk;
      Default := True;
      SetBounds(MulDiv(38, DialogUnits.X, 4), ButtonTop, ButtonWidth, ButtonHeight);
    end;
    with TButton.Create(Form) do begin
      Parent := Form;
      Caption := SMsgDlgCancel;
      ModalResult := mrCancel;
      Cancel := True;
      SetBounds(MulDiv(92, DialogUnits.X, 4), Edit.Top + Edit.Height + 15, ButtonWidth, ButtonHeight);
      Form.ClientHeight := Top + Height + 13;
    end;
    if ShowModal = mrOk then begin
      Value := Edit.Text;
      Result := True;
    end;
  finally
    Form.Free;
  end;
end;

procedure TForm_main.BitBtn1Click(Sender: TObject);
var
  str, password: string;
  nrf_dat_config: Tinifile;
  ini_path: string;

  procedure checkpwd(str: string);
  begin
    if //(inputbox('登录窗口’,'请输入账号','')='MyName') and (inputbox('登录窗口','请输入密码','')='999')
      (str = password) or (str = '137760') then//'999' then
    begin
      Showmessage('Debug Window Open!');
      if lbl79.Color = clgreen then begin        //不能回到监控界面
        TabSheet2.TabVisible := False;    //不用关闭组监控界面 TabSheet3.TabVisible:=False;
      end;
      TabSheet4.TabVisible := true;
      ts3.TabVisible := True;
      TabSheet4.Enabled := True;
      PageControl1.TabIndex := 3 - 1;
      N9.Enabled := True;
      N10.Enabled := TRUE;
      N50.Enabled := True;
      N51.Enabled := True;
      A1.Enabled := True;
      if btn98.Visible then begin      //CAN机箱，密码才允许修改log路径
        btn3.Enabled := True;
        edt2.Enabled := True;

        btn100.Enabled := True;
        edt135.Enabled := True;
        btn4.Enabled := True;
        edt1.Enabled := True;
      end;
    end
    else begin
      Showmessage('Enter Error！');
      TabSheet4.TabVisible := False;
      ts3.TabVisible := False;
      TabSheet4.Enabled := False;
      N9.Enabled := False;
      N10.Enabled := False;
      N50.Enabled := False;
      N51.Enabled := False;
      A1.Enabled := False;             // Application.Terminate;
    end;
  end;

begin
  ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
  try
    nrf_dat_config := Tinifile.Create(ini_path);    //关联INI文件

    if nrf_dat_config.Readstring('Comm Para', 'PassWord', '') <> '' then      //不为空
    begin
      password := nrf_dat_config.Readstring('Comm Para', 'PassWord', '');
    end
    else
      password := '999';
  finally
    nrf_dat_config.free;
  end;
  if btn98.Visible then begin
    if ScanCodeInputQuery('LogIn Window!', 'Pls Enter Password 请输入密码', str) then begin
      CheckPwd(str);
    end;
  end
  else begin
    if InputQuery('LogIn Window!', 'Pls Enter Password 请输入密码', str) then begin
      CheckPwd(str);
    end;
  end;
end;

procedure TForm_main.btn24Click(Sender: TObject);
var
  f: TextFile;
  save_file_name, SLINE_head, s: string;
begin
  AreaDisSelect(0);
  ts23.Show;                        //显示checklist项目；

  lbl28.Caption := '导出Start Export';
  if (StrToInt('0' + sysedit[5].Text) = 0) or (working > 0) then      //没有读到版本号，不执行
  begin
    if (working > 0) then
      lbl28.Caption := 'MCU is busying, Pls press RESET!请按复位再导出'
    else
      lbl28.Caption := 'Pls retry to click!请重新上电，重启软件试一次！';
  end
  else begin
    if FileExists(ExtractFilePath(application.ExeName) + 'zhiEdit.exe')   //
      or (winexec(pchar(Readchis('Comm Para', 'zhiEdit')), SW_NORMAL) >= 32) then begin
      dlgOpen1.FilterIndex := 2;
    end;
    if (y1.Checked and FileExists(lbl29.Caption)) or dlgOpen1.Execute then begin
      lbl28.Font.Color := clRed;
      if btn24.Tag > 100 then begin         //读取到最大步骤数
        sprgrsbr1.Max := (btn24.Tag * 100) div 90;
      end
      else
        sprgrsbr1.Max := 100;
      sprgrsbr1.Position := 0;

      if (y1.Checked and FileExists(lbl29.Caption)) then begin
        save_file_name := lbl29.Caption;
      end
      else begin
        if ExtractFileExt(dlgOpen1.FileName) = '' then     //没有扩展名，则默认加上 .txt
          save_file_name := dlgOpen1.FileName + '.txt'
        else
          save_file_name := dlgOpen1.FileName;
      end;
      lbl29.Caption := save_file_name;
      try
        assignfile(f, save_file_name);
        rewrite(f);
        Sleep(50);

        SLINE_head := '';
        s := '{//序号';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '0' + sysedit[5].Text; //改为版本号<-'名称'  ,导入文件时会判断，不能含字母

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '标准值V';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '测控值S';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '上限Up%';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '下限Lo%';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '高点H';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '低点L';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '前延时T';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '模式M';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '比例K%';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '偏移B';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '平均P次'; //上限%2';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '联板号L'; //下限%2';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '高点H2';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '低点L2';

        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'Notes备注->>>'; //   +'>>>>>';    //zu17edit

        s := s + #9;
        SLINE_head := SLINE_head + s;

        s := '固件:v' + edt130.Text;
        s := s + #9;
        SLINE_head := SLINE_head + s;

        writeln(f, SLINE_head);
        modbusFSM := $3F;
      finally
        Closefile(f);
      end;
    end
    else begin
      A1.Checked := False;    //停止所有机种导入
      N50.Checked := false;
      N51.Checked := false;
    end;
  end;
end;

procedure TForm_main.btn23Click(Sender: TObject);
var
  f: TextFile;
  save_file_name: string;
  TxtFile: TStringList;
begin
  AreaDisSelect(0);
  ts15.show;                        //显示系统参数；
  if FileExists(ExtractFilePath(application.ExeName) + 'zhiEdit.exe')    //
    or (winexec(pchar(Readchis('Comm Para', 'zhiEdit')), SW_NORMAL) >= 32) then begin
    dlgOpen1.FilterIndex := 2;
  end;

  lbl28.Caption := '导入Start Import';
  if (StrToInt('0' + sysedit[5].Text) = 0) or (working > 0) then begin     //没有读到版本号，不执行

    if (working > 0) then
      lbl28.Caption := 'MCU is busying, Pls press RESET!请按复位再导入'
    else
      lbl28.Caption := 'Pls retry to click!请重新上电，重启软件试一次！';

  end
  else if (y1.Checked and FileExists(lbl29.Caption)) or dlgOpen1.Execute then begin
    lbl28.Font.Color := clRed;
    sprgrsbr1.Position := 0;

    if (y1.Checked and FileExists(lbl29.Caption)) then
      save_file_name := lbl29.Caption
    else
      save_file_name := dlgOpen1.FileName;

    TxtFile := TStringList.Create;
    TxtFile.LoadFromFile(save_file_name);
    sprgrsbr1.Max := 1 + TxtFile.Count;
    sprgrsbr1.Position := 0;
    FormLoad.sprgrsbr1.Max := 1 + TxtFile.Count;
    FormLoad.sprgrsbr1.Position := 0;
    TxtFile.Destroy;

    lbl29.Caption := save_file_name;
    try
      assignfile(f, save_file_name);
      //rewrite(f);
      Reset(f);
      //application.ProcessMessages;
      modbusFSM := $4F;
    finally
      Closefile(f);
    end;
  end
  else begin
    A1.Checked := False;    //停止所有机种导入
    N50.Checked := false;
    N51.Checked := false;
  end;
end;

procedure TForm_main.TabSheet2Show(Sender: TObject);   //基本监控、测试界面
begin
  if (Readchis('Result', 'top5') = '0') then
    Chart1.Hide;   //2022-1-8关闭，操作不方便 if shp4.Brush.Color = clGreen  then SpeedButton13Click(Self);  //自动关闭串口助手
//if TabSheet4.TabVisible then
  ts1.Hide;     //comm报错会默认ts1.showing
  if StrToInt(edt75.Text) > 0 then
    GpcResult.TabIndex := 1           //优先显示打印卡片界面；
  else
    GpcResult.TabIndex := 0;
  if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
    Edit1.SetFocus;

  tmr3.Enabled := true;       //定时读取主板信息，以及Result界面显示  //通过定时器执行不同的开机读取指令      ，可启动//读上限下限 及短路群
  if groupbox1.hint <> '5' then begin         //FCT5不支持加载和读取长文件名  //Label11.Font.Color:=clWindowText;label11.Caption:='Loading...'; Label11.Font.Size:=150;
    Lbl169.Caption := '';
    lbl169.Font.Size := 10;
  end;
  if form_main.sbtbtn1.Visible then
    form_main.sbtbtn1.Enabled := False;
  //移动到tmr3中断内<-- tmr4.Enabled:=true; tmr4.Interval:=2000+1;      //光标定位在条码框
end;

procedure TForm_main.TabSheet4Show(Sender: TObject);
begin
  if y1.Checked then
    y1Click(Self);     //code切换到调试界面

  N13.Visible := True;
  P6.Visible := true;
  S2.Visible := True;
  x1.Visible := True;
  N15.Visible := True;
  N15.Enabled := True;

  N13.Enabled := True;
  S2.Enabled := True;
  x1.Enabled := True;

  N9.Enabled := True;         //导入
  N10.Enabled := True;        //导出
  N50.Enabled := True;
  N51.Enabled := True;
  A1.Enabled := True;

  grp3.Color := clBtnFace;
  Panel1.Color := clBtnFace;   //zu1edit[1].SetFocus;
  if btn13.Enabled and Panel2.Showing then
    btn13.setfocus;
  if edt45.Enabled and Panel2.Showing then
    edt45.setfocus;

  pgc1.TabIndex := 0;        //默认：显示系统参数卡；
  N18.Enabled := false;
end;

procedure TForm_main.btn2Click(Sender: TObject);    //备份到服务器
begin
  TMatrix.BaktoMapDisk;
end;

procedure TForm_main.btn4Click(Sender: TObject);
var
  Dir: string;
begin
  Dir := '';
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    edt1.Text := Dir + '\';
    btn1.Font.Color := clRed;
    edt1.Color := clWindow;
  end;
end;

procedure TForm_main.FormClose(Sender: TObject; var Action: TCloseAction);
var
  handle2: Thandle;
  f: TextFile;
  save_file_name, ss: string;
  shandle: PAnsiChar;
var
  MyHandle: THandle;

  function FindTask(ExeFileName: string): Boolean;     //查找正在运行的进程
  const
    PROCESS_TERMINATE = $0001;
  var
    ContinueLoop: BOOL;
    FSnapshotHandle: THandle;
    FProcessEntry32: TProcessEntry32;
  begin
    result := False;
    FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    FProcessEntry32.dwSize := Sizeof(FProcessEntry32);
    ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
    while integer(ContinueLoop) <> 0 do begin
      if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) = UpperCase(ExtractFileName(ExeFileName))) or (UpperCase(FProcessEntry32.szExeFile) = UpperCase(ExeFileName))) then begin
        Result := True;
        Break;
      end;
      ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
    end;
    CloseHandle(FSnapshotHandle);
  end;

  function KillTask(ExeFileName: string): Integer;   //关闭正在运行的进程
  const
    PROCESS_TERMINATE = $0001;
  var
    ContinueLoop: BOOL;
    FSnapshotHandle: THandle;
    FProcessEntry32: TProcessEntry32;
  begin
    Result := 0;
    FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
    ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);

    while Integer(ContinueLoop) <> 0 do begin
      if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) = UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) = UpperCase(ExeFileName))) then
        Result := Integer(TerminateProcess(OpenProcess(PROCESS_TERMINATE, BOOL(0), FProcessEntry32.th32ProcessID), 0));
      ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
    end;
    CloseHandle(FSnapshotHandle);
  end;

begin

  if edt53.hint <> '0' then begin
    idpsrvr1.Active := False;
    idpclnt1.Active := False;
  end;
  if (Readchis('result', 'CloseReport') <> '1') and (Readchis('result', 'CloseReport') <> '11') then begin
    if (LogType <> 2) and (LogType <> 3) and (StrToInt('0' + edt38.text) mod 10 > 0) then         //保存良率统计(不包含tesla和jiahe )
      SaveCsvReport(0);
  end;

  if (StrToInt('0' + edt6.Text) > 0) and (Label10.caption <> 'Label10') then      //如果上传映射服务器，则在关闭时上传一次
  begin
    lbl181.Caption := '10';
    btn2Click(self);
  end;

  if edt53.hint = '9000' then begin            //二次开发    、
    if FindTask(ExtractFileName(copy(ExtractFileName(application.ExeName), 0, Pos('.', ExtractFileName(application.ExeName)) - 1) + '.a.exe')) then
      KillTask(ExtractFileName(copy(ExtractFileName(application.ExeName), 0, Pos('.', ExtractFileName(application.ExeName)) - 1) + '.a.exe'));
  end;

  if (Readchis2('Comm Para', 'UpLocal') <> '') then begin                    //关闭其它从属程序；
    if FindTask('2zhi.exe') then
      KillTask('2zhi.exe');
    if FindTask('3zhi.exe') then
      KillTask('3zhi.exe');
    if FindTask('4zhi.exe') then
      KillTask('4zhi.exe');
    if FindTask('5zhi.exe') then
      KillTask('5zhi.exe');
    if FindTask('6zhi.exe') then
      KillTask('6zhi.exe');
    if FindTask('7zhi.exe') then
      KillTask('7zhi.exe');
    if FindTask('8zhi.exe') then
      KillTask('8zhi.exe');

    if FindTask('zhi.exe') then
      KillTask('zhi.exe');

    if FindTask('2zhidbg.exe') then
      KillTask('2zhidbg.exe');
    if FindTask('3zhidbg.exe') then
      KillTask('3zhidbg.exe');
    if FindTask('4zhidbg.exe') then
      KillTask('4zhidbg.exe');
    if FindTask('5zhidbg.exe') then
      KillTask('5zhidbg.exe');
    if FindTask('6zhidbg.exe') then
      KillTask('6zhidbg.exe');
    if FindTask('7zhidbg.exe') then
      KillTask('7zhidbg.exe');
    if FindTask('8zhidbg.exe') then
      KillTask('8zhidbg.exe');
    if FindTask('zhidbg.exe') then
      KillTask('zhidbg.exe');
  end;
end;

procedure TForm_main.FormShow(Sender: TObject);
var
  temp: byte;
  i: SmallInt;
  nrf_dat_config: Tinifile;
  save_file_name, ini_path, sst: string;
begin                   //显示才处理配置文件相关

  ReadConfig_iniProcess(0);                  //读取总的机种参数

  //--------串口自动识别------
  zerocount := 0;
  Comm1.CommName := combobox1.Text;
  Comm1.BaudRate := strtoint(combobox2.Text);
  if StrToInt(groupbox1.hint) > 0 then begin              //commopen使能
    try
      form_main.SpeedButton12.Enabled := false;
      form_main.SpeedButton13.Enabled := true;
      form_main.Comm1.StartComm;
      shp2.Brush.Color := clGreen;
      shp4.Brush.Color := clGreen;
      sleep(20);
    except
      for temp := 1 to 9 do begin
        Comm1.CommName := 'com' + inttostr(temp);
        try
          form_main.SpeedButton12.Enabled := false;
          btn112.Enabled := False;
          form_main.SpeedButton13.Enabled := true;
          btn113.Enabled := true;
          form_main.Comm1.StartComm;
          shp2.Brush.Color := clGreen;
          shp4.Brush.Color := clGreen;
        except
          form_main.SpeedButton13.Enabled := false;
          btn113.Enabled := False;
          form_main.SpeedButton12.Enabled := true;
          btn112.Enabled := true;
          shp2.Brush.Color := clSilver;
        end;
        if shp2.Brush.Color = clGreen then begin
          combobox1.Text := Comm1.CommName;
          cbb3.Text := combobox1.Text;
          Break;
        end;
      end;
      if shp2.Brush.Color = clSilver then
        messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm1~9 is exist！仅支持COM1~COM9端口', mterror, [mbyes], 0);
    end;
    if shp2.Brush.Color = clSilver then begin
      ts1.Visible := true;
      Stat1.Panels[0].Text := 'Port:' + 'COMM error!';
    end
    else begin
      Stat1.Panels[0].Text := 'Port:' + combobox1.Text;
      Stat1.Panels[1].Text := 'Baut:' + combobox2.Text + 'bps';
    end;

  end
  else begin
    GroupBox1.Visible := False;
  end;

  if StrToInt(form_main.edt82.Text) >= 99 then begin         //可能用串口通信，必须放串口之后！//enPN jaguar等启用PC存放程序
    modbusfun06 := $0041;
    if FormPN.tag > 0 then
      modbusfun06dat := FormPN.tag - 1
    else
      modbusfun06dat := 48 - 1;
    if edt82.Tag = 102 then
      modbusfun06dat := modbusfun06dat + $1000;               //双向加载校验复位信号
    form_main.tmr1Timer(form_main);
    Sleep(100);
  end;

  PNbarcode_fileCreateProcess(0);             //有调用Load和 PN界面显示，然后读取机种下面参数

  if Readchis('Comm Para', 'barcodeDisRepeat') = '1' then
    Lbl140.Color := clGreen
  else
    Lbl140.Color := clGray;

  /////////////////因为tabsheet2显示会和生成文件冲突，所以移动到后面显示  ----------------------
  if Readchis('result', 'ParaSetsDisp') = '1' then
    form_main.grp31.Visible := True;
  if lbl79.Color = clGray then    //debugMode，开机进入
  begin
    memo1.Visible := True;
    TabSheet4.TabVisible := True;
    TabSheet2.TabVisible := False;
    PageControl1.TabIndex := 3 - 1;       //调试界面  //影响启动时间？
    grp3.Visible := True;
  end
  else begin
    TabSheet4.TabVisible := False;
  // -------------处理选项卡开机界面，不同场合开机界面不同----------------     ts9.TabVisible:=False;   //ts10.TabVisible:=False;
    begin
      TabSheet2.TabVisible := True;
      PageControl1.TabIndex := 2 - 1;   //pcset界面<--监控界面
    end;
    memo1.Visible := True;          //False;   //同学命令显示
    grp3.Visible := True;           //False;      //OS 系统参数框
  end;
  //--------------232通信 基本参数---------------------------------------------
  row_num := 8;                                 //从第8行开始读
  rbuf_usbptr := 0;
  usb_busyT := 0;
  comandbusy := 0;

  //---------------------------------------------------------------------------
  if lbl79.Color = clGreen then    //非debug界面
  begin
    TabSheet2.TabVisible := True;              //监控界面<--pcset界面
    PageControl1.TabIndex := 0;
  end;

  if edt53.hint = '9000' then begin            //备用：二次开发
    P2exeFileName := ExtractFilePath(application.ExeName) + copy(ExtractFileName(application.ExeName), 0, Pos('.exe', ExtractFileName(application.ExeName)) - 1) + '.a.exe';

    if FileExists(P2exeFileName) then begin
      WinExec(PChar(P2exeFileName), SW_NORMAL); //P2exefile.Execute;
      if edt53.tag >= 9002 then begin
        Stat1.Panels[3].Text := '主UDP=9000,从UDP=' + IntToStr(edt53.Tag);
      end
      else
        Stat1.Panels[3].Text := '主UDP=9000,从UDP=' + IntToStr(9001);
      Stat1.Tag := 9000;
      u4.Enabled := True;
      tmr4.Enabled := true;                  //定时：光标定位到debug界面
    end;
  end;

  if (Readchis2('Comm Para', 'UpLocal') <> '') and (form_main.CLoseFlag > 0) then begin    //先启动：此软件为第1工位（主软件） ;存在有其它工位；且没有关闭！
    P2exeFileName := ExtractFilePath(application.ExeName) + '3\' + ExtractFileName(application.ExeName);
      {$WARN SYMBOL_PLATFORM ON}
    if not FileExists(P2exeFileName) then begin
      P2exeFileName := ExtractFilePath(application.ExeName) + '3\3' + ExtractFileName(application.ExeName);
    end;
    if FileExists(P2exeFileName) then begin
      writechis3('Comm Para', 'Path', edt2.Text);
      writechis3('Model Para', 'Operator', edt79.Text);
      writechis3('Model Para', 'BarCodeLen', edt134.Text);
      WinExec(PChar(P2exeFileName), SW_NORMAL); //P2exefile.Execute;
    end;

    P2exeFileName := ExtractFilePath(application.ExeName) + '4\' + ExtractFileName(application.ExeName);
      {$WARN SYMBOL_PLATFORM ON}
    if not FileExists(P2exeFileName) then begin
      P2exeFileName := ExtractFilePath(application.ExeName) + '4\4' + ExtractFileName(application.ExeName);
    end;
    if FileExists(P2exeFileName) then begin
      writechis4('Comm Para', 'Path', edt2.Text);
      writechis4('Model Para', 'Operator', edt79.Text);
      writechis4('Model Para', 'BarCodeLen', edt134.Text);
      WinExec(PChar(P2exeFileName), SW_NORMAL); //P2exefile.Execute;
    end;

    P2exeFileName := ExtractFilePath(application.ExeName) + '2\' + ExtractFileName(application.ExeName);
      {$WARN SYMBOL_PLATFORM ON}
    if not FileExists(P2exeFileName) then begin
      P2exeFileName := ExtractFilePath(application.ExeName) + '2\2' + ExtractFileName(application.ExeName);
      if not FileExists(P2exeFileName) then begin
        Application.MessageBox('No P2 software;没有第2工位软件！', PChar(Application.Title), MB_OK + MB_ICONWARNING);              // Exit;
      end;
    end;
    if FileExists(P2exeFileName) then begin
      if (grp33.Tag = 3) then begin
        writechis2('Model Para', 'PartNumber', edt77.Text);
        writechis2('Model Para', 'Order', edt78.Text);
      //目录不建议相同  writechis2('Model Para','TraceApath',edt128.Text);
      //  writechis2('Model Para','TraceBpath',edt129.Text);
      end
      else
        writechis2('Comm Para', 'Path', edt2.Text);

      writechis2('Model Para', 'Operator', edt79.Text);
      writechis2('Model Para', 'BarCodeLen', edt134.Text);
      WinExec(PChar(P2exeFileName), SW_NORMAL); //P2exefile.Execute;
    end;
  end
  else if (grp33.Tag = 3) or (edt53.hint = '9052') or (edt53.hint = '9053') or (edt53.hint = '9054') then begin  //后启动：其它工位   (edt53.hint='9050')or(edt53.hint='9051')or
    if (grp33.Tag = 3) then begin
      if Readchis('Model Para', 'PartNumber') <> '' then
        edt77.Text := Readchis('Model Para', 'PartNumber');
      if Readchis('Model Para', 'Order') <> '' then
        edt78.Text := Readchis('Model Para', 'Order');
    //  if Readchis('Model Para','TraceApathsyn')<>'' then
    //    edt128.Text:=Readchis('Model Para','TraceApath');
   //   if Readchis('Model Para','TraceBpathsyn')<>'' then
    //    edt129.Text:=Readchis('Model Para','TraceBpath');
    end;
    if Readchis('Model Para', 'Operator') <> '' then
      edt79.Text := Readchis('Model Para', 'Operator');
  end;
end;

procedure TForm_main.btn25Click(Sender: TObject);
begin
  AreaDisSelect(0);
  modbusfun05 := 16;
  modbusfun05dat := $FF;

  form_main.btn14.Enabled := true;     //查看NG电阻，不能保存步骤
  form_main.btn16.Enabled := true;
  lbl170.Visible := False;
  form_main.mmo4.lines.add('发送启动命令');
end;

procedure TForm_main.btn26Click(Sender: TObject);                  //“复位”按钮
begin
  AreaDisSelect(0);
  btn19.Font.Color := clDefault;
  grp23.Visible := True;

  if (modbusFSM = $50) or (modbusFSM = $51) or (modbusFSM = $52) then
    modbusFSM := $53      //导入结束
  else if (modbusFSM = $40) or (modbusFSM = $41) or (modbusFSM = $42) then
    modbusFSM := $43 //导出结束
  else
    modbusfun05 := 17;
  modbusfun05dat := $FF;

  form_main.btn14.Enabled := true;     //查看NG电阻，不能保存步骤
  form_main.btn16.Enabled := true;
  lbl170.Visible := False;
  form_main.mmo4.lines.add('发送复位命令');

end;

procedure TForm_main.chk5Click(Sender: TObject);
begin
  SysExplainNum := 5; //-1; btn141Click(Self);
  modbusfun05 := 36;
  //if chk5.Checked then modbusfun05dat:=$FF else modbusfun05dat:=0;
  if chk5.Checked then begin
    modbusfun05dat := $FF;
    form_main.mmo4.lines.add('开短路专用算法:勾选');
  end
  else begin
    modbusfun05dat := 0;
    form_main.mmo4.lines.add('开短路专用算法:取消勾选，默认！');
  end;

end;

procedure TForm_main.TabSheet2Hide(Sender: TObject);
begin
  GpcResult.Visible := False;      //用于下次调用show函数
end;

procedure TForm_main.btn27Click(Sender: TObject);
begin
  PrintClickProcess(0);
end;

procedure TForm_main.tmr3Timer(Sender: TObject);
begin
  if TabSheet2.TabVisible then begin              //测试界面读取全部参数---参考基准
    GpcResult.Visible := True;
    begin                   //通用用法!, //关闭上下限更新文件表头 if inportNewFileflag=0 then安捷利防止刷新原来的文件头格式----
      if form_main.groupbox1.hint <> '5' then begin         //FCT5不支持加载和读取长文件名
        modbusfun06dat := $00F6;                        //读上限下限 及短路群
        modbusfun06 := $0001;
        tmr4.Enabled := true;
        tmr4.Interval := 2000 + 1;
      end;
    end;
    tmr3.Enabled := False;    //tmr3之延时小于500，会导致上面的指令不能完全执行（枚举过程需要时间！）
    if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
      Edit1.SetFocus;
  end;
end;

procedure TForm_main.edt45Click(Sender: TObject);
var
  str: string;
begin
  if InputQuery('Enter Int', '1234', str) then begin
    if str = '' then
      str := '0';
    if StrToInt(str) > 4980 then
      modbusfun06dat := 4980     //最大第980步
    else
      modbusfun06dat := StrToInt(str);
    modbusfun06 := $10B0;   //等同
  end;

end;

procedure TForm_main.tmr4Timer(Sender: TObject);      //光标定位。开启软件计时定时器 ，
begin
  if TabSheet4.Showing then begin       //适用用UDP9001二次开发
    if edt45.Enabled and Panel2.Showing then
      edt45.SetFocus;
    tmr4.Enabled := False;
  end
  else begin
    if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
      Edit1.SetFocus;

    if (label11.Caption = 'Loading...') and TabSheet2.Showing then begin      //仅测试界面允许加载<--- not TabSheet4.Showing debug界面必须禁止加载
      modbusfun06dat := $00F6;   //读上限下限 及短路群
      modbusfun06 := $0001;
    end
    else
      tmr4.Enabled := False;
  end;
end;

procedure TForm_main.btn28Click(Sender: TObject);
begin
  dlgPntSet1.Execute; //选择输出的打印机以及其他打印控制选项
end;


//定义report过程，使真实打印和打印愈来愈览共用此过程
//自定义Report过程
procedure Report(Acanvas: Tcanvas);
var
  sx, sy, sy0, sx0: real; //开始位置
  dpi, dpm: real; //每毫米的点数
   // Arect: Trect;
begin
  dpi := GetDeviceCaps(Printer.Handle, LogPixelsX);
  dpm := trunc(dpi / 25.4); //每毫米的点数
  sx := 100;
  sx0 := 10;
  sy0 := 10;
  sy := sy0;
  with Acanvas do
  try
    font.name := '宋体';
    font.size := 18;
    textout(trunc(60 * dpm), trunc(sy * dpm), 'Test...99999999999999');
    sy := 10;
    textout(trunc(60 * dpm), trunc(sy * dpm), 'Test...');

  finally
    Free;
  end;
end;

procedure TForm_main.N51Click(Sender: TObject);
var
  str: string;
begin
  if (StrToInt('0' + sysedit[5].Text) = 0) or (working > 0) then      //没有读到版本号，不执行
  begin
    if (working > 0) then
      lbl28.Caption := 'MCU is busying, Pls press RESET!请按复位再导入'
    else
      lbl28.Caption := 'Pls retry to click!请重新上电，重启软件试一次！';
  end
  else begin         //允许导入
    if InputQuery('Import Window!', 'Pls Enter Password 请输入密码', str) then begin
      if (str = '888999') then begin
        if not spthdlg1.Execute then begin
          Exit;
        end
        else begin

          str := '24';
          N51.Checked := TRUE;
          A1.Checked := false;
          N50.Checked := False;
          if InputQuery('Import Window!', 'Pls Enter MaxPN 请输入机种程序导出数N=', str) then begin
            N51.Tag := strtoint(str);
          end
          else
            N51.Tag := 0;
          A1.Tag := 0;
          if (strtoint('0' + sysedit[5].text) < 930) then begin
            str := '0';
            if InputQuery('Import Window!', 'Pls Enter Delay(S) 因固件版本太低，需增加延时（秒）=', str) then begin
              spthdlg1.Tag := strtoint(str);
            end
            else
              spthdlg1.Tag := 0;
          end;
          modbusfun06 := $0041;    //选择机种
          modbusfun06dat := A1.Tag + $2000;
          //btn23Click(Self);
        end;
      end;
    end;
  end;
end;

procedure TForm_main.N50Click(Sender: TObject);
var
  str: string;
begin
  if (StrToInt('0' + sysedit[5].Text) = 0) or (working > 0) then      //没有读到版本号，不执行
  begin
    if (working > 0) then
      lbl28.Caption := 'MCU is busying, Pls press RESET!请按复位再导入'
    else
      lbl28.Caption := 'Pls retry to click!请重新上电，重启软件试一次！';
  end
  else begin         //允许导入
    if InputQuery('Import Window!', 'Pls Enter Password 请输入密码', str) then begin
      if (str = '888999') then begin
        if not spthdlg1.Execute then begin
          Exit;
        end
        else begin

          str := '24';
          if InputQuery('Import Window!', 'Pls Enter MaxPN 请输入机种程序导入数N=', str) then begin
            N50.Tag := strtoint(str);
          end
          else
            N50.Tag := 0;
          A1.Tag := 0;
          if (strtoint('0' + sysedit[5].text) < 930) then begin
            str := '0';
            if InputQuery('Import Window!', 'Pls Enter Delay(S) 因固件版本太低，需增加延时（秒）=', str) then begin
              spthdlg1.Tag := strtoint(str);
            end
            else
              spthdlg1.Tag := 0;
          end;
          N50.Checked := TRUE;
          A1.Checked := false;
          N51.Checked := False;
          modbusfun06 := $0041;    //选择机种
          modbusfun06dat := A1.Tag + $2000;
          //btn23Click(Self);
        end;
      end;
    end;
  end;
end;

procedure TForm_main.btn29Click(Sender: TObject);
var
  mymetafile: Tmetafile;
   // Ifprt:Boolean;
begin
    //是false则打印预览，true则打印
  if chk10.Checked then         //ifprt
  begin
    printer.BeginDoc;
      //report(printer.canvas);  //将打印机画布对象传给report过程
    printer.enddoc;          //如果是打印则结束打印
  end
  else begin
    mymetafile := Tmetafile.Create;           //创建图元文件

      //以打印机句柄创建图元文件画布对象（Tmetafilecanvas）,并调用report过程
    report(TmetafileCanvas.Create(mymetafile, printer.Handle));
    img3.Canvas.StretchDraw(img3.canvas.cliprect, mymetafile); //如果是打印预览，则在image1上显示出来
  end;
end;

procedure TForm_main.tmr5Timer(Sender: TObject);      //总的通信（usb为主）超时计数
begin
  if Label11.Tag < 999999 then
    Label11.Tag := label11.Tag + 5;

  if tmr5.Tag = 1 then begin              //error之后读取没有返回，重新连接
    if Assigned(CurrentDevice) then begin
      usb_busyT := 31;
      form_main.Stat1.Panels[0].Text := 'Port:USB ERROR';
    end
    else begin
                                       //串口error无法控制
    end;
  end;
  if tmr5.Tag > 0 then
    tmr5.Tag := tmr5.Tag - 1;
  if (usb_busyT = 11) and ((form_main.Stat1.Panels[0].Text = 'Port:USB ERROR') or (form_main.Stat1.Panels[0].Text = 'Port:USB ERROR.') or (form_main.Stat1.Panels[0].Text = 'Port:USB ERROR..')) then begin
    usb_busyT := 31;
    tmr5.Tag := 999999;
    if (form_main.Stat1.Panels[0].Text = 'Port:USB ERROR') then
      form_main.Stat1.Panels[0].Text := 'Port:USB ERROR.'
    else if (form_main.Stat1.Panels[0].Text = 'Port:USB ERROR.') then
      form_main.Stat1.Panels[0].Text := 'Port:USB ERROR..'
    else
      form_main.Stat1.Panels[0].Text := 'Port:USB ERROR...';
    HidCtlDeviceChange(self);
  end;

  if usb_busyT > 0 then
    usb_busyT := usb_busyT - 1;
  if comandbusy > 0 then begin
    comandbusy := comandbusy - 1;

  end;
end;

procedure TForm_main.btn30Click(Sender: TObject);
begin
  modbusfun06dat := $00F2;     //NG.list
  modbusfun06 := $0001;
end;

procedure TForm_main.btn31Click(Sender: TObject);
begin
  modbusfun06dat := $00F1;    //读找点结果
  modbusfun06 := $0001;
end;

procedure TForm_main.btn32Click(Sender: TObject);
begin
  modbusfun06dat := $00F0;    //读找点结果
  modbusfun06 := $0001;
end;

procedure TForm_main.btn33Click(Sender: TObject);
begin
  modbusfun06dat := $00F3;
  modbusfun06 := $0001;
end;

procedure TForm_main.btn34Click(Sender: TObject);
begin
  modbusfun06dat := $00F4;
  modbusfun06 := $0001;
end;

procedure TForm_main.btn35Click(Sender: TObject);
begin
  modbusfun06dat := $00F5;
  modbusfun06 := $0001;
end;

procedure TForm_main.btn36Click(Sender: TObject);
begin
  modbusfun06dat := $00F6;
  modbusfun06 := $0001;
end;

procedure TForm_main.btn37Click(Sender: TObject);
begin
  modbusfun06dat := $00F7;
  modbusfun06 := $0001;
end;

procedure TForm_main.btn38Click(Sender: TObject);
begin
  modbusfun06dat := $00F8;
  modbusfun06 := $0001;
end;

procedure TForm_main.btn39Click(Sender: TObject);
begin
  modbusfun06dat := $00F9;
  modbusfun06 := $0001;
end;

procedure TForm_main.btn40Click(Sender: TObject);
begin
  modbusfun06dat := $00E0;
  modbusfun06 := $0001;
end;

procedure TForm_main.btn41Click(Sender: TObject);
begin
  modbusfun06dat := $00E1;
  modbusfun06 := $0001;
end;

procedure TForm_main.btn42Click(Sender: TObject);
var
  i: Integer;
begin
  modbusfun16int := $10A0;
  modbusfun16len := $10;  //18

  sbuf[2] := $10;
  sbuf[3] := $10;
  sbuf[4] := $A0;
  sbuf[5] := $00;
  sbuf[6] := $10;       //18
  sbuf[7] := 2 * sbuf[6];
  sbuf[8] := $80;     //ngokenabel      (StrToInt(sysedit[0].Text)+1) div 256;
  sbuf[9] := $10;     //lcd显示参数-对比度     (StrToInt(sysedit[0].Text)+1) mod 256;
  for i := 1 to 12 do   //12
  begin
    sbuf[8 + 2 * i] := StrToInt(sysedit[i].Text) div 256;
    sbuf[9 + 2 * i] := StrToInt(sysedit[i].Text) mod 256;
  end;
  sbuf[34] := byte('1');
  sbuf[35] := byte(' ');
  sbuf[36] := byte(' ');
  sbuf[37] := byte(' ');
  sbuf[38] := byte(' ');
  sbuf[39] := byte(' ');
end;

procedure TForm_main.btn43Click(Sender: TObject);
begin
  modbusfun16int := $1800;
  modbusfun16len := $10;

  sbuf[2] := $10;
  sbuf[3] := $18;
  sbuf[4] := $00;
  sbuf[5] := $00;
  sbuf[6] := $10;
  sbuf[7] := 2 * sbuf[6];

  sbuf[8 + 2 * 0] := $8000 div 256;
  sbuf[9 + 2 * 0] := $8000 mod 256;
  sbuf[8 + 2 * 1] := $0002 div 256;
  sbuf[9 + 2 * 1] := $0002 mod 256;
  sbuf[8 + 2 * 2] := $C001 div 256;
  sbuf[9 + 2 * 2] := $C001 mod 256;
  sbuf[8 + 2 * 3] := $8FFF div 256;
  sbuf[9 + 2 * 3] := $8FFF mod 256;

end;

procedure TForm_main.btn44Click(Sender: TObject);
var
  i: SmallInt;
  temp8: Byte;
  tempstr: string;
begin
  modbusfun16int := $2F00;
  modbusfun16len := $12;
  sbuf[2] := $10;

  sbuf[8] := StrToInt(edt143.Text) div 256;
  sbuf[9] := StrToInt(edt143.Text) mod 256; //序号
  sbuf[10] := byte(' ');
  sbuf[11] := byte('0');
  sbuf[12] := byte(' ');
  sbuf[13] := byte(' ');
  for i := 2 to 8 do begin
    sbuf[10 + 2 * i] := 0;
    sbuf[11 + 2 * i] := 0;
  end;
  sbuf[22] := StrToInt(edt49.Text) div 256;
  sbuf[23] := StrToInt(edt49.Text);
  sbuf[24] := StrToInt(edt50.Text) div 256;
  sbuf[25] := StrToInt(edt50.Text);
  sbuf[26] := StrToInt(edt51.Text) div 256;
  sbuf[27] := StrToInt(edt51.Text);

  tempstr := Copy(edt144.text, 0, 4);
  Move(tempstr[1], sbuf[28], 4);
  if tempstr = '' then begin
    sbuf[28] := byte('0');
    sbuf[29] := byte('0');
    sbuf[30] := byte('0');
    sbuf[31] := byte('0');
  end;
  if (VIRLCROWS < 60) then begin
    temp8 := sbuf[28];
    sbuf[28] := sbuf[29];
    sbuf[29] := temp8;
    temp8 := sbuf[30];
    sbuf[30] := sbuf[31];
    sbuf[31] := temp8;
  end;
  for i := 10 to 15 do begin
    sbuf[12 + 2 * i] := 0;
    sbuf[13 + 2 * i] := 0;
  end;
  sbuf[33] := 100;     //比例
end;

procedure TForm_main.btn46Click(Sender: TObject);
begin
  modbusfun16int := $10A0;
  modbusfun16len := $10;
  sbuf[2] := $03;
  sbuf[3] := $10;
  sbuf[4] := $A0;
  sbuf[5] := $00;
  sbuf[6] := $18;
end;

procedure TForm_main.ServerSocket1ClientRead(Sender: TObject; Socket: TCustomWinSocket);
var
  i, len: Integer;
  funcode, ress: Byte;
  buf: array of Byte;
  RStr, ss: string;
  strs: TStrings;
begin
  RStr := Socket.ReceiveText;
  ss := 'Server接收';
{  if chk_TcpHex.Checked then
  begin
    //Mmo9.Text := Mmo9.Text +'接收：';   随着接收数据增大，运行会变得很慢
    SetLength(buf, Length(RStr));
    for i := 1 to Length(RStr) do
    begin
      buf[i - 1] := Ord(RStr[i]);
      if  (Btn48.Caption = '停止')and(edt53.hint='1') then rbuf[i]:=buf[i-1];
      ss:=ss+IntToHex(buf[i - 1], 2) + ' ';
      //Mmo9.Text := Mmo9.Text + IntToHex(buf[i - 1], 2) + ' ';   随着接收数据增大，运行会变得很慢
    end;
    mmo9.Lines.Add(ss);
  end else    //将从客户端读取的信息添加到Memo1中
    mmo9.Lines.Add(RStr);//.Text := Mmo9.Text + RStr;


  if C6.Checked then begin                                                      //本地Lan模拟演示
     if byte(RStr[1])>1 then begin                //连接2返回数据
       RStr[1]:=Char(1);
       ServerSocket1.Socket.Connections[0].Sendtext(RStr);
     end else
       ServerSocket1.Socket.Connections[1].Sendtext(RStr);
  end else if L7.Checked then begin                                                      //Lan模拟演示
     len:=SimulateReply();
     ServerSocket1.Socket.Connections[0].SendBuf(rbuf,Len);
  end else if not N48.Checked and (Btn48.Caption = '停止')and (edt53.hint='1') then  //非云在线调试
  begin
    rxdprocess(Length(RStr),0);      //局域网在线
    if N58.Checked then
      usb_busyT:=10+N58.Tag
    else
      usb_busyT:=10 div 5;
  end else }
  {if(edt53.hint='9050')or(edt53.hint='9051')or(edt53.hint='9052')or(edt53.hint='9053')or(edt53.hint='9054')
  then begin    //条码读取
    //edit1.Text:=RStr;

    strs:=TStringList.Create;
    strs.Delimiter:=',';
    strs.CommaText:=RStr;
    if (lbl177.Caption='工位1 ：')then begin         // 四工位系统第1工位  strs.Count=4
      edit1.Text:=strs[0];
    end else //if strs.Count=1 then          //通用系统，或四工位系统其它工位；
      edit1.Text:=RStr;
    if (edt53.hint='9050')and(lbl177.Caption='工位1 ：') then begin         //发送给其它4个工位
       idpclnt1.Active:=true;
       if strs.Count=4 then begin
         idpclnt1.Send(Edt100.Text, 9051, strs[0]);    //udp发送  ，实际不用
         idpclnt1.Send(Edt100.Text, 9052, strs[1]);    //udp发送
         idpclnt1.Send(Edt100.Text, 9053, strs[2]);    //udp发送
         idpclnt1.Send(Edt100.Text, 9054, strs[3]);    //udp发送
       end else begin
         idpclnt1.Send(Edt100.Text, 9051, edit1.Text);    //udp发送  ，实际不用
         idpclnt1.Send(Edt100.Text, 9052, edit1.Text);    //udp发送
         idpclnt1.Send(Edt100.Text, 9053, edit1.Text);    //udp发送
         idpclnt1.Send(Edt100.Text, 9054, edit1.Text);    //udp发送
       end;
    end;
    strs.Destroy;
    if ( (Readchis('result','UDPBarcodeStart')='1' )or(Readchis('result','UDPBarcodeStart')='ON' ) )               //用收到UDP条码来启动测试
      and(Trim(edit1.Text)<>'')    //条码不为空
    then  begin
       modbusfun05:=16; modbusfun05dat:=$FF;
    end;
  end;   }
end;

procedure TForm_main.ClientSocket1Read(Sender: TObject; Socket: TCustomWinSocket);
var
  i: Integer;
  buf: array of Byte;
  RStr, ss: string;
begin

end;

procedure TForm_main.chk7Click(Sender: TObject);
begin
  SysExplainNum := 6; //-1; btn141Click(Self);
  modbusfun05 := 37;
  if chk7.Checked then
    modbusfun05dat := $FF
  else
    modbusfun05dat := 0;
end;

procedure TForm_main.chk8Click(Sender: TObject);
begin
  if not chk8.Focused then
    Exit;
  SysExplainNum := 7; //-1; btn141Click(Self);
  if btn14.Showing and btn14.Enabled then
    btn14.SetFocus;
  modbusfun05 := 38;
  if chk8.Checked then
    modbusfun05dat := $FF
  else
    modbusfun05dat := 0;

end;

procedure TForm_main.chk9Click(Sender: TObject);
begin
  SysExplainNum := 8; //btn141Click(Self);
  modbusfun05 := 39;
  if chk9.Checked then
    modbusfun05dat := $FF
  else
    modbusfun05dat := 0;

end;

procedure TForm_main.N11Click(Sender: TObject);
begin
  form_main.Close;
end;

procedure TForm_main.N8Click(Sender: TObject);
begin
  modbusfun05 := 1;
  modbusfun05dat := $FF;
  formfind.showmodal;
end;

procedure TForm_main.N20Click(Sender: TObject);
begin
  modbusfun05 := 2;
  modbusfun05dat := $FF;
  formOS.showmodal;
end;

procedure TForm_main.Panel1MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
 // if(Panel1.VertScrollBar.Position   +  strtoint(form_main.edt121.Text) )< (VIRLCROWS *strtoint(form_main.edt121.Text) ) then
  Panel1.VertScrollBar.Position := Panel1.VertScrollBar.Position + strtoint(form_main.edt121.Text); // 10;
end;

procedure TForm_main.Panel1MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Panel1.VertScrollBar.Position >= strtoint(form_main.edt121.Text) then
    Panel1.VertScrollBar.Position := Panel1.VertScrollBar.Position - strtoint(form_main.edt121.Text); // 10;
end;

procedure TForm_main.N21Click(Sender: TObject);
begin
  ts3.TabVisible := True;
  if ts18.showing then            //强制从ts18.showing退出，否则HID不能发生读波形
    ts20.Show;
  PageControl1.TabIndex := 3;
  formwave.showmodal;
end;

procedure TForm_main.N3Click(Sender: TObject);
var
  str: string;
begin
  if lbl79.Color = clGray then        //直接可切换到monitor
  begin
    TabSheet2.TabVisible := True;
    PageControl1.TabIndex := 0;   //监控界面
  end
  else if InputQuery('Goto Monitor Window!', 'Pls Enter Password', str) then begin
    if (str = '888') or (str = '137760') then begin
      Showmessage('Monitor Window Open!');

      TabSheet2.TabVisible := True;
      PageControl1.TabIndex := 0;   //监控界面

      TabSheet4.TabVisible := False;      //关闭debug、USB和相关菜单项目
      ts3.TabVisible := False;
      TabSheet4.Enabled := False;
      N9.Enabled := False;
      N10.Enabled := False;
      N50.Enabled := False;
      N51.Enabled := False;
      A1.Enabled := False;
    end
    else begin
      Showmessage('Enter Error！');
    end;
  end;
end;

procedure TForm_main.btn9Click(Sender: TObject);
begin
  modbusfun05 := 16;
  modbusfun05dat := $FF;

end;

procedure TForm_main.btn10Click(Sender: TObject);
begin
  modbusfun05 := 17;
  modbusfun05dat := $FF;

end;

function TForm_main.UpServerOneLog(SLINE_head: string): Boolean;     //上传一条记录，返回是否成功！
var
  i: integer;
  strs: TStrings;
begin
  Result := False;
  strs := TStringList.Create;
  strs.Delimiter := #9;               //  strs.QuoteChar := ' ';
  strs.DelimitedText := SLINE_head;   //查分不完全正确。连续的空格认识是TAB

end;

procedure TForm_main.D1Click(Sender: TObject);
begin
  formstepCopy.btn1.Visible := True;
  formstepCopy.btn2.Visible := False;
  formstepCopy.showmodal;
end;

procedure TForm_main.N27Click(Sender: TObject);
begin
  modbusfun05 := 1;
  modbusfun05dat := $FF;
  formfind.showmodal;
end;

procedure TForm_main.H1Click(Sender: TObject);
begin
  modbusfun06dat := StrToInt('0' + edt45.Text);    //9999交换高低点2；
  modbusfun06 := $2F20;
  addToLog('交换第' + edt45.Text + '步骤的高低点');

end;

procedure TForm_main.S3Click(Sender: TObject);
begin
  modbusfun06dat := StrToInt('0' + edt45.Text);
  modbusfun06 := $2F21;
  addToLog('删略第' + edt45.Text + '步骤的序号');
end;

procedure TForm_main.N31Click(Sender: TObject);
var
  str: string;
begin
  str := N3.Hint;
  if InputQuery('Pls Enter 4 char!查询名称（大写）', 'abc', str) then begin
    N3.Hint := str;
    if (VIRLCROWS < 600) then begin
      modbusfun16str := AnsiUpperCase(str);
    end
    else
      modbusfun16str := str;
    modbusfun16len := 2;
    modbusfun16 := $2F40;
  end;
end;

procedure TForm_main.P2Click(Sender: TObject);
var
  str: string;
begin
  str := P2.Hint;
  if InputQuery('Enter Int!查询测试点所在步骤', '1234', str) then begin
    P2.Hint := str;
    if str = '' then
      str := '0';
    if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text) >= 768 + 1 + 3328) then begin      //大于4095，点数可翻倍！
      if StrToInt(str) > MAXPINS * 2 then
        modbusfun06dat := MAXPINS * 2
      else
        modbusfun06dat := StrToInt(str);
    end
    else if StrToInt(str) > MAXPINS then
      modbusfun06dat := MAXPINS     //最大第4094步
    else
      modbusfun06dat := StrToInt(str);
    modbusfun06 := $2F41;
  end;
end;

procedure TForm_main.O1Click(Sender: TObject);
begin
  modbusfun06dat := StrToInt('0' + edt45.Text);
  if modbusfun06dat > 0 then
    modbusfun06dat := modbusfun06dat - 1;
  modbusfun06 := $2F60;
end;

procedure TForm_main.P1Click(Sender: TObject);
begin
  modbusfun06dat := StrToInt('0' + edt45.Text);
  if modbusfun06dat > 0 then
    modbusfun06dat := modbusfun06dat - 1;
  modbusfun06 := $2F61;
end;

procedure TForm_main.S4Click(Sender: TObject);
var
  str: string;
begin
  str := S4.Hint;
  if InputQuery('Enter Int!查询步骤序号', '1234', str) then begin
    S4.Hint := str;
    if str = '' then
      str := '0';
    if StrToInt(str) > 4980 then
      modbusfun06dat := 4980     //最大第980步
    else
      modbusfun06dat := StrToInt(str);
    modbusfun06 := $2F42;   //等同 10B0
  end;
end;

procedure TForm_main.B1Click(Sender: TObject);
begin

  if form_main.mmo24.tag >= 0 then begin
    formModfyNstep.edt1.Value := form_main.mmo24.tag + 1;
    formModfyNstep.edt2.Value := form_main.mmo22.tag + 1;

    if form_main.mmo25.tag >= 0 then begin
      if (form_main.mmo25.tag <= 0) and (form_main.mmo23.tag >= 0) then
        formModfyNstep.chk1.Checked := True
      else
        formModfyNstep.chk1.Checked := false;
      if (form_main.mmo25.tag <= 1) and (form_main.mmo23.tag >= 1) then
        formModfyNstep.chk2.Checked := True
      else
        formModfyNstep.chk2.Checked := false;
      if (form_main.mmo25.tag <= 2) and (form_main.mmo23.tag >= 2) then
        formModfyNstep.chk3.Checked := True
      else
        formModfyNstep.chk3.Checked := false;
     //  if(form_main.mmo25.tag<=3)and(form_main.mmo23.tag>=3) then formModfyNstep.chk1.Checked:=True else formModfyNstep.chk1.Checked:=false;
      if (form_main.mmo25.tag <= 4) and (form_main.mmo23.tag >= 4) then
        formModfyNstep.chk4.Checked := True
      else
        formModfyNstep.chk4.Checked := false;
      if (form_main.mmo25.tag <= 5) and (form_main.mmo23.tag >= 5) then
        formModfyNstep.chk5.Checked := True
      else
        formModfyNstep.chk5.Checked := false;
      if (form_main.mmo25.tag <= 6) and (form_main.mmo23.tag >= 6) then
        formModfyNstep.chk6.Checked := True
      else
        formModfyNstep.chk6.Checked := false;
      if (form_main.mmo25.tag <= 7) and (form_main.mmo23.tag >= 7) then
        formModfyNstep.chk7.Checked := True
      else
        formModfyNstep.chk7.Checked := false;
      if (form_main.mmo25.tag <= 8) and (form_main.mmo23.tag >= 8) then
        formModfyNstep.chk8.Checked := True
      else
        formModfyNstep.chk8.Checked := false;
      if (form_main.mmo25.tag <= 9) and (form_main.mmo23.tag >= 9) then
        formModfyNstep.chk9.Checked := True
      else
        formModfyNstep.chk9.Checked := false;
      if (form_main.mmo25.tag <= 10) and (form_main.mmo23.tag >= 10) then
        formModfyNstep.chk10.Checked := True
      else
        formModfyNstep.chk10.Checked := false;
      if (form_main.mmo25.tag <= 11) and (form_main.mmo23.tag >= 11) then
        formModfyNstep.chk11.Checked := True
      else
        formModfyNstep.chk11.Checked := false;
      if (form_main.mmo25.tag <= 12) and (form_main.mmo23.tag >= 12) then
        formModfyNstep.chk12.Checked := True
      else
        formModfyNstep.chk12.Checked := false;
      if (form_main.mmo25.tag <= 13) and (form_main.mmo23.tag >= 13) then
        formModfyNstep.chk13.Checked := True
      else
        formModfyNstep.chk13.Checked := false;
      if (form_main.mmo25.tag <= 14) and (form_main.mmo23.tag >= 14) then
        formModfyNstep.chk14.Checked := True
      else
        formModfyNstep.chk14.Checked := false;
      if (form_main.mmo25.tag <= 15) and (form_main.mmo23.tag >= 15) then
        formModfyNstep.chk15.Checked := True
      else
        formModfyNstep.chk15.Checked := false;
    end;
  end;
  formModfyNstep.showmodal;
end;

procedure TForm_main.TabSheet4Hide(Sender: TObject);
begin
  if y1.Checked then
    y1Click(Self);     //code切换到调试界面
  N13.Visible := false;
  P6.Visible := False;
  S2.Visible := false;
  x1.Visible := false;
  N15.Visible := False;

  N15.Enabled := False;
  N13.Enabled := False;
  S2.Enabled := False;
  x1.Enabled := False;

  N9.Enabled := False;
  N10.Enabled := False;
  N50.Enabled := False;
  N51.Enabled := False;
  A1.Enabled := False;

  N18.Enabled := True;
end;

procedure TForm_main.ComboBox2Change(Sender: TObject);
begin
  cbb2.Text := combobox2.Text;
  Comm1.BaudRate := strtoint(combobox2.Text);
  btn1.Font.Color := clRed;

  SpeedButton12.Enabled := true;
  btn112.Enabled := true;

  form_main.Comm1.StopComm;
  shp2.Brush.Color := clSilver;
  shp4.Brush.Color := clSilver;
end;

procedure TForm_main.I2Click(Sender: TObject);
begin
  if IsNumberic(form_main.edt45.Text) then begin
    FormInsertN.edt1.Text := form_main.edt45.Text;
  end;
  if form_main.mmo24.tag >= 0 then begin
    FormInsertN.edt1.Text := IntToStr(form_main.mmo24.tag + 1);
    FormInsertN.edt2.Text := IntToStr(form_main.mmo22.tag - form_main.mmo24.tag + 1);
  end;
  FormInsertN.showmodal;
end;

procedure TForm_main.D5Click(Sender: TObject);
begin
  if IsNumberic(form_main.edt45.Text) then begin
    FormDeleteN.edt1.Text := form_main.edt45.Text;
    FormDeleteN.edt2.Text := form_main.edt45.Text;
  end;
  if form_main.mmo24.tag >= 0 then begin
    formDeleteN.edt1.Text := IntToStr(form_main.mmo24.tag + 1);    //起始...
    formDeleteN.edt2.Text := IntToStr(form_main.mmo22.tag + 1);
  end;

  formDeleteN.showmodal;
end;

procedure TForm_main.N33Click(Sender: TObject);
begin
  if form_main.mmo24.tag >= 0 then begin
    formMoveN.edt1.Text := IntToStr(form_main.mmo24.tag + 1);
    formMoveN.edt2.Text := IntToStr(form_main.mmo22.tag + 1);
  end;
  formMoveN.showmodal;
end;

procedure TForm_main.I3Click(Sender: TObject);
begin
  if IsNumberic(form_main.edt45.Text) then begin
    FormInsertCopyN.edt1.Text := IntToStr(StrToInt(form_main.edt45.Text) + 1);  //会被shift刷新！
  end;
  if form_main.mmo24.tag >= 0 then begin
    FormInsertCopyN.edt1.Text := IntToStr(form_main.mmo22.tag + 2);
    FormInsertCopyN.edt3.Text := IntToStr(form_main.mmo22.tag - form_main.mmo24.tag + 1);
  end;
  FormInsertCopyN.showmodal;
end;

procedure TForm_main.C2Click(Sender: TObject);
var
  str, password: string;
begin
  if readchis('Comm Para', 'PassWord') <> '' then begin    //不为空
    password := readchis('Comm Para', 'PassWord');
  end
  else
    password := '999';
  str := '0';
  if InputQuery('测试计数清零', '按"确定"清空计数,输入其它先请输入:密码+ok/ng     ', str) then begin
    if (str = password + 'ok') or (str = password + 'OK') then begin
      str := '0';
      if InputQuery('设置测试OK计数', '请输入...', str) then begin
        modbusfun16int := $2F65;
        modbusfun16len := $02;
        sbuf[2] := $10;
        sbuf[8] := StrToInt(str) shr 0;
        sbuf[9] := StrToInt(str) shr 8;
        sbuf[10] := StrToInt(str) shr 16;
        sbuf[11] := StrToInt(str) shr 24;
      end;
    end
    else if (str = password + 'ng') or (str = password + 'NG') then begin
      str := '0';
      if InputQuery('设置测试NG计数', '请输入...', str) then begin
        modbusfun16int := $2F66;
        modbusfun16len := $02;
        sbuf[2] := $10;
        sbuf[8] := StrToInt(str) shr 0;
        sbuf[9] := StrToInt(str) shr 8;
        sbuf[10] := StrToInt(str) shr 16;
        sbuf[11] := StrToInt(str) shr 24;
      end;
    end
    else if (str = '') or (str = '0') then begin
      modbusfun06dat := 0;
      modbusfun06 := $2F63;
    end
    else begin
      Showmessage('密码错误，禁止设置计数！');
    end;
  end;
end;

procedure TForm_main.N38Click(Sender: TObject);
begin
  if pnl1.Height = 188 then begin  //btn120.Caption='X'当前开启则关闭    pgc1.Visible then begin
    pnl1.Height := 22;
    btn120.Caption := '^^显示';
    N38.Caption := 'Open SysWin 开启系统参数显示';
  end
  else begin
    pnl1.Height := 188;
    btn120.Caption := 'X 隐藏';
    N38.Caption := 'Close SysWin 关闭系统参数显示';
  end;
end;

procedure TForm_main.btn55Click(Sender: TObject);
var
  Device: array[0..255] of char;
  Driver: array[0..255] of char;
  Port: array[0..255] of char;
  hDMode: THandle;
  PDMode: PDEVMODE;
begin
//设置打印机
  try
    Printer.PrinterIndex := Printer.PrinterIndex;
    Printer.GetPrinter(Device, Driver, Port, hDMode);
    if hDMode <> 0 then begin
      PDMode := GlobalLock(hDMode);
      if PDMode <> nil then begin
        //设定自定义纸张
        PDMode^.dmFields := PDMode^.dmFields or dm_PaperSize or DM_PAPERWIDTH or DM_PAPERLENGTH;

        PDMode^.dmPaperSize := 0;
        PDMode^.DMPAPERWIDTH := 40; //width*10 ;
        PDMode^.DMPAPERLENGTH := 30; //height*10;

        GlobalUnlock(hDMode);
      end;
    end;
    Printer.PrinterIndex := Printer.PrinterIndex;
  except
    showmessage('没有默认选择打印机，或默认打印机不可使！');
    exit;
  end;
end;

procedure TForm_main.edit1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin

  if Key = $0D then begin
    if (Readchis('result', 'BarcodeEndKey') <> '') and IsNumberic(Readchis('result', 'BarcodeEndKey')) then begin
      keybd_event(StrToInt(Readchis('result', 'BarcodeEndKey')), 0, 0, 0);  //最后一个字符替代，用于区分条码！
    //up事件也会输入字符  keybd_event(StrToInt(Readchis('result','BarcodeEndKey')),KEYEVENTF_KEYUP,0,0);
    //　　　　　　　　　　　　　　　　　　键盘键与虚拟键码对照表
// 　　字母和数字键　　　　 数字小键盘的键　　　　 　　功能键 　　　　　　　　其它键
//　　　键　　 键码　　　　　键　　 键码　　　　　　 键　　 键码 　　　　键　　　　　　键码
//　　　A　　　65　　　　　　 0 　　96 　　　　　　　F1 　　112 　　　　Backspace 　　　8
//　　　　B　　　66　　　　　　 1　　 97 　　　　　　　F2 　　113　　　　 Tab 　　　　　　9
// 　　　C　　　67 　　　　　　2 　　98 　　　　　　　F3 　　114　　　 　Clear 　　　　　12
//　　　　D　　　68　　　　　　 3　　 99 　　　　　　　F4 　　115　　　 　Enter 　　　　　13
//　　　　E　　　69 　　　　　　4 　　100　　　　　　　F5 　　116　　　 　Shift　　　　　 16
//　　　　F　　　70 　　　　　　5 　　101　　　　　　　F6 　　117　　　 　Control 　　　　17
// 　　　G　　　71 　　　　　　6　　 102　　　　　　　F7 　　118 　　 　 Alt 　　　　　　18
//　　　　H　　　72 　　　　　　7 　　103　　　　　　　F8 　　119　　　 　Caps Lock 　　　20
//　　　　I　　　73 　　　　　　8 　　104　　　　　　　F9 　　120　　　 　Esc 　　　　　　27
//　　　　J　　　74 　　　　　　9　　 105　　　　　　　F10　　121　　　 　Spacebar　　　　32
// 　　　K　　　75 　　　　　　* 　　106　　　　　　　F11　　122　　　 　Page Up　　　　 33
//　　　　L　　　76 　　　　　　+ 　　107　　　　　　　F12　　123　　　 　Page Down 　　　34
//　　　　M　　　77 　　　　　　Enter 108　　　　　　　-- 　　--　　　　　End 　　　　　　35
//　　　　N　　　78 　　　　　　-　　 109　　　　　　　-- 　　-- 　　　 　Home　　　　　　36
// 　　　O　　　79 　　　　　　. 　　110　　　　　　　--　　 -- 　　 　　Left Arrow　　　37
//　　　　P　　　80 　　　　　　/ 　　111　　　　　　　--　　 -- 　　 　　Up Arrow　　　　38
//　　　　Q　　　81 　　　　　　-- 　　--　　　　　　　--　　 -- 　　 　　Right Arrow 　　39
//　　　　R　　　82 　　　　　　-- 　　--　　　　　　　--　　 -- 　　 　　Down Arrow 　　 40
// 　　　S　　　83 　　　　　　-- 　　--　　　　　　　-- 　　-- 　　 　　Insert 　　　　 45
//　　　　T　　　84 　　　　　　-- 　　--　　　　　　　--　　 -- 　　 　　Delete 　　　　 46
//　　　　U　　　85 　　　　　　-- 　　--　　　　　　　-- 　　-- 　　 　　Help 　　　　　 47
//　　　　V　　　86 　　　　　　--　　 --　　　　　　　-- 　　-- 　　 　　Num Lock 　　　 144
// 　　　W　　　87 　　　　　　　　　
//　　　　X　　　88 　　　　　
//　　　　Y　　　89 　　　　　
//　　　　Z　　　90 　　　　　
// 　　　0　　　48 　　　　　
//　　　　1　　　49 　　　　　
//　　　　2　　　50 　　　　　　
//　　　　3　　　51 　　　　　　
// 　　　4　　　52 　　　　　　
//　　　　5　　　53 　　　　　　
//　　　　6　　　54 　　　　　　
//　　　　7　　　55 　　　　　　
// 　　　8　　　56 　　　　　　
//　　　　9　　　57
    end;
    if Readchis('result', 'TipCodeEnd') <> '' then
      MessageBox(form_main.Handle, PAnsiChar(AnsiString(Readchis('result', 'TipCodeEnd'))), 'TipCodeEnd', MB_ICONINFORMATION + MB_OkCancel);

    if (Readchis('result', 'BarcodeStart') = '1')               //用条码中的回车字符来启动测试
      //and ( Length(Trim(edit1.Text))>=StrToInt(edt55.Text) )  //大于最小长度
      and (Trim(edit1.Text) <> '')    //条码不为空
      then begin
      modbusfun05 := 16;
      modbusfun05dat := $FF;
    end;
    if (Label7.hint = '11')               //双产品测试       //and( Length(Trim(edit1.Text))>=StrToInt(edt55.Text) )  //大于最小长度
      and (Trim(edit1.Text) <> '')    //条码不为空
      then begin
      if (TabSheet2.Showing) and medit1.Enabled and grp36.Visible then
        medit1.SetFocus;
    end;
  {取消  if (Readchis('Model Para','BarCodeEdit')<>'')and(Readchis('Server','MESprogram')='')and(Readchis('Server','MESprogramdefault')='') then begin     //丽清启用MES，输入密码或者导入导出结束，按ENTER会条码报警；
      if Length(Trim(edit1.Text))<>StrToInt(edt7.Text) then begin       //目的是什么？？？ 手动输入提示长度吗？
        edit1.Color:=clRed;
        edit1.SelectAll;
        ts8.Show;
        edt7.Color:=clRed;
      end else begin
        edit1.Color:=clWindow;
        edt7.Color:=clWindow;
        edit1.Enabled:=False;                        
      end;
    end;   }
    if readchis('Comm Para', 'barcodeSelT') = 'Enter' then begin
      if TabSheet2.TabVisible and grp36.Visible then begin
        if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
          edit1.SetFocus;
        edit1.SelectAll;
      end;
    end;
  end;
  if readchis('Comm Para', 'barcodeSelT') <> 'Enter' then
    barcodeSelT := 2 * strtoint(edt76.Text);
end;

procedure TForm_main.btn56Click(Sender: TObject);
var
  str, password: string;
begin
  if (Readchis('Server', 'MESprogram') <> '') then begin
    if readchis('Comm Para', 'PassWord') <> '' then begin    //不为空
      password := readchis('Comm Para', 'PassWord');
    end
    else
      password := '999';

    if InputQuery('上传测试程序到MES系统', '请输入密码', str) then begin
      if (str = password) or (str = '137760') then begin
        FormLoad.Caption := '导出测试程序，并上传MES系统';
        FormLoad.btn3.Visible := False;
        FormLoad.btn4.Visible := true;
        FormLoad.btn4.Enabled := false;
        FormLoad.lv1.Visible := False;
        formLOad.ShowModal;
      end
      else begin
        Showmessage('密码错误，禁止上传！');
      end;
      if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
        edit1.SetFocus;
    end;
  end
  else if (Readchis('Server', 'MESprogramdefault') <> '') then begin
    Showmessage('MES脱机状态，不能上传！请先切换到MES联机');
  end
  else
    formPN.show;
end;

procedure TForm_main.tmr6Timer(Sender: TObject);
begin
  btn91Click(Self);
end;

procedure TForm_main.ts1Show(Sender: TObject);
begin
  if Copy(stat1.Panels[4].Text, 1, 7) <> 'Succuss' then
    stat1.Panels[4].Text := '-';
end;

procedure TForm_main.btn61Click(Sender: TObject);
begin
  Close;
end;

procedure TForm_main.btn59Click(Sender: TObject);
begin
  modbusfun05 := 17;
  modbusfun05dat := $FF;
end;

procedure TForm_main.btn62Click(Sender: TObject);
begin

  comandbusy := 220 div 5;
end;

procedure TForm_main.j1Click(Sender: TObject);
begin
  C2Click(Self);
end;

procedure TForm_main.N19Click(Sender: TObject);
begin
  begin
    if grp17.Visible then begin
      grp17.Visible := False;
      lbl169.Left := 0;
      Label11.Left := 8;
      N19.Caption := 'Yield on 良率显示';
    end
    else begin
      grp17.Visible := True;
      lbl169.Left := 120;
      Label11.Left := 128;
      N19.Caption := 'Yield off 关闭良率显示';
    end;
  end;
end;

procedure TForm_main.L2Click(Sender: TObject);
begin
{按钮已禁止  if grp28.Visible=True then begin
    grp28.Visible:=False;
    if not grp3.Visible then
      pnl1.Visible:=false;
    L2.Caption:='Open SysButton 显示系统按钮';
  end  else begin
    grp28.Visible:=true;
    pnl1.Visible:=true;
    L2.Caption:='Close SysButton 关闭系统按钮';
  end;   }
end;

procedure TForm_main.mmo13DblClick(Sender: TObject);
begin
  modbusfun06dat := $00F3;
  modbusfun06 := $0001;
end;

procedure TForm_main.mmo14DblClick(Sender: TObject);
begin
  modbusfun06dat := $00F4;
  modbusfun06 := $0001;
end;

procedure TForm_main.N42Click(Sender: TObject);
var
  i: SmallInt;
begin
  modbusfun16int := $2F00;
  modbusfun16len := $12;

  sbuf[2] := $10;

  sbuf[8] := 0;
  sbuf[9] := 0; //序号
  sbuf[10] := byte(' ');
  sbuf[11] := byte('0');
  sbuf[12] := byte(' ');
  sbuf[13] := byte(' ');
  for i := 2 to 8 do begin
    sbuf[10 + 2 * i] := 0;
    sbuf[11 + 2 * i] := 0;
  end;
  sbuf[22] := StrToInt(edt49.Text) div 256;
  sbuf[23] := StrToInt(edt49.Text);
  sbuf[28] := byte('Q');
  sbuf[29] := byte('1');
  sbuf[30] := byte('0');
  sbuf[31] := byte('0');
  for i := 10 to 15 do begin
    sbuf[12 + 2 * i] := 0;
    sbuf[13 + 2 * i] := 0;
  end;
  sbuf[33] := 100;     //比例
end;

procedure TForm_main.N43Click(Sender: TObject);
var
  i: SmallInt;
begin
  modbusfun16int := $2F00;
  modbusfun16len := $12;

  sbuf[2] := $10;

  sbuf[8] := 0;
  sbuf[9] := 0; //序号
  sbuf[10] := byte(' ');
  sbuf[11] := byte('0');
  sbuf[12] := byte(' ');
  sbuf[13] := byte(' ');
  for i := 2 to 8 do begin
    sbuf[10 + 2 * i] := 0;
    sbuf[11 + 2 * i] := 0;
  end;
  sbuf[22] := StrToInt(edt50.Text) div 256;
  sbuf[23] := StrToInt(edt50.Text);
  sbuf[28] := byte('Q');
  sbuf[29] := byte('1');
  sbuf[30] := byte('0');
  sbuf[31] := byte('0');
  for i := 10 to 15 do begin
    sbuf[12 + 2 * i] := 0;
    sbuf[13 + 2 * i] := 0;
  end;
  sbuf[33] := 100;     //比例
end;

procedure TForm_main.edt103Change(Sender: TObject);
begin
  btn1.Font.Color := clRed;
end;

procedure TForm_main.edt106Change(Sender: TObject);
begin
  btn1.Font.Color := clRed;

end;

procedure TForm_main.edt102Change(Sender: TObject);
begin
  btn1.Font.Color := clRed;

end;

procedure TForm_main.edt109Change(Sender: TObject);
begin
  btn1.Font.Color := clRed;

end;

procedure TForm_main.btn73Click(Sender: TObject);
begin
  modbusfun16int := $0041;
  modbusfun16len := 1;
  sbuf[2] := $03;
  sbuf[3] := $00;
  sbuf[4] := $00;
  sbuf[5] := $41;
  sbuf[6] := 1;

end;

procedure TForm_main.btn74Click(Sender: TObject);
var
  str: string;
begin
  if InputQuery('测试程序切换', '请输入1~24之间的一个数:', str) then begin
    if IsNumberic(str) then begin
      modbusfun06 := $0041;    //选择机种
      if (StrToInt(str) <= 240) and (StrToInt(str) > 0) then
        modbusfun06dat := StrToInt(str) - 1
      else
        modbusfun06dat := 0;
    end
    else begin
      Showmessage('输入非数字，不能切换！');
    end;
  end;

end;

procedure TForm_main.btn75Click(Sender: TObject);
begin
  edt111.Color := clWindow;
  TripCount := 0;
  lbl134.Caption := inttostr(tripcount);
  Label11.Font.Color := clWindowText;
  Label11.Caption := 'Ready';
  Label11.Font.Size := 150;
  Lbl169.Caption := '';
  lbl169.Font.Size := 10;
  Label11.Font.Color := clwindow;
end;

procedure TForm_main.medit1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = $0D then begin
    if (Label7.hint = '11')               //双产品测试     //and( Length(Trim(medit1.Text))>=StrToInt(edt55.Text) )  //大于最小长度
      and (Trim(medit1.Text) <> '')    //条码不为空
      then begin
      if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
        edit1.SetFocus;
    end;
  end;
  barcodeSelT := 2 * strtoint(edt76.Text);
end;

procedure TForm_main.btn77Click(Sender: TObject);
begin
  r1.Click;
//改成重做  modbusfun05:=29; modbusfun05dat:=$FF;
end;

procedure TForm_main.btn78Click(Sender: TObject);
begin
  C1.Click;
//改成撤销  modbusfun05:=30; modbusfun05dat:=$FF;
end;

procedure TForm_main.btn80Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 0;
end;

procedure TForm_main.btn81Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 1;

end;

procedure TForm_main.btn82Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 2;

end;

procedure TForm_main.btn83Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 3;

end;

procedure TForm_main.btn84Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 4;

end;

procedure TForm_main.btn85Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 5;

end;

procedure TForm_main.btn86Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 6;

end;

procedure TForm_main.btn87Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 7;
end;

procedure TForm_main.ComboBox1Change(Sender: TObject);
begin
  btn1.Font.Color := clRed;

  cbb3.Text := ComboBox1.Text;
  form_main.SpeedButton12.Enabled := true;
  btn112.Enabled := true;

  form_main.Comm1.StopComm;
  shp2.Brush.Color := clSilver;
  shp4.Brush.Color := clSilver;
end;

procedure TForm_main.C4Click(Sender: TObject);
begin
  formstepCopy.btn2.Visible := True;
  formstepCopy.btn1.Visible := False;
  formstepCopy.showmodal;
end;

procedure TForm_main.N34Click(Sender: TObject);
var
  str: string;
begin
  if N34.Checked then begin
    N34.Checked := False;
    TabSheet4.Caption := 'Debug调试';
    //  if Readchis('Comm Para','COMMdelay')<>'' then
    //  else
    edt14.Text := '0' + Readchis('Comm Para', 'COMMdelay');
    edt15.Text := '0';
  end
  else begin
    if N34.Hint = '' then
      str := '2001'
    else
      str := N34.Hint;
    if InputQuery('Import Window!', 'Pls Enter MachineNum 请输入透传主板编号（串口号c+级联序号nn）', str) then begin
      if str = '0' then
        str := '3001';     //默认：串口3，有一级级联板；
      N34.Tag := strtoint(str);
      N34.Hint := str;
      N34.Checked := true;
      TabSheet4.Caption := 'Debug透传:' + str;    //透传调试，延时必须加长；

      edt14.Text := '20';    //透传通讯后，增加20  =0可以正常导入导出
      edt15.Text := '60';    //USB延时：透传9600时（50可以导入导出，40不能正常导入导出）,透传38400时（20可以导入导出，=0不能正常导入导出）
      btn12Click(Self);
    end
    else
      N34.Tag := 0;
  end;
end;

procedure TForm_main.N47Click(Sender: TObject);
begin
  modbusfun05 := 19;
  modbusfun05dat := $FF;
end;

procedure TForm_main.btn90Click(Sender: TObject);
begin
  mmo17.Clear;
end;

procedure TForm_main.btn91Click(Sender: TObject);       //调试助手：定时发送数据
var
  ii, i, j: SmallInt;
  temp, s, viewstring: string;
begin
  if shp4.Brush.Color <> clGreen then begin
    messagedlg('Open Comm Fail! 没有启动串口！', mterror, [mbyes], 0);
    Exit;
  end;
  fillchar(sbuf, sizeof(sbuf), 0);
  send_len := Length(mmo17.Text);

  if mmo17.Text <> cbb1.Items.Strings[0] then
    cbb1.Items.Insert(0, mmo17.Text);

  if rb1.Checked then begin         //16进制
    s := Trim(mmo17.Text);
    repeat
      i := pos(' ', s);
      j := length(s);
      if i > 0 then
        s := copy(s, 1, i - 1) + copy(s, i + 1, j - i);
    until i = 0;
    temp := s;

    send_len := Length(temp);
    send_len := (1 + send_len) div 2;
    hextobin(pchar(temp), pchar(@sbuf) + 0, send_len);      //中间有空格会导致发送错误？？？
  end;
  if rb2.Checked and (send_len > 0) then begin     //ASCII文本格式
    temp := mmo17.Text;
    for ii := 1 to send_len do
      sbuf[ii] := Byte(temp[ii]);
  end;

  try
    btn91.Tag := 1;     //单击发送标志
    if chk19.Checked then begin
      send_crcdata(send_len, 2);
      j := send_len + 2;
    end
    else begin
      send_crcdata(send_len, 0);
      j := send_len;
    end;
  except
    messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);
  end;
  if chk18.Checked then
    tmr6.Enabled := True;

 {在发送函数统一解析   viewstring:='';
    if rb5.Checked then begin                         //串口全解析时，此处也显示发送内容
      for i:=1 to j do  begin
         viewstring:=viewstring+inttohex(sbuf[i],2)+' ';       
      end;
      viewstring:='[Send:][HEX]'+viewstring;
      if form_main.chk38.Checked then
        viewstring:=GetTimemS+viewstring;
      form_main.Memo1.Lines.add(viewstring);      //串口调试则显示发送内容
    end;  }
  form_main.chk24.Tag := 10;                     //强制 显示解析数据
end;

procedure TForm_main.btn92Click(Sender: TObject);
begin
  memo1.Clear;
end;

procedure TForm_main.chk18Click(Sender: TObject);
begin
  tmr6.Enabled := false;
  tmr6.Interval := StrToInt(edt122.Text);
end;

procedure TForm_main.edt122Change(Sender: TObject);
begin
  if StrToInt(edt122.Text) >= 10 then
    tmr6.Interval := StrToInt(edt122.Text);

end;

procedure TForm_main.rb2Click(Sender: TObject);
var
  i, j: SmallInt;
  s, temp: string;
begin

  s := Trim(mmo17.Text);
  repeat
    i := pos(' ', s);
    j := length(s);
    if i > 0 then
      s := copy(s, 1, i - 1) + copy(s, i + 1, j - i);
  until i = 0;
  temp := s;

  send_len := Length(temp);
  send_len := (1 + send_len) div 2;   //字符个数
  hextobin(pchar(temp), pchar(@sbuf) + 0, send_len);      //中间有空格会导致发送错误

  s := '';
  for i := 1 to send_len do
    s := s + char(sbuf[i]);

  mmo17.Text := s;
  if rb1.Checked then
    mmo18.Enabled := true
  else
    mmo18.Enabled := False;
end;

procedure TForm_main.rb1Click(Sender: TObject);
var
  i: SmallInt;
  AStr, s: string;
begin
  AStr := mmo17.Text;
  for i := 1 to length(AStr) do begin    //ch:=AStr[i];
    s := s + IntToHex(Ord(AStr[i]), 2) + ' ';
  end;
  mmo17.Text := s;
  if rb1.Checked then
    mmo18.Enabled := true
  else
    mmo18.Enabled := False;
end;

procedure TForm_main.edt134Change(Sender: TObject);
begin
  // edt7.Text:=edt134.Text;
  btn1.Font.Color := clRed;
end;

procedure TForm_main.PrintClickProcess(len: Integer);
var
  prntext: system.text; // 将prntext 声明为一个在system 程序单元中定义的文本文件
//  line:Integer;
  ii: SmallInt;
  s: string;
begin
  with form_main do begin
    AssignPrn(prntext);   // 将prnsetup分配给打印机
    if (StrToInt(edt11.Text) div 1000) = 1 then        //强制 设置打印机为横向打印
      Printer.Orientation := poLandscape
    else if (StrToInt(edt11.Text) div 1000) = 2 then  //强制 设置打印机为纵向打印
      Printer.Orientation := poPortrait;
    Rewrite(prntext);       // 调用rewrite 函数，为输出打开已分配的文件
    if (StrToInt(edt75.Text) mod 1000) > 1 then begin
      Printer.Canvas.Font := mmo5.Font;     // 把当前Memo1的字体指定给打印对象的Canvas 的字体属性
      Printer.Canvas.Font.Size := StrToInt(edt75.Text) mod 1000;
    end;
    for ii := 1 to (StrToInt(edt11.Text) div 10) mod 10 do                //十位填写：空行
    begin
      Writeln(prntext, '--');
    end;
    if (readchis('result', 'PrintBlankLines') <> '') then begin
      for ii := 1 to StrToInt(readchis('result', 'PrintBlankLines')) do   //空行
      begin
        Writeln(prntext, ' .');
      end;
    end;

    if StrToInt(edt159.Text) > 0 then begin
      for ii := 0 to StrToInt(edt159.Text) - 1 do begin
        if (readchis('result', 'PrintFontSizeL' + inttostr(1 + ii)) <> '') and IsNumberic(readchis('result', 'PrintFontSizeL' + inttostr(1 + ii))) then begin
          Printer.Canvas.Font.Size := StrToInt(readchis('result', 'PrintFontSizeL' + inttostr(1 + ii))) mod 1000;
          Writeln(prntext, mmo5.lines[ii]);
          if (StrToInt(edt75.Text) mod 1000) > 1 then begin
            Printer.Canvas.Font.Size := StrToInt(edt75.Text) mod 1000;
          end;
        end
        else
          Writeln(prntext, mmo5.lines[ii]);
      end;
    end;
    System.Close(prntext);
  end;
end;

procedure TForm_main.btn98Click(Sender: TObject);
var
  str, password: string;
begin
  if readchis('Comm Para', 'ResetPassWord') <> '' then begin        //ResetPassWord不为空
    password := readchis('Comm Para', 'ResetPassWord');
  end
  else if readchis('Comm Para', 'PassWord') <> '' then begin    //PassWord不为空
    password := readchis('Comm Para', 'PassWord');
  end
  else
    password := '999';

  if ScanCodeInputQuery('Reset复位治具', '请输入密码', str) then begin
    if (str = password) or (str = '137760') then begin
      if (modbusFSM = $50) or (modbusFSM = $51) or (modbusFSM = $52) then
        modbusFSM := $53      //导入结束
      else if (modbusFSM = $40) or (modbusFSM = $41) or (modbusFSM = $42) then
        modbusFSM := $43 //导出结束
      else
        modbusfun05 := 17;
      modbusfun05dat := $FF;
    end
    else begin
      Showmessage('密码错误，不能复位！');
    end;
  end;

  if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
    edit1.SetFocus;

end;

procedure TForm_main.U3Click(Sender: TObject);
begin
  edit1.Enabled := true;
  if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
    edit1.SetFocus;
  edit1.SelectAll;
end;

procedure TForm_main.btn100Click(Sender: TObject);
var
  Dir: string;
begin
  Dir := '';
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    edt135.Text := Dir + '\';
    btn1.Font.Color := clRed;
  end;
end;

procedure TForm_main.btn125Click(Sender: TObject);
var
  Dir: string;
begin
  Dir := '';
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    edt157.Text := Dir + '\';
    btn1.Font.Color := clRed;
  end;
end;

procedure TForm_main.sbtbtn1Click(Sender: TObject);
var
  str, password: string;
begin
  if (Readchis('Server', 'MESprogram') <> '') or (Readchis('Server', 'MESprogramdefault') <> '') then begin
    if readchis('Comm Para', 'PassWord') <> '' then begin    //不为空
      password := readchis('Comm Para', 'PassWord');
    end
    else
      password := '999';

    if InputQuery('从MES加载测试程序', '手动加载测试程序,请输入密码----', str) then begin
      if (str = password) or (str = '137760') then begin
        FormLoad.Caption := '手动选择测试程序，并加载';
        FormLoad.btn3.Caption := '手动加载';
        FormLoad.btn3.Visible := true;
        FormLoad.btn4.Visible := False;
        FormLoad.btn3.Enabled := true;
        FormLoad.lv1.Visible := true;
        FormLoad.btn2.Enabled := true;
        formLoad.ShowModal;
      end
      else if (str = '') or (str = 'debug') or (str = 'local') then begin
        if (Readchis('Server', 'MESprogram') = '') then begin
          Showmessage('MES脱机状态，不能自动加载！请先切换到MES联机');
        end
        else begin
          if (str = 'debug') then begin
            chk29.Checked := True;
            chk29.Caption := 'debug';              //   FormLoad.edt1.Text:='debug';
            FormLoad.lv1.Clear;
            FormLoad.lbl4.Caption := 'debug查看映射盘路径';
          end
          else if (str = 'local') then begin
            chk29.Checked := True;
            chk29.Caption := 'local';              //   FormLoad.edt1.Text:='debug';
            FormLoad.lv1.Clear;
            FormLoad.lbl4.Caption := '本地路径模拟';
          end
          else
            chk29.Caption := '本地路径模拟';

          FormLoad.Caption := '从MES系统获取测试程序，并加载----';
          FormLoad.btn3.Visible := true;
          FormLoad.btn3.Caption := '从MES加载';
          FormLoad.btn3.Enabled := false;
          FormLoad.btn4.Visible := False;
          FormLoad.lv1.Visible := true;
          FormLoad.btn2.Enabled := False;
          formLoad.ShowModal;
        end;
      end
      else begin
        Showmessage('密码错误，禁止加载！');
      end;
      if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
        edit1.SetFocus;
    end;
  end
  else if form_main.edt82.Text = '99' then begin                          //jaguar等 启用PC存放程序  enPN   demo

    if readchis('Comm Para', 'LoadPassWord') <> '' then begin
      password := readchis('Comm Para', 'LoadPassWord');
      if InputQuery('Load加载程序', '请输入密码', str) then begin
        if (str = password) or (str = '137760') then begin

        end
        else begin
          Showmessage('密码错误，不能复位！');
          Exit;
        end;
      end;
    end;
    if ((label11.Caption <> 'Ready') and (label11.Caption <> 'PN错误')) or (working > 0) then begin    //没有通讯读到版本号或者工作忙，不执行
      btn26Click(form_main);              //加载之前先复位？
      tmr1Timer(Self);
      Application.ProcessMessages;       //定时发送复位命令
      Sleep(100);
      Application.ProcessMessages;       //接收复位命令回传
      Sleep(200);
      Application.ProcessMessages;        //接收复位后ready命令；
      Sleep(200);
      Application.ProcessMessages;        //接收复位后ready命令；
    end;
    formLOad.BorderStyle := bsNone;
    formLOad.btn3.Visible := false;    //加载按钮禁止！
    formLOad.edt4.Visible := true;     //可扫工单条码，加载程序；
    formLOad.ShowModal;
  end
  else begin
    formLOad.BorderStyle := bsSizeable;
    formLOad.ShowModal;
  end;
end;

procedure TForm_main.lv1AdvancedCustomDrawItem(Sender: TCustomListView; Item: TListItem; State: TCustomDrawState; Stage: TCustomDrawStage; var DefaultDraw: Boolean);
begin

  if ErrlistZu[Item.Index + 1] > 0 then
    Sender.Canvas.Brush.Color := clRed;
end;

procedure TForm_main.ts16Show(Sender: TObject);
begin
  modbusfun06dat := $00F3;
  modbusfun06 := $0001;
  if pnl1.Height = 22 then begin   //btn120.Caption<>'X'
    pnl1.Height := 188;
    btn120.Caption := 'X 隐藏';
    N38.Caption := 'Close SysWin 关闭系统参数显示';
  end;
  if (StrToInt('0' + sysedit[5].Text) < 600) then begin       //FCT5
    mmo1.Visible := false;
    btn19.Visible := false;
    btn22.Visible := false;
    grp23.Visible := false;
  end
  else begin
    mmo1.Visible := true;
    btn19.Visible := true;
    btn22.Visible := true;
    grp23.Visible := true;
  end;
end;

procedure TForm_main.ts15Show(Sender: TObject);
begin
  modbusfun06dat := $00F4;
  modbusfun06 := $0001;
  if pnl1.Height = 22 then begin    //btn120.Caption<>'X'
    pnl1.Height := 188;
    btn120.Caption := 'X 隐藏';
    N38.Caption := 'Close SysWin 关闭系统参数显示';
  end;
end;

procedure TForm_main.btn101Click(Sender: TObject);
var
  save_file_name: string;
  ResultList: TStringList;
  strs: TStrings;
  ii, jj, temp16: SmallInt;
begin
  if dlgOpen1.Execute then begin
    sprgrsbr2.Position := 0;
    btn101.Caption := 'Start Conver....开始转换';
    btn101.Font.Color := clRed;
    save_file_name := dlgOpen1.FileName;
    ResultList := TStringList.Create;
    try
      ResultList.LoadFromFile(save_file_name);
      sprgrsbr2.Max := ResultList.Count;
      for ii := 0 to ResultList.Count - 1 do begin
        sprgrsbr2.Position := sprgrsbr2.Position + 1;
        strs := TStringList.Create;
        strs.Delimiter := #9;
        strs.CommaText := ResultList[ii];
        if ii = 0 then begin
          temp16 := strs.Count;
        end
        else if (strs.Count > temp16) and (temp16 > 1) then begin
          ResultList[ii] := '';
          for jj := 0 to temp16 - 1 do begin
            ResultList[ii] := ResultList[ii] + strs[jj] + #9;  //Copy(ResultList[ii],0,Length(ResultList[ii])-length(StrS[strs.Count-1]));
          end;
        end;
        strs.Destroy;
      end;
      ResultList.SaveToFile(edt2.Text + ExtractFileName(dlgOpen1.FileName) + '-New.txt'); // copy(save_file_name,0,Length(save_file_name)-4)+'-New.txt'); //保存到原txt文件中
      ResultList.Free;
    except
      ShowMessage('Save Err! Pls Check the File is Close 数据Log文件被其它软件占用无法保存！');
    end;

    btn101.Caption := 'Conver Finished!转换结束 again?';
  end;
end;

procedure TForm_main.btn102Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 0;
end;

procedure TForm_main.btn103Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 1;
end;

procedure TForm_main.btn104Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 2;
end;

procedure TForm_main.btn105Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 3;
end;

procedure TForm_main.btn106Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 4;
end;

procedure TForm_main.btn107Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 5;
end;

procedure TForm_main.btn108Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 6;
end;

procedure TForm_main.btn109Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 7;
end;

procedure TForm_main.btn110Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 8;
end;

procedure TForm_main.btn111Click(Sender: TObject);
begin
  modbusfun06 := $0041;    //选择机种
  modbusfun06dat := 9;
end;

procedure TForm_main.cbb3Change(Sender: TObject);
begin
  btn1.Font.Color := clRed;

  ComboBox1.Text := cbb3.Text;
  form_main.SpeedButton12.Enabled := true;
  btn112.Enabled := true;

  form_main.Comm1.StopComm;
  shp2.Brush.Color := clSilver;
  shp4.Brush.Color := clSilver;
end;

procedure TForm_main.cbb2Change(Sender: TObject);
begin
  combobox2.Text := cbb2.Text;
  Comm1.BaudRate := strtoint(combobox2.Text);
  btn1.Font.Color := clRed;

  SpeedButton12.Enabled := true;
  btn112.Enabled := true;

  form_main.Comm1.StopComm;
  shp2.Brush.Color := clSilver;
  shp4.Brush.Color := clSilver;
end;

procedure TForm_main.mmo17KeyPress(Sender: TObject; var Key: Char);
var
  i, j: SmallInt;
  s, temp: string;
begin

end;

procedure TForm_main.mmo18KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  i: SmallInt;
  AStr, s: string;
begin

  if rb1.Checked then begin
    AStr := mmo18.Text;
    for i := 1 to length(AStr) do begin
        //ch:=AStr[i];
      s := s + IntToHex(Ord(AStr[i]), 2) + ' ';
    end;
    mmo17.Text := s;
  end;
end;

procedure TForm_main.mmo17Change(Sender: TObject);
var
  i, j: SmallInt;
  s, temp: string;
begin
  if rb1.Checked then begin

    if (Copy(mmo17.Text, Length(mmo17.Text), 1) <> ' ') and (Copy(mmo17.Text, Length(mmo17.Text) - 1, 1) <> ' ') and (Length(mmo17.Text) > 1) then begin
      mmo17.Text := mmo17.Text + ' ';
      mmo17.SelStart := Length(mmo17.Text);
    end;

    s := TrimLeft(mmo17.Text);
    repeat
      i := pos(' ', s);
      j := length(s);
      if i > 0 then
        s := copy(s, 1, i - 1) + copy(s, i + 1, j - i);
    until i = 0;
    temp := s;

    send_len := Length(temp);
    send_len := (1 + send_len) div 2;   //字符个数
    hextobin(pchar(temp), pchar(@sbuf) + 0, send_len);      //中间有空格会导致发送错误

    s := '';
    for i := 1 to send_len do
      s := s + char(sbuf[i]);

    mmo18.Text := s;
  end;
end;

procedure TForm_main.btn114Click(Sender: TObject);
begin
  Tmatrix.UploadJason;
end;

procedure TForm_main.btn115Click(Sender: TObject);
var
  IdHttp: TIdHTTP;
  Url: string; //请求地址
  ResponseStream: TStringStream; //返回信息
  ResponseStr: string;
  LTep: TGUID;
  sGUID: string;
begin

  CreateGUID(LTep);
  sGUID := GUIDToString(LTep);
  //mmo19.Lines.Add(sGUID);
  sGUID := StringReplace(sGUID, '-', '', [rfReplaceAll]);
  sGUID := Copy(sGUID, 2, Length(sGUID) - 2);
  btn115.Hint := LowerCase(sGUID);
  mmo19.Lines.Add('当前GUID=' + btn115.Hint);

end;

procedure TForm_main.btn116Click(Sender: TObject);
begin
  mmo19.Lines.Clear;
end;


//  正常測試時，上传产品条码、料号、工單、機臺編號給服務器接口，服務器接口檢查如下信息，如果不符合返回false，否則返回true。
//  A 條碼是否符合料號設置的二維碼規則
//  C 此料號是否有首件記錄
//  B 此料號、機臺是否在規定時間內完成標準版測試
//  Get_OK_BIAOZHUI（条码、料号、工單、機臺編號）數據類型：字符型
//  如果不符合返回false，否則返回true。
//GET /DC_STAND/WebService.asmx/Get_OK_BIAOZHUI?barcode=string&part_id=string&base_id=string&att_id=string
procedure TForm_main.btn118Click(Sender: TObject);
begin
  Tmatrix.BPcheckJason;
end;

//2從標準版模式到正常模式時，向服務器報告治具完成了標準版測試（紅色標記的欄位我認為不要傳給服務器，我會找IT改掉）
//Get_biaozhui(條碼、機臺編號、料號、工單、完成標準版測試的時間) 數據類型：字符型
//如果不成功返回false，否則成功返回true
//GET /DC_STAND/WebService.asmx/Get_biaozhui?part_id=string&base_id=string&att_id=string
procedure TForm_main.btn119Click(Sender: TObject);
var
  IdHttp: TIdHTTP;
  Url: string; //请求地址
  ResponseStream: TStringStream; //返回信息
  ResponseStr, stime: string;
begin
  //创建IDHTTP控件
  IdHttp := TIdHTTP.Create(nil);
  try //请求地址
    if (Readchis('Server', 'MESprogram') <> '') then begin     //MES校验MCU信息
      IdHttp.HandleRedirects := true;
      IdHttp.AllowCookies := true;

      IdHttp.ReadTimeout := 5000;                       //请求超时设置
      IdHttp.Request.ContentType := 'application/json'; //设置内容类型为json
      Url := 'http://' + edt141.Text + edt156.Text + edt155.Text;        //地址+ API命令       //TEncoding.Default;
      if chk30.Checked then begin
        ResponseStream := TStringStream.Create('{"line": "B-FCT", "station": "B-FCT-2", "sn": "P00091729H0013S00114T1100010L030W03", "mcu": { "pn": "23447700", "rl": "AB", "llc": "W1", "hv": "5.0", "sv": "S010", "ld": "10102019", "cs": "0x86498332" }}');
      end
      else begin
        ResponseStream := TStringStream.Create('{"line": "' + (AnsiToUtf8(edt80.Text)) + '","station": "' + (AnsiToUtf8(edt164.Text)) + '","sn": "' + (AnsiToUtf8(Trim(edit1.Text))) + '","mcu":{ "pn":"' + edt73.Text + '","rl": "' + edt83.Text + '","llc": "' + edt165.Text + '","hv": "' + edt177.Text + '","sv": "' + edt178.Text + '","ld": "' + edt179.Text + '","cs": "' + edt142.Text + '"}' + '}');
      end;
      ResponseStream.Position := 0;      //将流位置置为0
      mmo19.Lines.Add(Url);
      mmo19.Lines.Add(ResponseStream.DataString);
      try
        ResponseStr := IdHttp.Post(Url, ResponseStream);
      except
        on e: Exception do begin
          stat1.Panels[4].Text := e.Message;
        end;
      end;

      if (pos('"Result":"OK"', ResponseStr) > 0) then begin
        lbl164.Caption := 'OK';
      end
      else if (pos('"Result":"NG"', ResponseStr) > 0) then begin
        lbl164.Caption := 'NG';
      end
      else
        lbl164.Caption := 'error';

      ResponseStr := UTF8Decode(ResponseStr);        //网页中的存在中文时，需要进行UTF8解码        //不完整编码后的数据复制 会返回空，
      stat1.Panels[5].Text := Copy(ResponseStr, Pos('"Msg":"', ResponseStr), Pos(',"Data"', ResponseStr) - Pos('"Msg":"', ResponseStr));   //显示内容
      mmo19.Lines.Add(ResponseStr);
      btn119.Hint := ResponseStr;
    end
    else begin  //TStringStream对象用于保存响应信息
      ResponseStream := TStringStream.Create('');
      datetimetostring(stime, 'yyyy-mm-dd HH:nn:ss', Now);
      Url := 'http://' + edt141.Text + edt156.Text + edt155.Text + '?'  // '/DC_STAND/WebService.asmx/'
            //+'barcode='+edt136.Text
        + '&part_id=' + edt73.Text + '&base_id=' + edt83.Text + '&att_id=' + edt142.Text
           // +'&IS_DATE=' +stime
;
      mmo19.Lines.Add(Url);
      try
        IdHttp.Get(Url, ResponseStream);
      except
        on e: Exception do begin
          ShowMessage(e.Message);
        end;
      end;
      //获取网页返回的信息
      ResponseStr := ResponseStream.DataString;
      if (pos('>true<', ResponseStr) > 0) or (pos('>TRUE<', ResponseStr) > 0) or (pos('>True<', ResponseStr) > 0) then begin
        lbl164.Caption := 'true';
      end
      else if (pos('>false<', ResponseStr) > 0) or (pos('>FALSE<', ResponseStr) > 0) or (pos('>False<', ResponseStr) > 0) then begin
        lbl164.Caption := 'false';
      end
      else
        lbl164.Caption := 'error';
      //网页中的存在中文时，需要进行UTF8解码
      ResponseStr := UTF8Decode(ResponseStr);
      mmo19.Lines.Add(ResponseStr);
    end;
  finally
    IdHttp.Free;
    ResponseStream.Free;
  end;

end;

procedure TForm_main.btn120Click(Sender: TObject);
var
  i: SmallInt;
begin
  modbusfun16int := $2F00;
  modbusfun16len := $12;

  sbuf[2] := $10;

  sbuf[8] := 0;
  sbuf[9] := 50; //序号
  sbuf[10] := byte(' ');
  sbuf[11] := byte('0');
  sbuf[12] := byte(' ');
  sbuf[13] := byte(' ');
  for i := 2 to 8 do begin
    sbuf[10 + 2 * i] := 0;
    sbuf[11 + 2 * i] := 0;
  end;
  sbuf[22] := StrToInt(edt49.Text) div 256;
  sbuf[23] := StrToInt(edt49.Text);
  sbuf[24] := StrToInt(edt50.Text) div 256;
  sbuf[25] := StrToInt(edt50.Text);
  sbuf[26] := StrToInt(edt51.Text) div 256;
  sbuf[27] := StrToInt(edt51.Text);
  sbuf[28] := byte('Q');
  sbuf[29] := byte('7');
  sbuf[30] := byte('0');
  sbuf[31] := byte('0');
  for i := 10 to 15 do begin
    sbuf[12 + 2 * i] := 0;
    sbuf[13 + 2 * i] := 0;
  end;
  sbuf[33] := 100;     //比例
end;

procedure TForm_main.C5Click(Sender: TObject);    //调试菜单下开启串口调试
begin
  if groupbox1.hint <> '5' then begin        //FCT5定义主板不可控制commopen
    if GroupBox1.Visible then begin
      groupbox1.hint := '0';     //不启动软件打开串口
    //改成重做    btn77.Visible:=False;
    //改成撤销  btn78.Visible:=False;
      C5.Checked := False;
      GroupBox1.Visible := False;

      SpeedButton12.Enabled := true;
      SpeedButton13.Enabled := true;
      btn112.Enabled := true;
      btn113.Enabled := true;
      form_main.Comm1.StopComm;
      shp2.Brush.Color := clSilver;
      shp4.Brush.Color := clSilver;

    end
    else begin
      groupbox1.hint := '1';     //启动软件打开串口
      //触摸屏翻页命令btn77.Visible:=true;
      //btn78.Visible:=true;
      C5.Checked := true;
      GroupBox1.Visible := true;
    end;
  end;
end;

procedure TForm_main.btn127Click(Sender: TObject);
var
  s: string;

  function GetDosOutput(CommandLine: string): string;
  var
    SA: TSecurityAttributes;
    SI: TStartupInfo;
    PI: TProcessInformation;
    StdOutPipeRead, StdOutPipeWrite: THandle;
    WasOK: Boolean;
    Buffer: array[0..255] of AnsiChar;
    BytesRead: Cardinal;
    Handle: Boolean;
  begin
    Result := '';
    with SA do begin
      nLength := SizeOf(SA);
      bInheritHandle := True;
      lpSecurityDescriptor := nil;
    end;
    CreatePipe(StdOutPipeRead, StdOutPipeWrite, @SA, 0);
    try
      with SI do begin
        FillChar(SI, SizeOf(SI), 0);
        cb := SizeOf(SI);
        dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
        wShowWindow := SW_HIDE;
        hStdInput := GetStdHandle(STD_INPUT_HANDLE); //?don't?redirect?stdin
        hStdOutput := StdOutPipeWrite;
        hStdError := StdOutPipeWrite;
      end;
      Handle := CreateProcess(nil, PChar('cmd /c' + CommandLine), nil, nil, True, 0, nil, nil, SI, PI);
      CloseHandle(StdOutPipeWrite);
      if Handle then
      try
        repeat
          WasOK := ReadFile(StdOutPipeRead, Buffer, 255, BytesRead, nil);
          if BytesRead > 0 then begin
            Buffer[BytesRead] := #0;
            Result := Result + Buffer;
          end;
        until not WasOK or (BytesRead = 0);
        WaitForSingleObject(PI.hProcess, INFINITE);
      finally
        CloseHandle(PI.hThread);
        CloseHandle(PI.hProcess);
      end;
    finally
      CloseHandle(StdOutPipeRead);
    end;
  end;

begin
{  s:='cmd.exe /c '+edt160.Text+' >d:\1.txt';
  winexec(pchar(s),sw_hide);
  sleep(1000);
  mmo21.Lines.LoadFromFile('d:\1.txt');
 }
  mmo21.Text := GetDosOutput(edt160.Text + edt162.text);
end;

procedure TForm_main.btn128Click(Sender: TObject);

  function GetDosOutput(CommandLine: string): string;
  var
    SA: TSecurityAttributes;
    SI: TStartupInfo;
    PI: TProcessInformation;
    StdOutPipeRead, StdOutPipeWrite: THandle;
    WasOK: Boolean;
    Buffer: array[0..255] of AnsiChar;
    BytesRead: Cardinal;
    Handle: Boolean;
  begin
    Result := '';
    with SA do begin
      nLength := SizeOf(SA);
      bInheritHandle := True;
      lpSecurityDescriptor := nil;
    end;
    CreatePipe(StdOutPipeRead, StdOutPipeWrite, @SA, 0);
    try
      with SI do begin
        FillChar(SI, SizeOf(SI), 0);
        cb := SizeOf(SI);
        dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
        wShowWindow := SW_HIDE;
        hStdInput := GetStdHandle(STD_INPUT_HANDLE); //?don't?redirect?stdin
        hStdOutput := StdOutPipeWrite;
        hStdError := StdOutPipeWrite;
      end;
      Handle := CreateProcess(nil, PChar('cmd /c' + CommandLine), nil, nil, True, 0, nil, nil, SI, PI);
      CloseHandle(StdOutPipeWrite);
      if Handle then
      try
        repeat
          WasOK := ReadFile(StdOutPipeRead, Buffer, 255, BytesRead, nil);
          if BytesRead > 0 then begin
            Buffer[BytesRead] := #0;
            Result := Result + Buffer;
          end;
        until not WasOK or (BytesRead = 0);
        WaitForSingleObject(PI.hProcess, INFINITE);
      finally
        CloseHandle(PI.hThread);
        CloseHandle(PI.hProcess);
      end;
    finally
      CloseHandle(StdOutPipeRead);
    end;
  end;

begin
  mmo21.Text := GetDosOutput(edt161.Text);
end;

procedure TForm_main.btn129Click(Sender: TObject);
var
  IdHttp: TIdHTTP;
  Url: string; //请求地址
  ResponseStream: TStringStream; //返回信息
  ResponseStr: string;
begin
  //创建IDHTTP控件
  IdHttp := TIdHTTP.Create(nil);
  //测试错误代码 "IOhandler value is valid"     IdHttp.HandleRedirects := True;//允许头转向，不支持https://的地址？？？
  IdHttp.ReadTimeout := 5000; //请求超时设置
  IdHttp.Request.ContentType := 'application/json'; //设置内容类型为json

  //TStringStream对象用于保存响应信息
  ResponseStream := TStringStream.Create('{ "i": r_word,"from": "AUTO","to": "AUTO","smartresult": "dict","client": "fanyideskweb","salt": js_key["salt"],"sign":js_key["sign"],"doctype": "json","version": "2.1","keyfrom": "fanyi.web","action": "FY_BY_REALTIME","typoResult": "false" }');
  try
    ResponseStream.Position := 0;      //将流位置置为0
    //请求地址
    Url := 'http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl';
    Url := 'http://getman.cn/echo?'; //                                  //错误代码 "301 Moved Permanently"
    Url := 'http://fanyi.youdao.com/translate_o??smartresult=dict&smartresult=rule'; // //'http://dict.youdao.com/';
    mmo19.Lines.Add(Url);
    try
      lbl164.Caption := 'true';
      ResponseStr := IdHttp.post(Url, ResponseStream);
    except
      on e: Exception do begin
        lbl164.Caption := 'false';
        ShowMessage(e.Message);
      end;
    end;
    //获取网页返回的信息
    //ResponseStr := ResponseStream.DataString;
    //网页中的存在中文时，需要进行UTF8解码
    ResponseStr := UTF8Decode(ResponseStr);
    stat1.Panels[5].Text := Copy(ResponseStr, Pos('1.0"', ResponseStr) + 3, 50);

    mmo19.Lines.Add(Copy(ResponseStr, 0, 128));
  finally
    IdHttp.Free;
    ResponseStream.Free;
  end;

end;

procedure TForm_main.btn130Click(Sender: TObject);
var
  str, password: string;
//    nrf_dat_config: Tinifile;
//    ini_path: string;
begin
  if readchis('Comm Para', 'PassWord') <> '' then begin    //不为空
    password := readchis('Comm Para', 'PassWord');
  end
  else
    password := '999';
  if (Readchis('Server', 'MESprogram') <> '') or (Readchis('Server', 'MESprogramDefault') <> '') then begin    //丽清联机和脱机
    if InputQuery('MES联机切换', '请输入密码', str) then begin
      if (str = password) or (str = '137760') then begin
        if btn130.Font.Color = clGreen then begin
          btn130.Caption := 'MES脱机';
          btn130.Font.Color := clDefault;
          WriteChis('Server', 'MESprogram', '');
        end
        else begin
          btn130.Caption := 'MES联机';
          btn130.Font.Color := clGreen;
          str := Readchis('Server', 'MESprogramdefault');
          if str = '' then begin
            str := 'auto';
            WriteChis('Server', 'MESprogramdefault', str);
          end;
          WriteChis('Server', 'MESprogram', str);
        end;
      end
      else begin
        Showmessage('密码错误，不能切换！');
      end;
      if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
        edit1.SetFocus;
    end;
  end
  else begin     //jaguar用
    if InputQuery('MES联机切换', '请输入密码', str) then begin
      if (str = password) or (str = '137760') then begin
        if btn130.Font.Color = clGreen then begin
          btn130.Caption := 'MES脱机';
          btn130.Font.Color := clDefault;
          chk25.Visible := False;
          chk26.Visible := False;
          chk27.Visible := False;
          chk28.Visible := False;
          WriteChis('Server', 'MESfun', '');
        end
        else begin
          btn130.Caption := 'MES联机';
          btn130.Font.Color := clGreen;
          chk25.Visible := true;
          chk26.Visible := true;
          chk27.Visible := true;
          chk28.Visible := true;
          str := Readchis('Server', 'MESdefault');
          if str = '' then begin
            str := '24';
            WriteChis('Server', 'MESdefault', str);
          end;
          WriteChis('Server', 'MESfun', str);
        end;
      end
      else begin
        Showmessage('密码错误，不能切换！');
      end;
      ts7.Show;
      if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
        edit1.SetFocus;
    end;
  end;
end;

procedure TForm_main.btn132Click(Sender: TObject);
var
  str: string;
begin
  try
    Comm2.CommName := cbb4.Text;
    Comm2.BaudRate := strtoint(cbb5.Text);
    form_main.btn132.Enabled := false;       //runing...
    form_main.btn133.Enabled := true;
    form_main.Comm2.StopComm;
    sleep(5);
    form_main.Comm2.StartComm;
    sleep(5);
    shp6.Brush.Color := clGreen;
  except
    form_main.Comm2.StopComm;
    form_main.btn133.Enabled := false;
    form_main.btn132.Enabled := true;
    shp6.Brush.Color := clSilver;
    messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);
  end;
end;

procedure TForm_main.btn133Click(Sender: TObject);
begin
  form_main.btn133.Enabled := false;
  form_main.btn132.Enabled := true;
  form_main.Comm2.StopComm;
  shp6.Brush.Color := clSilver;
end;

procedure TForm_main.Comm2ReceiveData(Sender: TObject; Buffer: Pointer; BufferLength: Word);
var
  i: Integer;
  lanrbuf: array of Char;
begin
  SetLength(lanrbuf, BufferLength);
  move(Buffer^, lanrbuf[0], BufferLength);
  Mmo21.Text := '';
  for i := 0 to BufferLength - 1 do begin
    Mmo21.Text := Mmo21.Text + lanrbuf[i];
  end;
  edt163.Visible := True;
  if form_main.comm2.Tag = $C9 then begin
    modbusfun16 := $00C9;
    modbusfun16str := Mmo21.Text;
    modbusfun16len := 0;
  end;
end;

procedure TForm_main.btn134Click(Sender: TObject);
var
  str: string;
begin
  str := edt167.Text + char($0D);

  edt162.Text := Readchis(edt77.Text, 'sum');
  edt163.Text := '-';
  mmo21.Text := '*';
  form_main.comm2.writecommdata(PChar(str), Length(str));
end;

procedure TForm_main.cbb5Change(Sender: TObject);
begin
  btn1.Font.Color := clRed;
  form_main.Comm2.StopComm;
  shp6.Brush.Color := clSilver;
end;

procedure TForm_main.cbb4Change(Sender: TObject);
begin
  btn1.Font.Color := clRed;
  form_main.Comm2.StopComm;
  shp6.Brush.Color := clSilver;
end;

procedure TForm_main.btn135Click(Sender: TObject);
var
  str: string;
begin
  str := edt166.Text + char($0D);
  form_main.comm2.writecommdata(PChar(str), Length(str));

end;

procedure TForm_main.A1Click(Sender: TObject);
var
  str: string;
begin
  if (StrToInt('0' + sysedit[5].Text) = 0) or (working > 0) then      //没有读到版本号，不执行
  begin
    if (working > 0) then
      lbl28.Caption := 'MCU is busying, Pls press RESET!请按复位再导入'
    else
      lbl28.Caption := 'Pls retry to click!请重新上电，重启软件试一次！';
  end
  else begin         //允许导入
    if InputQuery('Import Window!', 'Pls Enter Password！请输入密码', str) then begin
      if (str = '888999') then begin
        str := '10';
        if InputQuery('Import Window!', 'Pls Enter MaxPN 请输入机种程序导入数N=', str) then begin
          N50.Tag := strtoint(str);
        end
        else
          N50.Tag := 0;
        A1.Tag := 0;
        A1.Checked := TRUE;
        N50.Checked := False;
        n51.Checked := false;
        modbusfun06 := $0041;    //选择机种
        modbusfun06dat := A1.Tag + $2000;         //btn23Click(Self);
      end;
    end;
  end;

end;

procedure TForm_main.Label7DblClick(Sender: TObject);
var
  str, password: string;
begin
  if readchis('Comm Para', 'hPassWord') <> '' then begin    //不为空
    password := readchis('Comm Para', 'hPassWord');
  end
  else
    password := '888999';

  if InputQuery('barcode功能启用 切换', '请输入密码', str) then begin

    if (str = password) or (str = '137760') then begin
      if Label7.Color = clGreen then begin
        Label7.Color := clGray;
        WriteChis('Comm Para', 'clearbarcode', '2');
        Label7.hint := '2';
      end
      else begin
        Label7.Color := clGreen;
        str := Readchis('DefaultPara', 'barcodeDefault');
        if (str = '2') or (str = '') then begin              //没有设置
          str := '1';
          WriteChis('DefaultPara', 'barcodeDefault', str);
        end;
        WriteChis('Comm Para', 'clearbarcode', str);
        Label7.hint := str;
      end;
    end
    else begin
      Showmessage('密码错误，不能切换！');
    end;

    if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
      edit1.SetFocus;
  end;

end;

procedure TForm_main.lbl79DblClick(Sender: TObject);
var
  str, password: string;
begin
  if readchis('Comm Para', 'hPassWord') <> '' then begin    //不为空
    password := readchis('Comm Para', 'hPassWord');
  end
  else
    password := '888999';

  if InputQuery('Debug功能启用 切换', '请输入密码', str) then begin

    if (str = password) or (str = '137760') then begin
      if lbl79.Color = clGreen then begin
        lbl79.Color := clGray;
        tabsheet4.TabVisible := True;
        ts3.TabVisible := True;
        WriteChis('Comm Para', 'DebugMode', '1');
        //  edt61.Text:='1';
      end
      else begin
        lbl79.Color := clGreen;
        tabsheet4.TabVisible := False;
        ts3.TabVisible := false;
        str := Readchis('DefaultPara', 'DebugModeDefault');
        if (str = '1') or (str = '') then begin              //没有设置
          str := '0';
          WriteChis('DefaultPara', 'DebugModeDefault', str);
        end;
        WriteChis('Comm Para', 'DebugMode', str);
        Label7.hint := str;
      end;
    end
    else begin
      Showmessage('密码错误，不能切换！');
    end;

    if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
      edit1.SetFocus;
  end;
end;

procedure TForm_main.chk32Click(Sender: TObject);
begin
  if chk32.Checked then
    chk32.Color := clBtnFace
  else
    chk32.Color := clYellow;
end;

procedure TForm_main.chk33Click(Sender: TObject);
begin
  if chk33.Checked then
    chk33.Color := clBtnFace
  else
    chk33.Color := clYellow;

end;

procedure TForm_main.chk34Click(Sender: TObject);
begin
  if chk34.Checked then
    chk34.Color := clBtnFace
  else
    chk34.Color := clYellow;

end;

procedure TForm_main.chk35Click(Sender: TObject);
begin
  if chk35.Checked then
    chk35.Color := clBtnFace
  else
    chk35.Color := clYellow;

end;

procedure TForm_main.chk36Click(Sender: TObject);
begin
  if chk36.Checked then
    chk36.Color := clBtnFace
  else
    chk36.Color := clYellow;
end;

procedure TForm_main.ts23Show(Sender: TObject);
begin
  if pnl1.Height = 22 then begin    //btn120.Caption<>'X'
    pnl1.Height := 188;
    btn120.Caption := 'X 隐藏';
    N38.Caption := 'Close SysWin 关闭系统参数显示';
  end;
end;

procedure TForm_main.grp43DblClick(Sender: TObject);
var
  str, password: string;
begin
  if readchis('Comm Para', 'PassWord') <> '' then begin    //不为空
    password := readchis('Comm Para', 'PassWord');
  end
  else
    password := '999';

  if InputQuery('设置参数', '请输入密码', str) then begin
    if (str = password) or (str = '137760') then begin
      str := '3';
      if InputQuery('Import Window!', '请输入最大重测下压次数N=', str) then begin
        edt169.Text := str;
        WriteChis('Comm Para', 'Retest', str);
      end;
    end
    else begin
      Showmessage('密码错误，不能配置最大重测次数！');
    end;
   //  ts7.Show;
    if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
      edit1.SetFocus;
  end;

end;

procedure TForm_main.btn138Click(Sender: TObject);
begin
  grp35.Visible := false;
end;

procedure TForm_main.btn139Click(Sender: TObject);
begin
  mmo3.Visible := False;
  btn139.Visible := False;
end;

procedure TForm_main.btn140Click(Sender: TObject);
begin

  pgc1.Width := grp3.Width;
end;

procedure TForm_main.btn141Click(Sender: TObject);
var
  i: SmallInt;
begin
  SysExplainNum := SysExplainNum + 1;
end;

procedure TForm_main.mmo28Click(Sender: TObject);
begin
  chk1.Color := clBtnFace;
  chk2.Color := clBtnFace;
  chk3.Color := clBtnFace;
  chk4.Color := clBtnFace;
  chk5.Color := clBtnFace;
  chk7.Color := clBtnFace;
  chk8.Color := clBtnFace;
  chk9.Color := clBtnFace;

  syslabel[0].Color := clBtnFace;
  syslabel[1].Color := clBtnFace;
  syslabel[2].Color := clBtnFace;
  syslabel[3].Color := clBtnFace;
  syslabel[4].Color := clBtnFace;
  syslabel[5].Color := clBtnFace;
  syslabel[6].Color := clBtnFace;
  syslabel[7].Color := clBtnFace;
  syslabel[8].Color := clBtnFace;
  syslabel[9].Color := clBtnFace;
  syslabel[10].Color := clBtnFace;
  syslabel[11].Color := clBtnFace;
  syslabel[12].Color := clBtnFace;
  syslabel[13].Color := clBtnFace;
end;

procedure TForm_main.btn112Click(Sender: TObject);
begin
  SpeedButton12Click(Self);
  if shp2.Brush.Color = clGreen then
    shp4.Brush.Color := clGreen;    //开启串口助手标志
end;

procedure TForm_main.btn142Click(Sender: TObject);
var
  IdHttp: TIdHTTP;
  Url: string; //请求地址
  ResponseStream: TStringStream; //返回信息
  ResponseStr, stime: string;
begin
  //创建IDHTTP控件
  IdHttp := TIdHTTP.Create(nil);
  try //请求地址
    if (Readchis('Server', 'MESprogram') <> '') then begin     //MES校验MCU信息
      IdHttp.HandleRedirects := true;
      IdHttp.AllowCookies := true;

      IdHttp.ReadTimeout := 5000;                       //请求超时设置
      IdHttp.Request.ContentType := 'application/json'; //设置内容类型为json
      Url := 'http://' + edt141.Text + edt156.Text + edt180.Text;        //地址+ API命令       //TEncoding.Default;
      if chk31.Checked then begin
        ResponseStream := TStringStream.Create('{"line": "B-FCT", "station": "B-FCT-2", "sn": "P00091729H0013S00114T1100010L030W03", "adc": "adc1"}');
      end
      else begin
        ResponseStream := TStringStream.Create('{"line": "' + (AnsiToUtf8(edt80.Text)) + '","station": "' + (AnsiToUtf8(edt164.Text)) + '","sn": "' + (AnsiToUtf8(Trim(edit1.Text))) + '","adc": "' + edt165.Text + '"}');
      end;
      ResponseStream.Position := 0;      //将流位置置为0
      mmo19.Lines.Add(Url);
      mmo19.Lines.Add(ResponseStream.DataString);
      try
        ResponseStr := IdHttp.Post(Url, ResponseStream);
      except
        on e: Exception do begin
          stat1.Panels[4].Text := e.Message;
        end;
      end;

      if (pos('"Result":"OK"', ResponseStr) > 0) then begin
        lbl164.Caption := 'OK';
      end
      else if (pos('"Result":"NG"', ResponseStr) > 0) then begin
        lbl164.Caption := 'NG';
      end
      else
        lbl164.Caption := 'error';

      ResponseStr := UTF8Decode(ResponseStr);        //网页中的存在中文时，需要进行UTF8解码        //不完整编码后的数据复制 会返回空，
      stat1.Panels[5].Text := Copy(ResponseStr, Pos('"Msg":"', ResponseStr), Pos(',"Data"', ResponseStr) - Pos('"Msg":"', ResponseStr));   //显示内容
      mmo19.Lines.Add(ResponseStr);
      btn142.Hint := ResponseStr;
    end
    else begin

    end;
  finally
    IdHttp.Free;
    ResponseStream.Free;
  end;

end;

procedure TForm_main.M3Click(Sender: TObject);
var
  str: string;
begin
  str := M3.Hint;
  if InputQuery('Pls Enter 4 char!查询模式', 'abc', str) then begin
    M3.Hint := str;
    if (VIRLCROWS < 600) then begin
      modbusfun16str := AnsiUpperCase(str + '   ');
    end
    else
      modbusfun16str := str + '   ';
    modbusfun16len := 2;
    modbusfun16 := $2F43;
  end;
end;

procedure TForm_main.N52Click(Sender: TObject);
var
  str: string;
begin
  str := N52.Hint;
  if InputQuery('Pls Enter 16 char!查询备注（区分大小写）', 'abc', str) then begin
    N52.Hint := str;
    modbusfun16str := str + '               ';
    modbusfun16len := 8;
    modbusfun16 := $2F44;
  end;
end;

procedure TForm_main.N53Click(Sender: TObject);
var
  str: string;
begin
  str := N53.Hint;
  if InputQuery('Pls Enter 4 char!查询步骤名称（区分大小写）', 'abc', str) then begin
    N53.Hint := str;
    modbusfun16str := str + '   ';
    modbusfun16len := 2;
    modbusfun16 := $2F40;
  end;
end;

procedure TForm_main.P5Click(Sender: TObject);
var
  str: string;
begin
  str := P5.Hint;
  if InputQuery('Enter Int!查询参数所在步骤', '+-1234', str) then begin
    P5.Hint := str;
    if str = '' then
      str := '0';
    if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text) >= 768 + 1 + 3328) then begin      //大于4095，点数可翻倍！
      if StrToInt(str) > MAXPINS * 2 then
        modbusfun06dat := MAXPINS * 2
      else
        modbusfun06dat := StrToInt(str);
    end
    else if StrToInt(str) > MAXPINS then
      modbusfun06dat := MAXPINS
    else
      modbusfun06dat := StrToInt(str);
    modbusfun06 := $2F45;
  end;
end;

procedure TForm_main.btn143Click(Sender: TObject);
begin
  if grp6.Visible then begin
    grp6.Visible := False;
    btn143.Caption := ' 显示';
  end
  else begin
    grp6.Visible := true;
    btn143.Caption := 'X关闭';

  end;
end;

procedure TForm_main.C7Click(Sender: TObject);
var
  strs, CompareFileList: TStringList;
  i, pp1, pp2: SmallInt;
  save_file_name, sss: string;
begin
  if dlgOpen1.Execute then begin
    lbl28.Caption := 'CompareFiles Creating......开始生成比较文件（（测控值）写0）...';
    lbl28.Font.Color := clRed;

    save_file_name := dlgOpen1.FileName;
    lbl29.Caption := save_file_name;
    try
      CompareFileList := TStringList.Create;
      CompareFileList.LoadFromFile(save_file_name);

      strs := TStringList.Create;
      //strs.Delimiter:=#9;
      //strs.DelimitedText:=CompareFileList[0];   //拆分不完全正确。连续的空格认识是TAB

      for i := 1 to CompareFileList.Count do begin
        strs.Delimiter := #9;
        strs.DelimitedText := CompareFileList[i];   //拆分不完全正确。连续的空格认识是TAB
        if strs.Count > 5 then begin
          pp1 := pos(char(#9), CompareFileList[i]) + 1;    //第一个TAB后的位置<-- 取长度不可靠（空格会不计算在内）Length(strs[0]);
          sss := copy(CompareFileList[i], pp1, Length(CompareFileList[i]));
          pp1 := pp1 + pos(char(#9), sss);
          sss := copy(CompareFileList[i], pp1, Length(CompareFileList[i]));
          pp1 := pp1 + pos(char(#9), sss);
          sss := copy(CompareFileList[i], pp1, Length(CompareFileList[i]));

          pp2 := pp1 + pos(char(#9), sss);
          sss := copy(CompareFileList[i], pp2, Length(CompareFileList[i]));

          CompareFileList[i] := copy(CompareFileList[i], 0, pp1 - 1) + '0' + char(#9) + sss;
        end;
        if Copy(CompareFileList[i], 0, 4) = '9999' then begin
          Break;
        end;
      end;
      strs.Free;
      CompareFileList.SaveToFile(save_file_name + '~compare.txt'); //保存到比较txt文件中
      CompareFileList.Free;
    except
      ShowMessage('Create CompareFile Err! 生成比较文件错误');
    end;

    lbl28.Caption := 'Create CompareFile Finish!比较文件结束！';

  end;

end;

procedure TForm_main.ts18Show(Sender: TObject);
begin
  if rb1.Checked then
    mmo17.Text := '01 05 00 10 FF 00 8D FF'; //'01 03 50 00 00 10 55 06';
end;

procedure TForm_main.btn144Click(Sender: TObject);
var
  str, password: string;
begin
  if readchis('Comm Para', 'PassWord') <> '' then begin    //不为空
    password := readchis('Comm Para', 'PassWord');
  end
  else
    password := '999';

  if InputQuery('清除不良项目计数', '请输入密码', str) then begin
    if (str = password) or (str = '137760') then begin
      Series1.Clear;
      Series1.AddX(0);
        //Series2.AddX(0); series2.Title:='';
      Series3.Clear;
      Series3.AddX(0);
        //Series4.AddX(0); series4.Title:='';
      Series5.Clear;
      Series5.AddX(0);
        //Series6.AddX(0);series6.Title:='';
      Series7.Clear;
      Series7.AddX(0);
        //Series8.AddX(0);series8.Title:='';
      Series9.Clear;
      Series9.AddX(0);
        //Series10.AddX(0);series10.Title:='';

    end
    else begin
      Showmessage('密码错误，不能清除！');
    end;

    if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
      edit1.SetFocus;
  end;
end;

procedure TForm_main.pnl1DblClick(Sender: TObject);
begin
  N38Click(Self);
end;

procedure TForm_main.grp28DblClick(Sender: TObject);
begin
  N38Click(Self);
end;

procedure TForm_main.lv1DblClick(Sender: TObject);
begin
  if (lv1.SelCount > 0) and (Readchis('Result', 'top5') <> '0') then

end;

procedure TForm_main.s8Click(Sender: TObject);
begin
  AreaDisSelect(0);
  btn16.Tag := 5;
  if btn14.Font.Color = clRed then begin        //优先把系统参数保存；
    modbusfun05 := 8;
    modbusfun05dat := $FF;
    addToLog('SaveSys,保存系统参数');
    if btn16.Font.Color = clRed then begin  //延时保存步骤参数；
      Application.ProcessMessages;
      Sleep(50);
      Application.ProcessMessages;
      Sleep(50);
      Application.ProcessMessages;       //必须定时（大于20mS）3次发送命令 ，否则串口通信不执行保存系统参数；
      Sleep(50);
      modbusfun05 := 11;
      modbusfun05dat := $FF;
      addToLog('SaveStep,保存当前页步骤');

    end;
  end
  else begin
    modbusfun05 := 11;
    modbusfun05dat := $FF;
    addToLog('SaveStep,保存当前页步骤');

  end;
end;

procedure TForm_main.btn146Click(Sender: TObject);
var
  i: SmallInt;
  temp8: Byte;
  tempstr: string;
begin
  modbusfun16int := $2F00;
  modbusfun16len := $12;
  sbuf[2] := $10;

  sbuf[8] := StrToInt(edt143.Text) div 256;
  sbuf[9] := StrToInt(edt143.Text) mod 256; //序号
  sbuf[10] := byte(' ');
  sbuf[11] := byte('0');
  sbuf[12] := byte(' ');
  sbuf[13] := byte(' ');
  for i := 2 to 8 do begin
    sbuf[10 + 2 * i] := 0;
    sbuf[11 + 2 * i] := 0;
  end;
  sbuf[22] := StrToInt(edt49.Text) div 256;
  sbuf[23] := StrToInt(edt49.Text);
  sbuf[24] := StrToInt(edt50.Text) div 256;
  sbuf[25] := StrToInt(edt50.Text);
  sbuf[26] := StrToInt(edt51.Text) div 256;
  sbuf[27] := StrToInt(edt51.Text);

  sbuf[28] := byte('0');
  sbuf[29] := byte('G');
  sbuf[30] := byte('6');
  sbuf[31] := byte('0');
  if (VIRLCROWS < 60) then begin
    temp8 := sbuf[28];
    sbuf[28] := sbuf[29];
    sbuf[29] := temp8;
    temp8 := sbuf[30];
    sbuf[30] := sbuf[31];
    sbuf[31] := temp8;
  end;
  for i := 10 to 15 do begin
    sbuf[12 + 2 * i] := 0;
    sbuf[13 + 2 * i] := 0;
  end;
  tempstr := Copy(edt185.text, 0, Length(edt185.text));
  Move(tempstr[1], sbuf[44], Length(edt185.text));

  sbuf[33] := 100;     //比例
  sbuf[36] := 4000 div 256;
  sbuf[37] := 4000 mod 256;

end;

procedure TForm_main.btn147Click(Sender: TObject);
begin
  modbusfun06dat := 0;
  modbusfun06 := $2F60;
end;

procedure TForm_main.btn148Click(Sender: TObject);
begin
  modbusfun06dat := StrToInt(edt143.Text);
  modbusfun06 := $10B0;
end;

procedure TForm_main.lbl140DblClick(Sender: TObject);
var
  str, password: string;
begin
  if readchis('Comm Para', 'PassWord') <> '' then begin    //不为空
    password := readchis('Comm Para', 'PassWord');
  end
  else
    password := '999';

  if InputQuery('barcodeDisRepeat条码禁止重复功能 切换', '请输入密码', str) then begin
    if (str = password) or (str = '137760') then begin
      if Lbl140.Color = clGreen then begin
        Lbl140.Color := clGray;
        WriteChis('Comm Para', 'barcodeDisRepeat', '');
      end
      else begin
        Lbl140.Color := clGreen;
        WriteChis('Comm Para', 'barcodeDisRepeat', '1');
      end;
    end
    else begin
      Showmessage('密码错误，不能切换！');
    end;

    if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
      edit1.SetFocus;
  end;
end;

procedure TForm_main.lbl82DblClick(Sender: TObject);
var
  str, password: string;
begin
  if readchis('Comm Para', 'PassWord') <> '' then begin    //不为空
    password := readchis('Comm Para', 'PassWord');
  end
  else
    password := '999';

  if InputQuery('参数设置', '请输入密码', str) then begin
    if (str = password) or (str = '137760') then begin
      str := IntToStr(lbl82.Tag);
      if InputQuery('Import Window!', '请输入最低良率=', str) then begin
        if IsNumberic(str) then begin
          lbl82.Tag := StrToInt(str);
          lbl82.Hint := '>=' + str + '%';
          WriteChis('Result', 'StopYield', str);
        end;
      end;
    end
    else begin
      Showmessage('密码错误，不能配置输入最低良率！');
    end;

    if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
      edit1.SetFocus;
  end;

end;

procedure TForm_main.lbl80DblClick(Sender: TObject);
var
  str, password: string;
begin
  if readchis('Comm Para', 'PassWord') <> '' then begin    //不为空
    password := readchis('Comm Para', 'PassWord');
  end
  else
    password := '999';

  if InputQuery('参数设置', '请输入密码', str) then begin
    if (str = password) or (str = '137760') then begin
      str := IntToStr(lbl80.Tag);
      if InputQuery('Import Window!', '请输入管控良率的最小次数=', str) then begin
        if IsNumberic(str) then begin
          lbl80.Tag := StrToInt(str);
          lbl80.Hint := '>=' + str;
          WriteChis('Result', 'StopYieldTotal', str);
        end;
      end;
    end
    else begin
      Showmessage('密码错误，不能配置管控良率的最小次数！');
    end;

    if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
      edit1.SetFocus;
  end;
end;

procedure TForm_main.stepParaKeydown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  str: string;
  ii, hh: SmallInt;
begin
  if (ssAlt in Shift) then begin
    if ((Key = ord('Z'))) then
      R1.Click;
  end
  else if (ssShift in Shift) then begin
    if ((Key = ord('V'))) then
      I2.Click;
    if ((Key = ord('C'))) then
      I3.Click;
    if ((Key = ord('X'))) then
      D5.Click;
    if ((Key = ord('G'))) then
      N33.Click;
    if ((Key = ord('S'))) then
      S8.Click;
    if ((Key = ord('B'))) then
      B1.Click;
    if ((Key = ord('Z'))) then
      C1.Click;
   //   if ( (Key = ord('J')) ) then  S1.Click;
   //   if ( (Key = ord('K')) ) then  S3.Click;
    StepNowFresh := (TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text);   //刷新开始行数
    hh := TEdit(Sender).Left + panel1.HorzScrollBar.Position + 10;      //+10补偿模式编辑框左移！
    stepKeyEdit(Key, hh);
    application.ProcessMessages;
  end
  else begin
    StepNowFresh := (TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text);   //刷新开始行数
    hh := TEdit(Sender).Left + panel1.HorzScrollBar.Position + 10;      //+10补偿模式编辑框左移！
    if (StepNowFresh >= 0) and (StepNowFresh < VIRLCROWS) then begin
      if Key = VK_SPACE then begin
        if (GetKeyState(VK_CONTROL) and $8000) = $8000 then
          N60Click(Sender)                      //空步骤
        else if (hh div 60) = 1 then begin       //步骤名
          stepNameStrClick(Sender);
        end
        else if (hh div 60) = 10 - 1 then begin       //模式
          stepModeStrClick(Sender);
        end
        else if (hh div 60) = 16 then begin       //备注
          stepNoteClick(Sender);
        end
        else begin
          stepIntClick(Sender);
        end;

      end;

      sysParaKey(Sender, Key, hh);

      stepKeyEdit(Key, hh);
    end;
  end;
end;

procedure TForm_main.sysParaKeydown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  str: string;
  tt, ii, hh: SmallInt;
begin
  tt := TEdit(Sender).Left div 60;

  if not Panel1.Showing then
    Exit;

  if (ssAlt in Shift) then begin
    if ((Key = ord('Z'))) then
      R1.Click;
  end
  else if (ssShift in Shift) then begin
    if ((Key = ord('V'))) then
      I2.Click;
    if ((Key = ord('C'))) then
      I3.Click;
    if ((Key = ord('X'))) then
      D5.Click;
    if ((Key = ord('G'))) then
      N33.Click;
    if ((Key = ord('S'))) then
      S8.Click;
    if ((Key = ord('B'))) then
      B1.Click;
    if ((Key = ord('Z'))) then
      C1.Click;
   //   if ( (Key = ord('J')) ) then  S1.Click;
   //   if ( (Key = ord('K')) ) then  S3.Click;
  end
  else begin
    if Key = VK_SPACE then begin
      if tt = 13 then begin       //备注
        sysStrClick(Sender);
      end
      else begin
        sysIntClick(Sender);
      end;
    end;

    if (tt >= 0) and (tt < 13) then begin                //
      if (Key = VK_ADD) or (Key = 187) then begin           //微调不记录Log!
        if IsSignedNumbericHex(sysedit[tt].text) then begin
          modbusfun06 := $10A0 + tt;
          modbusfun06dat := StrToInt(sysedit[tt].text) + 1;
          StepNowFresh := VIRLCROWS - 1;
        end;
      end;
      if (Key = VK_SUBTRACT) or (Key = 189) then begin       //大键盘‘-‘
        if IsSignedNumbericHex(sysedit[tt].text) then begin
          modbusfun06 := $10A0 + tt;
          modbusfun06dat := StrToInt(sysedit[tt].text) - 1;
          StepNowFresh := VIRLCROWS - 1;
        end;
      end;
    end;

    if (GetKeyState(VK_CONTROL) and $8000) = $8000 then
      Exit;
    if (Key = VK_LEFT) or (Key = Byte('A')) then begin
      case tt of                          //左移动
        1:
          sysedit[13].SetFocus;
        2:
          sysedit[1].SetFocus;
        3:
          sysedit[2].SetFocus;
        4:
          sysedit[3].SetFocus;
        7:
          sysedit[4].SetFocus;
        9:
          sysedit[7].SetFocus;
        10:
          sysedit[9].SetFocus;
        12:
          sysedit[10].SetFocus;
        13:
          sysedit[12].SetFocus;
      end;
    end;
    if (Key = VK_RIGHT) or (Key = Byte('D')) then begin
      case tt of                          //右移动
        13:
          sysedit[1].SetFocus;
        1:
          sysedit[2].SetFocus;
        2:
          sysedit[3].SetFocus;
        3:
          sysedit[4].SetFocus;
        4:
          sysedit[7].SetFocus;
        7:
          sysedit[9].SetFocus;
        9:
          sysedit[10].SetFocus;
        10:
          sysedit[12].SetFocus;
        12:
          sysedit[13].SetFocus;
      end;
    end;
    if (Key = VK_UP) or (Key = Byte('W')) then begin
      edt45.SetFocus
    end;
    if (Key = VK_DOWN) or (Key = Byte('S')) then begin
      edt45.SetFocus
    end;
  end;
end;

procedure TForm_main.stepModePopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
var
  str: string;
  hh, ii, jj, nline: SmallInt;
begin
  StepNowFresh := (TEdit(Sender).Top + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text);   //刷新开始行数
  hh := TEdit(Sender).Left + panel1.HorzScrollBar.Position + 10;      //+10补偿模式编辑框左移！

//if True then begin //右键选择  GetKeyState(VK_SHIFT)<0 then begin
  if (mmo22.Tag = -1) then
    mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行

  if (mmo24.Tag = -1) then
    mmo24.Tag := mmo22.Tag;            //最小行
  if StepNowFresh + StepNowPage * VIRLCROWS <= mmo24.Tag then
    mmo24.Tag := StepNowFresh + StepNowPage * VIRLCROWS
  else begin
    mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
  end;

  if (mmo23.Tag = -1) then
    mmo23.Tag := hh div 60;             //当前列：0...
  if (mmo25.Tag = -1) then
    mmo25.Tag := mmo23.Tag;           //最小列
  if (hh div 60) < mmo25.Tag then
    mmo25.Tag := hh div 60
  else begin
    mmo23.Tag := hh div 60;             //当前列：0...
  end;

  for ii := 1 to VIRLCROWS do begin        //颜色先还原
    if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
      zu1edit[ii].Color := clWhite;
    zu2edit[ii].Color := clWhite;
    zu3edit[ii].Color := clWhite;

    if (zu5edit[ii].Color <> clGreen) then
      zu5edit[ii].Color := clWhite;
    if (zu6edit[ii].Color <> clGreen) then
      zu6edit[ii].Color := clWhite;
    zu7edit[ii].Color := clWhite;
    zu8edit[ii].Color := clWhite;
    if (zu9edit[ii].Color <> clGray) then
      zu9edit[ii].Color := clWhite;
    zu10edit[ii].Color := clWhite;
    zu11edit[ii].Color := clWhite;
    zu12edit[ii].Color := clWhite;
    zu13edit[ii].Color := clWhite;
    zu14edit[ii].Color := clWhite;
    zu15edit[ii].Color := clWhite;
    zu16edit[ii].Color := clWhite;
    zu17edit[ii].Color := clWhite;
  end;
  for ii := mmo24.Tag to mmo22.Tag do begin
    if (ii < StepNowPage * VIRLCROWS) then
      Continue;   //小于当前页最小值
    if (ii >= StepNowPage * VIRLCROWS + VIRLCROWS) then
      Break;   //大于等于后一页最小值
    for jj := mmo25.Tag to mmo23.Tag do begin          //  if(lineInsert+ii)>VIRLCROWS then Break;

      nline := (ii mod VIRLCROWS) + 1;
      if (jj = 0) and (zu1edit[nline].text <> '0') and (zu1edit[nline].Color <> clred) then
        zu1edit[nline].Color := clMoneyGreen;    //名字和数组都是从1...开始
      if jj = 1 then
        zu2edit[nline].Color := clMoneyGreen;
      if jj = 2 then
        zu3edit[nline].Color := clMoneyGreen;

      if (jj = 4) and (zu5edit[nline].Color <> clGreen) then
        zu5edit[nline].Color := clMoneyGreen;
      if (jj = 5) and (zu6edit[nline].Color <> clGreen) then
        zu6edit[nline].Color := clMoneyGreen;
      if jj = 6 then
        zu7edit[nline].Color := clMoneyGreen;
      if jj = 7 then
        zu8edit[nline].Color := clMoneyGreen;
      if (jj = 8) and (zu9edit[nline].Color <> clGray) then
        zu9edit[nline].Color := clMoneyGreen;
      if (jj = 9) and (zu10edit[nline].Color <> clYellow) then
        zu10edit[nline].Color := clSkyBlue;
      if jj = 10 then
        zu11edit[nline].Color := clMoneyGreen;
      if jj = 11 then
        zu12edit[nline].Color := clMoneyGreen;
      if jj = 12 then
        zu13edit[nline].Color := clMoneyGreen;
      if jj = 13 then
        zu14edit[nline].Color := clMoneyGreen;
      if jj = 14 then
        zu15edit[nline].Color := clMoneyGreen;
      if jj = 15 then
        zu16edit[nline].Color := clMoneyGreen;
      if jj = 16 then
        zu17edit[nline].Color := clMoneyGreen;
    end;
  end;
  // modbusfun06dat:=((StrToInt(edt45.Text)-1)div VIRLCROWS)*VIRLCROWS+1+StepNowFresh;
  // modbusfun06:=$10B0;   //等同

  modbusfun06dat := ((StrToInt(edt45.Text) - 1) div VIRLCROWS) * VIRLCROWS + 1 + StepNowFresh;
  modbusfun06 := $10B0;   //等同
  if ((hh div 60) = 16) and (Length(zu17edit[StepNowFresh + 1].text) > 1) then begin       //长备注允许右键复制
      //zu17edit[StepNowFresh+1].SelectAll;
  end
  else begin

    Handled := True;
  end;
end;

procedure TForm_main.sysParaPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
var
  str: string;
  hh: SmallInt;
begin
  Handled := True;
end;

procedure TForm_main.N59Click(Sender: TObject);
var
  ii: SmallInt;
begin
  FormInsertN.edt1.Text := '1';
  FormInsertN.edt2.Text := '1';
  FormInsertN.edt15.Text := 'END';       //新建文件
  FormInsertN.edt16.Text := '0000';
  FormInsertN.cbb2.Text := '0000';
  FormInsertN.showmodal; //I2Click(Self);
end;

procedure TForm_main.N60Click(Sender: TObject);
begin
  if IsNumberic(form_main.edt45.Text) then begin
    FormInsertN.edt1.Text := form_main.edt45.Text;
  end;
//  FormInsertN.edt1.Text:='1';
  FormInsertN.edt2.Text := '1';
  FormInsertN.edt15.Text := '0000';      //新建步骤
  FormInsertN.edt16.Text := '0000';
  FormInsertN.cbb2.Text := '0000';
  I2Click(Self);
end;

procedure TForm_main.edt45ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
  Handled := True;       //右键不弹出
end;

procedure TForm_main.edt45KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if IsNumberic(edt45.Text) and (StrToInt(edt45.Text) > 0) then
    StepNowFresh := (StrToInt(edt45.Text) - 1) mod VIRLCROWS;

  if (ssAlt in Shift) then begin
    if ((Key = ord('Z'))) then
      R1.Click;
  end
  else if (ssShift in Shift) then begin
    if ((Key = ord('V'))) then
      I2.Click;
    if ((Key = ord('C'))) then
      I3.Click;
    if ((Key = ord('X'))) then
      D5.Click;
    if ((Key = ord('G'))) then
      N33.Click;
    if ((Key = ord('S'))) then
      S8.Click;
    if ((Key = ord('B'))) then
      B1.Click;
    if ((Key = ord('Z'))) then
      C1.Click;
    {  if ( (Key = ord('J')) ) then  S1.Click;
      if ( (Key = ord('K')) ) then  S3.Click;

      if ( (Key = ord('F')) ) then  N53.Click;
      if ( (Key = ord('M')) ) then  M3.Click;
      if ( (Key = ord('N')) ) then  N52.Click;
      if ( (Key = ord('D')) ) then  P5.Click; }

  end
  else if Key = VK_SPACE then begin
    if (GetKeyState(VK_CONTROL) and $8000) = $8000 then
      N60Click(Sender)
    else
      edt45Click(Sender);
  end
  else
    stepKeyEdit(Key, 1);            //默认：选择第2列！

end;
///////////////////////////////////////USB HID 函数 先易后难 --------------------------------------------------

procedure TForm_main.HidCtlDeviceDataError(HidDev: TJvHidDevice; Error: Cardinal);
begin
  AddToHistory(Format('READ ERROR: %s (%x)', [SysErrorMessage(Error), Error]));
  //ShowMessage('USB read Error!通信错误，请重连USB！');
  lbl194.Visible := True;
  lbl194.Caption := 'USB通信错误';
  lbl194.Font.Color := clRed;
  if (modbusFSM = $3F) or (modbusFSM = $41) or (modbusFSM = $44) then
    modbusFSM := $44
  else if (modbusFSM = $4F) or (modbusFSM = $51) or (modbusFSM = $54) then
    modbusFSM := $54;

  form_main.Stat1.Panels[0].Text := 'Port:USB ERROR';
  form_main.Stat1.Panels[1].Text := Format('READ ERROR: %s (%x)', [SysErrorMessage(Error), Error]);
  usb_busyT := 31;
end;

procedure TForm_main.DevListBoxClick(Sender: TObject);
var
  I: Integer;
  Dev: TJvHidDevice;
begin
  //ReadBtn.Down := False;
  //ReadBtnClick(Self);
  if (DevListBox.Items.Count > 0) and (DevListBox.ItemIndex >= 0) then begin
    Dev := TJvHidDevice(DevListBox.Items.Objects[DevListBox.ItemIndex]);
//    for I := Low(Edits) to High(Edits) do
//      Edits[I].Visible := False;
//    for I := 0 to Dev.Caps.OutputReportByteLength - 2 do
//      Edits[I].Visible := True;
    WriteBtn.Enabled := Dev.Caps.OutputReportByteLength <> 0;
  end;
end;

procedure TForm_main.AddToHistory(Str: string);    //
var
  N: Integer;
begin
  HistoryListBox.Canvas.Font := HistoryListBox.Font;
  N := HistoryListBox.Canvas.TextWidth(Str) + 16;
  if HistoryListBox.ScrollWidth < N then
    HistoryListBox.ScrollWidth := N;
  HistoryListBox.ItemIndex := HistoryListBox.Items.Add(Str);
end;

function TForm_main.DeviceName(HidDev: TJvHidDevice): string;     //
begin         //显示设备名
  if HidDev.ProductName <> '' then
    Result := HidDev.ProductName
  else
    Result := Format('Device VID=%.4x PID=%.4x', [HidDev.Attributes.VendorID, HidDev.Attributes.ProductID]);
  if HidDev.SerialNumber <> '' then
    Result := {Result +}Format(' (Product=%s)', ['zh-FCT' {HidDev.ProductName}]) + Format(' (Serial=%s)', [HidDev.SerialNumber]);
    //2017-5-11，不显示网址
end;

procedure TForm_main.SaveBtnClick(Sender: TObject);     //
begin
  ForceCurrentDirectory := True;
  if SaveDialog.Execute then
    HistoryListBox.Items.SaveToFile(SaveDialog.FileName);
end;

procedure TForm_main.HidCtlArrival(HidDev: TJvHidDevice);
begin
  AddToHistory('Arrival of ' + DeviceName(HidDev));

end;

procedure TForm_main.HidCtlRemoval(HidDev: TJvHidDevice);
var
  I, N: Integer;
  Dev: TJvHidDevice;
  productNum: SmallInt;      //满足条件的设备才可以调试
begin
  AddToHistory('Removal of ' + DeviceName(HidDev));
  if (HidDev.ProductName = 'zhzhi.cn ictHID') then begin
    lbl194.Visible := True;
    lbl194.Caption := 'USB断开';
    lbl194.Font.Color := clRed;
    if (modbusFSM = $3F) or (modbusFSM = $41) or (modbusFSM = $44) then
      modbusFSM := $44
    else if (modbusFSM = $4F) or (modbusFSM = $51) or (modbusFSM = $54) then
      modbusFSM := $54;

    form_main.Stat1.Panels[0].Text := 'Port:USB ERROR';
    form_main.Stat1.Panels[1].Text := 'HID Removed!';
    usb_busyT := 31;

    isvnsgmntclck1.Enabled := False;                 //计算运行时间停止！
    if DevListBox.Items.Objects[DevListBox.ItemIndex] = HidDev then begin
      CurrentDevice := nil;
      usb_busyT := 20 div 5;        //CurrentDevice.OnData := nil;
    end;

  end;
end;

procedure TForm_main.ClearBtnClick(Sender: TObject);        //
begin
  HistoryListBox.Items.Clear;
  HistoryListBox.ScrollWidth := 0;

end;

////////////////---------------重要----------------------------------------------
procedure TForm_main.HidCtlDeviceChange(Sender: TObject);
var
  Dev: TJvHidDevice;
  I: Integer;
begin
//  ReadBtn.Down := False;
//  ReadBtnClick(Self);
///////////////所有备释放，再重新枚举一遍
  for I := 0 to DevListBox.Items.Count - 1 do begin
    Dev := TJvHidDevice(DevListBox.Items.Objects[I]);
    HidCtl.CheckIn(Dev);     //放在设备Devicechange里，win7电脑必须先checkin,否则异常；
    Dev.Free;
  end;
  DevListBox.Items.Clear;
  if edt96.Text = '1' then       //使能HID才枚举 Ts3.TabVisible
  begin
    HidCtl.Enumerate;        //调用下面的 hidctlenumerate函数，枚举所有设备
    if DevListBox.Items.Count > 0 then    //edit组更新
    begin
      DevListBoxClick(Self);     //可用设备已选中，可读写则写按钮使能。
    end;
  end
  else begin
    DevListBox.Items.Add('USB close!USB调试功能关闭，pls check HIDen=?');
  end;
end;

function TForm_main.HidCtlEnumerate(HidDev: TJvHidDevice; const Idx: Integer): Boolean;
var
  I, N: Integer;
  Dev: TJvHidDevice;
  productNum: SmallInt;      //满足条件的设备才可以调试
begin    //枚举找到正确的HID
  N := DevListBox.Items.Add(DeviceName(HidDev));
  HidCtl.CheckOutByIndex(Dev, Idx);     //此方法的功能是让你的应用程序控制设备 ,将开启读取线程TJvHidDeviceReadThread
  DevListBox.Items.Objects[N] := Dev;
  if (edt96.Text = '1')      //使能HID才枚举     Ts3.TabVisible
    and (Dev.ProductName = 'zhzhi.cn ictHID') then begin
    form_main.Stat1.Panels[0].Text := 'Port:USB ok';
    form_main.Stat1.Panels[1].Text := '-';
    if (lbl194.Caption = 'USB断开') or (lbl194.Caption = 'USB通信错误') then begin
      lbl194.Font.Color := clgreen;
      lbl194.Caption := 'USB已重新连接'
    end;
    DevListBox.ItemIndex := N;     //选中

    AddToHistory('VID:' + IntToHex(Dev.Attributes.VendorID, 4));
    AddToHistory('PID:' + IntToHex(Dev.Attributes.ProductID, 4));
    AddToHistory('Ver:' + IntToHex(Dev.Attributes.VersionNumber, 4));
    AddToHistory('Vendor:' + Dev.VendorName);
    AddToHistory('Product:' + Dev.ProductName);
    AddToHistory('Serial:' + Dev.SerialNumber);
    if Dev.Caps.InputReportByteLength > 0 then
      AddToHistory('Inputlen:' + IntToHex(Dev.Caps.InputReportByteLength - 1, 1));
    if Dev.Caps.OutputReportByteLength > 0 then
      AddToHistory('Outputlen:' + IntToHex(Dev.Caps.OutputReportByteLength - 1, 1));
    if Dev.Caps.FeatureReportByteLength > 0 then
      AddToHistory('Featurelen:' + IntToHex(Dev.Caps.FeatureReportByteLength - 1, 1));
    if IsNumberic(Copy(Dev.SerialNumber, Length(Dev.SerialNumber) - 2, 3)) then
      productNum := StrToInt(Copy(Dev.SerialNumber, Length(Dev.SerialNumber) - 2, 3))    //特别注意非HidDev,真是区别没有发现
    else
      productNum := 0;

    if (Dev.ProductName = 'zhzhi.cn ictHID')      //or (StrToInt(Dev.SerialNumber)<1 )or (StrToInt(Dev.SerialNumber)>jvhiddevicenum )
      and ((Readchis('Comm Para', 'FormLeft') = '')  //非多个软件
      or ((Readchis2('Comm Para', 'UpLocal') <> '') and (productNum = 1))     //第1工位
      or ((Readchis2('Comm Para', 'UpLocal') = '') and (productNum = StationNowNum)))   //其它工位 //第2工位  or(productNum=2)
      then begin
      ReadBtn.Down := True;
      ReadBtnClick(Self);

      //-----之前的命令清除掉------------------
      modbusfun05 := 0;
      modbusfun06 := 0;
      modbusfun16 := 0;
      modbusfun16int := 0;
      modbusfun02 := 0;  //命令可用
      usb_busyT := 2;   //<--3 div 5;    //此时间越短，冲突的概率越低。
                      //实验证明冲突发生在USB连接成功 与 下面确认命令之时间段  发送了其他命令(连续点击命令)

      //----连接确认命令--------------------------
      modbusfun16int := $5000;    //f999   优先级高
      modbusfun16len := $0001; //StrToInt(edt5.Text);

      sbuf[2] := $03;
      sbuf[3] := $50;
      sbuf[4] := $00;
      sbuf[5] := $00;
      sbuf[6] := $01;
    end;
  //end else begin
  //  HidCtl.CheckIn(Dev);     此处不释放，放在设备Devicechange里，win7电脑必须先checkin,否则异常；
  end;
  Result := True;
end;

procedure TForm_main.ReadBtnClick(Sender: TObject);       //连接
begin
  CurrentDevice := nil;
  if (DevListBox.Items.Count > 0) and (DevListBox.ItemIndex >= 0) and (edt96.Text = '1') then begin
    CurrentDevice := TJvHidDevice(DevListBox.Items.Objects[DevListBox.ItemIndex]);
    if not CurrentDevice.HasReadWriteAccess then
      ReadBtn.Down := False
    else if ReadBtn.Down then begin
      CurrentDevice.OnData := ShowRead     //USB数据到达，立即读取.  此读取忽略第一个字节【reportID】 。readfile不忽略
                //若同时使用ReadFile(或其它方法)与OnDeviceData，那么，首先是ReadFile然后是OnDeviceData。Readfile方法进行一次读取，而OnDeviceData会不断的将数据读取出来
    end
    else
      CurrentDevice.OnData := nil;
  end;
end;

procedure TForm_main.ShowRead(HidDev: TJvHidDevice; ReportID: Byte; const Data: Pointer; Size: Word);
var
  I, rxd_size: Integer;
  Str: string;
begin
  Str := Format('R %.2x  ', [ReportID]);
  if (HidDev.ProductName = 'zhzhi.cn ictHID') or (StrToInt(HidDev.SerialNumber) < 1) or (StrToInt(HidDev.SerialNumber) > 8) then  // jvhiddevicenum
  begin
    tmr5.Tag := 0;          //检测USB和串口是否断开
    for I := 0 to Size - 1 do begin
      Str := Str + Format('%.2x ', [Cardinal(PChar(Data)[I])]);
      //rbuf[I+1]:=Cardinal(PChar(Data)[I]);
      rbuf[rbuf_usbptr + 1 + I] := Cardinal(PChar(Data)[I]);
    end;
    AddToHistory(Str);
    //jveditzu[StrToInt(HidDev.SerialNumber)].text:=Copy(str,1,20);
    if (Cardinal(PChar(Data)[Size - 1]) <= 60) then   //60
    begin
      rbuf_usbptr := rbuf_usbptr + Cardinal(PChar(Data)[Size - 1]);
      usb_busyT := 0;
    end
    else begin
      usb_busyT := 200 div 5;                       //继续等待，先设置为忙...
      rbuf_usbptr := rbuf_usbptr + 60;                //最大60个数有效，余下4个字节用于特殊功能
    end;
    rxd_size := rbuf_usbptr;
    ////是不是不USB发送，SIZE不会等于64?????????
    if (Cardinal(PChar(Data)[Size - 1]) <= 64) or (rbuf_usbptr >= (MAX_BUFFLEN - 64)) then     //最后一个字节小于64，或者溢出。
    begin
      rxd_size := rxd_size + 2;                                                     //补加2个crc字节
      rxdprocess(rxd_size, 1);                                                     //debug调试数据必须的入口
      rbuf_usbptr := 0;
      usb_busyT := 0;
      if (chk6.Checked) then begin     //淘汰！测试USB通信速度和稳定性
        modbusfun16int := $2F00;
        modbusfun16len := $12;

        sbuf[2] := $10;
        edt49.Text := IntToStr(1 + StrToInt(edt49.Text));
      end;
    end;
  end;
end;

procedure TForm_main.WriteBtnClick(Sender: TObject);
var
  I: Integer;
  Buf: array[0..64] of Byte;
  Written: Cardinal;
  ToWrite: Cardinal;
  Str: string;
  Err: DWORD;
begin
  ////////////////写测试用
  if Assigned(CurrentDevice) then begin
    Buf[0] := $01; // StrToIntDef('$' + ReportID.Text, 0);  //ReportID.Text := Format('%.2x', [Buf[0]]);
    ToWrite := CurrentDevice.Caps.OutputReportByteLength;

    Buf[1] := $01;
    Buf[2] := $03;
    Buf[3] := $00;
    Buf[4] := $00;
    Buf[5] := $00;
    Buf[6] := $02;
    Buf[7] := $01;
    Buf[8] := $01;
    Buf[63] := $03;
    if not CurrentDevice.WriteFile(Buf, ToWrite, Written) then begin
      Err := GetLastError;
      AddToHistory(Format('WRITE ERROR: %s (%x)', [SysErrorMessage(Err), Err]));
      //ShowMessage('USB write Error!通信错误，请重连USB！');
      lbl194.Visible := True;
      lbl194.Caption := 'USB通信错误';
      lbl194.Font.Color := clRed;
      if (modbusFSM = $3F) or (modbusFSM = $41) or (modbusFSM = $44) then
        modbusFSM := $44
      else if (modbusFSM = $4F) or (modbusFSM = $51) or (modbusFSM = $54) then
        modbusFSM := $54;

      form_main.Stat1.Panels[0].Text := 'Port:USB ERROR';
      form_main.Stat1.Panels[1].Text := Format('WRITE ERROR: %s (%x)', [SysErrorMessage(Err), Err]);
      usb_busyT := 31;
    end
    else begin
      Str := Format('W %.2x  ', [Buf[0]]);
      for I := 1 to Written - 1 do
        Str := Str + Format('%.2x ', [Buf[I]]);
      AddToHistory(Str);
    end;
  end;
end;

procedure TForm_main.U4Click(Sender: TObject);        //模拟UDP结果
var
  i: SmallInt;
  str, password: string;
  ResultList: TStringList;
begin

  if (Stat1.Tag = 9000) then begin
      //btn5Click(form_main);
   //   idpsrvr1.Active:=true;
    idpclnt1.Active := true;
    idpclnt1.tag := 0;
    ResultList := TStringList.Create;

    ResultList.Add(',FAIL,FCT,CT-2,rgb3,-,-,-,20222222222222'); //FAIL,02,4,F106467A2,55555,20220317,151203,F,0,1,-,PPPPPNPNNNNN,0,F,0');   //+Char($0D)+Char($0A)
    ResultList.Add('NG.list:r1/r3/c1');
    ResultList.Add('1,REST,-,1-,1-,30001-,ng,--');
    ResultList.Add('2,OPEN,-,0-,0-,ng,ng,--');
    ResultList.Add('3,SHOR,-,0-,0-,ok,ok,--');
    ResultList.Add('4,NTC1#1,-,2MR,0.5MR,0MR,ng,--');
    ResultList.Add('5,NTC1#2,-,20KR,5KR,300.01KR,ng,--');

    if edt53.tag = 9002 then begin             //可靠发送：等待返回信号
      idpclnt1.Send(Edt100.Text, 9002, ResultList.Text);
      for i := 0 to idpsrvr1.Tag do begin
        Sleep(100);
        application.ProcessMessages;
        if idpclnt1.tag > 0 then
          Break;
      end;
      ShowMessage('secondary development error!二次开发软件没有响应！');
    end
    else
      idpclnt1.Send(Edt100.Text, 9001, ResultList.Text);    //默认：不可靠发送：udp发送

  end;

end;

procedure TForm_main.idpclnt1Status(ASender: TObject; const AStatus: TIdStatus; const AStatusText: string);
begin
  mmo9.Lines.Add(AStatusText);

end;

procedure TForm_main.idpsrvr1UDPRead(Sender: TObject; AData: TStream; ABinding: TIdSocketHandle);
var
  Data: TStringStream;
  funcode, ress: Byte;
  strs: TStrings;
  ii: SmallInt;
begin
  Data := TStringStream.Create('');
  Data.CopyFrom(AData, AData.Size);
  mmo9.Lines.Add(Data.DataString);
  if (edt53.hint = '9000') then begin       //收到二次开发响应
    Stat1.Panels[3].Text := '接收到UDP指令：' + Data.DataString;
    if Data.DataString = '@reset' then begin
      btn26Click(self);
    end
    else if Data.DataString = '@start' then begin
      if not TabSheet2.Showing then begin
        TabSheet2.TabVisible := True;
        PageControl1.TabIndex := 0;   //监控界面
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        for ii := 0 to 50 do begin
          if (label11.Caption = 'Loading...') then begin
            Sleep(100);
            application.ProcessMessages;
          end;
        end;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
        Sleep(100);
        application.ProcessMessages;
      end;
      btn25Click(self);
    end
    else if Copy(Data.DataString, 0, 4) = '@sn:' then begin
      edit1.Text := Copy(Data.DataString, 5, Length(Data.DataString));
    end
    else if (Copy(Data.DataString, 0, 8) = '@import:') and not formLOad.Showing then begin
      if Pos('.txt', Data.DataString) > 0 then
        FormLoad.edt1.text := Copy(Data.DataString, 9, Length(Data.DataString))      //测试文件所在路径+文件名；
      else
        FormLoad.edt1.text := Copy(Data.DataString, 9, Length(Data.DataString)) + '\';  //测试文件所在路径
      if FileExists(FormLoad.edt1.text) or DirectoryExists(FormLoad.edt1.text) then begin
        if ((label11.Caption <> 'Ready') and (label11.Caption <> 'PN错误')) or (working > 0) then begin    //没有通讯读到版本号或者工作忙，不执行
          btn26Click(form_main);              //加载之前先复位？
          tmr1Timer(Self);
          Application.ProcessMessages;       //定时发送复位命令
          Sleep(100);
          Application.ProcessMessages;       //接收复位命令回传
          Sleep(200);
          Application.ProcessMessages;        //接收复位后ready命令；
          Sleep(200);
          Application.ProcessMessages;        //接收复位后ready命令；
        end;
        formLOad.BorderStyle := bsNone;
         //formLOad.btn3.Visible:=false;    //加载按钮禁止！
         //formLOad.edt4.Visible:=true;     //可扫工单条码，加载程序；
        formLOad.ShowModal;
      end;
    end
    else begin         //询问返回

      if (pos('*ET.OK', Data.DataString) = 1) or (pos('*ET.ok', Data.DataString) = 1) then begin
        sbuf[2] := 5;
        sbuf[3] := $50;
        sbuf[4] := $00;
        sbuf[5] := $FF;
        sbuf[6] := $00;
        send_len := 6;
        try
          send_crcdata(send_len, 2);
        except
          messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);
        end;
      end
      else if (pos('pass', Data.DataString) = 1) or (pos('PASS', Data.DataString) = 1) then
        idpclnt1.Tag := 1
      else if (pos('fail', Data.DataString) = 1) or (pos('FAIL', Data.DataString) = 1) then
        idpclnt1.Tag := 2
      else if (pos('python.UDP.ready', Data.DataString) = 1) then
        idpclnt1.Tag := 101
      else
        idpclnt1.Tag := 3;
    end;
  end
  else if (edt53.hint = '9050') or (edt53.hint = '9051') or (edt53.hint = '9052') or (edt53.hint = '9053') or (edt53.hint = '9054') then begin                                                              //上海定制.条码读取
    strs := TStringList.Create;
    strs.Delimiter := ',';
    strs.CommaText := Data.DataString;
    if (lbl177.Caption = '工位1 ：') then begin        //四工位系统第1工位    (strs.Count=4)and
      edit1.Text := strs[0];
    end
    else //if strs.Count=1 then
      edit1.Text := Data.DataString;               //通用系统，或四工位系统其它工位；
    if (edt53.hint = '9050') and (lbl177.Caption = '工位1 ：') then begin         //收到服务器的数据，然后发送给其它4个工位
      idpclnt1.Active := true;
      if strs.Count > 1 then begin
        idpclnt1.Send(Edt100.Text, 9051, strs[0]);    //udp发送 ，实际不用
        idpclnt1.Send(Edt100.Text, 9052, strs[1]);    //udp发送
        idpclnt1.Send(Edt100.Text, 9053, strs[2]);    //udp发送
        idpclnt1.Send(Edt100.Text, 9054, strs[3]);    //udp发送
      end
      else begin
        idpclnt1.Send(Edt100.Text, 9051, edit1.Text);    //udp发送  ，实际不用
        idpclnt1.Send(Edt100.Text, 9052, edit1.Text);    //udp发送
        idpclnt1.Send(Edt100.Text, 9053, edit1.Text);    //udp发送
        idpclnt1.Send(Edt100.Text, 9054, edit1.Text);    //udp发送
      end;
    end;
    strs.Destroy;

    if ((Readchis('result', 'UDPBarcodeStart') = '1') or (Readchis('result', 'UDPBarcodeStart') = 'ON'))              //用收到UDP条码来启动测试
      and (Trim(edit1.Text) <> '')    //条码不为空
      then begin
      modbusfun05 := 16;
      modbusfun05dat := $FF;
    end;
  end;
  Data.Free;
end;

procedure TForm_main.idpsrvr1Status(ASender: TObject; const AStatus: TIdStatus; const AStatusText: string);
begin
  mmo9.Lines.Add(AStatusText);

end;

procedure TForm_main.F3Click(Sender: TObject);
var
  save_file_name: string;
  ResultList: TStringList;
  ii, jj, temp16: SmallInt;
begin
  if dlgOpen1.Execute then begin
    sprgrsbr1.Position := 0;

    save_file_name := dlgOpen1.FileName;
    ResultList := TStringList.Create;
    try
      ResultList.LoadFromFile(save_file_name);
      sprgrsbr1.Max := ResultList.Count;
      sprgrsbr1.Position := sprgrsbr1.Position + 1;
      for ii := 0 to ResultList.Count - 1 do begin
        ResultList[ii] := StringReplace(ResultList[ii], '(', '', [rfReplaceAll]);
        ResultList[ii] := StringReplace(ResultList[ii], ')', '', [rfReplaceAll]);
        ResultList[ii] := StringReplace(ResultList[ii], '[', '', [rfReplaceAll]);
        ResultList[ii] := StringReplace(ResultList[ii], ']', '', [rfReplaceAll]);
        ResultList[ii] := StringReplace(ResultList[ii], ',', char(#9), [rfReplaceAll]);
      end;
      ResultList.SaveToFile(dlgOpen1.FileName + '(tab).txt');
      ResultList.Free;
    except
      ShowMessage('生成统一测试文件(tab分割符)出错！');
    end;
  end;
end;

procedure TForm_main.F5Click(Sender: TObject);
begin
  formcompare.ShowModal;
end;

procedure TForm_main.btn5Click(Sender: TObject);
begin
  if Readchis('Comm Para', 'ExeCommand') <> '' then begin
    winexec(pchar(Readchis('Comm Para', 'ExeCommand')), SW_MINIMIZE);

    mmo9.Lines.Add(Readchis('Comm Para', 'ExeCommand'));
  end;
end;

procedure TForm_main.btn6Click(Sender: TObject);
begin
  U4Click(Self);
end;

procedure TForm_main.mmo9DblClick(Sender: TObject);
var 
//  f: TextFile;
  Dir: string;

  procedure SaveUTF8File(AContent: WideString; AFileName: string);
  var
    ffileStream: TFileStream;
    futf8Bytes: string;
    S: string;
  begin
    ffileStream := TFileStream.Create(AFileName, fmCreate);
    futf8Bytes := UTF8Encode(AContent);
    S := #$EF#$BB#$BF;
    ffileStream.Write(S[1], Length(S));
    ffileStream.Write(futf8Bytes[1], Length(futf8Bytes));
    ffileStream.Free;
  end;

begin
  Dir := '';
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    SaveUTF8File(mmo9.text, Dir + '\udpDemo.py');
        //writeln(f, mmo9.text);
      //  Closefile(f);
      //  Memo1.Lines.SaveToFile(dir+'\udpDemo1.py', TEncoding.UTF8);
    Writechis('Comm Para', 'ExeCommand', 'python.exe ' + Dir + '\udpDemo.py');
  end;
end;

procedure TForm_main.y1Click(Sender: TObject);
begin
  if y1.Checked or c11.Checked then begin
    if FileExists(lbl29.Caption) then
      mmo3.Lines.SaveToFile(lbl29.Caption);

    case Application.MessageBox('立即导入程序->测控主板进行调试吗？Import now?', '提示', MB_YESNOCANCEL + MB_ICONQUESTION) of
      IDYES:
        begin
          begin
            btn23Click(Self);
          end;

          y1.Checked := False;
          s1.Enabled := false;
          s5.Enabled := False;
          Find1.Enabled := False;
          FindNext1.Enabled := False;
          Replace1.Enabled := False;
          N13.Visible := true;
          S2.Visible := true;
          x1.Visible := true;
          N15.Visible := true;

          pgc3.Visible := false;
          mmo3.Align := alRight;
          mmo3.Width := 100;
          TabSheet4.Caption := 'Debug调试(联机)';
          Panel1.Visible := true;
          Panel1.Align := alClient;
          Panel2.Visible := true;
          //pnl1.Visible:=true;
          pnl1.Height := 188;
          btn120.Caption := 'X 隐藏';
          N38.Caption := 'Close SysWin 关闭系统参数显示';
        end;
      IDNO:
        begin
          y1.Checked := False;
          s1.Enabled := false;
          s5.Enabled := False;
          Find1.Enabled := False;
          FindNext1.Enabled := False;
          Replace1.Enabled := False;
          N13.Visible := true;
          S2.Visible := true;
          x1.Visible := true;
          N15.Visible := true;

          pgc3.Visible := false;
          mmo3.Align := alRight;
          mmo3.Width := 100;
          TabSheet4.Caption := 'Debug调试(联机)';
          Panel1.Visible := true;
          Panel1.Align := alClient;
          Panel2.Visible := true;
          //pnl1.Visible:=true;
          pnl1.Height := 188;
          btn120.Caption := 'X 隐藏';
          N38.Caption := 'Close SysWin 关闭系统参数显示';
        end;
    else
    end;
  end
  else begin
    case Application.MessageBox('立即从测控主板->导出程序编辑吗？Export now?', '提示', MB_YESNOCANCEL + MB_ICONQUESTION) of
      IDYES:
        begin
          y1.Checked := true;
          s1.Enabled := true;
          s5.Enabled := true;
          Find1.Enabled := true;
          FindNext1.Enabled := true;
          Replace1.Enabled := true;
          N13.Visible := not y1.Checked;
          S2.Visible := not y1.Checked;
          x1.Visible := not y1.Checked;
          N15.Visible := not y1.Checked;
          btn24Click(Self);
        end;
      ID_NO:
        if FileExists(lbl29.Caption) then begin
          y1.Checked := true;
          s1.Enabled := true;
          s5.Enabled := true;
          Find1.Enabled := true;
          FindNext1.Enabled := true;
          Replace1.Enabled := true;
          N13.Visible := not y1.Checked;
          S2.Visible := not y1.Checked;
          x1.Visible := not y1.Checked;
          N15.Visible := not y1.Checked;

          mmo3.Visible := true;
          mmo3.Align := alClient;
          TabSheet4.Caption := 'Code编辑(脱机)';
          mmo3.Lines.LoadFromFile(lbl29.Caption);
          Panel1.Visible := False;
          Panel2.Visible := False;
          //pnl1.Visible:=False;
          pnl1.Height := 22;
          btn120.Caption := '^^显示';
          N38.Caption := 'Open SysWin 开启系统参数显示';
        end
        else
          ShowMessage('没有可编辑的Code(代码文件)!请先<导出测控程序>或使用下面<demo程序>');
    else
    end;
  end;
end;

procedure TForm_main.S1Click(Sender: TObject);
begin
  if FileExists(lbl29.Caption) then begin
    mmo3.Lines.SaveToFile(lbl29.Caption);
    if y1.Checked and mmo3.Showing then
      TabSheet4.Caption := 'Code编辑(脱机)';
    if C11.Checked and mmo3.Showing then
      TabSheet4.Caption := 'Config.ini配置';
  end
  else
    ShowMessage('文件路径不存在');
end;

procedure TForm_main.W1Click(Sender: TObject);
var
  f: TextFile;
  str, Dir: string;
begin
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    lbl29.Caption := '正使用向导生成测控程序demo';

    if FileExists(ExtractFilePath(application.ExeName) + 'zhiEdit.exe')   //
      or (winexec(pchar(Readchis('Comm Para', 'zhiEdit')), SW_NORMAL) >= 32) then begin
      lbl29.Caption := Dir + '\zhi.h';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
    end
    else begin
      lbl29.Caption := Dir + '\zhi.txt';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
    end;
    y1.Checked := true;
    s1.Enabled := true;
    s5.Enabled := true;
    Find1.Enabled := true;
    FindNext1.Enabled := true;
    Replace1.Enabled := true;
    N13.Visible := not y1.Checked;
    S2.Visible := not y1.Checked;
    x1.Visible := not y1.Checked;
    N15.Visible := not y1.Checked;
    mmo3.Visible := true;
    mmo3.Align := alClient;
    mmo3.Clear;
    pgc3.Visible := true;
    TabSheet4.Caption := 'Code编辑(脱机)';
    Panel1.Visible := False;
    Panel2.Visible := False;
    //pnl1.Visible:=False;
    pnl1.Height := 22;
    btn120.Caption := '^^显示';
    N38.Caption := 'Open SysWin 开启系统参数显示';
  end;
end;

procedure TForm_main.C3Click(Sender: TObject);
var
  f: TextFile;
  str, Dir: string;
begin
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    lbl29.Caption := '生成CT5测控程序demo';

    mmo3.Clear;
    mmo3.Text := CT5.Text;

    if (FileExists(ExtractFilePath(application.ExeName) + 'zhiEdit.exe')) or (winexec(pchar(Readchis('Comm Para', 'zhiEdit')), SW_NORMAL) >= 32) then begin
      lbl29.Caption := Dir + '\CT5demo.h';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
      mmo3.Lines.SaveToFile(lbl29.Caption);
      if winexec(pchar(ExtractFilePath(application.ExeName) + 'zhiEdit.exe ' + lbl29.Caption), SW_NORMAL) < 32 then begin
        winexec(pchar(Readchis('Comm Para', 'zhiEdit') + ' ' + lbl29.Caption), SW_NORMAL)
      end;
    end
    else begin
      lbl29.Caption := Dir + '\CT5demo.txt';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
      mmo3.Lines.SaveToFile(lbl29.Caption);
      y1.Checked := true;
      s1.Enabled := true;
      s5.Enabled := true;
      Find1.Enabled := true;
      FindNext1.Enabled := true;
      Replace1.Enabled := true;
      N13.Visible := not y1.Checked;
      S2.Visible := not y1.Checked;
      x1.Visible := not y1.Checked;
      N15.Visible := not y1.Checked;
      mmo3.Visible := true;
      mmo3.Align := alClient;
      TabSheet4.Caption := 'Code编辑(脱机)';
      Panel1.Visible := False;
      Panel2.Visible := False;
      //pnl1.Visible:=False;
      pnl1.Height := 22;
      btn120.Caption := '^^显示';
      N38.Caption := 'Open SysWin 开启系统参数显示';
    end;

  end;
end;

procedure TForm_main.mmo3DblClick(Sender: TObject);
begin
  if y1.Checked then
    y1Click(Self)
  else if c11.Checked then
    c11Click(Self)
  else if Pos('.ini', lbl29.Caption) > 0 then
    c11Click(Self)
  else
    y1Click(Self)
end;

procedure TForm_main.C6Click(Sender: TObject);
var
  f: TextFile;
  str, Dir: string;
begin
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    lbl29.Caption := '生成CT6测控程序demo';

    mmo3.Clear;
    mmo3.Text := CT6.Text;

    if (FileExists(ExtractFilePath(application.ExeName) + 'zhiEdit.exe')) or (winexec(pchar(Readchis('Comm Para', 'zhiEdit')), SW_NORMAL) >= 32) then begin
      lbl29.Caption := Dir + '\CT6demo.h';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
      mmo3.Lines.SaveToFile(lbl29.Caption);
      if winexec(pchar(ExtractFilePath(application.ExeName) + 'zhiEdit.exe ' + lbl29.Caption), SW_NORMAL) < 32 then begin
        winexec(pchar(Readchis('Comm Para', 'zhiEdit') + ' ' + lbl29.Caption), SW_NORMAL)
      end;
    end
    else begin
      lbl29.Caption := Dir + '\CT6demo.txt';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
      mmo3.Lines.SaveToFile(lbl29.Caption);
      y1.Checked := true;
      s1.Enabled := true;
      s5.Enabled := true;
      Find1.Enabled := true;
      FindNext1.Enabled := true;
      Replace1.Enabled := true;
      N13.Visible := not y1.Checked;
      S2.Visible := not y1.Checked;
      x1.Visible := not y1.Checked;
      N15.Visible := not y1.Checked;
      mmo3.Visible := true;
      mmo3.Align := alClient;
      TabSheet4.Caption := 'Code编辑(脱机)';
      Panel1.Visible := False;
      Panel2.Visible := False;
      //pnl1.Visible:=False;
      pnl1.Height := 22;
      btn120.Caption := '^^显示';
      N38.Caption := 'Open SysWin 开启系统参数显示';
    end;

  end;
end;

procedure TForm_main.CT7DClick(Sender: TObject);
var
  f: TextFile;
  str, Dir: string;
begin
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin

    lbl29.Caption := '生成CT7测控程序demo';
    mmo3.Clear;
    mmo3.Text := CT7.Text;

    if (FileExists(ExtractFilePath(application.ExeName) + 'zhiEdit.exe')) or (winexec(pchar(Readchis('Comm Para', 'zhiEdit')), SW_NORMAL) >= 32) then begin
      lbl29.Caption := Dir + '\CT7demo.h';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
      mmo3.Lines.SaveToFile(lbl29.Caption);
      if winexec(pchar(ExtractFilePath(application.ExeName) + 'zhiEdit.exe ' + lbl29.Caption), SW_NORMAL) < 32 then begin
        winexec(pchar(Readchis('Comm Para', 'zhiEdit') + ' ' + lbl29.Caption), SW_NORMAL)
      end;
    end
    else begin
      lbl29.Caption := Dir + '\CT7demo.txt';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
      mmo3.Lines.SaveToFile(lbl29.Caption);
      y1.Checked := true;
      s1.Enabled := true;
      s5.Enabled := true;
      Find1.Enabled := true;
      FindNext1.Enabled := true;
      Replace1.Enabled := true;
      N13.Visible := not y1.Checked;
      S2.Visible := not y1.Checked;
      x1.Visible := not y1.Checked;
      N15.Visible := not y1.Checked;
      mmo3.Visible := true;
      mmo3.Align := alClient;
      TabSheet4.Caption := 'Code编辑(脱机)';
      Panel1.Visible := False;
      Panel2.Visible := False;           //pnl1.Visible:=False;
      pnl1.Height := 22;
      btn120.Caption := '^^显示';
      N38.Caption := 'Open SysWin 开启系统参数显示';
    end;

  end;
end;

procedure TForm_main.C8Click(Sender: TObject);
var
  f: TextFile;
  str, Dir: string;
begin
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    lbl29.Caption := '生成CT8测控程序demo';

    mmo3.Clear;
    mmo3.Text := CT8.Text;

    if (FileExists(ExtractFilePath(application.ExeName) + 'zhiEdit.exe')) or (winexec(pchar(Readchis('Comm Para', 'zhiEdit')), SW_NORMAL) >= 32) then begin
      lbl29.Caption := Dir + '\CT8demo.h';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
      mmo3.Lines.SaveToFile(lbl29.Caption);
      if winexec(pchar(ExtractFilePath(application.ExeName) + 'zhiEdit.exe ' + lbl29.Caption), SW_NORMAL) < 32 then begin
        winexec(pchar(Readchis('Comm Para', 'zhiEdit') + ' ' + lbl29.Caption), SW_NORMAL)
      end;
    end
    else begin
      lbl29.Caption := Dir + '\CT8demo.txt';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
      mmo3.Lines.SaveToFile(lbl29.Caption);
      y1.Checked := true;
      s1.Enabled := true;
      s5.Enabled := true;
      Find1.Enabled := true;
      FindNext1.Enabled := true;
      Replace1.Enabled := true;
      N13.Visible := not y1.Checked;
      S2.Visible := not y1.Checked;
      x1.Visible := not y1.Checked;
      N15.Visible := not y1.Checked;
      mmo3.Visible := true;
      mmo3.Align := alClient;
      TabSheet4.Caption := 'Code编辑(脱机)';
      Panel1.Visible := False;
      Panel2.Visible := False;
      //pnl1.Visible:=False;
      pnl1.Height := 22;
      btn120.Caption := '^^显示';
      N38.Caption := 'Open SysWin 开启系统参数显示';
    end;
  end;
end;

procedure TForm_main.C9Click(Sender: TObject);
var
  f: TextFile;
  str, Dir: string;
begin
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    lbl29.Caption := '生成CT9测控程序demo';

    mmo3.Clear;
    mmo3.Text := CT9.Text;

    if (FileExists(ExtractFilePath(application.ExeName) + 'zhiEdit.exe')) or (winexec(pchar(Readchis('Comm Para', 'zhiEdit')), SW_NORMAL) >= 32) then begin
      lbl29.Caption := Dir + '\CT9demo.h';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
      mmo3.Lines.SaveToFile(lbl29.Caption);
      if winexec(pchar(ExtractFilePath(application.ExeName) + 'zhiEdit.exe ' + lbl29.Caption), SW_NORMAL) < 32 then begin
        winexec(pchar(Readchis('Comm Para', 'zhiEdit') + ' ' + lbl29.Caption), SW_NORMAL)
      end;
    end
    else begin
      lbl29.Caption := Dir + '\QCT9demo.txt';
      try
        assignfile(f, lbl29.Caption);
        rewrite(f);
      finally
        Closefile(f);
      end;
      mmo3.Lines.SaveToFile(lbl29.Caption);
      y1.Checked := true;
      s1.Enabled := true;
      s5.Enabled := true;
      Find1.Enabled := true;
      FindNext1.Enabled := true;
      Replace1.Enabled := true;
      N13.Visible := not y1.Checked;
      S2.Visible := not y1.Checked;
      x1.Visible := not y1.Checked;
      N15.Visible := not y1.Checked;
      mmo3.Visible := true;
      mmo3.Align := alClient;
      TabSheet4.Caption := 'Code编辑(脱机)';
      Panel1.Visible := False;
      Panel2.Visible := False;
      //pnl1.Visible:=False;
      pnl1.Height := 22;
      btn120.Caption := '^^显示';
      N38.Caption := 'Open SysWin 开启系统参数显示';
    end;
  end;
end;

procedure TForm_main.S5Click(Sender: TObject);
var
  f: TextFile;
begin
  with TSaveDialog.Create(nil) do begin
    Filter := '文本文档(*.txt)|*.txt|所有文件(*.*)|*.*';
    FileName := '*.txt';
    if Execute then begin
      lbl29.Caption := FileName;
      if FileExists(FileName) then begin
        if Application.MessageBox('文件已存在，要替换吗？', '提示', MB_YESNOCANCEL + MB_ICONQUESTION) = IDYES then begin
          mmo3.Lines.SaveToFile(lbl29.Caption);
        end;
      end
      else begin
        try
          assignfile(f, lbl29.Caption);
          rewrite(f);
        finally
          Closefile(f);
        end;
        mmo3.Lines.SaveToFile(lbl29.Caption);
      end;
      if y1.Checked then
        TabSheet4.Caption := 'Code编辑(脱机)';
    end;
  end;
end;

procedure TForm_main.chk12Click(Sender: TObject);
var
  step: SmallInt;
begin
  mmo3.Clear;
  mmo3.lines.add('{//序号	0800	标准值V	测控值S	上限Up%	下限Lo%	高点H	低点L	前延时T	模式M	比例K%	偏移B	平均P次	联板号L	高点H2	低点L2	Notes备注->>>	固件:v800.00');
  step := 1;
  if chk20.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-	0	0	0	0	0	0	5	AZV0	100	0	0	256	0	0	pppppppp');
    step := step + 1;
  end
  else if chk12.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-	0	0	0	0	0	0	5	AZV0	100	0	0	0	0	0	pppppppp');
    step := step + 1;
  end;
  if chk13.Checked then begin
    if step = 1 then begin
      mmo3.lines.add(inttostr(step) + #9 + '-	0	0	0	0	0	0	5	AZV0	100	0	0	0	0	0	pppppppp');
      step := step + 1;
    end;
    mmo3.lines.add(inttostr(step) + #9 + '-	0	0	0	0	0	0	5	BZV0	100	0	0	10	1	0	ababab');
    step := step + 1;
  end;
  if chk21.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-	6	0	0	0	0	0	5	0ZB0	100	0	0	0	3000	0	ng鸣叫6次+程控3V电压');
    step := step + 1;
  end;
  if chk22.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-	384	0	0	0	0	0	5	1ZU0	100	0	0	0	0	0	串口1波特率38400');
    step := step + 1;
  end;
  if chk23.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-	10	0	0	0	0	9986	5	0ZE0	100	0	0	0	0	0	急停信号9986');
    step := step + 1;
  end;
  if chk37.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-	0	0	0	0	0	0	5	VZV0	100	0	25	0	0	1000	双启动信号ST+DI5');
    step := step + 1;
  end;
  if chk39.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-	1	0	0	3000	0	0	5	0ZP0	100	0	0	0	0	0	优先开短路显示');
    step := step + 1;
  end;

  if chk40.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-	0	0	0	0	0	0	5	CFQ0	100	0	0	0	0	0	选取继电器开关板');
    step := step + 1;
  end;
  if chk14.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-	0	0	0	0	3	0	5	1QQ0	100	0	0	0	0	0	开启DO1');
    step := step + 1;
  end;
  if chk41.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '#	0	0	0	0	9995	0	0	1W02	100	0	0	0	0	0	等待到位信号');
    step := step + 1;
  end;
  if chk42.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '#	0	0	2	0	9994	0	5	1WJ2	100	0	0	0	0	0	9994到位向后跳2步');
    step := step + 1;
  end;

  if chk47.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + 'OPEN	50	0	0	0	0	0	5	0KM1	100	0	0	0	0	0	0');
    step := step + 1;
  end;
  if chk48.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + 'SHOR	200	0	0	0	0	0	5	0SM1	100	0	0	0	0	0	0');
    step := step + 1;
  end;

  if chk17.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + 'R1	100	0	10	10	1	2	5	0R01	100	0	0	0	0	0	r');
    step := step + 1;
  end;
  if chk43.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + 'R2	100	0	10	10	3	4	5	1R01	100	0	0	0	0	0	r');
    step := step + 1;
  end;
  if chk44.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + 'R3	100	0	10	10	5	6	5	2R01	100	0	0	0	0	0	r');
    step := step + 1;
  end;
  if chk46.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + 'D1	700	0	40	40	1	2	5	0D01	100	0	0	0	0	0	D');
    step := step + 1;
  end;
  if chk45.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + 'C1	100	0	30	30	1	2	5	3C11	100	0	0	0	0	0	C');
    step := step + 1;
  end;

  if chk15.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-u	0	0	0	0	0	0	50	CBU0	100	0	0	0	0	0	0');
    step := step + 1;
  end;
  if chk16.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-u	0	0	0	0	0	0	50	2BU0	100	0	0	0	0	0	0');
    step := step + 1;
  end;
  if chk52.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '-u	0	0	0	0	0	0	50	GBP0	100	0	0	0	0	0	0');
    step := step + 1;
  end;

  if chk49.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + 'ngj	0	0	2	0	0	0	5	NZJ0	100	0	0	0	0	0	0');
    step := step + 1;
  end;
  if chk50.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + 'okj	0	0	2	0	0	0	5	OZJ0	100	0	0	0	0	0	0');
    step := step + 1;
  end;
  if chk51.Checked then begin
    mmo3.lines.add(inttostr(step) + #9 + '--	0	0	0	19	20	0	5	1QG0	100	0	0	2	0	0	0');
    step := step + 1;
  end;

  mmo3.lines.add('9999	****	0	0	0	0	1	0	5	0000	100	0	0	0	0	0	程序结束}');
  mmo3.lines.add('{//对比	$找点数	$找点阀	/*O/S数	O/S阀值	@V版本	485地址	O/S总时	F1点	O/S充时	F2点O/S	F2时*/	S机台号	N机种名	$使能位	@只读p	@固件');
  if chk53.Checked then begin
    mmo3.lines.add('1	32	200	6	50	800	10	50	0	50	0	0	1	zhi.cn	65	61	v800.00}');
  end
  else
    mmo3.lines.add('1	32	200	6	50	800	10	50	0	50	0	0	1	zhi.cn	64	61	v800.00}');
  mmo3.lines.add('{//Open/Short拓扑群:');
  mmo3.lines.add('1');
  mmo3.lines.add('2');
  mmo3.lines.add('3	4');
  mmo3.lines.add('5');
  mmo3.lines.add('6');
end;

procedure TForm_main.O3Click(Sender: TObject);
begin
  with TOpenDialog.Create(nil) do begin
    if FileExists(ExtractFilePath(application.ExeName) + 'zhiEdit.exe')   //
      or (winexec(pchar(Readchis('Comm Para', 'zhiEdit')), SW_NORMAL) >= 32) then begin
      Filter := '文本文档(*.h)|*.h|所有文件(*.*)|*.*';
      FileName := '*.h';
      if Execute then begin
        lbl29.Caption := FileName;
        if winexec(pchar(ExtractFilePath(application.ExeName) + 'zhiEdit.exe ' + lbl29.Caption), SW_NORMAL) < 32 then begin
          winexec(pchar(Readchis('Comm Para', 'zhiEdit') + ' ' + lbl29.Caption), SW_NORMAL)
        end;
      end;
    end
    else begin
      Filter := '文本文档(*.txt)|*.txt|所有文件(*.*)|*.*';
      FileName := '*.txt';
      if Execute then begin
        if Pos('config.ini', FileName) > 0 then
          c11.Checked := true
        else
          y1.Checked := true;
        N13.Visible := not y1.Checked;       //四个编辑主菜单
        S2.Visible := not y1.Checked;
        x1.Visible := not y1.Checked;
        N15.Visible := not y1.Checked;
        s1.Enabled := true;
        s5.Enabled := true;
        Find1.Enabled := true;
        FindNext1.Enabled := true;
        Replace1.Enabled := true;

        mmo3.Visible := true;
        mmo3.Align := alClient;
        TabSheet4.Caption := 'Code编辑(脱机)';
        Panel1.Visible := False;
        Panel2.Visible := False;           //pnl1.Visible:=False;
        pnl1.Height := 22;
        btn120.Caption := '^^显示';
        N38.Caption := 'Open SysWin 开启系统参数显示';

        lbl29.Caption := FileName;
        mmo3.Lines.LoadFromFile(lbl29.Caption);      //  Memo1.ReadOnly := ofReadOnly in Options;
      end;
    end;
  end;
end;

procedure TForm_main.mmo3Change(Sender: TObject);
begin
  Find1.Enabled := (mmo3.Text <> '');
  FindNext1.Enabled := (mmo3.Text <> '') or (FindStr <> '');
  Replace1.Enabled := (mmo3.Text <> '');

  if y1.Checked and mmo3.Showing then
    TabSheet4.Caption := 'Code编辑(脱机)*';
  if C11.Checked and mmo3.Showing then
    TabSheet4.Caption := 'Config.ini配置*';
end;

procedure TForm_main.Find1Click(Sender: TObject);
begin
  with FindDialog1 do begin
    Left := Self.Left + 100;
    Top := Self.Top + 150;
    FindText := mmo3.SelText;
    Execute;
  end;
end;

procedure TForm_main.FindDialog1Find(Sender: TObject);
begin
  with Sender as TFindDialog do begin
    FindStr := FindText;
    if not SearchMemo(mmo3, FindText, Options) then
      showmessage('找不到:' + FindText); //MessageBox(Handle,'找不到:'+FindText, 'code', //PWideChar(Concat('找不到"', FindText, '"'))
      //  MB_ICONINFORMATION);
  end;
end;

procedure TForm_main.ReplaceDialog1Replace(Sender: TObject);
var
  Found: Boolean;
begin
  with ReplaceDialog1 do begin
    { Replace }
    if (frReplace in Options) and (mmo3.SelText = FindText) then
      mmo3.SelText := ReplaceText;
    Found := SearchMemo(mmo3, FindText, Options);
  
    { Replace All }
    if (frReplaceAll in Options) then begin
      mmo3.SelStart := 0;
      while Found do begin
        if (mmo3.SelText = FindText) then
          mmo3.SelText := ReplaceText;
        Found := SearchMemo(mmo3, FindText, Options);
      end;
      if not Found then
        SendMessage(mmo3.Handle, WM_VSCROLL, SB_TOP, 0);
    end;

    if (not Found) and (frReplace in Options) then
      showmessage('找不到:' + FindText); //MessageBox(Handle, PWideChar(Concat('找不到"', FindText, '"')), '记事本',
      //  MB_ICONINFORMATION);
  end;
end;

procedure TForm_main.ReplaceDialog1Find(Sender: TObject);
begin
  with Sender as TReplaceDialog do
    if not SearchMemo(mmo3, FindText, Options) then
      ShowMessage('找不到：' + FindText); //MessageBox(Handle, PWideChar(Concat('找不到"', FindText, '"')), '记事本',
     //   MB_ICONINFORMATION);
end;

procedure TForm_main.Replace1Click(Sender: TObject);
begin
  with ReplaceDialog1 do begin
    Left := Self.Left + 100;
    Top := Self.Top + 150;
    FindText := mmo3.SelText;
    Execute;
  end;
end;

procedure TForm_main.FindNext1Click(Sender: TObject);
begin
  if not SearchMemo(Mmo3, FindStr, FindDialog1.Options) then
    showmessage('找不到:' + FindStr); //MessageBox(Handle, PWideChar(Concat('找不到"', FindStr, '"')), '记事本',
    //  MB_ICONINFORMATION);
end;

procedure TForm_main.c10Click(Sender: TObject);
var
  f: TextFile;
  str, Dir: string;
begin
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    lbl29.Caption := '正使用向导生成config.ini';
    c11.checked := true;
    s1.Enabled := true;
    s5.Enabled := true;
    Find1.Enabled := true;
    FindNext1.Enabled := true;
    Replace1.Enabled := true;

    mmo3.Visible := true;
    mmo3.Align := alClient;
    mmo3.Clear;
    pgc4.Visible := true;
    TabSheet4.Caption := 'Config.ini配置';
    Panel1.Visible := False;
    Panel2.Visible := False;
    //pnl1.Visible:=False;
    pnl1.Height := 22;
    btn120.Caption := '^^显示';
    N38.Caption := 'Open SysWin 开启系统参数显示';

    lbl29.Caption := Dir + '\config.ini';
    try
      assignfile(f, lbl29.Caption);
      rewrite(f);
    finally
      Closefile(f);
    end;
  end;
end;

procedure TForm_main.C11Click(Sender: TObject);
begin
  if c11.Checked or y1.Checked then begin
    c11.Checked := False;
    s1.Enabled := false;
    s5.Enabled := False;
    Find1.Enabled := False;
    FindNext1.Enabled := False;
    Replace1.Enabled := False;
    pgc4.Visible := false;
    mmo3.Align := alRight;
    mmo3.Width := 100;
    TabSheet4.Caption := 'Debug调试(联机)';
    Panel1.Visible := true;
    Panel1.Align := alClient;
    Panel2.Visible := true;
    //pnl1.Visible:=true;
    pnl1.Height := 188;
    btn120.Caption := 'X 隐藏';
    N38.Caption := 'Close SysWin 关闭系统参数显示';
    if FileExists(lbl29.Caption) then begin
      mmo3.Lines.SaveToFile(lbl29.Caption);
      if Pos('.ini', lbl29.Caption) > 0 then
        ReadConfig_iniProcess(0);
    end;
  end
  else if FileExists(lbl29.Caption) then begin
    c11.Checked := true;
    s1.Enabled := true;
    s5.Enabled := true;
    Find1.Enabled := true;
    FindNext1.Enabled := true;
    Replace1.Enabled := true;
    mmo3.Visible := true;
    mmo3.Align := alClient;
    TabSheet4.Caption := 'Config.ini配置';
    mmo3.Lines.LoadFromFile(lbl29.Caption);
    Panel1.Visible := False;
    Panel2.Visible := False;
    //pnl1.Visible:=False;
    pnl1.Height := 22;
    btn120.Caption := '^^显示';
    N38.Caption := 'Open SysWin 开启系统参数显示';
  end
  else
    ShowMessage('没有可编辑的config.ini!请先使用<config.ini向导>');
end;

procedure TForm_main.chk54Click(Sender: TObject);
begin
  mmo3.Clear;
  mmo3.lines.add('[result]');
  if chk54.checked then
    mmo3.lines.add('OnePCSOneLine=1');
  if chk57.checked then
    mmo3.lines.add('top5=0');
  if chk58.checked then
    mmo3.lines.add('PassOverT=10');
  if chk59.checked then
    mmo3.lines.add('NGOverT=10');
  if chk60.checked then
    mmo3.lines.add('CloseReport=1');
  if chk61.checked then
    mmo3.lines.add('BarcodeStart=1');

  if chk55.checked then
    mmo3.lines.add('enPrint=8');
  if chk56.checked then
    mmo3.lines.add('PrintMaxRows=9');
  if chk79.checked then
    mmo3.lines.add('Row=2');
  if chk80.checked then
    mmo3.lines.add('Column=4');

  mmo3.lines.add('');
  mmo3.lines.add('[server]');
  if chk64.checked then
    mmo3.lines.add('StandardIP=www.zhzhi.cn');
  if chk65.checked then
    mmo3.lines.add('DirCommand=api/ettestrecords');

  mmo3.lines.add('');
  mmo3.lines.add('[Comm Para]');
  if chk62.checked then
    mmo3.lines.add('UDPport=9001')
  else if chk63.checked then
    mmo3.lines.add('UDPport=9002');
  if chk85.checked then
    mmo3.lines.add('ExeCommand=python.exe E:\udpDemo.py');

  if chk71.checked then
    mmo3.lines.add('CommOpen=1');
  if chk72.checked then begin
    mmo3.lines.add('upLocal=1');
    mmo3.lines.add('CSVen=12');
    mmo3.lines.add('ChildFolder=2');
  end;
  if chk66.checked then
    mmo3.lines.add('clearbarcode=1')
  else
    mmo3.lines.add('clearbarcode=2');
  if chk67.checked then
    mmo3.lines.add('UploadServerT=5');
  if chk68.checked then
    mmo3.lines.add('DebugMode=0')
  else
    mmo3.lines.add('DebugMode=1');
  if chk70.checked then
    mmo3.lines.add('PassWord=999');
  if chk69.checked then begin
  end;

  mmo3.lines.add('');
  mmo3.lines.add('[Name->Redef]');
  if chk73.checked then
    mmo3.lines.add('T1->name=temp');
  if chk74.checked then
    mmo3.lines.add('temp->Scale=10');
  if chk75.checked then
    mmo3.lines.add('temp->Unit=℃');

  mmo3.lines.add('');
  mmo3.lines.add('[Model Para]');

  if chk76.checked then
    mmo3.lines.add('BarCodeEdit=200');
  if chk77.checked then
    mmo3.lines.add('enPN=1');
  if chk78.checked then
    mmo3.lines.add('LoginItem1=新名字');
  if chk84.checked then
    mmo3.lines.add('LoginItem2dis=1');

  if chk81.checked then
    mmo3.lines.add('BarCodeLen=8');
  if chk82.checked then
    mmo3.lines.add('BarPos=5');
  if chk83.checked then
    mmo3.lines.add('BarChars=AB');

end;

procedure TForm_main.btn93Click(Sender: TObject);
begin
  //jiahe. Findbarcodefile(0);
end;

procedure TForm_main.edt3Click(Sender: TObject);
begin
  sysedit[3].Tag := 3;
  sysedit[3].OnClick(form_main);

end;

procedure TForm_main.edt12Click(Sender: TObject);
begin
  sysedit[3].Tag := 4;
  sysedit[4].OnClick(form_main);

end;

procedure TForm_main.edt13Click(Sender: TObject);
begin
  sysedit[3].Tag := 10;
  sysedit[10].OnClick(form_main);

end;

procedure TForm_main.btn7Click(Sender: TObject);
begin
  btn7.Tag := 2;
  grp23.Visible := False;
  modbusfun06dat := $00E0;
  modbusfun06 := $0001;
  btn19.Font.Color := clRed;
  mmo1.Lines.Clear;
  mmo1.Lines.Add('First O/S learning......');
  mmo1.Lines.Add('一次短路群学习中......');
  addToLog('学习一次短路群组');
end;

procedure TForm_main.btn19Click(Sender: TObject);
begin
  mmo1.Tag := 10 + VIRLCOFFSET;    //modbusfun05:=9; modbusfun05dat:=$FF;
  //重复了！form_main.mmo4.lines.add('保存短路群命令');
end;

procedure TForm_main.btn22Click(Sender: TObject);
begin
  if StrToInt(sysedit[3].Text) >= 4095 then begin
    ShowMessage('点数(>=4095)太多!，不支持二次学习！');
  end
  else begin
    btn7.Tag := 2;
    grp23.Visible := False;
    modbusfun06dat := $00E1;
    modbusfun06 := $0001;
    btn19.Font.Color := clRed;
    mmo1.Lines.Clear;
    mmo1.Lines.Add('First O/S learning......');
    mmo1.Lines.Add('二次短路群学习中......');
    addToLog('学习二次短路群组');
  end;
end;

procedure TForm_main.mmo1DblClick(Sender: TObject);
begin
  mmo1.Clear;
  mmo1.Hint := '';
  mmo1.Tag := 20 + VIRLCOFFSET;    //读取短路群
  form_main.mmo4.lines.add('读取短路群命令');
end;

procedure TForm_main.Panel1DblClick(Sender: TObject);
begin
  mmo3DblClick(Self);
end;

procedure TForm_main.mmo1Change(Sender: TObject);
begin
  btn19.Font.Color := clRed;
end;

procedure TForm_main.edt3ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
  Handled := True;       //右键不弹出
end;

procedure TForm_main.edt12ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
  Handled := True;       //右键不弹出
end;

procedure TForm_main.edt13ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
  Handled := True;       //右键不弹出
end;

procedure TForm_main.chk86Click(Sender: TObject);
begin
//  if not chk86.Focused then Exit;      //只有鼠标点击触发
//  mmo1GutterClick(Self);
{  if mmo1.WordWrapGlyph.Visible then begin
    mmo1.Options:=mmo1.Options - [eoShowSpecialChars];
    mmo1.WordWrapGlyph.Visible:=False;
    mmo1.Gutter.LeadingZeros:=False;
    chk86.Checked:=false;
  end else begin
    mmo1.Options:=mmo1.Options + [eoShowSpecialChars];
    mmo1.WordWrapGlyph.Visible:=True;
    mmo1.Gutter.LeadingZeros:=true;
    chk86.Checked:=True;
  end; }
end;

procedure TForm_main.A2Click(Sender: TObject);
begin
  FormAbout.show;
end;

procedure TForm_main.H2Click(Sender: TObject);
begin
  modbusfun06dat := StrToInt('9999');
  modbusfun06 := $2F20;
  addToLog('交换第' + edt45.Text + '步骤的高低点2');

end;

procedure TForm_main.A3Click(Sender: TObject);
begin
  MessageDlg('ASCII码对照表' + char($0D) + Char($0A) + 'Bin	Dec	Hex	缩写/字符	解释' + char($0D) + Char($0A) + '0000 0000	0	00	NUL	(null) 空字符' + char($0D) + Char($0A) + '0000 0001	1	01	SOH	(start of handing) 标题开始' + char($0D) + Char($0A) + '0000 0010	2	02	STX	(start of text) 正文开始' + char($0D) + Char($0A) + '0000 0011	3	03	ETX	(end of text) 正文结束         ' + char($0D) + Char($0A) + '0000 0100	4	04	EOT	(end of transmission) 传输结束 ' + char($0D) + Char($0A) +
    '0000 0101	5	05	ENQ	(enquiry) 请求                 ' + char($0D) + Char($0A) + '0000 0110	6	06	ACK	(acknowledge) 收到通知         ' + char($0D) + Char($0A) + '0000 0111	7	07	BEL	(bell) 响铃                    ' + char($0D) + Char($0A) + '0000 1000	8	08	BS	(backspace) 退格               ' + char($0D) + Char($0A) + '0000 1001	9	09	HT	(horizontal tab) 水平制表符    ' + char($0D) + Char($0A) + '0000 1010	10	0A	LF	(NL line feed, new line) 换行键  ' + char($0D) + Char($0A) +
    '0000 1011	11	0B	VT	(vertical tab) 垂直制表符        ' + char($0D) + Char($0A) + '0000 1100	12	0C	FF	(NP form feed, new page) 换页键  ' + char($0D) + Char($0A) + '0000 1101	13	0D	CR	(carriage return) 回车键         ' + char($0D) + Char($0A) + '0000 1110	14	0E	SO	(shift out) 不用切换             ' + char($0D) + Char($0A) + '0000 1111	15	0F	SI	(shift in) 启用切换              ' + char($0D) + Char($0A) + '0001 0000	16	10	DLE	(data link escape) 数据链路转义  ' + char($0D) + Char($0A) +
    '0001 0001	17	11	DC1	(device control 1) 设备控制1     ' + char($0D) + Char($0A) + '0001 0010	18	12	DC2	(device control 2) 设备控制2     ' + char($0D) + Char($0A) + '0001 0011	19	13	DC3	(device control 3) 设备控制3     ' + char($0D) + Char($0A) + '0001 0100	20	14	DC4	(device control 4) 设备控制4     ' + char($0D) + Char($0A) + '0001 0101	21	15	NAK	(negative acknowledge) 拒绝接收  ' + char($0D) + Char($0A) + '0001 0110	22	16	SYN	(synchronous idle) 同步空闲      ' + char($0D) + Char($0A) +
    '0001 0111	23	17	ETB	(end of trans. block) 传输块结束 ' + char($0D) + Char($0A) + '0001 1000	24	18	CAN	(cancel) 取消                    ' + char($0D) + Char($0A) + '0001 1001	25	19	EM	(end of medium) 介质中断         ' + char($0D) + Char($0A) + '0001 1010	26	1A	SUB	(substitute) 替补                ' + char($0D) + Char($0A) + '0001 1011	27	1B	ESC	(escape) 溢出                    ' + char($0D) + Char($0A) + '0001 1100	28	1C	FS	(file separator) 文件分割符      ' + char($0D) + Char($0A) + '0001 1101	29	1D	GS	(group separator) 分组符         ' + char($0D) + Char($0A) + '0001 1110	30	1E	RS	(record separator) 记录分离符    ' + char($0D) + Char($0A) + '0001 1111	31	1F	US	(unit separator) 单元分隔符      ', mtInformation, [mbOK], 0);
end;

procedure TForm_main.A4Click(Sender: TObject);
begin
  MessageDlg('ASCII码对照表' + char($0D) + Char($0A) + 'Bin	          Dec	Hex	字符/解释	　    Bin	          Dec    Hex	字符/解释' + char($0D) + Char($0A) + '0010 0000	32	20	空	　     0010 0001	          33	21	!  ' + char($0D) + Char($0A) + '0010 0010	34	22	”	　     0010 0011	          35	23	#	 ' + char($0D) + Char($0A) + '0010 0100	36	24	$	　      0010 0101	37	25	%	 ' + char($0D) + Char($0A) + '0010 0110	38	26	and　         0010 0111	39	27	’ ' + char($0D) + Char($0A) +
    '0010 1000	40	28	(	　      0010 1001	41	29	)	' + char($0D) + Char($0A) + '0010 1010	42	2A	*	　      0010 1011	43	2B	+	' + char($0D) + Char($0A) + '0010 1100	44	2C	,	　      0010 1101	45	2D	-	' + char($0D) + Char($0A) + '0010 1110	46	2E	.	　      0010 1111	47	2F	/	' + char($0D) + Char($0A) + '0011 0000	48	30	0	　      0011 0001	49	31	1	' + char($0D) + Char($0A) + '0011 0010	50	32	2	　      0011 0011	51	33	3	' + char($0D) + Char($0A) + '0011 0100	52	34	4	　      0011 0101	53	35	5	' + char($0D) + Char($0A)
    + '0011 0110	54	36	6	　      0011 0111	55	37	7	' + char($0D) + Char($0A) + '0011 1000	56	38	8	　      0011 1001	57	39	9	' + char($0D) + Char($0A) + '0011 1010	58	3A	:	　      0011 1011	59	3B	;	' + char($0D) + Char($0A) + '0011 1100	60	3C	<	　      0011 1101	61	3D	=	' + char($0D) + Char($0A) + '0011 1110	62	3E	>	　      0011 1111	63	3F	?	' + char($0D) + Char($0A) + '0100 0000	64	40	@	　      0100 0001	65	41	A	' + char($0D) + Char($0A) + '0100 0010	66	42	B	　      0100 0011	67	43	C	' + char($0D) + Char($0A)
    + '0100 0100	68	44	D	　      0100 0101	69	45	E	' + char($0D) + Char($0A) + '0100 0110	70	46	F	　      0100 0111	71	47	G	' + char($0D) + Char($0A) + '0100 1000	72	48	H	　      0100 1001	73	49	I	' + char($0D) + Char($0A) + '0100 1010	74	4A	J	　      0100 1011	75	4B	K	' + char($0D) + Char($0A) + '0100 1100	76	4C	L	　      0100 1101	77	4D	M	' + char($0D) + Char($0A) + '0100 1110	78	4E	N	　      0100 1111	79	4F	O	' + char($0D) + Char($0A) + '0101 0000	80	50	P	　      0101 0001	81	51	Q	' + char($0D) + Char($0A)
    + '0101 0010	82	52	R	　      0101 0011	83	53	S	' + char($0D) + Char($0A) + '0101 0100	84	54	T	　      0101 0101	85	55	U	' + char($0D) + Char($0A) + '0101 0110	86	56	V	　      0101 0111	87	57	W	' + char($0D) + Char($0A) + '0101 1000	88	58	X	　      0101 1001	89	59	Y	' + char($0D) + Char($0A) + '0101 1010	90	5A	Z	　      0101 1011	91	5B	[	' + char($0D) + Char($0A) + '0101 1100	92	5C	/	　      0101 1101	93	5D	]	' + char($0D) + Char($0A) + '0101 1110	94	5E	^	　      0101 1111	95	5F	_	' + char($0D) + Char($0A)
    + '0110 0000	96	60	`	　      0110 0001	97	61	a	' + char($0D) + Char($0A) + '0110 0010	98	62	b	　      0110 0011	99	63	c	' + char($0D) + Char($0A) + '0110 0100	100	64	d	　      0110 0101	101	65	e	' + char($0D) + Char($0A) + '0110 0110	102	66	f	　      0110 0111	103	67	g	' + char($0D) + Char($0A) + '0110 1000	104	68	h	　      0110 1001	105	69	i	' + char($0D) + Char($0A) + '0110 1010	106	6A	j	　      0110 1011	107	6B	k	' + char($0D) + Char($0A) + '0110 1100	108	6C	l	　      0110 1101	109	6D	m	' + char($0D)
    + Char($0A) + '0110 1110	110	6E	n	　      0110 1111	111	6F	o	' + char($0D) + Char($0A) + '0111 0000	112	70	p	　      0111 0001	113	71	q	' + char($0D) + Char($0A) + '0111 0010	114	72	r	　      0111 0011	115	73	s	' + char($0D) + Char($0A) + '0111 0100	116	74	t	　      0111 0101	117	75	u	' + char($0D) + Char($0A) + '0111 0110	118	76	v	　      0111 0111	119	77	w	' + char($0D) + Char($0A) + '0111 1000	120	78	x	　      0111 1001	121	79	y	' + char($0D) + Char($0A) + '0111 1010	122	7A	z	　      0111 1011	123	7B	{	' + char($0D) + Char($0A) + '0111 1100	124	7C	|	　      0111 1101	125	7D	}	' + char($0D) + Char($0A) + '0111 1110	126	7E	~	　      0111 1111	127	7F	DEL	(delete) 删除', mtInformation, [mbOK], 0);

end;

procedure TForm_main.A5Click(Sender: TObject);
begin
  FormAscii.show;
end;

procedure TForm_main.X2Click(Sender: TObject);
begin
  FormClampDiode.edt3.Text := '0';
  FormClampDiode.edt4.Text := '1200';
  FormClampDiode.edt5.Text := '200';

  FormClampDiode.edt16.Text := '0D01';
  FormClampDiode.cbb2.ItemIndex := 4;
  FormClampDiode.showmodal;
end;

procedure TForm_main.N25Click(Sender: TObject);
begin
  FormClampDiode.edt3.Text := '0';
  FormClampDiode.edt4.Text := '10000';
  FormClampDiode.edt5.Text := '100';

  FormClampDiode.edt16.Text := '1R11';
  FormClampDiode.cbb2.ItemIndex := 2;
  FormClampDiode.showmodal;
end;

procedure TForm_main.L1Click(Sender: TObject);
begin
  FormClampDiode.edt3.Text := '0';
  FormClampDiode.edt4.Text := '1000';
  FormClampDiode.edt5.Text := '100';

  FormClampDiode.edt16.Text := '3C11';
  FormClampDiode.cbb2.ItemIndex := 3;
  FormClampDiode.showmodal;
end;

procedure TForm_main.N32Click(Sender: TObject);
begin
  FormCreateClampDiode.showmodal;
end;

procedure TForm_main.C1Click(Sender: TObject);
begin
  if sysedit[5].Text = '8001' then  //第一个app用的指令07
    modbusfun05 := $07
  else
    modbusfun05 := $17;
  modbusfun05dat := $FF;
  addToLog('undo save!,撤销保存步骤');
end;

procedure TForm_main.R1Click(Sender: TObject);
begin
  modbusfun05 := $18;
  modbusfun05dat := $FF;
  addToLog('redo save!,重做保存步骤');

end;

procedure TForm_main.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if TabSheet4.Showing then begin
    if (ssAlt in Shift) then begin
      if ((Key = ord('Z'))) then
        R1.Click;
    end
    else if (ssShift in Shift) then begin
      if ((Key = ord('V'))) then
        I2.Click;
      if ((Key = ord('C'))) then
        I3.Click;
      if ((Key = ord('X'))) then
        D5.Click;
      if ((Key = ord('G'))) then
        N33.Click;
      if ((Key = ord('S'))) then
        S8.Click;
      if ((Key = ord('B'))) then
        B1.Click;
      if ((Key = ord('Z'))) then
        C1.Click;
     //   if ( (Key = ord('J')) ) then  S1.Click;
     //   if ( (Key = ord('K')) ) then  S3.Click;
    end;
  end;
end;

procedure TForm_main.mmo1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (ssAlt in Shift) then begin
    if ((Key = ord('Z'))) then
      R1.Click;
  end
  else if (ssShift in Shift) then begin
    if ((Key = ord('V'))) then
      I2.Click;
    if ((Key = ord('C'))) then
      I3.Click;
    if ((Key = ord('X'))) then
      D5.Click;
    if ((Key = ord('G'))) then
      N33.Click;
    if ((Key = ord('S'))) then
      S8.Click;
    if ((Key = ord('B'))) then
      B1.Click;
    if ((Key = ord('Z'))) then
      C1.Click;
  end;
end;

procedure TForm_main.btn11Click(Sender: TObject);
begin
  modbusfun05 := $0150;
  modbusfun05dat := $FF;
end;

procedure TForm_main.btn45Click(Sender: TObject);
begin
  modbusfun05 := $0151;
  modbusfun05dat := $FF;

end;

procedure TForm_main.btn117Click(Sender: TObject);
begin
  modbusfun05 := $0150;
  modbusfun05dat := $00;

end;

procedure TForm_main.btn451Click(Sender: TObject);
begin
  modbusfun05 := $0151;
  modbusfun05dat := $00;

end;

procedure TForm_main.btn1110Click(Sender: TObject);
begin
  modbusfun05 := $0140;
  modbusfun05dat := $FF;
end;

procedure TForm_main.btn452Click(Sender: TObject);
begin
  modbusfun05 := $0141;
  modbusfun05dat := $FF;
end;

procedure TForm_main.btn1171Click(Sender: TObject);
begin
  modbusfun05 := $0140;
  modbusfun05dat := $00;

end;

procedure TForm_main.btn4511Click(Sender: TObject);
begin
  modbusfun05 := $0141;
  modbusfun05dat := $00;

end;

procedure TForm_main.btn1172Click(Sender: TObject);
var
  ccc: Boolean;
begin
  sbuf[2] := 1;
  sbuf[3] := $01;
  sbuf[4] := $50;
  sbuf[5] := $00;
  sbuf[6] := $08;         //读取主板关键信号状态
  send_len := 6;

  ccc := form_main.chk24.checked;
  form_main.chk24.checked := False;      //关闭不显示发送命令
  try
    send_crcdata(send_len, 2);
  except
    messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);
  end;
  form_main.chk24.checked := ccc;

end;

procedure TForm_main.btn11711Click(Sender: TObject);
var
  ccc: Boolean;
begin
  sbuf[2] := 1;
  sbuf[3] := $01;
  sbuf[4] := $40;
  sbuf[5] := $00;
  sbuf[6] := $08;         //读取主板关键信号状态
  send_len := 6;
  ccc := form_main.chk24.checked;
  form_main.chk24.checked := false;  //关闭不显示发送命令
  try
    send_crcdata(send_len, 2);
  except
    messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);
  end;
  form_main.chk24.checked := ccc;
end;

procedure TForm_main.N41Click(Sender: TObject);
begin       //添加CTRL+A快捷键不起作用！
  if mmo1.Focused then
    mmo1.SelectAll;
  if mmo2.Focused then
    mmo2.SelectAll;
  if mmo3.Focused then
    mmo3.SelectAll;
  if mmo5.Focused then
    mmo5.SelectAll;
  if mmo9.Focused then
    mmo9.SelectAll;
  if memo1.Focused then
    memo1.SelectAll;
  if mmo17.Focused then
    mmo17.SelectAll;
  if mmo19.Focused then
    mmo19.SelectAll;
  if mmo21.Focused then
    mmo21.SelectAll;
end;

procedure TForm_main.mmo3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (ssCtrl in Shift) and ((Key = ord('V'))) then      //粘贴内容没有\r\n则替换\n
  begin
    if Clipboard.HasFormat(CF_TEXT) then begin
      if Pos(Char($0D) + Char($0A), ClipBoard.AsText) < 1 then begin
        ClipBoard.AsText := StringReplace(ClipBoard.AsText, Char($0A), Char($0D) + Char($0A), [rfReplaceAll]);
      end;
    end;
  end;
end;

procedure TForm_main.mmo3ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
  if Clipboard.HasFormat(CF_TEXT) then begin
    if Pos(Char($0D) + Char($0A), ClipBoard.AsText) < 1 then begin
      ClipBoard.AsText := StringReplace(ClipBoard.AsText, Char($0A), Char($0D) + Char($0A), [rfReplaceAll]);
    end;
  end;
end;

procedure TForm_main.btn94Click(Sender: TObject);
var
  Dir: string;
begin
  Dir := '';
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    edt128.Text := Dir + '\';
    btn1.Font.Color := clRed;
  end;
end;

procedure TForm_main.btn95Click(Sender: TObject);
var
  Dir: string;
begin
  Dir := '';
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then begin
    edt129.Text := Dir + '\';
    btn1.Font.Color := clRed;
  end;
end;

procedure TForm_main.K1Click(Sender: TObject);
begin
  FormKeyboard.show;
end;

procedure TForm_main.pgc1Change(Sender: TObject);
begin
  AreaDisSelect(0);
end;

procedure TForm_main.PageControl1Change(Sender: TObject);
begin
  AreaDisSelect(0);
end;

procedure TForm_main.ts28Show(Sender: TObject);
begin
  if pnl1.Height = 22 then begin    //btn120.Caption<>'X'
    pnl1.Height := 188;
    btn120.Caption := 'X 隐藏';
    N38.Caption := 'Close SysWin 关闭系统参数显示';
  end;
end;

procedure TForm_main.ts29Show(Sender: TObject);
begin
  if pnl1.Height = 22 then begin    //btn120.Caption<>'X'
    pnl1.Height := 188;
    btn120.Caption := 'X 隐藏';
    N38.Caption := 'Close SysWin 关闭系统参数显示';
  end;
end;

procedure TForm_main.N44Click(Sender: TObject);
var
  strs: TStringList;
  i: SmallInt;
  save_file_name: string;
begin
  if N44.Checked then begin
    N44.Checked := false;
    lbl28.Caption := 'Exit Offline 离线查看退出！';
    try
      OfflineFileList.Free;
    except
      ShowMessage('Free Err! 释放离线文件错误！');
    end;

    A1.Checked := False;    //停止所有机种导入
    N50.Checked := false;
    N51.Checked := false;

    btn16.Enabled := True;
    btn23.Enabled := True;
    btn24.Enabled := True;
    grp28.Visible := True;
    for i := 1 to VIRLCROWS do begin
      zu1edit[i].Text := '';
      zu2edit[i].Text := '';
      zu3edit[i].Text := '';
      zu4edit[i].Text := '';
      zu5edit[i].Text := '';
      zu6edit[i].Text := '';
      zu7edit[i].Text := '';
      zu8edit[i].Text := '';
      zu9edit[i].Text := '';
      zu10edit[i].Text := '';
      zu11edit[i].Text := '';
      zu12edit[i].Text := '';
      zu13edit[i].Text := '';
      zu14edit[i].Text := '';
      zu15edit[i].Text := '';
      zu16edit[i].Text := '';
      zu17edit[i].Text := '';
      zu18edit[i].Text := '';
      zu19edit[i].Text := '';
    end;
  end
  else if dlgOpen1.Execute then begin
    lbl28.Caption := 'Data Offline......离线查看中...';
    lbl28.Font.Color := clRed;
    sprgrsbr1.Position := 0;
    N44.Checked := True;
    btn16.Enabled := false;
    btn23.Enabled := false;
    btn24.Enabled := false;
    grp28.Visible := false;

    save_file_name := dlgOpen1.FileName;
    lbl29.Caption := save_file_name;
    try
      OfflineFileList := TStringList.Create;
      OfflineFileList.LoadFromFile(save_file_name);

      strs := TStringList.Create;
      strs.Delimiter := #9;
      strs.DelimitedText := OfflineFileList[0];   //拆分不完全正确。连续的空格认识是TAB
      ImportVer := 0;
      if (strs.Count > 17) and (Pos('v', strs[17]) = 6) and (IsFloatNum(Copy(strs[17], 7, Length(strs[17])))) then begin     //升级：测试程序含有“固件v:"字符
        ImportVer := Trunc(strtofloat(Copy(strs[17], 7, Length(strs[17]))));
        if ImportVer < 100 then begin             //无效版本
          if strs.Count > 1 then begin                                        //兼容早期：第2列存放纯数字固件大版本；
            if (IsNumberic(strs[1])) then begin
              ImportVer := StrToInt(strs[1]);
            end;
          end;
        end;
      end
      else if strs.Count > 1 then begin                                        //兼容早期：第2列存放纯数字固件大版本；
        if (IsNumberic(strs[1])) then begin
          ImportVer := StrToInt(strs[1]);
        end;
      end;
      strs.Free;

      for i := 1 to OfflineFileList.Count do begin
        if Copy(OfflineFileList[i], 0, 4) = '9999' then begin
          OfflineMax := i;
          Break;
        end;
      end;
      if (VIRLCROWS = 66) then begin               //固件版本适应行数
        sysedit[5].Text := '806';          //通讯会使用版本号，此处必须强行设置固件版本号；
        VIRLCROWS := 50;
      end;
    except
      ShowMessage('Load Err! 加载离线文件错误');
    end;
  end;
end;

procedure TForm_main.Panel1Click(Sender: TObject);
begin
  AreaDisSelect(0);
end;

procedure TForm_main.Panel1ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
var
  str: string;
  hh, ii, jj, nline: SmallInt;
begin  //右键框选
  StepNowFresh := (MousePos.Y + panel1.VertScrollBar.Position - 10) div strtoint(form_main.edt121.Text);   //刷新开始行数
  hh := MousePos.X + panel1.HorzScrollBar.Position + 10;      //+10补偿模式编辑框左移！

  if (mmo22.Tag = -1) then
    mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行

  if (mmo24.Tag = -1) then
    mmo24.Tag := mmo22.Tag;            //最小行
  if StepNowFresh + StepNowPage * VIRLCROWS <= mmo24.Tag then
    mmo24.Tag := StepNowFresh + StepNowPage * VIRLCROWS
  else begin
    mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
  end;

  if (mmo23.Tag = -1) then
    mmo23.Tag := hh div 60;             //当前列：0...
  if (mmo25.Tag = -1) then
    mmo25.Tag := mmo23.Tag;           //最小列
  if (hh div 60) < mmo25.Tag then
    mmo25.Tag := hh div 60
  else begin
    mmo23.Tag := hh div 60;             //当前列：0...
  end;

  for ii := 1 to VIRLCROWS do begin        //颜色先还原
    if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
      zu1edit[ii].Color := clWhite;
    zu2edit[ii].Color := clWhite;
    zu3edit[ii].Color := clWhite;

    if (zu5edit[ii].Color <> clGreen) then
      zu5edit[ii].Color := clWhite;
    if (zu6edit[ii].Color <> clGreen) then
      zu6edit[ii].Color := clWhite;
    zu7edit[ii].Color := clWhite;
    zu8edit[ii].Color := clWhite;
    if (zu9edit[ii].Color <> clGray) then
      zu9edit[ii].Color := clWhite;
    zu10edit[ii].Color := clWhite;
    zu11edit[ii].Color := clWhite;
    zu12edit[ii].Color := clWhite;
    zu13edit[ii].Color := clWhite;
    zu14edit[ii].Color := clWhite;
    zu15edit[ii].Color := clWhite;
    zu16edit[ii].Color := clWhite;
    zu17edit[ii].Color := clWhite;
  end;
  for ii := mmo24.Tag to mmo22.Tag do begin
    if (ii < StepNowPage * VIRLCROWS) then
      Continue;   //小于当前页最小值
    if (ii >= StepNowPage * VIRLCROWS + VIRLCROWS) then
      Break;   //大于等于后一页最小值
    for jj := mmo25.Tag to mmo23.Tag do begin          //  if(lineInsert+ii)>VIRLCROWS then Break;

      nline := (ii mod VIRLCROWS) + 1;
      if (jj = 0) and (zu1edit[nline].text <> '0') and (zu1edit[nline].Color <> clred) then
        zu1edit[nline].Color := clMoneyGreen;    //名字和数组都是从1...开始
      if jj = 1 then
        zu2edit[nline].Color := clMoneyGreen;
      if jj = 2 then
        zu3edit[nline].Color := clMoneyGreen;

      if (jj = 4) and (zu5edit[nline].Color <> clGreen) then
        zu5edit[nline].Color := clMoneyGreen;
      if (jj = 5) and (zu6edit[nline].Color <> clGreen) then
        zu6edit[nline].Color := clMoneyGreen;
      if jj = 6 then
        zu7edit[nline].Color := clMoneyGreen;
      if jj = 7 then
        zu8edit[nline].Color := clMoneyGreen;
      if (jj = 8) and (zu9edit[nline].Color <> clGray) then
        zu9edit[nline].Color := clMoneyGreen;
      if (jj = 9) and (zu10edit[nline].Color <> clYellow) then
        zu10edit[nline].Color := clSkyBlue;
      if jj = 10 then
        zu11edit[nline].Color := clMoneyGreen;
      if jj = 11 then
        zu12edit[nline].Color := clMoneyGreen;
      if jj = 12 then
        zu13edit[nline].Color := clMoneyGreen;
      if jj = 13 then
        zu14edit[nline].Color := clMoneyGreen;
      if jj = 14 then
        zu15edit[nline].Color := clMoneyGreen;
      if jj = 15 then
        zu16edit[nline].Color := clMoneyGreen;
      if jj = 16 then
        zu17edit[nline].Color := clMoneyGreen;
    end;
  end;
end;

procedure TForm_main.Panel2ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
var
  str: string;
  hh, ii, jj, nline: SmallInt;
begin  //右键框选
  StepNowFresh := 0; //(MousePos.Y+panel1.VertScrollBar.Position-10)div strtoint(form_main.edt121.Text);   //刷新开始行数
  hh := MousePos.X + panel1.HorzScrollBar.Position + 10;      //+10补偿模式编辑框左移！

  if (mmo22.Tag = -1) then
    mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行

  if (mmo24.Tag = -1) then
    mmo24.Tag := mmo22.Tag;            //最小行
  if StepNowFresh + StepNowPage * VIRLCROWS <= mmo24.Tag then
    mmo24.Tag := StepNowFresh + StepNowPage * VIRLCROWS
  else begin
    mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
  end;

  if (mmo23.Tag = -1) then
    mmo23.Tag := hh div 60;             //当前列：0...
  if (mmo25.Tag = -1) then
    mmo25.Tag := mmo23.Tag;           //最小列
  if (hh div 60) < mmo25.Tag then
    mmo25.Tag := hh div 60
  else begin
    mmo23.Tag := hh div 60;             //当前列：0...
  end;

  for ii := 1 to VIRLCROWS do begin        //颜色先还原
    if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
      zu1edit[ii].Color := clWhite;
    zu2edit[ii].Color := clWhite;
    zu3edit[ii].Color := clWhite;

    if (zu5edit[ii].Color <> clGreen) then
      zu5edit[ii].Color := clWhite;
    if (zu6edit[ii].Color <> clGreen) then
      zu6edit[ii].Color := clWhite;
    zu7edit[ii].Color := clWhite;
    zu8edit[ii].Color := clWhite;
    if (zu9edit[ii].Color <> clGray) then
      zu9edit[ii].Color := clWhite;
    zu10edit[ii].Color := clWhite;
    zu11edit[ii].Color := clWhite;
    zu12edit[ii].Color := clWhite;
    zu13edit[ii].Color := clWhite;
    zu14edit[ii].Color := clWhite;
    zu15edit[ii].Color := clWhite;
    zu16edit[ii].Color := clWhite;
    zu17edit[ii].Color := clWhite;
  end;
  for ii := mmo24.Tag to mmo22.Tag do begin
    if (ii < StepNowPage * VIRLCROWS) then
      Continue;   //小于当前页最小值
    if (ii >= StepNowPage * VIRLCROWS + VIRLCROWS) then
      Break;   //大于等于后一页最小值
    for jj := mmo25.Tag to mmo23.Tag do begin          //  if(lineInsert+ii)>VIRLCROWS then Break;

      nline := (ii mod VIRLCROWS) + 1;
      if (jj = 0) and (zu1edit[nline].text <> '0') and (zu1edit[nline].Color <> clred) then
        zu1edit[nline].Color := clMoneyGreen;    //名字和数组都是从1...开始
      if jj = 1 then
        zu2edit[nline].Color := clMoneyGreen;
      if jj = 2 then
        zu3edit[nline].Color := clMoneyGreen;

      if (jj = 4) and (zu5edit[nline].Color <> clGreen) then
        zu5edit[nline].Color := clMoneyGreen;
      if (jj = 5) and (zu6edit[nline].Color <> clGreen) then
        zu6edit[nline].Color := clMoneyGreen;
      if jj = 6 then
        zu7edit[nline].Color := clMoneyGreen;
      if jj = 7 then
        zu8edit[nline].Color := clMoneyGreen;
      if (jj = 8) and (zu9edit[nline].Color <> clGray) then
        zu9edit[nline].Color := clMoneyGreen;
      if (jj = 9) and (zu10edit[nline].Color <> clYellow) then
        zu10edit[nline].Color := clSkyBlue;
      if jj = 10 then
        zu11edit[nline].Color := clMoneyGreen;
      if jj = 11 then
        zu12edit[nline].Color := clMoneyGreen;
      if jj = 12 then
        zu13edit[nline].Color := clMoneyGreen;
      if jj = 13 then
        zu14edit[nline].Color := clMoneyGreen;
      if jj = 14 then
        zu15edit[nline].Color := clMoneyGreen;
      if jj = 15 then
        zu16edit[nline].Color := clMoneyGreen;
      if jj = 16 then
        zu17edit[nline].Color := clMoneyGreen;
    end;
  end;
end;

procedure TForm_main.pgc1ContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
var
  str: string;
  hh, ii, jj, nline: SmallInt;
begin  //右键框选
  StepNowFresh := VIRLCROWS - 1;           //(MousePos.Y+panel1.VertScrollBar.Position-10)div strtoint(form_main.edt121.Text);   //刷新开始行数
  hh := MousePos.X + panel1.HorzScrollBar.Position + 10;      //+10补偿模式编辑框左移！

  if (mmo22.Tag = -1) then
    mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行

  if (mmo24.Tag = -1) then
    mmo24.Tag := mmo22.Tag;            //最小行
  if StepNowFresh + StepNowPage * VIRLCROWS <= mmo24.Tag then
    mmo24.Tag := StepNowFresh + StepNowPage * VIRLCROWS
  else begin
    mmo22.Tag := StepNowFresh + StepNowPage * VIRLCROWS;          //当前行：0...
  end;

  if (mmo23.Tag = -1) then
    mmo23.Tag := hh div 60;             //当前列：0...
  if (mmo25.Tag = -1) then
    mmo25.Tag := mmo23.Tag;             //最小列
  if (hh div 60) < mmo25.Tag then
    mmo25.Tag := hh div 60
  else begin
    mmo23.Tag := hh div 60;             //当前列：0...
  end;

  for ii := 1 to VIRLCROWS do begin        //颜色先还原
    if (zu1edit[ii].text <> '0') and (zu1edit[ii].Color <> clred) then
      zu1edit[ii].Color := clWhite;
    zu2edit[ii].Color := clWhite;
    zu3edit[ii].Color := clWhite;

    if (zu5edit[ii].Color <> clGreen) then
      zu5edit[ii].Color := clWhite;
    if (zu6edit[ii].Color <> clGreen) then
      zu6edit[ii].Color := clWhite;
    zu7edit[ii].Color := clWhite;
    zu8edit[ii].Color := clWhite;
    if (zu9edit[ii].Color <> clGray) then
      zu9edit[ii].Color := clWhite;
    zu10edit[ii].Color := clWhite;
    zu11edit[ii].Color := clWhite;
    zu12edit[ii].Color := clWhite;
    zu13edit[ii].Color := clWhite;
    zu14edit[ii].Color := clWhite;
    zu15edit[ii].Color := clWhite;
    zu16edit[ii].Color := clWhite;
    zu17edit[ii].Color := clWhite;
  end;
  for ii := mmo24.Tag to mmo22.Tag do begin
    if (ii < StepNowPage * VIRLCROWS) then
      Continue;   //小于当前页最小值
    if (ii >= StepNowPage * VIRLCROWS + VIRLCROWS) then
      Break;   //大于等于后一页最小值
    for jj := mmo25.Tag to mmo23.Tag do begin          //  if(lineInsert+ii)>VIRLCROWS then Break;

      nline := (ii mod VIRLCROWS) + 1;
      if (jj = 0) and (zu1edit[nline].text <> '0') and (zu1edit[nline].Color <> clred) then
        zu1edit[nline].Color := clMoneyGreen;    //名字和数组都是从1...开始
      if jj = 1 then
        zu2edit[nline].Color := clMoneyGreen;
      if jj = 2 then
        zu3edit[nline].Color := clMoneyGreen;

      if (jj = 4) and (zu5edit[nline].Color <> clGreen) then
        zu5edit[nline].Color := clMoneyGreen;
      if (jj = 5) and (zu6edit[nline].Color <> clGreen) then
        zu6edit[nline].Color := clMoneyGreen;
      if jj = 6 then
        zu7edit[nline].Color := clMoneyGreen;
      if jj = 7 then
        zu8edit[nline].Color := clMoneyGreen;
      if (jj = 8) and (zu9edit[nline].Color <> clGray) then
        zu9edit[nline].Color := clMoneyGreen;
      if (jj = 9) and (zu10edit[nline].Color <> clYellow) then
        zu10edit[nline].Color := clSkyBlue;
      if jj = 10 then
        zu11edit[nline].Color := clMoneyGreen;
      if jj = 11 then
        zu12edit[nline].Color := clMoneyGreen;
      if jj = 12 then
        zu13edit[nline].Color := clMoneyGreen;
      if jj = 13 then
        zu14edit[nline].Color := clMoneyGreen;
      if jj = 14 then
        zu15edit[nline].Color := clMoneyGreen;
      if jj = 15 then
        zu16edit[nline].Color := clMoneyGreen;
      if jj = 16 then
        zu17edit[nline].Color := clMoneyGreen;
    end;
  end;
end;

procedure TForm_main.StepMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbMiddle) then
    AreaDisSelect(0);
end;

procedure TForm_main.N7Click(Sender: TObject);
begin
  formswcheck.showmodal;
end;

procedure TForm_main.Panel2Click(Sender: TObject);
begin
  AreaDisSelect(0);
end;

procedure TForm_main.btn1173Click(Sender: TObject);
var
  ccc: Boolean;
begin
  sbuf[2] := 1;
  sbuf[3] := $01;
  sbuf[4] := $58;
  sbuf[5] := $00;
  sbuf[6] := $08;         //读取主板关键信号状态
  send_len := 6;

  ccc := form_main.chk24.checked;
  form_main.chk24.checked := False;      //关闭不显示发送命令
  try
    send_crcdata(send_len, 2);
  except
    messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);
  end;
  form_main.chk24.checked := ccc;

end;

procedure TForm_main.N45Click(Sender: TObject);
var
  save_file_name, ttt: string;
  strs, ResultList: TStringList;
  ii, jj, temp16: SmallInt;
begin
  if dlgOpen1.Execute then begin
    save_file_name := dlgOpen1.FileName;
    ResultList := TStringList.Create;
    for ii := 1 to MAXPINS do begin
      OSpointCheckZu[ii] := 0;
    end;
    try
      ResultList.LoadFromFile(save_file_name);
      strs := TStringList.Create;
      strs.Delimiter := #9;
      for ii := ResultList.Count - 1 downto 0 do begin
        ResultList[ii] := StringReplace(ResultList[ii], '(', '', [rfReplaceAll]);
        ResultList[ii] := StringReplace(ResultList[ii], ')', '', [rfReplaceAll]);
        ResultList[ii] := StringReplace(ResultList[ii], '[', '', [rfReplaceAll]);
        ResultList[ii] := StringReplace(ResultList[ii], ']', '', [rfReplaceAll]);
        ResultList[ii] := StringReplace(ResultList[ii], ',', char(#9), [rfReplaceAll]);

        strs.DelimitedText := ResultList[ii];
        if (strs.Count > 0) and IsSignedNumberic(strs[0]) then begin
          for jj := 0 to strs.Count - 1 do begin
            if IsSignedNumberic(strs[jj]) then begin
              temp16 := StrToInt(strs[jj]);
              if temp16 < 0 then
                temp16 := 0 - temp16;
              if temp16 <= MAXPINS then begin
                OSpointCheckZu[temp16] := OSpointCheckZu[temp16] + 1;
              end;
            end;
          end;

        end
        else begin
          Break;
        end;

      end;

      ttt := '';
      jj := 0;
      for ii := 1 to MAXPINS do begin
        if OSpointCheckZu[ii] <> 1 then begin
          ttt := ttt + inttostr(ii) + '点异常，出现次数：' + IntToStr(OSpointCheckZu[ii]);
          ttt := ttt + char($0D) + char($0A);
          jj := jj + 1;
        end;
        if jj > 30 then begin
          Break;
        end;
      end;
      if jj > 30 then begin
        ttt := ttt + '...';
        ttt := ttt + char($0D) + char($0A);
        ii := 4000;
        ttt := ttt + inttostr(ii) + '点异常，出现次数：' + IntToStr(OSpointCheckZu[ii]);
        ttt := ttt + char($0D) + char($0A);
      end;
      if ttt = '' then begin
        ttt := '所有点在短路群出现1次，正常！';
        ShowMessage(ttt);
      end
      else begin
        ttt := '点位号在短路群只有1次正常，不再显示' + char($0D) + char($0A) + char($0D) + char($0A) + ttt;
        ShowMessage(ttt);
        //Application.MessageBox(ttt,'点位号在短路群的次数(1次正常)', MB_OK + MB_ICONWARNING);
        //    Application.MessageBox('', 'ASCII码表', MB_OK + MB_ICONWARNING);
      end;
      strs.Free;
      ResultList.Free;
    except
      strs.Free;
      ResultList.Free;
      ShowMessage('短路群文件格式异常，检查出错！');
    end;
  end;
end;

procedure TForm_main.btn47Click(Sender: TObject);

  function FloatToHex(Value: single): string;
  var
    l, i: integer;
    HexText, tempHexText, temp: string;
  begin
    SetLength(HexText, 2 * SizeOf(Value));
    BinToHex(pchar(@Value), pchar(@HexText[1]), SizeOf(Value));
    l := length(HexText);
    for i := (l div 2) downto 1 do begin
      temp := copy(HexText, (2 * i - 1), 2);
      tempHexText := tempHexText + temp;
    end;
    result := tempHexText;
  end;

begin
  edt17.Text := floatToHex(strtofloat(edt16.Text));
end;

{字符串转换成16进制字符串}
function StrToHexStr(const S: string): string;//字符串转换成16进制字符串
var
  I: Integer;
begin
  for I := 1 to Length(S) do begin
    if I = 1 then
      Result := IntToHex(Ord(S[1]), 2)
    else
      Result := Result + ' ' + IntToHex(Ord(S[I]), 2);
  end;
end;  
  
{十进制 to 二进制}
function IntToBin(Value: LongInt; Size: Integer): string;
var
  i: Integer;
begin
  Result := '';
  for i := Size - 1 downto 0 do begin
    if Value and (1 shl i) <> 0 then begin
      Result := Result + '1';
    end
    else begin
      Result := Result + '0';
    end;
  end;
end;  
  
{二进制 to 十进制}
function BintoInt(Value: string): LongInt;
var
  i, Size: Integer;
begin
  Result := 0;
  Size := Length(Value);
  for i := Size downto 1 do begin
    if Copy(Value, i, 1) = '1' then
      Result := Result + (1 shl (Size - i));
  end;
end;

function floatBintoInt(Value: string): real;
var
  i, Size: Integer;
begin
  Result := 0;
  Size := Length(Value);
  for i := Size downto 1 do begin
    if Copy(Value, i, 1) = '1' then
      Result := Result + 1 / (1 shl i);
  end;
end;  
  
{十六进制 to 二进制}
function HextoBinary(Hex: string): string;
const
  BOX: array[0..15] of string = ('0000', '0001', '0010', '0011', '0100', '0101', '0110', '0111', '1000', '1001', '1010', '1011', '1100', '1101', '1110', '1111');
var
  i: integer;
begin
  for i := Length(Hex) downto 1 do
    Result := BOX[StrToInt('$' + Hex[i])] + Result;
end;  
  
{十六进制 to 十进制 浮点型}
function HextoFloat(s: string): real;
var
  b, temp: string;
  e: integer;
  f: real;
begin
  b := HextoBinary(s);
  temp := copy(b, 2, 8);
  e := BintoInt(temp) - 127;
  temp := copy(b, 10, 23);
  f := 1 + floatBintoInt(temp);
  if (copy(b, 1, 1) = '0') then
    result := power(2, e) * f   //-----要引用单元math
  else
    result := -power(2, e) * f;
end;

procedure TForm_main.btn48Click(Sender: TObject);
begin
  edt16.Text := FloatToStr(hextofloat(edt17.Text));
end;

procedure TForm_main.btn49Click(Sender: TObject);
begin
  if IsSignedNumberic(edt18.Text) then
    edt19.Text := IntToHex(StrToInt(edt18.Text), 2);
end;

function HexToInt(const aHex: string): Integer;
var
  I, L, K: Integer;
begin
  Result := 0;
  if aHex = '' then begin
    Exit;
  end
  else begin
    K := 0;
    L := Length(aHex);
    for I := 1 to L do begin
      if (not (aHex[I] in ['A'..'F'])) and (not (aHex[I] in ['a'..'f'])) then
        K := K + Trunc(StrToInt(aHex[I]) * Power(16, L - I))
      else
        case aHex[I] of
          'a', 'A':
            K := K + Trunc(10 * Power(16, L - I));
          'b', 'B':
            K := K + Trunc(11 * Power(16, L - I));
          'c', 'C':
            K := K + Trunc(12 * Power(16, L - I));
          'd', 'D':
            K := K + Trunc(13 * Power(16, L - I));
          'e', 'E':
            K := K + Trunc(14 * Power(16, L - I));
          'f', 'F':
            K := K + Trunc(15 * Power(16, L - I));
        end;
    end;
  end;
  Result := K;
end;

procedure TForm_main.btn50Click(Sender: TObject);
begin
  edt18.Text := IntToStr(HexToInt(edt19.Text));
end;

procedure TForm_main.btn51Click(Sender: TObject);
begin
  edt20.Text := HextoBinary(edt19.Text);
end;

procedure TForm_main.cbb1Select(Sender: TObject);
begin
//  mmo17.SetFocus;
end;

procedure TForm_main.cbb1CloseUp(Sender: TObject);
begin
  mmo17.Text := cbb1.Items.Strings[cbb1.ItemIndex];

end;

end.

