{
//=======数据处理 ===================================================================
USB交互不行？
**专用于CSVen=5定制log****
}
unit Unit5eCar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ActnList, NB30, registry, shellapi, dbclient, inifiles, db,
  adodb, FileCtrl, StdCtrls, StrUtils;

type
  Tcar = class
//  class procedure   bakToServerDisk();
//  class procedure   upload;
//  class procedure   bpcheck;
//  class procedure   printNTC(len_ss:integer);
    class procedure SaveFlagProcess(devnum: integer; SaveLen: SmallInt);
    class procedure FreshDatalogFromMemo(tt: SmallInt);
    class procedure bakToServerDisk();
    class function Request(len: SmallInt): SmallInt;
  public
    { Public declarations }
  private
    { Private declarations }
  end;

implementation

uses
  StepCopy, main, OSstudy, FindPoint, PNenter, Unit0_globalVariant, LogHeadTips,
  Unit2_Communication, Unit3appFSM, LoadFile, ULJKTcp;

//二次刷新文件表头参数，
//标准文件格式，在加载完MCU测试程序参数后刷新，由rxdmonitorName-unit-upp函数决定文件表头
//非标准文件格式（yijia或者机箱专用、加贺格式、早期jaguar）由creatFile函数决定文件表头
class procedure Tcar.FreshDatalogFromMemo(tt: SmallInt);       //Log文件更新前面几行
var
  save_file_name: string;
  ResultList: TStringList;
begin
  with form_main do begin      //不同文件格式，处理方式不同

  end;
end;

class function Tcar.Request(Len: SmallInt): SmallInt;       //主板ABP3模式上传请求
var
  barcode, str, SLINE_head, ssdatetime: string;
  i: SmallInt;
  sbuzhou:string;
begin
  result := 0;
  with Form_main do begin
    str := '';
    for i := 4 to Len - 2 do begin                       //rbuf[]从第4个字节为交互标志
      str := str + char(rbuf[i]);
    end;
    result := 1;         //处理1

    barcode := Form_main.edit1.text;
      //插入自定义开发代码...
    //2.2.2 MES Feasa测试-----------------
    if (Char(rbuf[4]) = '*') and ( Char(rbuf[5]) in ['1'..'9','a'..'z','A'..'Z'] ) then
    begin // (Char(rbuf[5]) = '1')
      if ( Char(rbuf[5]) in ['1'..'9'] ) then
       sbuzhou:='00'+Char(rbuf[5])
      else
       sbuzhou:=Char(rbuf[5]);
       DmLJKTcp.MySendCode(barcode,'300',sbuzhou);//A,B多条码用逗号分隔
    end
    else if (Char(rbuf[4]) = '*') and (Char(rbuf[5]) = '*') then
    begin         //本地UDP模拟确认

      if (Char(rbuf[6]) = '*') then begin         //自测试通讯链路
        if (Char(rbuf[7]) = '*') then begin    //****回传ok
          sbuf[5] := $FF;
        end
        else
          sbuf[5] := $00;                      //***回传ng

        sbuf[1] := 1;
        sbuf[2] := 5;
        sbuf[3] := $50;
        sbuf[4] := $00;
        sbuf[6] := $00;
        send_len := 6;
        try
          send_crcdata(send_len, 2);
        except
          messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);
        end;
      end
      else begin
        {
          Label11.Font.Size:=80;
          lbl169.Font.Size:=60;
          lbl169.caption:=str;             //显示收到的数据内容

          idpsrvr1.Active:=true;
          edt53.tag:=9001;
          Stat1.Panels[3].Text :='主UDP=9000,从UDP='+IntToStr(9001);
          Stat1.Tag:=9000;
          idpclnt1.Active:=true; idpclnt1.tag:=0;

          SLINE_head := trim(edit1.Text)+',plsCheck,FCT';             //本地UDB数准备
          if(readchis('Model Para','MachineNum')<>'')  then           //从config获取参数
            SLINE_head :=SLINE_head+','+readchis('Model Para','MachineNum')
          else
            SLINE_head :=SLINE_head+',CT-1'+inttostr(rbuf[11]);
          SLINE_head :=SLINE_head+','+ edt77.Text;                   //登录参数.机种料号
          SLINE_head :=SLINE_head+','+ edt78.Text;                   //lot
          SLINE_head :=SLINE_head+','+ edt79.Text;                   //作业员
          SLINE_head :=SLINE_head+','+ edt80.Text;                   //其它
          datetimetostring(ssdatetime,'yyyymmddhhnnss',Now);
          SLINE_head :=SLINE_head+','+ ssdatetime;
          str:=str+','+SLINE_head;
          idpclnt1.Send(Edt100.Text, 9001, str);      }

      end;
    end;


  end;
end;

class procedure Tcar.bakToServerDisk();
var
  f: TextFile;
  Edt2temp, save_file_name, ss, save_file_name_Sever: string;

  procedure MoveBarcodefile(barfiletype: SmallInt);        //定时：条码文件复制到网盘；
  var
    SearchRec: TSearchRec;
    found, fCopynum: integer;
    F: TextFile;
    ss, strc, PNnameTemp: string;
  begin
    with form_main do begin                 //仅找一个txt文件 Result := TStringList.Create;
      Edt2temp := '';           //temp\';
      //---------轮询 遍历其它机种文件---------------------------------------

      fCopynum := 0;                                                              //单次上传计数；

      if Readchis('Comm Para', 'ChildFolder') = '1' then begin     //总文件夹！
        PNnameTemp := '';
      end
      else
        PNnameTemp := edt77.text;
      found := FindFirst(edt2.Text + Edt2temp + PNnameTemp + '\' + '*.csv', faAnyFile, SearchRec); //第一文件
      while found = 0 do begin             //找到文件             //AssignFile(F,edt2.Text+lbl90.hint+'\' +SearchRec.Name);
        if lbl181.Caption <> '1' then         //状态改变，退出定时上传
          Exit;
        save_file_name := PNnameTemp + '\' + SearchRec.Name;
        save_file_name_Sever := SearchRec.Name;
        if (Label10.caption <> save_file_name) then             //非当前被占用文件
        try
          if not DirectoryExists(edt1.Text) then begin
            edt1.Color := clRed;
            stat1.Panels[3].Text := 'SerVer Folder not exist!';
            Exit;
          end
          else begin
            if not CopyFile(PChar(edt2.Text + Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then begin //如果文件已存在则返回错误
              stat1.Panels[4].Text := 'Copy File Failed!';
                //Exit;
            end
            else begin
              if not DirectoryExists(edt2.Text + lbl183.Caption) then                 //备份路径
              try
                begin
                  CreateDir(edt2.Text + lbl183.Caption);  //创建目录
                end;
              except
                stat1.Panels[4].Text := 'Cannot Create ' + edt2.Text + lbl183.Caption;
                Exit;
              end;
              if not DirectoryExists(edt2.Text + lbl183.Caption + PNnameTemp + '\') then   //PN路径
              try
                begin
                  CreateDir(edt2.Text + lbl183.Caption + PNnameTemp + '\');  //创建目录
                end;
              except
                stat1.Panels[4].Text := 'Cannot Create ' + edt2.Text + lbl183.Caption + PNnameTemp + '\';
                Exit;
              end;                                                          //重复copy不会成功，防止遗漏！
              CopyFile(PChar(edt2.Text + Edt2temp + save_file_name), PChar(edt2.Text + lbl183.Caption + save_file_name), True); ////RenameFile(edt2.Text + save_file_name, edt2.Text +'bak'+ save_file_name);
              DeleteFile(edt2.Text + Edt2temp + save_file_name);
              stat1.Panels[4].Text := 'Copy File Succeed!';
            end;
            fCopynum := fCopynum + 1;
            if fCopynum > 10 * strtoint(edt6.Text) then
              Exit;
          end;
        finally
          //Closefile(f);
        end;
        found := FindNext(SearchRec);
      end;
      FindClose(SearchRec);

      if (edt135.Visible) then begin                //单PCS路径不同，则分别轮询；不用备份
        fCopynum := 0;                                                              //单次上传计数；

        if readchis('Comm Para', 'FileExtName') <> '' then
          found := FindFirst(edt135.Text + Edt2temp + PNnameTemp + '\' + '*.' + readchis('Comm Para', 'FileExtName'), faAnyFile, SearchRec) //第一文件
        else
          found := FindFirst(edt135.Text + Edt2temp + PNnameTemp + '\' + '*.txt', faAnyFile, SearchRec); //第一文件
        while found = 0 do begin             //找到文件             //AssignFile(F,edt2.Text+lbl90.hint+'\' +SearchRec.Name);
          if lbl181.Caption <> '1' then         //状态改变，退出定时上传
            Exit;
          save_file_name := PNnameTemp + '\' + SearchRec.Name;
          save_file_name_Sever := SearchRec.Name;               //一次保存完，不存在占用！if(Label10.caption<>save_file_name) then             //非当前被占用文件
          try
            if not DirectoryExists(edt1.Text) then begin
              edt1.Color := clRed;
              stat1.Panels[3].Text := 'SerVer Folder not exist!';
              Exit;
            end
            else begin
              if not CopyFile(PChar(edt135.Text + Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then begin //如果文件已存在则返回错误
                stat1.Panels[4].Text := 'Copy File Failed!';
                    //Exit;
              end
              else begin
                DeleteFile(edt135.Text + Edt2temp + save_file_name);
                stat1.Panels[4].Text := 'Copy File Succeed!';
              end;
              fCopynum := fCopynum + 1;
              if fCopynum > 10 * strtoint(edt6.Text) then
                Exit;
            end;
          finally
          end;
          found := FindNext(SearchRec);
        end;
        FindClose(SearchRec);
      end;
    end;
  end;

begin
  with form_main do begin
    if ((Label10.caption <> 'Label10') and (lbl181.Caption = '10')) then    //1.关闭窗口；仅一次，对其它程序无影响；第一文件子路径+文件名 已生成！
    begin
      Edt2temp := ''; //temp\';                                              //SMT默认：集总文件复制到临时文件夹temp\并剪切到映射盘

      save_file_name := Label10.caption;
      if Pos('\', Label10.caption) > 0 then
        save_file_name_Sever := copy(Label10.caption, pos('\', Label10.caption) + 1, length(Label10.caption))
      else
        save_file_name_Sever := Label10.caption;
      try
      //--1------------------条码文件先备份起来 -----------------------------
        if not DirectoryExists(edt2.Text + lbl183.Caption) then
        try
          begin
            CreateDir(edt2.Text + lbl183.Caption);  //创建备份目录
          end;
        except  //finally
          stat1.Panels[4].Text := 'Cannot Create ' + edt2.Text + lbl183.Caption;
          Exit;
        end;
        if not DirectoryExists(edt2.Text + lbl183.Caption + lbl90.hint + '\') then                 //本地备份路径
        try
          begin
            CreateDir(edt2.Text + lbl183.Caption + lbl90.hint + '\');  //创建备份子目录
          end;
        except  //finally
          stat1.Panels[4].Text := 'Cannot Create ' + edt2.Text + lbl183.Caption + lbl90.hint + '\';
          Exit;
        end;
        CopyFile(PChar(edt2.Text + save_file_name), PChar(edt2.Text + lbl183.Caption + save_file_name), True);

        if not DirectoryExists(edt1.Text) then begin
          if CopyFile(PChar(edt2.Text + save_file_name), PChar(edt2.Text + Edt2temp + save_file_name), True) then
            DeleteFile(edt2.Text + save_file_name);
          edt1.Color := clRed;
          stat1.Panels[3].Text := 'SerVer Folder not exist!';
        end
        else begin
          if not CopyFile(PChar(edt2.Text + Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then  //如果文件已存在则返回错误
            stat1.Panels[4].Text := 'Copy File Failed!'
          else begin
            DeleteFile(edt2.Text + Edt2temp + save_file_name);
            stat1.Panels[4].Text := 'Copy File Succeed!';
          end;
        end;
      finally
      end;
    end;

    if lbl181.Caption = '1' then begin                        //2.轮询时间到，一次结束； //if stat1.Panels[4].Text='Copy File Succeed!' then
      MoveBarcodefile(0);
      lbl181.Caption := '0';
    end
    else if ((lbl181.Caption = '5')) then begin           //3.刚生成条码文件（早期jaguar剪切单PCS文件到映射盘）
      if ((lbl161.caption <> 'lbl161') and edt135.Visible) then    //生成单PCS文件时，触发进入剪切单PCS文件
      begin
        Edt2temp := '';       //不备份 temp\';
        save_file_name := lbl161.caption;
        if Pos('\', lbl161.caption) > 0 then
          save_file_name_Sever := copy(lbl161.caption, pos('\', lbl161.caption) + 1, length(lbl161.caption))
        else
          save_file_name_Sever := lbl161.caption;
        try
          if not DirectoryExists(edt1.Text) then begin
            edt1.Color := clRed;
            stat1.Panels[3].Text := 'SerVer Folder not exist!';
          end
          else begin
            if not CopyFile(PChar(edt135.Text + Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then  //如果文件已存在则返回错误
              stat1.Panels[4].Text := 'Copy File Failed!'
            else begin
              DeleteFile(edt135.Text + Edt2temp + save_file_name);
              stat1.Panels[4].Text := 'Copy File Succeed!';
            end;
          end;
        finally
        end;
      end;
    end;
  end;

end;
//////////////////////////////启动，生成一个新的带日期.CSV文件
//改成文件不存在就生成！
//<--- 输入 tt=0：开机生成文件， 参数显示在memo里；
//          tt=1:非开机生成文件  ,没有处理
//          tt=3:  备份文件生成  ,没有启用
//          tt=9000:  生成第2 log文件，等同与 =5文件；


//输入 barcode:取第几个条码框的条码 ,=9999则不生成条码文件

procedure CreatAnewCSV(tt: integer; barcode: SmallInt);
var
  nrf_dat_config: Tinifile;
  ini_path: string;
  s, SLINE_head, SLINE_head2: string;
  i, temp: integer;
  f, file2: TextFile;
  save_file_name, sst, ssdatetime: string;

  procedure createLedBoxFile();        //车灯机箱专用
  var
    i, j: Integer;
  begin
    with form_main do begin
      if edt62.Text = '2' then begin
        if not DirectoryExists(edt135.Text + '\' + edt77.Text + '\') then
        try
          begin
            CreateDir(edt135.Text + '\' + edt77.Text + '\');        //   创建目录
          end;
        except
          raise Exception.Create('Cannot Create ' + edt135.Text + '\' + edt77.Text + '\');
        end;
      end;

      if btn98.Visible and (readchis('Model Para', 'MachineNum') = '') then begin  //上海丽清
        if rbuf[4] = 0 then
          SLINE_head := trim(edit1.Text) + ',PASS,FCT'
        else
          SLINE_head := trim(edit1.Text) + ',FAIL,FCT';
        SLINE_head := SLINE_head + ',' + edt77.Text;
        SLINE_head := SLINE_head + ',' + edt80.Text;   //线别
        SLINE_head := SLINE_head + ',' + edt79.Text;
        SLINE_head := SLINE_head + ',' + ssdatetime;
        edt135.hint := SLINE_head;
        edt135.tag := 1;
      end
      else begin                                                      //东莞丽清
        if rbuf[4] = 0 then
          SLINE_head := trim(edit1.Text) + ',PASS,FCT'
        else
          SLINE_head := trim(edit1.Text) + ',FAIL,FCT';

        if (readchis('Model Para', 'MachineNum') <> '') then
          SLINE_head := SLINE_head + ',' + readchis('Model Para', 'MachineNum')
        else
          SLINE_head := SLINE_head + ',CT-' + inttostr(rbuf[11]);   //机台
        SLINE_head := SLINE_head + ',' + edt77.Text;
        SLINE_head := SLINE_head + ',' + edt78.Text;   //LOT
        SLINE_head := SLINE_head + ',' + edt79.Text;   //作业员
        SLINE_head := SLINE_head + ',' + edt80.Text;   //
        SLINE_head := SLINE_head + ',' + ssdatetime;
        edt135.hint := SLINE_head;
        edt135.tag := 1;
      end;

    end;
  end;

begin             //-----------------生成CSV总的入口----------------------------

  datetimetostring(ssdatetime, 'yyyymmddhhnnss', Now);
  with form_main do begin
    if (label10.tag < 24) then
      sst := Copy(ssdatetime, 0, 8)    //一天一个文件
    else
      sst := ssdatetime;
  //----------------集总主文件名 生成--------(只有MFLEX条码作为主文件)-----------------------------
    if (StrToInt(edt62.Text) >= 1) then begin    //通用：文件夹使能 ,文件名 不能含有以下9种字符：?*:"<>\/|

      if edt62.Text = '2' then begin
        save_file_name := edt77.Text + '\' + DelSpecialChar(trim(edit1.Text)) + '-';
      end
      else
        save_file_name := DelSpecialChar(trim(edit1.Text)) + '-';

      if (rbuf[4] = 0) then
        save_file_name := save_file_name + 'PASS' + '-' + sst
      else
        save_file_name := save_file_name + 'FAIL' + '-' + sst;
      if (lbl177.Caption = '工位1 ：') then begin
        save_file_name := save_file_name + '-1';
      end;
      if (lbl177.Caption = '工位2 ：') then begin
        save_file_name := save_file_name + '-2';
      end;
      if (lbl177.Caption = '工位3 ：') then begin
        save_file_name := save_file_name + '-3';
      end;
      if (lbl177.Caption = '工位4 ：') then begin
        save_file_name := save_file_name + '-4';
      end;
      if readchis('Comm Para', 'FileExtName') <> '' then
        save_file_name := save_file_name + '.' + readchis('Comm Para', 'FileExtName')
      else
        save_file_name := save_file_name + '.txt';

    end
    else                                     //仅生产单一文件
      save_file_name := lbl90.hint + '-' + sst + '.csv';
  //-----------------------创建文件  处理-------------------------------

    lbl161.caption := save_file_name;
    createLedBoxFile();

  end;
end;

{   函数：saveCsvdata(len_ss:integer)
说明:接收到的数据保存为.txt文件，可支持
1.连片输出数据的保存，仅保存到同一个文件里
2.根据条码规则自动生成所有同一规律的条码
3.区分指示灯显示
}
procedure saveCsvdata(len_ss: integer; NewLogFileFlag: byte);
var
  f: TextFile;
  save_file_name: string;
  strtemp, stime, SLINE_head, SLINE_head1, SLINE_head2, sline_sn, sntime: string;
  i, j, bb: integer;
  zuptr, dd, cboflag, zzoflag: SmallInt;
  ff: Real;

  procedure OtherDataPre();                      //数据头几个项目预处理，显示在测试界面.memo1框
  var
    i, j: Integer;
  begin
    with form_main do begin
      if StrToInt('0' + Label7.hint) = 2 then begin       //clearbarcode=2，条码用编号代替
        if Trim(edit1.Text) = '' then
          SLINE_head := IntToStr(TestNum) + ','
        else
          SLINE_head := Trim(edit1.Text) + ',';
      end
      else if (StrToInt(edt62.Text) >= 10) then
        SLINE_head := Trim(edit1.Text) + ','            //条形码 二维码
      else
        SLINE_head := Trim(edit1.Text) + ',';

      datetimetostring(stime, 'yyyy-mm-dd HH:nn:ss', Now);
      SLINE_head := SLINE_head + stime + ',';    //时间
      if (rbuf[4] = 0)              //结果OK
        then begin
        SLINE_head := SLINE_head + 'PASS' + ',';
      end
      else begin
        SLINE_head := SLINE_head + 'FAIL' + ',';
      end;

      SLINE_head := SLINE_head + '-,' + '-,' + '-,' + '-,' + '-,' + '-,';

    end;
  end;

  procedure DataInMemoDisp();
  var
    i, j: Integer;
  begin
    with form_main do begin
      bb := 1;
      if rbuf[3] = $EE then     //CSV格式联板则先处理MEMO显示数据，仅显示用。
      begin
        zuptr := 1;             //表头指针，当前显示指针
        for i := bb to (len_ss div 2) do begin        //遍历所有数据全部显示,用i指示，实际显示指针为zuptr

          dd := (rbuf[10 + 4 * i] * 256 + rbuf[11 + 4 * i]);
          if ((Copy(ModeZu[i], 2, 1) = 'S') or (Copy(ModeZu[i], 2, 1) = 'O') or (Copy(ModeZu[i], 2, 1) = 'K')) and ((copy(NameStrZu[i], 0, 4) = 'OPEN') or (copy(NameStrZu[i], 0, 4) = 'SHOR') or (copy(NameStrZu[i], 0, 5) = 'SHORT')) then begin
            if dd < 1 then begin
              SLINE_head1 := SLINE_head1 + 'right' + #9;
            end
            else begin
              SLINE_head1 := SLINE_head1 + 'error' + #9;
            end;
          end
          else if (Copy(ModeZu[i], 2, 2) = 'XH') or (Copy(ModeZu[i], 2, 2) = 'YH') or (Copy(ModeZu[i], 2, 2) = 'IZ') or (Copy(ModeZu[i], 2, 2) = 'JZ') then begin      //16进制显示
            SLINE_head1 := SLINE_head1 + '0x' + inttohex(word(dd), 2) + #9;
          end
          else begin
            ff := dd / strtofloat(ScaleZu[zuptr]);
            SLINE_head1 := SLINE_head1 + copy(floattostr(ff), 0, 7) + #9;
          end;
          if (zuptr < UpStepMax) then
            zuptr := zuptr + 1;

        end;

        DispErrlist(len_ss);

      end;
    end;
  end;

  procedure saveNPCSresult();
  var
    i, j: Integer;
    str1: string;
    ResultList: TStringList;

    procedure saveCarLedlog(tt: SmallInt);
    var
      i, j: Integer;
    begin
      with form_main do begin
        try
          assignfile(f, edt135.Text + lbl161.caption);
          rewrite(f);
          Sleep(5);
          writeln(f, edt135.hint);
          if (edt135.tag > 1) then
            writeln(f, '');

          if (Readchis('Server', 'MESprogram') <> '') then begin     //MES校验MCU信息
            writeln(f, btn119.hint);
            writeln(f, btn142.hint);
          end
          else if readchis('Comm Para', 'FileExtName') = '' then begin   //非anjieli定义文件格式
          //  writeln(f,'脱机测试');
          end;
          if (btn30.hint <> '') and (readchis('Comm Para', 'OSpointPos') <> 'OFF') then
            writeln(f, btn30.Hint);

          for i := 1 to (len_ss div 2) do      //遍历所有数据， 放在多行存放
          begin
            dd := (rbuf[10 + 4 * i] * 256 + rbuf[11 + 4 * i]);
            ff := dd / strtofloat(ScaleZu[zuptr]);
            if (Copy(ModeZu[i], 2, 2) = 'XH') or (Copy(ModeZu[i], 2, 2) = 'YH') or (Copy(ModeZu[i], 2, 2) = 'IZ') or (Copy(ModeZu[i], 2, 2) = 'JZ') then begin                    //专用文件格式
              strtemp := inttostr(zuptr) + ',' + NameStrZu[zuptr] + ',-' + ',0x' + inttohex(StrToInt(UpperZu[zuptr]), 2) + UnitStrZu[zuptr] + ',0x' + inttohex(StrToInt(LowerZu[zuptr]), 2) + UnitStrZu[zuptr]               //上限带单位+下限带单位
                + ',0x' + inttohex(dd, 2) + UnitStrZu[zuptr];
            end
            else begin
              strtemp := inttostr(zuptr) + ',' + NameStrZu[zuptr] + ',-' + ',' + UpperZu[zuptr] + UnitStrZu[zuptr] + ',' + LowerZu[zuptr] + UnitStrZu[zuptr]  //上限带单位+下限带单位
;
              if ((Copy(ModeZu[i], 2, 1) = 'S') or (Copy(ModeZu[i], 2, 1) = 'O') or (Copy(ModeZu[i], 2, 1) = 'K')) then begin
                if ((copy(NameStrZu[i], 0, 4) = 'OPEN') or (copy(NameStrZu[i], 0, 4) = 'SHOR') or (copy(NameStrZu[i], 0, 5) = 'SHORT')) then begin
                  if dd < 1 then begin
                    strtemp := strtemp + ',ok';
                  end
                  else begin
                    if (j = 0) and (readchis('Comm Para', 'OSpointPos') <> 'OFF') then begin
                      if (Copy(ModeZu[i], 2, 1) = 'S') and (btn34.Hint <> '') then
                        strtemp := strtemp + ',ng' + btn34.Hint
                      else if (Copy(ModeZu[i], 2, 1) = 'O') and (btn33.Hint <> '') then
                        strtemp := strtemp + ',ng' + btn33.Hint
                      else if (Copy(ModeZu[i], 2, 1) = 'K') and (btn33.Hint <> '') then
                        strtemp := strtemp + ',ng' + btn33.Hint
                      else
                        strtemp := strtemp + ',ng';
                    end
                    else
                      strtemp := strtemp + ',ng';
                  end;
                end
                else begin
                  strtemp := strtemp + ',' + copy(floattostr(ff), 0, 7);                //不带单位
                end;
              end
              else begin
                strtemp := strtemp + ',' + copy(floattostr(ff), 0, 7) + UnitStrZu[zuptr];                //测量值带单位
              end;
            end;

            if (Stat1.tag = 9000) and (readchis('Comm Para', 'CSV5raw') = '') then begin        //使用新的数据格式！
              if (rbuf[13 + 4 * i] and $01) = $01 then begin         //NG
                if ((copy(NameStrZu[zuptr], 0, 4)) = 'OPEN') then begin           //UpperCase
                  strtemp := strtemp + ',ng,--';
                end
                else if ((copy(NameStrZu[zuptr], 0, 4)) = 'SHOR') or ((copy(NameStrZu[zuptr], 0, 5)) = 'SHORT') then begin    //UpperCase
                  strtemp := strtemp + ',ng,--';
                end
                else if (UpperZu[zuptr] <> '') and (LowerZu[zuptr] <> '') and (UpperZu[zuptr] <> '30009') and (LowerZu[zuptr] <> '30009') then begin
                  if dd >= StrToInt(UpperZu[zuptr]) then  //20240201 by jdx修改 HEX数据卡死 if ff >= StrToFloat(UpperZu[zuptr]) then
                    strtemp := strtemp + ',ng,--'
                  else if dd <= StrToInt(LowerZu[zuptr]) then //20240201 by jdx修改 HEX数据卡死   else if ff <= StrToFloat(LowerZu[zuptr]) then
                    strtemp := strtemp + ',ng,--'
                  else
                    strtemp := strtemp + ',ng,--';
                end
                else
                  strtemp := strtemp + ',ng,--';
              end
              else
                strtemp := strtemp + ',ok,--';
            end
            else begin
              if (rbuf[13 + 4 * i] and $01) = $01 then begin         //NG
                if (copy(NameStrZu[zuptr], 0, 4) = 'OPEN') or (copy(NameStrZu[zuptr], 0, 4) = 'SHOR') or (copy(NameStrZu[zuptr], 0, 5) = 'SHORT') then begin
                  strtemp := strtemp + ',NG,--';
                end
                else if (UpperZu[zuptr] <> '') and (LowerZu[zuptr] <> '') and (UpperZu[zuptr] <> '30009') and (LowerZu[zuptr] <> '30009') then begin
                  if dd >= StrToInt(UpperZu[zuptr]) then // 20240201 by jdx
                    strtemp := strtemp + '↑,NG,--'
                  else if dd <= StrToInt(LowerZu[zuptr]) then   // 20240201 by jdx
                    strtemp := strtemp + '↓,NG,--'
                  else
                    strtemp := strtemp + ',NG,--';
                end
                else
                  strtemp := strtemp + ',NG,--';
              end
              else
                strtemp := strtemp + ',PASS,--';
            end;
            if (zuptr < UpStepMax) then
              zuptr := zuptr + 1;
            writeln(f, strtemp);
          end;

          writeln(f, '--');              //标志下面数据是扩展原始数据

          if (mmo2String[10] <> '') and (Readchis('Result', 'saveRawData') = '1') then begin
            writeln(f, mmo2String[10]);
            mmo2String[10] := '';
          end;
        finally
          Closefile(f);
        end;

        if (edt53.hint = '9000') and (Stat1.tag = 9000) then begin    // 2BU0上传数据,用UDP接口转发

          idpclnt1.Active := true;
          idpclnt1.tag := 0;             //  idpsrvr1.Active:=True;
          ResultList := TStringList.Create;
          try
            ResultList.LoadFromFile(edt135.Text + lbl161.caption);
          finally
          end;
          if edt53.tag = 9002 then begin             //可靠发送：等待返回信号
            idpclnt1.Send(Edt100.Text, 9002, ResultList.Text);
            for i := 0 to idpsrvr1.Tag do begin
              Sleep(100);
              application.ProcessMessages;
              if idpclnt1.tag > 0 then
                Break;
            end;
            ShowMessage('secondary development error!二次开发软件没有响应！');
          end
          else
            idpclnt1.Send(Edt100.Text, 9001, ResultList.Text);    //默认：不可靠发送：udp发送
        end;

        if (StrToInt(edt6.Text) > 0) then begin         //启用映射盘，必须放在  UDP之后
          lbl181.Caption := '5';
          Tcar.bakToServerDisk(); // btn2Click(form_main);
        end;
      end;
    end;

  begin
    with form_main do begin
      for j := 0 to StrToInt(edt59.Text) * strtoint(edt60.Text) - 1 do      //搜索有输出结果的连片序号；如果只有1连片则单独处理；
      begin

        if j > 0 then
          TestNum := TestNum + 1;        //序号依次加一
        if Rleds[j].Active = TRUE then begin
          zuptr := 1;

          saveCarLedlog(0);                  //车灯机箱

        end;

      end;
    end;
  end;

begin                  //-------------保存CSV文件入口----------------
  with form_main do begin

    CreatAnewCsv(1, 0);              //每次都生成条码命名文件，调用文件生成函数      xx mflex专用，

    save_file_name := Label10.caption;     //默认文件名处理完成

    OtherDataPre();                     //其他 ，通用数据的前几项处理

    mmo2.Lines[1] := '测试结果：' + Copy(SLINE_head, 0, 1024);
//--------2.---------------以下处理结果数据部分,仅显示用---------------------------------------------------------------
    SLINE_head1 := 'result-' + #9;                  //用于在MEMO1里显示

    DataInMemoDisp();                           //要保存的数据，SLINE_head1仅先显示一下
    mmo2.Lines[3] := Copy(SLINE_head1, 0, 1024);   //结果 显示，TAB分隔
    mmo2.Lines[1] := mmo2.Lines[1];               //使能 显示区域在开始位置

//----------3.--------------以下开始处理连片数据，以及数据保存---及单PCS保存用的数据（后面可以直接保存不用二次处理）其中多连片的开头几项重新开始------------------------------------------

    if (Label10.caption <> 'Label10') and isfileinuse(edt2.Text + Label10.caption) then begin   //存在文件且被占用
      Application.MessageBox('LogFile in use！Log文件被占用，不能写入数据！', '警告', MB_OK + MB_ICONWARNING);
    end
    else begin

      saveNPCSresult();

    end;

  end;
end;

class procedure Tcar.SaveFlagProcess(devnum: integer; SaveLen: SmallInt);         //数据保存函数，唯一入口
var
  NewLogFileFlag: Byte;                          //应该生成新文件标志
  devi, i, j, k, crc_temp: SmallInt;   // ,tt
  save_file_name, str, ss, mss, sst: string;
  UsbBuf: array[0..64] of Byte;
  Written: Cardinal;
  ToWrite: Cardinal;
  Err: DWORD;

  procedure UnlockClearBarcode();
  begin
    with form_main do begin
      if (StrToInt(Label7.hint) = 1) or (StrToInt(Label7.hint) = 2) or (StrToInt(Label7.hint) > 10) then       //自动清除条码
      begin
        if not Edit1.Enabled then begin
          if Readchis('Model Para', 'BarCodeEdit') <> '' then begin
            if StrToInt(Readchis('Model Para', 'BarCodeEdit')) < 1000 then begin
              edit1.Enabled := true;
              if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
                Edit1.SetFocus;
            end;
          end
          else begin
            Edit1.Enabled := true;
            if (TabSheet2.Showing) and edit1.Enabled and grp36.Visible then
              Edit1.SetFocus;
          end;
        end;       //开启条码框输入功能
        if not mEdit1.Enabled then
          medit1.Enabled := true;

        Edit1.Text := '';
        medit1.Text := '';
      end;
    end;
  end;

begin
  with form_main do begin
    //-------------------------------------单机数据更新 ------------------------------
    if (SaveLen > 0) then begin
      {if grp45.Visible and (Readchis('Result','StandardSave')='' )    //通用：标准板数据不记录；只有配置了保存标准板才执行
        and ( (edt171.Color<>clLime)or( edt173.Color<>clLime )or(edt175.Color<>clLime) ) then begin    //有一个标准板没有验证
         Label11.Font.Color:=clWindowText;
         Label11.Caption:='标准板验证...';
         if (rbuf[4]=0) then lbl191.Caption:='当前结果：PASS' else lbl191.Caption:='当前结果：FAIL';
         lbl193.Caption:='当前条码：';
         edt176.Text:=Trim(edit1.Text);
         if Trim(edt171.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt170.Text='PASS')or(edt170.Text='pass')or(edt170.Text='OK')or(edt170.Text='ok')) then begin
             edt171.Color:=clLime;  edt170.Color:=clLime;
           end else if (rbuf[4]>0)and((edt170.Text='FAIL')or(edt170.Text='fail')or(edt170.Text='NG')or(edt170.Text='ng')) then begin
             edt171.Color:=clLime;  edt170.Color:=clLime;
           end else
             edt171.Color:=clred;
         end;
         if Trim(edt173.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt172.Text='PASS')or(edt172.Text='pass')or(edt172.Text='OK')or(edt172.Text='ok')) then begin
             edt173.Color:=clLime;  edt172.Color:=clLime;
           end else if (rbuf[4]>0)and((edt172.Text='FAIL')or(edt172.Text='fail')or(edt172.Text='NG')or(edt172.Text='ng')) then begin
             edt173.Color:=clLime;  edt172.Color:=clLime;
           end else
             edt173.Color:=clred;

         end;
         if Trim(edt175.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt174.Text='PASS')or(edt174.Text='pass')or(edt174.Text='OK')or(edt174.Text='ok')) then begin
             edt175.Color:=clLime;  edt174.Color:=clLime;
           end else if (rbuf[4]>0)and((edt174.Text='FAIL')or(edt174.Text='fail')or(edt174.Text='NG')or(edt174.Text='ng')) then begin
             edt175.Color:=clLime;  edt174.Color:=clLime;
           end else
             edt175.Color:=clred;

         end;
         if ( Trim(edt171.Text)<>Trim(edit1.Text) )and( Trim(edt173.Text)<>Trim(edit1.Text) )and( Trim(edt175.Text)<>Trim(edit1.Text) ) then begin
           lbl193.Caption:='非标准板条码：'; lbl193.Color:=clred;
           edt176.Text:=Trim(edit1.Text);
         end;
        //------------数据上传成功，给MCU确认信息//////////--------------------------------------------------------------

        Sleep(40);             //延时，等待通信链接命令确认完成！
        if (Comm1.BaudRate=9600)and (not SpeedButton12.Enabled ) then        //FCT5增加100mS延时；
          Sleep(100);
        begin

          sbuf[2]:=5; sbuf[3]:=$50;sbuf[4]:=$00;
          sbuf[5]:=$FF; sbuf[6]:=$00; send_len:=6;
          try
              send_crcdata(send_len,2);
          except
              messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
          end;
        end;

        SaveLen:=0;    //保存数据个数清零
        UnlockClearBarcode();       //解锁条码并清空
        mmo19.Lines.Add('8.标准板测试') ;
        LV1MemoDisp(SaveLen);
        exit;
      end;  }
                            //有收到数据，新文件导入标志清除
      NewLogFileFlag := 0;       //测量结果预处理，是否用下位机机种生产数据文件前先清除标志；

      /////////////-----------文件夹名字来源,用edt77.Text和save_file_name表示----------------------------------------
      if (edt77.Text <> '') and (edt77.Text <> '-') then begin         //不能 空或者-文件夹。
        if Length(edt77.text) < 2 then begin               //SaveLen:=Saveflag; Saveflag:=0;   //数据长度替换，避免多线程造成重复进入！
          case Application.MessageBox('请确认测试程序第一步骤是否使用AZV0正确设置P/N，继续保存log数据吗？', 'P/N warning! 料号长度异常！', MB_YESNO + MB_ICONWARNING + MB_DEFBUTTON2) of
            IDYES:
              begin
                save_file_name := edt77.Text;
              end;
            IDNO:
              begin
                edt77.Color := clRed;
                edit1.Enabled := True;
                Exit;
              end;
          end;
        end
        else
          save_file_name := edt77.Text;
      end
      else begin
        save_file_name := '';
        if (Char(rbuf[6]) >= '0') and (Char(rbuf[6]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[6]);
        if (Char(rbuf[5]) >= '0') and (Char(rbuf[5]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[5]);
        if (Char(rbuf[8]) >= '0') and (Char(rbuf[8]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[8]);
        if (Char(rbuf[7]) >= '0') and (Char(rbuf[7]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[7]);
        if (Char(rbuf[10]) >= '0') and (Char(rbuf[10]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[10]);
        if (Char(rbuf[9]) >= '0') and (Char(rbuf[9]) <= 'z') then
          save_file_name := save_file_name + Char(rbuf[9]);

        if (edt77.Text = '') or (edt77.Text = '-') then        //不能为空或者-文件夹。
          edt77.Text := save_file_name;
      end;

      //--不知何故会在此处循环警告，用edt77.visible内容控制不循环---------------------
      if edt77.Visible and ((edt77.Text = '') or (edt77.Text = '')) then begin   //PN料号不能为空或者-，否提示
        edt77.Visible := False;
        case Application.MessageBox('Invalid P/N,no logfile,continue?;料号无效，不能生成测试Log文件，继续吗？', '警告', MB_OKCANCEL + MB_ICONINFORMATION) of
          IDOK:
            begin
              edt77.Visible := true;
            end;
          IDCANCEL:
            begin
              form_main.Close;
            end;
        end;
      end;
      /////////////-----------文件夹生----------------------------------------

      if (save_file_name <> '') and (lbl90.hint <> (lbl114.Caption + save_file_name)) then begin  //机种名不同，产生新文件

        save_file_name := lbl114.Caption + save_file_name;      //文件名命名（加上前缀）
        lbl90.hint := save_file_name;                         //机种名称，不同于label10(文件名)同时作为子文件夹名 ，可限制重复生成文件。
        if GroupBox3.Visible then begin
          if not DirectoryExists(edt2.Text + save_file_name + '\') then
          try
            begin
              CreateDir(edt2.Text + save_file_name + '\');        //   创建目录
            end;
          except  //finally
            raise Exception.Create('Cannot Create ' + edt2.Text + save_file_name + '\');
          end;
        end;
        if edt157.Visible then begin
          if not DirectoryExists(edt157.Text + save_file_name + '\') then
          try
            begin
              CreateDir(edt157.Text + save_file_name + '\');        //   创建目录
            end;
          except  //finally
            raise Exception.Create('Cannot Create ' + edt157.Text + save_file_name + '\');
          end;
        end;
        Sleep(10);
      /////////////-----以上处理文件夹生成------以下处理 文件生成与写数据时的文件生成冲突？？----------------------------------------
        NewLogFileFlag := 1;
        TestNum := 0;     //标志应该新生成文件，计数清空
      end;

      //---------以上处理完文【件夹命】名问题^^^^--------------------VVVV文件命名在数据保存函数中--------------------------------------------------------
      if SaveLen < 2 then begin           //去掉数据包头后的：字个数
        case Application.MessageBox('接收到的数据异常（少于2个字）！', '警告', MB_OKCANCEL + MB_ICONINFORMATION) of
          IDOK:
            begin
              //   form_main.Close;
            end;
          IDCANCEL:
            begin
              //   form_main.edt127.Visible:=true;
            end;
        end;
      end;
      Label7.Caption := 'BarCode';    //PC收到上传数据则更新标志，与UDP通信对应！
      /////////结果显示，记录更新，良品统计----------------------------------------------------------------------------
      TestNum := TestNum + 1;      //序号
      if TestNum > 999999 then
        TestNum := 1;

    {  lbl191.Caption:='-';lbl193.Caption:='-'; edt176.Text:='-';
      if grp45.Visible and ( (edt171.Color<>clLime)or(edt173.Color<>clLime)or(edt175.Color<>clLime) ) then begin    //早期标准板：标准板数据要记录；有一个标准板没有验证
         Label11.Font.Color:=clWindowText;
         Label11.Caption:='标准板验证...';
         if (rbuf[4]=0) then lbl191.Caption:='当前结果：PASS' else lbl191.Caption:='当前结果：FAIL';
         lbl193.Caption:='当前条码：';
         edt176.Text:=Trim(edit1.Text);
         if Trim(edt171.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt170.Text='PASS')or(edt170.Text='pass')or(edt170.Text='OK')or(edt170.Text='ok')) then begin
             edt171.Color:=clLime;
           end else if (rbuf[4]>0)and((edt170.Text='FAIL')or(edt170.Text='fail')or(edt170.Text='NG')or(edt170.Text='ng')) then begin
             edt171.Color:=clLime;
           end else
             edt171.Color:=clred;
         end else if Trim(edt173.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt172.Text='PASS')or(edt172.Text='pass')or(edt172.Text='OK')or(edt172.Text='ok')) then begin
             edt173.Color:=clLime;
           end else if (rbuf[4]>0)and((edt172.Text='FAIL')or(edt172.Text='fail')or(edt172.Text='NG')or(edt172.Text='ng')) then begin
             edt173.Color:=clLime;
           end else
             edt173.Color:=clred;

         end else if Trim(edt175.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt174.Text='PASS')or(edt174.Text='pass')or(edt174.Text='OK')or(edt174.Text='ok')) then begin
             edt175.Color:=clLime;
           end else if (rbuf[4]>0)and((edt174.Text='FAIL')or(edt174.Text='fail')or(edt174.Text='NG')or(edt174.Text='ng')) then begin
             edt175.Color:=clLime;
           end else
             edt175.Color:=clred;

         end else begin
           lbl193.Caption:='非标准板条码：'; lbl193.Color:=clred;
           edt176.Text:=Trim(edit1.Text);
         end;
      end else begin   }
      if (rbuf[4] = 0) then begin              //结果OK

        Label11.Font.Color := clLime;
        if (ts7.Tag = 0) or (StrToInt(edt59.Text) * strtoint(edt60.Text) = 1) or (edt74.Text = '10') then
          PassCount := PassCount + 1;
        Label11.Caption := 'PASS';
      end
      else begin
        Label11.Font.Color := clRed;
        if (ts7.Tag = 0) or (StrToInt(edt59.Text) * strtoint(edt60.Text) = 1) or (edt74.Text = '10') then
          NGcount := NGcount + 1;
        Label11.Caption := 'FAIL'
      end;

      PassOverT := StrToInt('0' + edt40.Text);    //超时显示待机
      NGOverT := StrToInt('0' + edt42.Text);

      if (ts7.Tag = 0) or (StrToInt(edt59.Text) * strtoint(edt60.Text) = 1) or (edt74.Text = '10') then     //////只有单PCS测试，才此处良品与不良品统计 。否则在联板测试良品与不良品统计
      begin
        edt4.Text := inttostr(PassCount + NGcount);
        edt63.Text := inttostr(PassCount);
        edt64.Text := inttostr(NGCount);
        if (PassCount + NGcount) > 0 then begin
          edt65.Text := floattostrF((PassCount * 100) / (PassCount + NGcount), ffFixed, 5, 2);
          edt66.Text := floattostrF((NGCount * 100) / (PassCount + NGcount), ffFixed, 5, 2);
        end
        else begin
          edt65.Text := '0';
          edt66.Text := '0';
        end;
      end;

    //  end;

      //------------------正常 数据上传及保存VVVVVVVVVVVVVVV-----------------------------------------------------------------------
      if StrToInt(edt38.text) mod 10 > 0 then begin                               //uplocal 且
        edt77.Color := clWindow;
        if Pos('?', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('*', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos(':', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('"', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('<', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('>', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('\', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('/', edt77.Text) > 0 then
          edt77.Color := clred;
        if Pos('|', edt77.Text) > 0 then
          edt77.Color := clred;

        if ((StrToInt(edt38.text) div 10 = 1) and (rbuf[4] > 0))                   //仅pass保存文件
          or ((StrToInt(edt38.text) div 10 = 2) and (rbuf[4] = 0)) then begin        //仅FAIL保存文件
          form_main.label10.caption := 'test fail';
          ;                         //不保存
        end
        else begin
          SaveCsvData(SaveLen, NewLogFileFlag)       //保存为CSV文件
        end;
      end
      else begin
        Label11.Font.Size := 80;
        Lbl169.Caption := 'pls set Uplocal=1';
        lbl169.Font.Size := 60;
        LV1MemoDisp(SaveLen);                    //没有启用保存log文件时的显示！
      end;

      /////////数据上传成功，给MCU确认信息//////////--------------------------------------------------------------
      Sleep(40);                    //延时，等待通信链接命令确认完成！
      if (Comm1.BaudRate = 9600) and (not SpeedButton12.Enabled) then        //FCT5增加100mS延时；
        Sleep(100);

      sbuf[2] := 5;
      sbuf[3] := $50;
      sbuf[4] := $00;
      sbuf[5] := $FF;
      sbuf[6] := $00;
      send_len := 6;
      try
        send_crcdata(send_len, 2);
      except
        messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .', mterror, [mbyes], 0);
      end;

      SaveLen := 0;                   //保存数据个数清零

      UnlockClearBarcode();         //解锁条码并清空
      mmo19.Lines.Add('8.barcode enable');

    end;

  end;
end;

end.
 
